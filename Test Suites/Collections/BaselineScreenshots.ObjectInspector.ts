<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>BaselineScreenshots.ObjectInspector</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a3817ab3-00b7-4daa-a7c3-d803cbb38940</testSuiteGuid>
   <testCaseLink>
      <guid>352acabb-fc32-456f-a2c6-9c482117aac3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BaseLineScreenshots/Widgets/ObjectInspector/Default</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>701cabdb-210d-45f0-8c3f-384c914026a8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BaseLineScreenshots/Widgets/ObjectInspector/ForegroundContainer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ca3343e5-11e6-46d7-b53e-9626c1aed630</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BaseLineScreenshots/Widgets/ObjectInspector/ItemMenu</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e763af6d-dfff-4160-b4d9-9046343b9552</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BaseLineScreenshots/Widgets/ObjectInspector/OITextPlaceholders</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1d0014f8-8669-42ba-b1e4-013714cc5b44</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BaseLineScreenshots/Widgets/ObjectInspector/ZOrder</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
