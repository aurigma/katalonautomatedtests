<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>BaselineScreenshots.ColorPicker</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>8b396cf5-2d97-4b92-9d2c-d3f08fd90fe9</testSuiteGuid>
   <testCaseLink>
      <guid>6d8686f7-969f-4b3e-b958-c5238575b9af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BaseLineScreenshots/Widgets/ColorPicker/Palette</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6ae103cf-b19a-4d95-b54d-ad9816ee5b91</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BaseLineScreenshots/Widgets/ColorPicker/PickerCMYK</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2a8703ba-a547-4725-a055-353a0f3ae693</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BaseLineScreenshots/Widgets/ColorPicker/PickerRGB</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>264378c3-2ae6-4ae1-a368-be2f64ae5146</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BaseLineScreenshots/Widgets/ColorPicker/RTEWidget</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2b8de4cf-1891-4e4e-b81b-f6b1745f7864</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BaseLineScreenshots/Widgets/ColorPicker/SectionCollapse</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d0c90af5-77e8-4ea7-b11e-70a763b00c6b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BaseLineScreenshots/Widgets/ColorPicker/SectionCollapsePaletteOnly</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e631d8f5-7caa-434e-9647-08ee9aa14a33</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BaseLineScreenshots/Widgets/ColorPicker/ShadowWidget</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c3d8ae72-a4e7-416a-b114-c138155538de</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BaseLineScreenshots/Widgets/ColorPicker/StrokeWidget</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>536b87f7-e507-48c3-badc-f91e6a99f6f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BaseLineScreenshots/Widgets/ColorPicker/OneSection</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0ec8a30c-de3b-4a6a-a6be-0deb50d1d7ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BaseLineScreenshots/Widgets/ColorPicker/SectionCollapseEnabled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ddb22061-16e6-43e3-996b-ac5c612703e1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BaseLineScreenshots/Widgets/ColorPicker/SectionCollapseDisabled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8ad9a9bd-76be-4c9b-8c02-64b32c172a90</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BaseLineScreenshots/Widgets/ColorPicker/Targets</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a73681d-b3c1-40d6-8851-299a4a710baa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BaseLineScreenshots/Widgets/ColorPicker/ThemeSection</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>40cf5a1c-7789-4e30-9024-91696092b8f2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BaseLineScreenshots/Widgets/ColorPicker/ViewType</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b227c40e-28ad-45dd-8db9-6066a0d696a8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BaseLineScreenshots/Widgets/ColorPicker/AvailableColorSpaces/Cmyk</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1e512ec8-beac-44e3-b436-3c64dda7a728</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BaseLineScreenshots/Widgets/ColorPicker/AvailableColorSpaces/CmykCmyk</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>961684dc-e6c2-439c-b41b-5610e7e56ef7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BaseLineScreenshots/Widgets/ColorPicker/AvailableColorSpaces/CmykRgb</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1e11ff32-4f99-4cf0-a603-d70f47c50bc1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BaseLineScreenshots/Widgets/ColorPicker/AvailableColorSpaces/CmykRgbCmyk</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f77ece6d-c6c8-484f-83f9-60663a4de11c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BaseLineScreenshots/Widgets/ColorPicker/AvailableColorSpaces/Rgb</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf9b6040-9fb0-40af-b03a-4b0c0df1cf23</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BaseLineScreenshots/Widgets/ColorPicker/AvailableColorSpaces/RgbCmyk</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7c3baf2d-0cb7-4c7d-b4f3-003613b4628c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BaseLineScreenshots/Widgets/ColorPicker/AvailableColorSpaces/RgbRgb</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bc36487c-54c9-4ac4-9d99-41e4b64e16c4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BaseLineScreenshots/Widgets/ColorPicker/AvailableColorSpaces/RgbRgbCmyk</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
