<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>VisualTests.ColorPicker</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>5695af3d-7885-41d8-9665-31a0b3827119</testSuiteGuid>
   <testCaseLink>
      <guid>093b1580-6d11-4368-ab9a-05058e2a26bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VisualTests/Widgets/ColorPicker/Palette</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>328fa2ba-5fa5-4d47-bc8f-297775cf1c7c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VisualTests/Widgets/ColorPicker/PickerCMYK</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>60eea358-1349-4759-812e-03d0c6a21042</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VisualTests/Widgets/ColorPicker/PickerRGB</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>68232829-cf85-4b88-b8c1-9db979ea030c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VisualTests/Widgets/ColorPicker/RTEWidget</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9615f7bd-79bc-419e-8eb5-9874b450721e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VisualTests/Widgets/ColorPicker/SectionColapse</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>560caa33-55b5-4c45-9365-a45e37990216</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VisualTests/Widgets/ColorPicker/SectionCollapsePaletteOnly</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8ba531c6-11dd-4bbc-be7a-f28e6a8ea97f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VisualTests/Widgets/ColorPicker/ShadowWidget</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a43d060-e0f2-4c45-8e8d-a9043a8713f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VisualTests/Widgets/ColorPicker/StrokeWidget</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>30f03a97-fb4a-43f1-a7b8-e6e6c43bcafa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VisualTests/Widgets/ColorPicker/AvailableColorSpaces/Cmyk</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fb7958e6-a087-48ca-a702-0233ad0a6aaa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VisualTests/Widgets/ColorPicker/AvailableColorSpaces/CmykCmyk</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d775a6be-379d-4446-9555-3151050709f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VisualTests/Widgets/ColorPicker/AvailableColorSpaces/CmykRgb</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3e8a637a-80ac-424f-8581-9bbe043446c8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VisualTests/Widgets/ColorPicker/AvailableColorSpaces/CmykRgbCmyk</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9e9390c9-d9bb-43ff-be9d-bb2ecd60990a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VisualTests/Widgets/ColorPicker/AvailableColorSpaces/Rgb</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e65068dc-fab7-45e0-bea4-84afb0854589</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VisualTests/Widgets/ColorPicker/AvailableColorSpaces/RgbCmyk</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d11fbf40-cb4f-4eb9-aa35-2ef9275e0eec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VisualTests/Widgets/ColorPicker/AvailableColorSpaces/RgbRgb</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f5244fa4-9fa8-491d-bfff-dd04efa238f2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VisualTests/Widgets/ColorPicker/AvailableColorSpaces/RgbRgbCmyk</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e24d5d6-e1ca-49e9-bea3-23c07cf5e127</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VisualTests/Widgets/ColorPicker/OneSection</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>010074f1-c5c9-40af-859d-474936a0e6a7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VisualTests/Widgets/ColorPicker/SectionCollapseDisabled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>89197cd2-382f-49de-b8bc-9c9139e5fd4c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VisualTests/Widgets/ColorPicker/SectionCollapseEnabled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b0de8223-2436-4be6-b129-1edb8d60d70c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VisualTests/Widgets/ColorPicker/Targets</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7a64f0f1-c1b9-479f-acce-725f85f94e53</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VisualTests/Widgets/ColorPicker/ThemeSection</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>73b37740-aeeb-434e-aa4a-43ebc9ba7a40</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VisualTests/Widgets/ColorPicker/ViewType</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
