<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>VisualTests.ObjectInspector</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>b2b2359b-c0f7-4b99-b4f6-a25c9bf48f1d</testSuiteGuid>
   <testCaseLink>
      <guid>2f7c4573-6cab-4e3d-8379-9fc6a7b575b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VisualTests/Widgets/ObjectInspector/Default</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>71445633-6348-4e42-8e89-6e121549ef44</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VisualTests/Widgets/ObjectInspector/ForegroundContainer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>02d40eed-1e3c-4e13-ac18-3a4ccc5dc9bf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VisualTests/Widgets/ObjectInspector/ItemMenu</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>111ff902-6fbe-4582-8348-a77bde010cef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VisualTests/Widgets/ObjectInspector/OITextPlaceholders</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d935d332-4329-4a36-925e-5025ef7f4778</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VisualTests/Widgets/ObjectInspector/ZOrder</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
