import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.chrome.ChromeDriver as ChromeDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('http://localhost:3000/')

WebUI.click(findTestObject('Test tests/button_RUN'))

WebUI.click(findTestObject('Test tests/div_Editor'))

WebUI.delay(4)

CustomKeywords.'scripts.AurigmaScreenshoter.takeAndSaveFolder'('Test tests/Page_Test-stand', System.getProperty('user.dir') + 
    '/Screenshots/Base/', findTestObject('Test tests/iframe_Please wait _editorFrame'))

CustomKeywords.'scripts.AurigmaScreenshoter.takePageAndSave'(System.getProperty('user.dir') + '/Screenshots/Base/page.png')

WebUI.closeBrowser(FailureHandling.CONTINUE_ON_FAILURE)

