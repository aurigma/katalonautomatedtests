import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('http://localhost:3000/')

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.delay(3)

toolbar = CustomKeywords.'scripts.ScreenshotHandler.takeObjectScreenshot'(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/WebElements/toolbarContainer'))

toolbarIsSimilar = CustomKeywords.'scripts.ScreenshotHandler.compareToBase'(toolbar, System.getProperty("user.dir") +"/Screenshots/Chrome/Base/Widgets/AssetManager/Configuration/WebElements/toolbarContainer.png")

assert toolbarIsSimilar == true

WebUI.switchToDefaultContent()

oiBgItem = CustomKeywords.'scripts.ScreenshotHandler.takeObjectScreenshot'(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/WebElements/OiBgItem'))

oiBgItemIsSimilar = CustomKeywords.'scripts.ScreenshotHandler.compareToBase'(oiBgItem, System.getProperty("user.dir") +"/Screenshots/Chrome/Base/Widgets/AssetManager/Configuration/WebElements/OiBgItem.png")

assert oiBgItemIsSimilar == true

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/addImageFromGallery'))

WebUI.delay(3)

WebUI.switchToDefaultContent()

galleryDialogInsertion = CustomKeywords.'scripts.ScreenshotHandler.takeObjectScreenshot'(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/WebElements/ModalDialogs/galleryDialog'))

galleryDialogInsertionIsSimilar = CustomKeywords.'scripts.ScreenshotHandler.compareToBase'(galleryDialogInsertion, System.getProperty("user.dir") +"/Screenshots/Chrome/Base/Widgets/AssetManager/Configuration/WebElements/ModalDialogs/galleryDialogAddImg.png")

assert galleryDialogInsertionIsSimilar == true

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/img_harrodsjpg_filename'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/GalleryInsertBtn'))

WebUI.delay(2)

WebUI.switchToDefaultContent()

canvasInsertImgFromGallery = CustomKeywords.'scripts.ScreenshotHandler.takeObjectScreenshot'(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/canvasViewerWrapper'))
canvasInsertImgFromGalleryIsSimilar = CustomKeywords.'scripts.ScreenshotHandler.compareToBase'(canvasInsertImgFromGallery, System.getProperty("user.dir") +"/Screenshots/Chrome/Base/Widgets/AssetManager/Configuration/WebElements/canvasInsertImgFromGallery.png")

assert canvasInsertImgFromGalleryIsSimilar == true

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/changeBackgroundGallery'))

WebUI.delay(2)

WebUI.switchToDefaultContent()

galleryDialogBgColor = CustomKeywords.'scripts.ScreenshotHandler.takeObjectScreenshot'(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/WebElements/ModalDialogs/galleryDialog'))
galleryDialogBgColorIsSimilar= CustomKeywords.'scripts.ScreenshotHandler.compareToBase'(galleryDialogBgColor, System.getProperty("user.dir") +"/Screenshots/Chrome/Base/Widgets/AssetManager/Configuration/WebElements/ModalDialogs/galleryDialogBgClr.png")

assert galleryDialogBgColorIsSimilar == true

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/bgColorFromGallery'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/GalleryBgUpdateBtn'))

WebUI.switchToDefaultContent()

WebUI.delay(2)

canvasBgClrFromGallery = CustomKeywords.'scripts.ScreenshotHandler.takeObjectScreenshot'(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/canvasViewerWrapper'))
canvasBgClrFromGalleryIsSimilar = CustomKeywords.'scripts.ScreenshotHandler.compareToBase'(canvasBgClrFromGallery, System.getProperty("user.dir") +"/Screenshots/Chrome/Base/Widgets/AssetManager/Configuration/WebElements/canvasBgClrFromGallery.png")

assert canvasBgClrFromGalleryIsSimilar == true

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/changeBackgroundGallery'))

WebUI.delay(2)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/europePhotosCategory'))

WebUI.switchToDefaultContent()

WebUI.delay(3)

galleryDialogBgImg = CustomKeywords.'scripts.ScreenshotHandler.takeObjectScreenshot'(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/WebElements/ModalDialogs/galleryDialog'))
galleryDialogBgImgIsSimilar= CustomKeywords.'scripts.ScreenshotHandler.compareToBase'(galleryDialogBgImg, System.getProperty("user.dir") +"/Screenshots/Chrome/Base/Widgets/AssetManager/Configuration/WebElements/ModalDialogs/galleryDialogBgImg.png")

assert galleryDialogBgImgIsSimilar == true

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/img_channeljpg_filename'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/GalleryBgUpdateBtn'))

WebUI.delay(2)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/addImageFromAM'))

WebUI.delay(3)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/AssetImg'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/AMInsertBtn'))

WebUI.delay(2)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/changeBackgroundAM'))

WebUI.delay(3)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/AssetBgImg'))



/*WebUI.delay()

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/OiBgItem'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/showImagePicker'))*/

WebUI.closeBrowser()

