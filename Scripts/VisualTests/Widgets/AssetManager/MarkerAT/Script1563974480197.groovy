import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('http://localhost:3000/')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/ATmarker/div_base-editor'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/ATmarker/div_Widgets'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/ATmarker/div_AssetManager'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/ATmarker/div_AssetManagerInterfaceMarke'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/ATmarker/button_RUN'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/ATmarker/div_Editor'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/ATmarker/div_Background'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/ATmarker/a_Image'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/ATmarker/asset_     8'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/ATmarker/button_Insert'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/ATmarker/paper-button_Finish'))

/*WebUI.closeBrowser()*/

