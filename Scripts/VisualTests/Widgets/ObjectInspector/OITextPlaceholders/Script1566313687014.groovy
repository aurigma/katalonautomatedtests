import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import org.testng.asserts.SoftAssert

import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.setViewPortSize(1930, 1180)

WebUI.navigateToUrl(GlobalVariable.TestStandUrl)

driver = DriverFactory.getExecutedBrowser().getName()

baseDir = (((System.getProperty('user.dir') + '/Screenshots/Baseline/') + driver) + '/')

reportsDir = System.getProperty('user.dir') + '/Screenshots/Reports/'

SoftAssert softAssertion = new SoftAssert();

WebUI.click(findTestObject('ConfigurationsTree/base-editor'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ObjectInspector/ObjectInspector'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ObjectInspector/OITextPlaceholders'))

WebUI.click(findTestObject('TestStandPanel/button_RUN'))

WebUI.click(findTestObject('TestStandPanel/EditorTab'))

WebUI.delay(4)

parent = findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Iframe')
WebElement parentElem = WebUI.findWebElement(parent, 30);
basePoint = parentElem.getLocation();
Actions builder = new Actions(DriverFactory.getWebDriver());
WebUI.switchToFrame(parent, 5)

WebUI.delay(5)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/OITextPlaceholder/OIContainer'),
	basePoint, baseDir + '/Widgets/ObjectInspector/OITextPlaceholder/OiInitial.png', reportsDir), GlobalVariable.Alert + 'OiInitial.png')

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ObjectInspector/OITextPlaceholder/CanvasInitial.png', reportsDir), GlobalVariable.Alert + 'CanvasInitial.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.delay(1)

WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/OITextPlaceholder/PlainText'), "Here is my text, the bext text ever!")

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/OITextPlaceholder/OIContainer'),
	basePoint, baseDir + '/Widgets/ObjectInspector/OITextPlaceholder/OiChagned1.png', reportsDir), GlobalVariable.Alert + 'OiChagned1.png')

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ObjectInspector/OITextPlaceholder/CanvasChanged1.png', reportsDir), GlobalVariable.Alert + 'CanvasChanged1.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/OITextPlaceholder/OpenRteMenu'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Aurigma.DesignEditor/Widgets/ObjectInspector/OITextPlaceholder/OpenRTE'))

WebUI.delay(2)

WebUI.switchToFrame(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/OITextPlaceholder/ckEditorFrame'), 5)

WebUI.delay(2)

WebElement rteTextArea = WebUiCommonHelper.findWebElement(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/OITextPlaceholder/RteTextArea'),30)
WebUI.executeJavaScript("arguments[0].innerText='My rich text baby!'", Arrays.asList(rteTextArea))

WebUI.switchToDefaultContent()

WebUI.delay(2)

WebUI.switchToFrame(parent, 5)

WebUI.delay(2)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/OITextPlaceholder/RTEUpdateButton'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/OITextPlaceholder/OIContainer'),
	basePoint, baseDir + '/Widgets/ObjectInspector/OITextPlaceholder/OiChagned2.png', reportsDir), GlobalVariable.Alert + 'OiChagned2.png')

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ObjectInspector/OITextPlaceholder/CanvasChanged2.png', reportsDir), GlobalVariable.Alert + 'CanvasChanged2.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.delay(1)

WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/OITextPlaceholder/PlainText'), "")

WebUI.delay(2)


softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/OITextPlaceholder/OIContainer'),
	basePoint, baseDir + '/Widgets/ObjectInspector/OITextPlaceholder/OiChagned3.png', reportsDir), GlobalVariable.Alert + 'OiChagned3.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/OITextPlaceholder/OpenRteMenu'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Aurigma.DesignEditor/Widgets/ObjectInspector/OITextPlaceholder/OpenRTE'))

WebUI.delay(2)

WebUI.switchToFrame(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/OITextPlaceholder/ckEditorFrame'), 5)

WebUI.delay(2)

WebElement rteTextArea2 = WebUiCommonHelper.findWebElement(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/OITextPlaceholder/RteTextArea2'),30)

WebUI.executeJavaScript("arguments[0].innerText=''", Arrays.asList(rteTextArea2))

WebUI.switchToDefaultContent()

WebUI.delay(2)

WebUI.switchToFrame(parent, 5)

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/OITextPlaceholder/RTEUpdateButton'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/OITextPlaceholder/OIContainer'),
	basePoint, baseDir + '/Widgets/ObjectInspector/OITextPlaceholder/OiChagned4.png', reportsDir), GlobalVariable.Alert + 'OiChagned4.png')

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ObjectInspector/OITextPlaceholder/CanvasChanged3.png', reportsDir), GlobalVariable.Alert + 'CanvasChanged3.png')

softAssertion.assertAll()

WebUI.closeBrowser()



