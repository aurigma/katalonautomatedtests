import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.Keys
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import org.testng.asserts.SoftAssert

import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.setViewPortSize(1930, 1180)

WebUI.navigateToUrl(GlobalVariable.TestStandUrl)

driver = DriverFactory.getExecutedBrowser().getName()

baseDir = (((System.getProperty('user.dir') + '/Screenshots/Baseline/') + driver) + '/')

reportsDir = System.getProperty('user.dir') + '/Screenshots/Reports/'

SoftAssert softAssertion = new SoftAssert();

WebUI.click(findTestObject('ConfigurationsTree/base-editor'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ObjectInspector/ObjectInspector'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ObjectInspector/ItemMenu'))

WebUI.click(findTestObject('TestStandPanel/button_RUN'))

WebUI.click(findTestObject('TestStandPanel/EditorTab'))

WebUI.delay(4)

parent = findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Iframe')
WebElement parentElem = WebUI.findWebElement(parent, 30);
basePoint = parentElem.getLocation();
Actions builder = new Actions(DriverFactory.getWebDriver());
WebUI.switchToFrame(parent, 5)

WebUI.delay(4)

WebElement OiContainer = WebUiCommonHelper.findWebElement(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ItemMenu/OIContainer'), 30)

CustomKeywords.'scripts.JSExecutor.executeJs'("arguments[0].scrollTop='600'", OiContainer)

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ItemMenu/OIContainer'),
	basePoint, baseDir + '/Widgets/ObjectInspector/ItemMenu/OiInitial.png', reportsDir), GlobalVariable.Alert + 'OiInitial.png')

WebElement image = WebUiCommonHelper.findWebElement(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ItemMenu/ImageItem'), 30)
WebElement line = WebUiCommonHelper.findWebElement(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ItemMenu/LineItem'), 30)
WebElement ellipse = WebUiCommonHelper.findWebElement(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ItemMenu/EllipseItem'), 30)

builder.keyDown(Keys.CONTROL).click(image).click(line).click(ellipse).keyUp(Keys.CONTROL).build().perform()

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ItemMenu/OIContainer'),
	basePoint, baseDir + '/Widgets/ObjectInspector/ItemMenu/OiItemsSelected.png', reportsDir), GlobalVariable.Alert + 'OiItemsSelected.png')

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ItemMenu/OpenLineMenu'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ItemMenu/ItemMenuCloneButton'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ItemMenu/OIContainer'),
	basePoint, baseDir + '/Widgets/ObjectInspector/ItemMenu/OiClonedItems.png', reportsDir), GlobalVariable.Alert + 'OiClonedItems.png')

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ObjectInspector/ItemMenu/CanvasClonedItems.png', reportsDir), GlobalVariable.Alert + 'CanvasClonedItems.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ItemMenu/OpenImageMenu'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ItemMenu/ItemMenuDialog'),
	basePoint, baseDir + '/Widgets/ObjectInspector/ItemMenu/ImageItemMenu.png', reportsDir), GlobalVariable.Alert + 'ImageItemMenu.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ItemMenu/ImageItem'))

WebUI.delay(1)

WebElement itemMenu = WebUiCommonHelper.findWebElement(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ItemMenu/ItemMenuDialog'),30)

Boolean isDisplayNone = WebUI.executeJavaScript("itemDisplay = arguments[0].style.display;  return itemDisplay == 'none'", Arrays.asList(itemMenu))

//KeywordUtil.logInfo(isDisplayNone.toString())

softAssertion.assertTrue(isDisplayNone, GlobalVariable.Alert + 'Display property must be equal to none!')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ItemMenu/OpenTextMenu'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ItemMenu/ItemMenuDialog'),
	basePoint, baseDir + '/Widgets/ObjectInspector/ItemMenu/TextItemMenu.png', reportsDir), GlobalVariable.Alert + 'TextItemMenu.png')

WebUI.delay(1)

Boolean isDisplayBlock = WebUI.executeJavaScript("itemDisplay = arguments[0].style.display;  return itemDisplay == 'block'", Arrays.asList(itemMenu))

softAssertion.assertTrue(isDisplayBlock, GlobalVariable.Alert + 'Display property must be equal to block!')

//KeywordUtil.logInfo(isDisplayBlock.toString())

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ItemMenu/SendToFront'))

CustomKeywords.'scripts.JSExecutor.executeJs'("arguments[0].scrollTop='0'", OiContainer)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ItemMenu/OIContainer'),
	basePoint, baseDir + '/Widgets/ObjectInspector/ItemMenu/OiOrderChanged.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ItemMenu/OpenFrontItemMenu'))

WebUI.delay(1)

softAssertion.assertTrue(isDisplayBlock, GlobalVariable.Alert + 'Display property must be equal to block!')

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ItemMenu/ItemMenuDialog'),
	basePoint, baseDir + '/Widgets/ObjectInspector/ItemMenu/FrontItemMenu.png')

softAssertion.assertAll()

WebUI.closeBrowser()


