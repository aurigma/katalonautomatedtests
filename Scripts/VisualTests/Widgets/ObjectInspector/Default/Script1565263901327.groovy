import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebElement
import org.testng.asserts.SoftAssert

import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.TestStandUrl)

WebUI.setViewPortSize(1930, 1180)

WebUI.navigateToUrl(GlobalVariable.TestStandUrl)

driver = DriverFactory.getExecutedBrowser().getName()

baseDir = (((System.getProperty('user.dir') + '/Screenshots/Baseline/') + driver) + '/')

reportsDir = System.getProperty('user.dir') + '/Screenshots/Reports/'

SoftAssert softAssertion = new SoftAssert();

WebUI.click(findTestObject('ConfigurationsTree/base-editor'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ObjectInspector/ObjectInspector'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ObjectInspector/Default'))

WebUI.click(findTestObject('TestStandPanel/button_RUN'))

WebUI.click(findTestObject('TestStandPanel/EditorTab'))

WebUI.delay(4)

parent = findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Iframe')
WebElement parentElem = WebUI.findWebElement(parent, 30);
basePoint = parentElem.getLocation();
WebUI.switchToFrame(parent, 5)

WebUI.delay(4)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/ObjectInspector'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Default/OIinitial.png', reportsDir), GlobalVariable.Alert + 'OIinitial.png')

WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/TextArea'), "Just text or so")

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/ObjectInspector'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Default/OIItemChanged1.png', reportsDir), GlobalVariable.Alert + 'OIItemChanged1.png')

WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/PlainTextField'), "Plain text:\nlorem ipsum\nsit omet\nconsectetur adipiscing elit, sed")

WebUI.delay(2)

//The following chech is too inconsistent, values missmatch. Uncomment or replace when able to find aproximate calculation or get more accurate values
//Boolean hasHeightChanged = WebUI.executeJavaScript("textHeight = spEditor._canvasViewer.getHandler(spEditor.model.currentItem).bounds.height;  return textHeight == 95.49874899999998", null)
//softAssertion.assertTrue(hasHeightChanged, GlobalVariable.Alert + 'Expected text height is 95.49874899999998')

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/ObjectInspector'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Default/OIItemChanged2.png', reportsDir), GlobalVariable.Alert + 'OIItemChanged2.png')

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Default/TextItemsChanged1.png', reportsDir), GlobalVariable.Alert + 'TextItemsChanged1.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.delay(1)

WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/PlainTextField'), "Plain text:\nlorem ipsum\nsit omet")

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/ObjectInspector'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Default/OIItemChanged3.png', reportsDir), GlobalVariable.Alert + 'OIItemChanged3.png')

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Default/TextItemsChanged2.png', reportsDir), GlobalVariable.Alert + 'TextItemsChanged2.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.delay(1)

WebUI.dragAndDropByOffset(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/OiItemGrab'), 0, 600)

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/ObjectInspector'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Default/OIItemChanged4.png', reportsDir), GlobalVariable.Alert + 'OIItemChanged4.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/OpenTextMenu'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/ItemMenuDialog'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Default/OIItemMenu.png', reportsDir), GlobalVariable.Alert + 'OIItemMenu.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/AlignmentMenu'))

WebUI.delay(3)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/AlignToBottom'))

WebUI.delay(1)

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Default/TextItemsChanged3.png', reportsDir), GlobalVariable.Alert + 'TextItemsChanged3.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.delay(3)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/OpenTextMenu'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/ItemMenuCloneButton'))

WebUI.delay(1)

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Default/TextItemsChanged4.png', reportsDir), GlobalVariable.Alert + 'TextItemsChanged4.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.delay(3)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/OpenTextMenu'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/ItemMenuDeleteButton'))

WebUI.delay(1)

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Default/TextItemsChanged5.png', reportsDir), GlobalVariable.Alert + 'TextItemsChanged5.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.delay(3)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/OpenTextMenu'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/ItemMenuDeleteButton'))

WebUI.delay(1)

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Default/TextItemsChanged6.png', reportsDir), GlobalVariable.Alert + 'TextItemsChanged6.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/OpenPlaceholderMenu'))

WebUI.mouseOver(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/AlignmentMenu'))

WebUI.delay(4)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/AlignToTop'))

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Default/ItemOrderChanged1.png', reportsDir), GlobalVariable.Alert + 'ItemOrderChanged1.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.delay(2)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/OpenPlaceholderMenu'))

//WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/SendToFront'))
	
WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Default/ItemOrderChanged2.png', reportsDir), GlobalVariable.Alert + 'ItemOrderChanged2.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.delay(2)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/OpenFrontItemMenu'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/LevelDown'))

Boolean isIndexChanged = WebUI.executeJavaScript("itemName = spEditor.productHandler.getAllItems()[12].name;  return itemName == 'Placeholder' ", null)

KeywordUtil.logInfo(isIndexChanged.toString())

softAssertion.assertTrue(isIndexChanged, GlobalVariable.Alert + 'Expected item name at 12 zOrder is Placeholder')

softAssertion.assertAll()

WebUI.closeBrowser()


