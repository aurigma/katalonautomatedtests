import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import org.testng.asserts.SoftAssert

import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.setViewPortSize(1930, 1180)

WebUI.navigateToUrl(GlobalVariable.TestStandUrl)

driver = DriverFactory.getExecutedBrowser().getName()

baseDir = (((System.getProperty('user.dir') + '/Screenshots/Baseline/') + driver) + '/')

reportsDir = System.getProperty('user.dir') + '/Screenshots/Reports/'

SoftAssert softAssertion = new SoftAssert();

WebUI.click(findTestObject('ConfigurationsTree/base-editor'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ObjectInspector/ObjectInspector'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ObjectInspector/ZOrder'))

WebUI.click(findTestObject('TestStandPanel/button_RUN'))

WebUI.click(findTestObject('TestStandPanel/EditorTab'))

WebUI.delay(4)

parent = findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Iframe')
WebElement parentElem = WebUI.findWebElement(parent, 30);
basePoint = parentElem.getLocation();
Actions builder = new Actions(DriverFactory.getWebDriver());
WebUI.switchToFrame(parent, 5)

WebUI.delay(4)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ZOrder/OIContainer'),
	basePoint, baseDir + '/Widgets/ObjectInspector/ZOrder/OiInitial.png', reportsDir), GlobalVariable.Alert + 'OiInitial.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ZOrder/OpenTextMenu'))

WebUI.delay(2)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ZOrder/SendToFront'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ZOrder/OIContainer'),
	basePoint, baseDir + '/Widgets/ObjectInspector/ZOrder/OiOrderChanged.png', reportsDir), GlobalVariable.Alert + 'OiOrderChanged.png')

/*The next commented strings are intended to check if generated PDF contains certain text substrings. 
*But unfortunately, Customer's Canvas generates text as vector shapes, that can't be parsed.
*Maybe one day this check will make sense
*/

//String hiResUrl = WebUI.executeJavaScript(" = spEditor.finishProductDesign();  return url.__zone_symbol__value.hiResOutputUrls[0];", null)
//String textPdf = CustomKeywords.'scripts.PDFHandler.ReadPDF'(hiResUrl)
//KeywordUtil.logInfo("PDF:" + textPdf)
//String substring = "t: 1234 5678\nf: 1234 5679\ne: johndoe@email.com";
//Boolean isPdfCorrect = textPdf.contains(substring)

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ObjectInspector/ZOrder/GeneratedPreview.png', reportsDir), GlobalVariable.Alert + 'GeneratedPreview.png')

softAssertion.assertAll()

WebUI.closeBrowser()