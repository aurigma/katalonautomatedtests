import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebElement
import org.testng.asserts.SoftAssert

import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.setViewPortSize(1930, 1180)

WebUI.navigateToUrl(GlobalVariable.TestStandUrl)

driver = DriverFactory.getExecutedBrowser().getName()

baseDir = (((System.getProperty('user.dir') + '/Screenshots/Baseline/') + driver) + '/')

reportsDir = System.getProperty('user.dir') + '/Screenshots/Reports/'

SoftAssert softAssertion = new SoftAssert();

WebUI.click(findTestObject('ConfigurationsTree/base-editor'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.Barcodes/LinearBarcodeDialog'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.Barcodes/BarcodeDefault'))

WebUI.click(findTestObject('TestStandPanel/button_RUN'))

WebUI.click(findTestObject('TestStandPanel/EditorTab'))

WebUI.delay(4)

parent = findTestObject('Aurigma.DesignEditor/Iframe')
WebElement parentElem = WebUI.findWebElement(parent, 30);
basePoint = parentElem.getLocation();
WebUI.switchToFrame(parent, 5)

WebUI.delay(5)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/OpenBarcodeMenu'))

WebUI.delay(2)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/AddBarcode'))

WebUI.delay(1)

WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/BarcodeValueInput'), 5)

WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/BarcodeValueInput'), '1234567')

WebUI.delay(3)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/BarcodeDialog'),
	basePoint, baseDir + '/Widgets/Barcodes/Barcode/BarcodeInitial1.png', reportsDir), GlobalVariable.Alert + 'BarcodeInitial1.png')

WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/BarcodeValueInput'), '987654321')

WebUI.delay(3)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/BarcodeDialog'),
	basePoint, baseDir + '/Widgets/Barcodes/Barcode/BarcodeChanged1.png', reportsDir), GlobalVariable.Alert + 'BarcodeChanged1.png')

WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/BarcodeValueInput'), 'ad124jh')

WebUI.delay(3)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/BarcodeDialog'),
	basePoint, baseDir + '/Widgets/Barcodes/Barcode/BarcodeChanged2.png', reportsDir), GlobalVariable.Alert + 'BarcodeChanged2.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/SwitchToEan13'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/LiEan13'))

WebUI.delay(3)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/BarcodeDialog'),
	basePoint, baseDir + '/Widgets/Barcodes/Barcode/BarcodeChanged3.png', reportsDir), GlobalVariable.Alert + 'BarcodeChanged3.png')

WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/BarcodeValueInput'), '1234567891235634')

WebUI.delay(3)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/BarcodeDialog'),
	basePoint, baseDir + '/Widgets/Barcodes/Barcode/BarcodeChanged4.png', reportsDir), GlobalVariable.Alert + 'BarcodeChanged4.png')

WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/BarcodeValueInput'), 'ad124jh45hdasdf')

WebUI.delay(3)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/BarcodeDialog'),
	basePoint, baseDir + '/Widgets/Barcodes/Barcode/BarcodeChanged5.png', reportsDir), GlobalVariable.Alert + 'BarcodeChanged5.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/SwitchToEan8'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/LiEan8'))

WebUI.delay(3)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/BarcodeDialog'),
	basePoint, baseDir + '/Widgets/Barcodes/Barcode/BarcodeChanged6.png', reportsDir), GlobalVariable.Alert + 'BarcodeChanged6.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/SwitchToEan13'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/LiEan13'))

WebUI.delay(2)

WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/BarcodeValueInput'), '123456789123')

WebUI.delay(3)

WebUI.click(findTestObject('/Aurigma.DesignEditor/Widgets/Barcodes/CloseDialog'))

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/Barcodes/Barcode/ChangeNotApplied.png', reportsDir), GlobalVariable.Alert + 'ChangeNotApplied.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/OpenBarcodeMenu'))

WebUI.delay(2)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/AddBarcode'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/BarcodeDialog'),
	basePoint, baseDir + '/Widgets/Barcodes/Barcode/BarcodeInitial2.png', reportsDir), GlobalVariable.Alert + 'BarcodeInitial2.png')

WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/BarcodeValueInput'), 5)

WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/BarcodeValueInput'), '4357629')

WebUI.delay(2)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/InsertButton'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/OIContainer'),
	basePoint, baseDir + '/Widgets/Barcodes/Barcode/KeepGoodQuality.png', reportsDir), GlobalVariable.Alert + 'KeepGoodQuality.png')

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/Barcodes/Barcode/ChangeApplied1.png', reportsDir), GlobalVariable.Alert + 'ChangeApplied1.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/OpenMenuFromOI'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/ItemMenuDialog'),
	basePoint, baseDir + '/Widgets/Barcodes/Barcode/ItemMenuView.png', reportsDir), GlobalVariable.Alert + 'ItemMenuView.png')

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/EditBarcodeButton'))

WebUI.delay(3)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/BarcodeDialog'),
	basePoint, baseDir + '/Widgets/Barcodes/Barcode/EditKeepChanges.png', reportsDir), GlobalVariable.Alert + 'EditKeepChanges.png')

WebElement barcodePreview = WebUiCommonHelper.findWebElement(findTestObject('Aurigma.DesignEditor/Widgets/Barcodes/BarcodePreview'), 30)

String previewUrl = WebUI.executeJavaScript("return arguments[0].currentSrc;", Arrays.asList(barcodePreview))
//CustomKeywords.'scripts.JSExecutor.executeJs'()

String barcodeText = CustomKeywords.'scripts.BarcodeReader.ReadBarcode'(previewUrl)

KeywordUtil.logInfo(barcodeText)

softAssertion.assertEquals(barcodeText, "43576296", GlobalVariable.Alert + 'The barcode content must be equal to 43576296!')

softAssertion.assertAll()

WebUI.closeBrowser()


