import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebElement as WebElement
import org.testng.asserts.SoftAssert

import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

WebUI.openBrowser('')

WebUI.delay(2)

WebUI.setViewPortSize(1930, 1180)

WebUI.navigateToUrl(GlobalVariable.TestStandUrl)

driver = DriverFactory.getExecutedBrowser().getName()

baseDir = (((System.getProperty('user.dir') + '/Screenshots/Baseline/') + driver) + '/')

reportsDir = System.getProperty('user.dir') + '/Screenshots/Reports/'

SoftAssert softAssertion = new SoftAssert();

WebUI.click(findTestObject('ConfigurationsTree/base-editor'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/ColorPicker'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/Targets'))

WebUI.click(findTestObject('TestStandPanel/button_RUN'))

WebUI.click(findTestObject('TestStandPanel/EditorTab'))

WebUI.delay(4)

parent = findTestObject('Aurigma.DesignEditor/Iframe')
WebElement parentElem = WebUI.findWebElement(parent, 30);
basePoint = parentElem.getLocation();
WebUI.switchToFrame(parent, 5)

WebUI.delay(4)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Targets/SelectTextColorButton'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Targets/TextColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/Targets/TextColorTarget.png', reportsDir), GlobalVariable.Alert + 'TextColorTarget.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Targets/SelectBorderColorButton'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Targets/TextBorderColorDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/Targets/TextBorderTarget.png', reportsDir), GlobalVariable.Alert + 'TextBorderTarget.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Targets/AddRectangle'))

WebUI.delay(2)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Targets/SelectBorderColorButton'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Targets/ShapeBorderColorDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/Targets/ShapeBorderTarget.png', reportsDir), GlobalVariable.Alert + 'ShapeBorderTarget.png')

WebUI.delay(2)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Targets/FillColorButton'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Targets/FillColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/Targets/ShapeFillTarget.png', reportsDir), GlobalVariable.Alert + 'ShapeFillTarget.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Targets/OIPickAnotherItem'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Targets/button_More'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Targets/TextShadowButton'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Targets/OpenShadowPalette'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Targets/TextShadowPalette'),
	basePoint, baseDir + '/Widgets/ColorPicker/Targets/TextShadowTarget.png', reportsDir), GlobalVariable.Alert + 'TextShadowTarget.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Targets/TextStrokeButton'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Targets/OpenStrokePalette'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Targets/TextStrokePalette'),
	basePoint, baseDir + '/Widgets/ColorPicker/Targets/TextStrokeTarget.png', reportsDir), GlobalVariable.Alert + 'TextStrokeTarget.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Targets/AddLine'))

WebUI.delay(2)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Targets/SelectLineColorButton'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Targets/LineColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/Targets/LineColorTarget.png', reportsDir), GlobalVariable.Alert + 'LineColorTarget.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Targets/AddRichtext'))

WebUI.delay(2)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Targets/RTETextColorButton'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Targets/RTEColorPicker'),
	basePoint, baseDir + '/Widgets/ColorPicker/Targets/RichTextColorTarget.png', reportsDir), GlobalVariable.Alert + 'RichTextColorTarget.png')

WebUI.closeBrowser()

