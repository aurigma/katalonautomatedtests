import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebElement as WebElement
import org.testng.asserts.SoftAssert

import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

WebUI.openBrowser('')

//WebUI.maximizeWindow()

WebUI.setViewPortSize(1930, 1180)

WebUI.navigateToUrl(GlobalVariable.TestStandUrl)

driver = DriverFactory.getExecutedBrowser().getName()

baseDir = (((System.getProperty('user.dir') + '/Screenshots/Baseline/') + driver) + '/')

reportsDir = System.getProperty('user.dir') + '/Screenshots/Reports/'

SoftAssert softAssertion = new SoftAssert();

WebUI.click(findTestObject('ConfigurationsTree/base-editor'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/ColorPicker'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/SectionCollapse'))

WebUI.click(findTestObject('TestStandPanel/button_RUN'))

WebUI.click(findTestObject('TestStandPanel/EditorTab'))

WebUI.delay(4)

parent = findTestObject('Aurigma.DesignEditor/Iframe')
WebElement parentElem = WebUI.findWebElement(parent, 30);
basePoint = parentElem.getLocation();
WebUI.switchToFrame(parent, 5)

WebUI.delay(4)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton'))

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'), 3)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/PickerWithCollapseSections.png', reportsDir), GlobalVariable.Alert + 'RTEPickerOpened.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/GrayscaleSectionCollapse'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/GrayscaleCollapsed.png', reportsDir), GlobalVariable.Alert + 'GrayscaleCollapsed.png')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RgbSectionCollapse'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/RGBCollapsed.png', reportsDir), GlobalVariable.Alert + 'RGBCollapsed.png')

WebUI.click(findTestObject('Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/CmykSectionCollapse'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/CMYKCollapsed.png', reportsDir), GlobalVariable.Alert + 'CMYKCollapsed.png')

WebUI.click(findTestObject('Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RecentSectionCollapse'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/AllCollapsed.png', reportsDir), GlobalVariable.Alert + 'AllCollapsed.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved.png', reportsDir), GlobalVariable.Alert + 'CollapsedConditionSaved.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/button_OK'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved2.png', reportsDir), GlobalVariable.Alert + 'CollapsedConditionSaved2.png')

WebUI.click(findTestObject('Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RecentSectionCollapse'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RgbSectionCollapse'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/OIPickAnotherItem'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved3.png', reportsDir), GlobalVariable.Alert + 'CollapsedConditionSaved3.png')

WebUI.click(findTestObject('Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/AddText'))

WebUI.delay(3)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved4.png', reportsDir), GlobalVariable.Alert + 'CollapsedConditionSaved4.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/button_OK'))

WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/TextArea'), "Test")

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton'))

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved5.png', reportsDir), GlobalVariable.Alert + 'CollapsedConditionSaved5')

WebUI.click(findTestObject('Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/UndoButton'))

WebUI.click(findTestObject('Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/OIPickAnotherItem'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/CollapsedConditionReverted.png', reportsDir), GlobalVariable.Alert + 'CollapsedConditionReverted.png')

WebUI.closeBrowser()















