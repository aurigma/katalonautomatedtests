import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebElement as WebElement
import org.testng.asserts.SoftAssert

import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

WebUI.openBrowser('')

WebUI.setViewPortSize(1930, 1180)

WebUI.navigateToUrl(GlobalVariable.TestStandUrl)

driver = DriverFactory.getExecutedBrowser().getName()

baseDir = (((System.getProperty('user.dir') + '/Screenshots/Baseline/') + driver) + '/')

reportsDir = System.getProperty('user.dir') + '/Screenshots/Reports/'

SoftAssert softAssertion = new SoftAssert();

WebUI.click(findTestObject('ConfigurationsTree/base-editor'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/ColorPicker'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/AvailableColorSpaces/AvailableColorSpaces'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/AvailableColorSpaces/CmykCmyk'))

WebUI.click(findTestObject('TestStandPanel/button_RUN'))

WebUI.click(findTestObject('TestStandPanel/EditorTab'))

WebUI.delay(4)

parent = findTestObject('Aurigma.DesignEditor/Iframe')
WebElement parentElem = WebUI.findWebElement(parent, 30);
basePoint = parentElem.getLocation();
WebUI.switchToFrame(parent, 5)

WebUI.delay(3)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/AvailableColorSpaces/SelectTextColorButton'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/AvailableColorSpaces/TextColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/AvailableColorSpaces/DefaultColorSpaceCmyk2.png', reportsDir), GlobalVariable.Alert + 'DefaultColorSpaceCmyk2.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/AvailableColorSpaces/Cmyk_YellowColor'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/AvailableColorSpaces/TextColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/AvailableColorSpaces/ColorChanged.png', reportsDir), GlobalVariable.Alert + 'ColorChanged.png')

WebUI.closeBrowser()

