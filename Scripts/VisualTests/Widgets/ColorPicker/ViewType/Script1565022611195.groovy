import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebElement as WebElement
import org.testng.asserts.SoftAssert

import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

WebUI.openBrowser('')

WebUI.delay(2)

WebUI.setViewPortSize(1930, 1180)

WebUI.navigateToUrl(GlobalVariable.TestStandUrl)

driver = DriverFactory.getExecutedBrowser().getName()

baseDir = (((System.getProperty('user.dir') + '/Screenshots/Baseline/') + driver) + '/')

reportsDir = System.getProperty('user.dir') + '/Screenshots/Reports/'

SoftAssert softAssertion = new SoftAssert();

WebUI.click(findTestObject('ConfigurationsTree/base-editor'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/ColorPicker'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/ViewType'))

WebUI.click(findTestObject('TestStandPanel/button_RUN'))

WebUI.click(findTestObject('TestStandPanel/EditorTab'))

WebUI.delay(4)

parent = findTestObject('Aurigma.DesignEditor/Iframe')
WebElement parentElem = WebUI.findWebElement(parent, 30);
basePoint = parentElem.getLocation();
WebUI.switchToFrame(parent, 5)

WebUI.delay(4)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/ViewType/SelectTextColorButton'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/ViewType/TextColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/ViewType/ViewTypes1.png', reportsDir), GlobalVariable.Alert + 'ViewTypes1.png')

WebElement element = WebUiCommonHelper.findWebElement(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/ViewType/SectionsView'), 30)

CustomKeywords.'scripts.JSExecutor.executeJs'("arguments[0].scrollTop='120'", element)

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/ViewType/TextColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/ViewType/ViewTypes2.png', reportsDir), GlobalVariable.Alert + 'ViewTypes2.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/ViewType/RGBSeection_BlueSquare'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/ViewType/SelectTextColorButton'),
	basePoint, baseDir + '/Widgets/ColorPicker/ViewType/ButtonColorChanged1.png', reportsDir), GlobalVariable.Alert + 'ButtonColorChanged1.png')

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ColorPicker/ViewType/ColorAppliedCheck1.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.click(findTestObject('Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/ViewType/RGBSection_OrangeCaption'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/ViewType/SelectTextColorButton'),
	basePoint, baseDir + '/Widgets/ColorPicker/ViewType/ButtonColorChanged2.png', reportsDir), GlobalVariable.Alert + 'ButtonColorChanged2.png')

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ColorPicker/ViewType/ColorAppliedCheck2.png', reportsDir), GlobalVariable.Alert + 'ColorAppliedCheck2.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

CustomKeywords.'scripts.JSExecutor.executeJs'("arguments[0].scrollTop='350'", element)

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/ViewType/TextColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/ViewType/ViewTypes3.png', reportsDir), GlobalVariable.Alert + 'ViewTypes3.png')

WebUI.delay(1)

CustomKeywords.'scripts.JSExecutor.executeJs'("arguments[0].scrollTop='0'", element)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/ViewType/GScaleSection_DarkGray'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/ViewType/SelectTextColorButton'),
	basePoint, baseDir + '/Widgets/ColorPicker/ViewType/ButtonColorChanged3.png', reportsDir), GlobalVariable.Alert + 'ButtonColorChanged3.png')

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ColorPicker/ViewType/ColorAppliedCheck3.png', reportsDir), GlobalVariable.Alert + 'ColorAppliedCheck3.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.closeBrowser()
