import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebElement as WebElement
import org.testng.asserts.SoftAssert

import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.setViewPortSize(1930, 1180)

WebUI.navigateToUrl(GlobalVariable.TestStandUrl)

driver = DriverFactory.getExecutedBrowser().getName()

baseDir = (((System.getProperty('user.dir') + '/Screenshots/Baseline/') + driver) + '/')

reportsDir = System.getProperty('user.dir') + '/Screenshots/Reports/'

SoftAssert softAssertion = new SoftAssert();

WebUI.click(findTestObject('ConfigurationsTree/base-editor'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/ColorPicker'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/OneSection'))

WebUI.click(findTestObject('TestStandPanel/button_RUN'))

WebUI.click(findTestObject('TestStandPanel/EditorTab'))

WebUI.delay(4)

parent = findTestObject('Aurigma.DesignEditor/Iframe')

WebElement parentElem = WebUI.findWebElement(parent, 30)

basePoint = parentElem.getLocation()

WebUI.switchToFrame(parent, 5)

WebUI.delay(4)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton'))

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'), 
    3)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/OneSectionPalette.png', reportsDir), GlobalVariable.Alert + 'OneSectionPalette.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/OneSection_BlueColor'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/TextColorButtonChanged.png', reportsDir), GlobalVariable.Alert + 'TextColorButtonChanged.png')

//assert CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton'), 
 //   basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/TextColorButtonChanged.png', reportsDir')

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('TestStandPanel/PreviewImage'),
	 basePoint, baseDir + '/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck.png', reportsDir),  GlobalVariable.Alert + 'ColorAppliedCheck.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton'), 
    basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/TextColorButtonSaved.png', reportsDir),  GlobalVariable.Alert + 'TextColorButtonSaved.png')


WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'), 
    basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/PaletteNoChanges1.png', reportsDir), GlobalVariable.Alert + 'PaletteNoChanges1.png')

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RevertButton'))

if ((driver == 'CHROME_DRIVER') || (driver == 'HEADLESS_DRIVER')) {
    WebUI.acceptAlert()
}

WebUI.delay(2)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/OIPickAnotherItem'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'), 
    basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/PaletteNoChanges2.png', reportsDir), GlobalVariable.Alert + 'PaletteNoChanges2.png')

WebUI.closeBrowser()

