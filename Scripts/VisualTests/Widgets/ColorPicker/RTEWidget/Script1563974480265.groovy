import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebElement as WebElement

import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.testng.asserts.SoftAssert;
import internal.GlobalVariable

WebUI.openBrowser('')

//WebUI.maximizeWindow()

WebUI.setViewPortSize(1930, 1180)

WebUI.navigateToUrl(GlobalVariable.TestStandUrl)	

driver = DriverFactory.getExecutedBrowser().getName()

baseDir = (((System.getProperty('user.dir') + '/Screenshots/Baseline/') + driver) + '/')

reportsDir = System.getProperty('user.dir') + '/Screenshots/Reports/'

SoftAssert softAssertion = new SoftAssert();

WebUI.click(findTestObject('ConfigurationsTree/base-editor'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/ColorPicker'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/Widgets'))

WebUI.click(findTestObject('TestStandPanel/button_RUN'))

WebUI.click(findTestObject('TestStandPanel/EditorTab'))

WebUI.delay(5)

parent = findTestObject('Aurigma.DesignEditor/Iframe')
WebElement parentElem = WebUI.findWebElement(parent, 30);
basePoint = parentElem.getLocation();
WebUI.switchToFrame(parent, 5)

WebUI.delay(4)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/AddRichText'))

WebUI.delay(2)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETextColorButton'))

WebUI.delay(2)

WebUI.focus(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETopBar'))

WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker'), 5)

WebUI.delay(1)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker'),
	basePoint, baseDir + '/Widgets/ColorPicker/Widgets/RTEPickerOpened.png', reportsDir), GlobalVariable.Alert + 'RTEPickerOpened.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorOk'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETopBar'),
	basePoint, baseDir + '/Widgets/ColorPicker/Widgets/RTEPickerClosed.png', reportsDir), GlobalVariable.Alert + 'RTEPickerClosed.png')

//TODO: resolve setting the innerText for a non-input element, i.e. CKEditor field
//WebUI.sendKeys(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/CKEditorTextField'), Keys.chord(Keys.ARROW_RIGHT))
//WebUI.setText(findTestObject('Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/CKEditorTextField'), "Orange text!")

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETextColorButton'))

WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColors_Orange'), 5)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColors_Orange'))

WebUI.delay(2)

WebUI.focus(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColors_Orange'))

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker'),
	basePoint, baseDir + '/Widgets/ColorPicker/Widgets/RTEPickerRecent.png', reportsDir), GlobalVariable.Alert + 'RTEPickerRecent.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/CloseRTEPalette'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETopBar'),
	basePoint, baseDir + '/Widgets/ColorPicker/Widgets/RTEFinial.png', reportsDir), GlobalVariable.Alert + 'RTEFinial.png')

WebUI.closeBrowser()
