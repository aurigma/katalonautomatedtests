import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebElement as WebElement
import org.testng.asserts.SoftAssert

import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

//WebUI.maximizeWindow()

WebUI.setViewPortSize(1930, 1180)

WebUI.navigateToUrl(GlobalVariable.TestStandUrl)

driver = DriverFactory.getExecutedBrowser().getName()

baseDir = (((System.getProperty('user.dir') + '/Screenshots/Baseline/') + driver) + '/')

reportsDir = System.getProperty('user.dir') + '/Screenshots/Reports/'

SoftAssert softAssertion = new SoftAssert();

WebUI.click(findTestObject('ConfigurationsTree/base-editor'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/ColorPicker'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/Widgets'))

WebUI.click(findTestObject('TestStandPanel/button_RUN'))

WebUI.click(findTestObject('TestStandPanel/EditorTab'))

WebUI.delay(5)

parent = findTestObject('Aurigma.DesignEditor/Iframe')
WebElement parentElem = WebUI.findWebElement(parent, 30);
basePoint = parentElem.getLocation();
WebUI.switchToFrame(parent, 5)

WebUI.delay(4)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/button_More'))

WebUI.delay(2)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokeButton'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokeDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/Widgets/StrokeDialogInitial.png', reportsDir), GlobalVariable.Alert + 'StrokeDialogInitial.png')


WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/OpenStrokePalette'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokePalette'),
	basePoint, baseDir + '/Widgets/ColorPicker/Widgets/StrokePaletteInitial.png', reportsDir), GlobalVariable.Alert + 'StrokePaletteInitial.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/StrokeOK'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokeDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/Widgets/BackToStrokeDialog.png', reportsDir), GlobalVariable.Alert + 'BackToStrokeDialog.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/OpenStrokePalette'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/StrokeColors_Yellow'))

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokePalette'), 5)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokePalette'),
	basePoint, baseDir + '/Widgets/ColorPicker/Widgets/StrokeRecentAdded.png', reportsDir), GlobalVariable.Alert + 'StrokeRecentAdded.png')

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ColorPicker/Widgets/StrokeApplied.png', reportsDir), GlobalVariable.Alert + 'StrokeApplied.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/StrokeOK'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokeDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/Widgets/EnabledStroke.png', reportsDir), GlobalVariable.Alert + 'EnabledStroke.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/OpenStrokePalette'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/CloseStrokePalette'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokeButton'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/StrokeEnabledCheckbox'))

WebUI.delay(2)

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ColorPicker/Widgets/StrokeRemoved.png', reportsDir), GlobalVariable.Alert + 'StrokeRemoved.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.delay(2)

softAssertion.assertTrue(CustomKeywords.'scripts.ScreenshotHandler.takeAndCompare'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokeDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/Widgets/StrokeDialogChanged.png', reportsDir), GlobalVariable.Alert + 'StrokeDialogChanged.png')

WebUI.closeBrowser()

