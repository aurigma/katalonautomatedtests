import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebElement as WebElement

import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

WebUI.openBrowser('')

WebUI.setViewPortSize(1930, 1180)

WebUI.navigateToUrl(GlobalVariable.TestStandUrl)

driver = DriverFactory.getExecutedBrowser().getName()

baseDir = (((System.getProperty('user.dir') + '/Screenshots/Baseline/') + driver) + '/')

WebUI.click(findTestObject('ConfigurationsTree/base-editor'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/ColorPicker'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/SectionCollapseEnabled'))

WebUI.click(findTestObject('TestStandPanel/button_RUN'))

WebUI.click(findTestObject('TestStandPanel/EditorTab'))

WebUI.delay(4)

parent = findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Iframe')
WebElement parentElem = WebUI.findWebElement(parent, 30);
basePoint = parentElem.getLocation();
WebUI.switchToFrame(parent, 5)

WebUI.delay(4)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton'))

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'), 3)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/UncollapsedSections1.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/OIPickAnotherItem'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/UncollapsedSections2.png')

WebUI.click(findTestObject('Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/FillColorButton'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/FillColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/UncollapsedSections3.png')

WebUI.closeBrowser()