import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebElement as WebElement

import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

WebUI.openBrowser('')

WebUI.delay(2)

WebUI.setViewPortSize(1930, 1180)

WebUI.navigateToUrl(GlobalVariable.TestStandUrl)

driver = DriverFactory.getExecutedBrowser().getName()

baseDir = (((System.getProperty('user.dir') + '/Screenshots/Baseline/') + driver) + '/')

WebUI.click(findTestObject('ConfigurationsTree/base-editor'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/ColorPicker'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/ThemeSection'))

WebUI.click(findTestObject('TestStandPanel/button_RUN'))

WebUI.click(findTestObject('TestStandPanel/EditorTab'))

WebUI.delay(4)

parent = findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Iframe')
WebElement parentElem = WebUI.findWebElement(parent, 30);
basePoint = parentElem.getLocation();
WebUI.switchToFrame(parent, 5)

WebUI.delay(4)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/ThemeSection/SelectTextColorButton'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/ThemeSection/TextColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/ThemeSection/WhiteThemeInitital.png')

WebUI.executeJavaScript("return spEditor.productHandler.applyProductTheme('Red');", null)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/ThemeSection/TextColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/ThemeSection/RedThemeInitital.png')

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('TestStandPanel/PreviewImage'), basePoint, baseDir + '/Widgets/ColorPicker/ThemeSection/ThemeAppliedCheck1.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.executeJavaScript("return spEditor.productHandler.applyProductTheme('Green');", null)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/ThemeSection/TextColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/ThemeSection/GreenThemeInitital.png')

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('TestStandPanel/PreviewImage'), basePoint, baseDir + '/Widgets/ColorPicker/ThemeSection/ThemeAppliedCheck2.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

//Check that the color applied from none theme section will be recovered with color from theme adter calling applyColorTheme function.

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/ThemeSection/RgbColor_DustBlue'))

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('TestStandPanel/PreviewImage'), basePoint, baseDir + '/Widgets/ColorPicker/ThemeSection/ColorAppliedCheck1.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.executeJavaScript("return spEditor.productHandler.applyProductTheme('Red');", null)

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('TestStandPanel/PreviewImage'), basePoint, baseDir + '/Widgets/ColorPicker/ThemeSection/ThemeAppliedCheck3.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

//Check that the color applied from Color Theme section is saved when calling applyColorTheme function. As it was C03 before applying, it will stay C03 but with color from a new theme.

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/ThemeSection/ThemeColor_C03'))

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('TestStandPanel/PreviewImage'), basePoint, baseDir + '/Widgets/ColorPicker/ThemeSection/ColorAppliedCheck2.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.executeJavaScript("return spEditor.productHandler.applyProductTheme('White');", null)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/ThemeSection/TextColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/ThemeSection/WhiteThemeColors.png')

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('TestStandPanel/PreviewImage'), basePoint, baseDir + '/Widgets/ColorPicker/ThemeSection/ThemeAppliedCheck4.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.closeBrowser()


