import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebElement as WebElement

import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

WebUI.openBrowser(GlobalVariable.TestStandUrl)

//WebUI.maximizeWindow()

WebUI.setViewPortSize(1930, 1180)

WebUI.navigateToUrl(GlobalVariable.TestStandUrl)

driver = DriverFactory.getExecutedBrowser().getName()

baseDir = (((System.getProperty('user.dir') + '/Screenshots/Baseline/') + driver) + '/')

WebUI.click(findTestObject('ConfigurationsTree/base-editor'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/ColorPicker'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/Picker'))

WebUI.click(findTestObject('TestStandPanel/button_RUN'))

WebUI.click(findTestObject('TestStandPanel/EditorTab'))

WebUI.delay(4)

parent = findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Iframe')
WebElement parentElem = WebUI.findWebElement(parent, 30);
basePoint = parentElem.getLocation();
WebUI.switchToFrame(parent, 5)

WebUI.delay(4)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton'))

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), 5)

WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch'), 5)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch'))

WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK'), 5)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK'))

WebUI.delay(1)

WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker'), 5)
WebUI.dragAndDropByOffset(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker'), 0, 15)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), 
    basePoint, baseDir + '/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png')

WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker'), 5)

WebUI.dragAndDropByOffset(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker'), 0, 85)

WebUI.delay(1)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), 
    basePoint, baseDir + '/Widgets/ColorPicker/PickerCMYK/HueChanged1.png')

WebUI.delay(1)

WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker'), 5)

WebUI.dragAndDropByOffset(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker'), 0, 110)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), 
   basePoint, baseDir + '/Widgets/ColorPicker/PickerCMYK/PickersFirstCheck.png')

WebUI.delay(1)

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('TestStandPanel/PreviewImage'), basePoint, baseDir + '/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck1.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), 5)

WebUI.delay(1)

WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker'), 5)

WebUI.dragAndDropByOffset(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker'), 5, 2)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), basePoint,
    baseDir + '/Widgets/ColorPicker/PickerCMYK/SaturationChanged2.png')

WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker'), 5)

WebUI.dragAndDropByOffset(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker'), 0, 20)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), basePoint,
    baseDir + '/Widgets/ColorPicker/PickerCMYK/HueChanged2.png')

WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker'), 5)

WebUI.dragAndDropByOffset(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker'), 0, 20)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), basePoint,
    baseDir + '/Widgets/ColorPicker/PickerCMYK/PickersSecondCheck.png')

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('TestStandPanel/PreviewImage'), basePoint, baseDir + '/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck2.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.delay(1)

WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), 5)

WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection'), 5)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection'))

WebUI.delay(2)

WebUI.focus(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection'))

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), basePoint,
    baseDir + '/Widgets/ColorPicker/PickerCMYK/RecentPaletteAdded.png')

WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel'), 5)
	
WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), basePoint,
    baseDir + '/Widgets/ColorPicker/PickerCMYK/ChannelSelectedLightning.png')

if (driver == "CHROME_DRIVER" || driver == "HEADLESS_DRIVER") {
	
	WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel'), '67')
	
	WebUI.delay(2)
	
	CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), basePoint,
	    baseDir + '/Widgets/ColorPicker/PickerCMYK/CChannelValidInput1.png')
	
	WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel'), 'abc')
	
	WebUI.delay(2)
	
	CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), basePoint,
	    baseDir + '/Widgets/ColorPicker/PickerCMYK/CChannelInvalidInput1.png')
	
	WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel'), '76')
	
	WebUI.delay(2)
	
	CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), basePoint,
	    baseDir + '/Widgets/ColorPicker/PickerCMYK/CChannelValidInput2.png')
	
	WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel'), '!@')
	
	WebUI.delay(2)
	
	CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), basePoint,
	    baseDir + '/Widgets/ColorPicker/PickerCMYK/CChannelInvalidInput2.png')
	
	WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel'), '150')
	
	WebUI.delay(2)
	
	CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), basePoint,
	    baseDir + '/Widgets/ColorPicker/PickerCMYK/CChannelInvalidInput3.png')


	WebUI.sendKeys(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel'), Keys.chord(Keys.TAB))
	
	WebUI.delay(2)
	
	CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), basePoint,
	    baseDir + '/Widgets/ColorPicker/PickerCMYK/TabChangedCursorToM.png')
	WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_M_Channel'), 5)
	WebUI.sendKeys(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_M_Channel'), Keys.chord(Keys.TAB))
	
	WebUI.delay(2)
	
	CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), basePoint,
	    baseDir + '/Widgets/ColorPicker/PickerCMYK/TabChangedCursorToY.png')
	WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_Y_Channel'), 5)
	WebUI.sendKeys(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_Y_Channel'), Keys.chord(Keys.TAB))
	
	WebUI.delay(2)
	
	CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), basePoint,
	    baseDir + '/Widgets/ColorPicker/PickerCMYK/TabChangedCursorToK.png')
	WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_K_Channel'), 5)
	WebUI.sendKeys(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_K_Channel'), Keys.chord(Keys.TAB))
	
	WebUI.delay(2)
	
	CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), basePoint,
	    baseDir + '/Widgets/ColorPicker/PickerCMYK/TabChangedCursorToA.png')
	WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_cmykA_Channel'), 5)
	
	WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_cmykA_Channel'), '22')
	
	WebUI.sendKeys(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_cmykA_Channel'), Keys.chord(Keys.SHIFT, 
	        Keys.TAB))
	
	WebUI.delay(2)
	
	CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), basePoint,
	    baseDir + '/Widgets/ColorPicker/PickerCMYK/TabChangedCursorBackToK.png')
}
WebUI.closeBrowser()

