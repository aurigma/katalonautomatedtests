import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebElement as WebElement

import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

WebUI.openBrowser('')

//WebUI.maximizeWindow()

WebUI.setViewPortSize(1930, 1180)

WebUI.navigateToUrl(GlobalVariable.TestStandUrl)

driver = DriverFactory.getExecutedBrowser().getName()
baseDir = (((System.getProperty('user.dir') + '/Screenshots/Baseline/') + driver) + '/')
KeywordUtil.logInfo(baseDir)

KeywordUtil.logInfo(driver)

WebUI.click(findTestObject('ConfigurationsTree/base-editor'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/ColorPicker'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/Palette'))

WebUI.click(findTestObject('TestStandPanel/button_RUN'))

WebUI.click(findTestObject('TestStandPanel/EditorTab'))

WebUI.delay(5)

parent = findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Iframe')
WebElement parentElem = WebUI.findWebElement(parent, 30);
basePoint = parentElem.getLocation();
WebUI.switchToFrame(parent, 5)

WebUI.delay(2)

if (driver == "CHROME_DRIVER" || driver == "HEADLESS_DRIVER") {

	WebUI.focus(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton'))
	
	CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton'),
		basePoint, baseDir + '/Widgets/ColorPicker/Palette/SelectColorButton.png')
}

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton'))

WebUI.delay(2)

//WebUI.waitForElementVisible(findTestObject('null'), 5, FailureHandling.STOP_ON_FAILURE)
WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog'), 5)

//CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('null'), 
// baseDir + '/Widgets/ColorPicker/Palette/TextColorDialog.png')


CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog'), 
   basePoint, baseDir + '/Widgets/ColorPicker/Palette/TextColorDialog.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Purple'))

WebUI.delay(1)

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection'))

WebUI.focus(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection'))

//WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/Palette/AddToRecentCheck1.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Burgundy'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RecentColor_Purple'))

WebUI.focus(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RecentColor_Purple'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/Palette/AddToRecentCheck2.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection'))

WebUI.focus(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/Palette/AddToRecentCheck3.png')


WebUI.navigateToUrl('http://localhost:3000/')

if (driver == "CHROME_DRIVER" || driver == "HEADLESS_DRIVER")
{
	WebUI.acceptAlert()
}

WebUI.click(findTestObject('ConfigurationsTree/base-editor'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/ColorPicker'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/Palette'))

WebUI.click(findTestObject('TestStandPanel/button_RUN'))

WebUI.click(findTestObject('TestStandPanel/EditorTab'))

WebUI.delay(5)

WebUI.switchToFrame(parent, 5)

WebUI.delay(2)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton'))

WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog'), 5, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/Palette/CPAtNewPage.png')

WebUI.delay(1)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Palette/FillColorButton'),
	basePoint, baseDir + '/Widgets/ColorPicker/Palette/FillButtonNoColor.png')

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Palette/FillColorButton'))

WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Palette/FillColorPickerDialog'), 5)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Palette/FillColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/Palette/FillColorDialog.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Yellow'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Palette/FillColorButton'),
	basePoint, baseDir + '/Widgets/ColorPicker/Palette/FillButtonColored.png')

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ColorPicker/Palette/ColorAppliedCheck2.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.delay(2)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Palette/NoColorSection'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Palette/FillColorButton'),
	basePoint, baseDir + '/Widgets/ColorPicker/Palette/FillButtonColorRemoved.png')

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('TestStandPanel/PreviewImage'),
	 basePoint, baseDir + '/Widgets/ColorPicker/Palette/ColorCanceledCheck.png')

WebUI.closeBrowser()

