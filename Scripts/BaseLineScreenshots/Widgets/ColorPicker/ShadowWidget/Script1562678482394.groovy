import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebElement as WebElement

import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

//WebUI.maximizeWindow()

WebUI.setViewPortSize(1930, 1180)

WebUI.navigateToUrl(GlobalVariable.TestStandUrl)

driver = DriverFactory.getExecutedBrowser().getName()

baseDir = (((System.getProperty('user.dir') + '/Screenshots/Baseline/') + driver) + '/')

WebUI.click(findTestObject('ConfigurationsTree/base-editor'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/ColorPicker'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/Widgets'))

WebUI.click(findTestObject('TestStandPanel/button_RUN'))

WebUI.click(findTestObject('TestStandPanel/EditorTab'))

WebUI.delay(5)

parent = findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Iframe')
WebElement parentElem = WebUI.findWebElement(parent, 30);
basePoint = parentElem.getLocation();
WebUI.switchToFrame(parent, 5)

WebUI.delay(4)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/button_More'))

WebUI.delay(2)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextShadowButton'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextShadowDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/Widgets/ShadowDialogInitial.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/OpenShadowPalette'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextShadowPalette'),
	basePoint, baseDir + '/Widgets/ColorPicker/PickerRGB/ShadowPaletteInitial.png')


WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/ShadowOK'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextShadowDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/Widgets/BackToShadowDialog.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/OpenShadowPalette'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/ShadowColors_Blue'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextShadowPalette'),
	basePoint, baseDir + '/Widgets/ColorPicker/Widgets/ShadowRecentAdded.png')

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ColorPicker/Widgets/ShadowApplied.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/ShadowOK'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextShadowDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/Widgets/EnabledShadow.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/OpenShadowPalette'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/CloseShadowPalette'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextShadowButton'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/ShadowEnabledCheckbox'))

WebUI.delay(2)

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ColorPicker/Widgets/ShadowRemoved.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextShadowDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/Widgets/ShadowDialogChanged.png')

WebUI.closeBrowser()

