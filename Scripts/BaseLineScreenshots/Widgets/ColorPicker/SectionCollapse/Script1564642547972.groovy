import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebElement as WebElement

import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

WebUI.openBrowser('')

//WebUI.maximizeWindow()

WebUI.setViewPortSize(1930, 1180)

WebUI.navigateToUrl(GlobalVariable.TestStandUrl)

driver = DriverFactory.getExecutedBrowser().getName()

baseDir = (((System.getProperty('user.dir') + '/Screenshots/Baseline/') + driver) + '/')

WebUI.click(findTestObject('ConfigurationsTree/base-editor'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/ColorPicker'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/SectionCollapse'))

WebUI.click(findTestObject('TestStandPanel/button_RUN'))

WebUI.click(findTestObject('TestStandPanel/EditorTab'))

WebUI.delay(4)

parent = findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Iframe')
WebElement parentElem = WebUI.findWebElement(parent, 30);
basePoint = parentElem.getLocation();
WebUI.switchToFrame(parent, 5)

WebUI.delay(4)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton'))

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'), 3)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/PickerWithCollapseSections.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/GrayscaleSectionCollapse'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/GrayscaleCollapsed.png')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RgbSectionCollapse'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/RGBCollapsed.png')

WebUI.click(findTestObject('Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/CmykSectionCollapse'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/CMYKCollapsed.png')

WebUI.click(findTestObject('Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RecentSectionCollapse'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/AllCollapsed.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/button_OK'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved2.png')

WebUI.click(findTestObject('Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RecentSectionCollapse'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RgbSectionCollapse'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/OIPickAnotherItem'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved3.png')

WebUI.click(findTestObject('Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/AddText'))

WebUI.delay(3)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved4.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/button_OK'))

WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/TextArea'), "Test")

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton'))

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved5.png')

WebUI.click(findTestObject('Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/UndoButton'))

WebUI.click(findTestObject('Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/OIPickAnotherItem'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton'))

WebUI.delay(2)
	
CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog'),
	basePoint, baseDir + '/Widgets/ColorPicker/SectionCollapse/CollapsedConditionReverted.png')

WebUI.closeBrowser()















