import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebElement as WebElement

import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

//WebUI.maximizeWindow()

WebUI.setViewPortSize(1930, 1180)

WebUI.navigateToUrl(GlobalVariable.TestStandUrl)

driver = DriverFactory.getExecutedBrowser().getName()

baseDir = (((System.getProperty('user.dir') + '/Screenshots/Baseline/') + driver) + '/')

WebUI.click(findTestObject('ConfigurationsTree/base-editor'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/ColorPicker'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ColorPicker/Picker'))

WebUI.click(findTestObject('TestStandPanel/button_RUN'))

WebUI.click(findTestObject('TestStandPanel/EditorTab'))

WebUI.delay(5)

parent = findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Iframe')
WebElement parentElem = WebUI.findWebElement(parent, 30);
basePoint = parentElem.getLocation();
WebUI.switchToFrame(parent, 5)

WebUI.delay(4)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton'))

WebUI.delay(1)

WebUI.dragAndDropByOffset(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker'), 0, 10)

WebUI.focus(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), 
    basePoint, baseDir + '/Widgets/ColorPicker/PickerRGB/SaturationChanged1.png')

WebUI.dragAndDropByOffset(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker'), 0, 130)

WebUI.focus(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), 
    basePoint, baseDir + '/Widgets/ColorPicker/PickerRGB/HueChanged1.png')

WebUI.dragAndDropByOffset(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker'), 0, 90)

WebUI.focus(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), 
    basePoint, baseDir + '/Widgets/ColorPicker/PickerRGB/PickersFirstCheck.png')

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('TestStandPanel/PreviewImage'), basePoint, baseDir + '/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck1.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.delay(2)

WebUI.dragAndDropByOffset(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker'), -5, -2)

WebUI.focus(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), 
    basePoint, baseDir + '/Widgets/ColorPicker/PickerRGB/SaturationChanged2.png')

WebUI.dragAndDropByOffset(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker'), 0, -70)

WebUI.focus(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), 
   basePoint,  baseDir + '/Widgets/ColorPicker/PickerRGB/HueChanged2.png')

WebUI.dragAndDropByOffset(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker'), 0, -30)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), 
    basePoint, baseDir + '/Widgets/ColorPicker/PickerRGB/PickersSecondCheck.png')

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('TestStandPanel/PreviewImage'), basePoint, baseDir + '/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck2.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection'))

WebUI.focus(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection'))

WebUI.delay(3)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), 
    basePoint, baseDir + '/Widgets/ColorPicker/PickerRGB/RecentPaletteAdded.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_R_Channel'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), 
    basePoint, baseDir + '/Widgets/ColorPicker/PickerRGB/ChannelSelectedLightning.png')

//The next section is working inadequate in Firefox, so I decided to leave these checks only in Chrome.
if (driver == "CHROME_DRIVER" || driver == "HEADLESS_DRIVER") {
    WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_R_Channel'), '25')

    WebUI.delay(2)

    CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), 
        basePoint, baseDir + '/Widgets/ColorPicker/PickerRGB/RChannelValidInput1.png')

    WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_R_Channel'), 'abc')

    WebUI.delay(3)

    CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), 
        basePoint, baseDir + '/Widgets/ColorPicker/PickerRGB/RChannelInvalidInput1.png')

    WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_R_Channel'), '76')

    WebUI.delay(2)

    CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), 
        basePoint, baseDir + '/Widgets/ColorPicker/PickerRGB/RChannelValidInput2.png')

    WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_R_Channel'), '!@')

    WebUI.delay(2)

    CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), 
        basePoint, baseDir + '/Widgets/ColorPicker/PickerRGB/RChannelInvalidInput2.png')

    WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_R_Channel'), '280')

    WebUI.delay(2)

    CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), 
        basePoint, baseDir + '/Widgets/ColorPicker/PickerRGB/RChannelInvalidInput3.png')

    WebUI.sendKeys(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_R_Channel'), Keys.chord(Keys.TAB))

    WebUI.delay(2)

    CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), 
        basePoint, baseDir + '/Widgets/ColorPicker/PickerRGB/TabChangedCursorToG.png')

    WebUI.sendKeys(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_G_Channel'), Keys.chord(Keys.TAB))

    WebUI.delay(2)

    CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), 
        basePoint, baseDir + '/Widgets/ColorPicker/PickerRGB/TabChangedCursorToB.png')

    WebUI.sendKeys(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_B_Channel'), Keys.chord(Keys.TAB))

    WebUI.delay(2)

    CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), 
        basePoint, baseDir + '/Widgets/ColorPicker/PickerRGB/TabChangedCursorToA.png')

    WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_rgbA_Channel'), '45')

    WebUI.sendKeys(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_rgbA_Channel'), Keys.chord(Keys.SHIFT, 
            Keys.TAB))

    WebUI.delay(2)

    CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog'), 
        basePoint, baseDir + '/Widgets/ColorPicker/PickerRGB/TabChangedCursorBackToB.png')
}

WebUI.closeBrowser()

