import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.Keys
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions

import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.setViewPortSize(1930, 1180)

WebUI.navigateToUrl(GlobalVariable.TestStandUrl)

driver = DriverFactory.getExecutedBrowser().getName()

baseDir = (((System.getProperty('user.dir') + '/Screenshots/Baseline/') + driver) + '/')

WebUI.click(findTestObject('ConfigurationsTree/base-editor'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ObjectInspector/ObjectInspector'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ObjectInspector/ZOrder'))

WebUI.click(findTestObject('TestStandPanel/button_RUN'))

WebUI.click(findTestObject('TestStandPanel/EditorTab'))

WebUI.delay(4)

parent = findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Iframe')
WebElement parentElem = WebUI.findWebElement(parent, 30);
basePoint = parentElem.getLocation();
Actions builder = new Actions(DriverFactory.getWebDriver());
WebUI.switchToFrame(parent, 5)

WebUI.delay(4)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ZOrder/OIContainer'),
	basePoint, baseDir + '/Widgets/ObjectInspector/ZOrder/OiInitial.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ZOrder/OpenTextMenu'))

WebUI.delay(2)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ZOrder/SendToFront'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ZOrder/OIContainer'),
	basePoint, baseDir + '/Widgets/ObjectInspector/ZOrder/OiOrderChanged.png')

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ObjectInspector/ZOrder/GeneratedPreview.png')

WebUI.closeBrowser()