import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.Keys
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions

import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.setViewPortSize(1930, 1180)

WebUI.navigateToUrl(GlobalVariable.TestStandUrl)

driver = DriverFactory.getExecutedBrowser().getName()

baseDir = (((System.getProperty('user.dir') + '/Screenshots/Baseline/') + driver) + '/')

WebUI.click(findTestObject('ConfigurationsTree/base-editor'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ObjectInspector/ObjectInspector'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ObjectInspector/ForegroundContainer'))

WebUI.click(findTestObject('TestStandPanel/button_RUN'))

WebUI.click(findTestObject('TestStandPanel/EditorTab'))

WebUI.delay(4)

parent = findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Iframe')
WebElement parentElem = WebUI.findWebElement(parent, 30);
basePoint = parentElem.getLocation();
Actions builder = new Actions(DriverFactory.getWebDriver());
WebUI.switchToFrame(parent, 5)

WebUI.delay(4)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ForegroundContainer/OIContainer'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Foreground/OiInitial.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ForegroundContainer/OpenRectangle1Menu'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ForegroundContainer/ItemMenuDialog'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Foreground/RectangleMenu.png')

WebUI.delay(1)
//check that buttons changing z-order are not available in top toolbar for foreground
CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ForegroundContainer/TopToolbar'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Foreground/TopTolbar.png')

WebUI.dragAndDropByOffset(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ForegroundContainer/Rectangle3Grab'), 0, -200)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ForegroundContainer/OIContainer'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Foreground/OiOrderChanged1.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ForegroundContainer/OpenRectangle2Menu'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ForegroundContainer/SendToFront'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ForegroundContainer/OIContainer'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Foreground/OiOrderChanged2.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ForegroundContainer/OpenRectangleMenu'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ForegroundContainer/LevelUp'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ForegroundContainer/OIContainer'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Foreground/OiOrderChanged3.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ForegroundContainer/AddTextMenu'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ForegroundContainer/AddPlainText'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ForegroundContainer/OIContainer'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Foreground/OiOrderChanged4.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ForegroundContainer/OpenTextMenu'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ForegroundContainer/LevelUp'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ForegroundContainer/OIContainer'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Foreground/OiOrderChanged5.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ForegroundContainer/AddShapeItem'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ForegroundContainer/AddRectangle'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ForegroundContainer/OIContainer'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Foreground/OiOrderChanged6.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ForegroundContainer/OpenRectangleMenu'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ForegroundContainer/LevelUp'))

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ForegroundContainer/OIContainer'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Foreground/OiOrderChanged7.png')

WebUI.dragAndDropByOffset(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ForegroundContainer/AddedRectangleGrab'), 0, -150)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/ForegroundContainer/OIContainer'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Foreground/OiOrderChanged8.png')

WebUI.closeBrowser()