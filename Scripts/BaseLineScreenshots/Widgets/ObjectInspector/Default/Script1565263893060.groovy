import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions

import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.setViewPortSize(1930, 1180)

WebUI.navigateToUrl(GlobalVariable.TestStandUrl)

driver = DriverFactory.getExecutedBrowser().getName()

baseDir = (((System.getProperty('user.dir') + '/Screenshots/Baseline/') + driver) + '/')

WebUI.click(findTestObject('ConfigurationsTree/base-editor'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ObjectInspector/ObjectInspector'))

WebUI.click(findTestObject('ConfigurationsTree/Widgets.ObjectInspector/Default'))

WebUI.click(findTestObject('TestStandPanel/button_RUN'))

WebUI.click(findTestObject('TestStandPanel/EditorTab'))

WebUI.delay(4)

parent = findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Iframe')
WebElement parentElem = WebUI.findWebElement(parent, 30);
basePoint = parentElem.getLocation();
Actions actions = new Actions(DriverFactory.getWebDriver());
WebUI.switchToFrame(parent, 5)

WebUI.delay(4)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/ObjectInspector'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Default/OIinitial.png')

WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/TextArea'), "Just text or so")

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/ObjectInspector'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Default/OIItemChanged1.png')

WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/PlainTextField'), "Plain text:\nlorem ipsum\nsit omet\nconsectetur adipiscing elit, sed")
//Move to visual test part, here are only screenshots
//Boolean hasHeightChanged = WebUI.executeJavaScript("textHeight = spEditor._canvasViewer.getHandler(spEditor.model.currentItem).bounds.height;  return textHeight==181.81521600000008", null)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/ObjectInspector'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Default/OIItemChanged2.png')

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Default/TextItemsChanged1.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.delay(1)

WebUI.setText(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/PlainTextField'), "Plain text:\nlorem ipsum\nsit omet")

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/ObjectInspector'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Default/OIItemChanged3.png')

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Default/TextItemsChanged2.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.delay(1)

WebUI.dragAndDropByOffset(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/OiItemGrab'), 0, 600)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/ObjectInspector'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Default/OIItemChanged4.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/OpenTextMenu'))

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/ItemMenuDialog'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Default/OIItemMenu.png')

//WebElement element = WebUiCommonHelper.findWebElement(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/AlignmentMenu'), 30);

//actions.clickAndHold(element).perform();

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/AlignmentMenu'))

WebUI.delay(3)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/AlignToBottom'))

WebUI.delay(1)

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Default/TextItemsChanged3.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.delay(3)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/OpenTextMenu'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/ItemMenuCloneButton'))

WebUI.delay(1)

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Default/TextItemsChanged4.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.delay(3)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/OpenTextMenu'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/ItemMenuDeleteButton'))

WebUI.delay(1)

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Default/TextItemsChanged5.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.delay(3)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/OpenTextMenu'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/ItemMenuDeleteButton'))

WebUI.delay(1)

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Default/TextItemsChanged6.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.delay(4)

WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/OpenPlaceholderMenu'), 5)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/OpenPlaceholderMenu'))

WebUI.delay(4)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/AlignmentMenu'))
WebUI.delay(3)
//WebUI.delay(1)
//actions.clickAndHold(element).perform();

//WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/AlignToTop'))

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Default/ItemOrderChanged1.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.delay(2)

//WebUI.waitForElementVisible(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/OpenPlaceholderMenu'), 5)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/OpenPlaceholderMenu'))

//WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/SendToFront'))
	
WebUI.switchToDefaultContent()

WebUI.click(findTestObject('TestStandPanel/FinishButton'))

WebUI.waitForElementVisible(findTestObject('TestStandPanel/PreviewImage'), 5)

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('TestStandPanel/PreviewImage'),
	basePoint, baseDir + '/Widgets/ObjectInspector/Default/ItemOrderChanged2.png')

WebUI.click(findTestObject('TestStandPanel/BackToEditorButton'))

WebUI.switchToFrame(parent, 5)

WebUI.delay(2)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/OpenFrontItemMenu'))

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/ObjectInspector/Default/LevelDown'))

WebUI.closeBrowser()




