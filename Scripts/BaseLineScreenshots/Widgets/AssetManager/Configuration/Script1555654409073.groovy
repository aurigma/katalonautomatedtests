
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebElement as WebElement
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('http://localhost:3000/')

driver = DriverFactory.getExecutedBrowser().getName()
baseDir = System.getProperty('user.dir') +'/Screenshots/Base/'+driver+'/';

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.delay(10)
iframe = WebUI.executeJavaScript("return auWizard.scopeInternal.\$.cc.\$.editorFrame.id;", null)
//auWizard.scopeInternal.$.cc.$.editorFrame
//auWizard.scopeInternal.$.cc.shadowRoot.querySelector("iframe")
Boolean isShadow = true
editorFrame = new TestObject(iframe)
editorFrame.setParentObject(new TestObject("return auWizard.scopeInternal.\$.cc.id;")) 
editorFrame.setParentObjectShadowRoot(isShadow)

WebUI.delay(5)

WebUI.click((findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/addImageFromGallery').setParentObject(editorFrame)))
CustomKeywords.'scripts.ScreenshotHandler.takeAndSaveFolder'('Widgets/AssetManager/Configuration/WebElements', baseDir, findTestObject('null').setParentObject(editorFrame))


WebUI.delay(3)



WebUI.switchToDefaultContent()

WebUI.delay(5)

/*CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/WebElements/ModalDialogs/galleryDialog'), 
    System.getProperty('user.dir') + '/Screenshots/Chrome/Base/Widgets/AssetManager/Configuration/WebElements/ModalDialogs/galleryDialogAddImg.png')*/

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/WebElements/ModalDialogs/galleryDialog'), baseDir+'galleryDialogAddImg.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/img_harrodsjpg_filename'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/GalleryInsertBtn'))

WebUI.delay(2)

WebUI.switchToDefaultContent()

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/WebElements/canvasViewerWrapper'),
	baseDir+'canvasInsertImgFromGallery.png')

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/changeBackgroundGallery'))

WebUI.switchToDefaultContent()

WebUI.delay(2)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/WebElements/ModalDialogs/galleryDialog'),
	baseDir+'galleryDialogBgClr.png')

WebUI.delay(2)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/bgColorFromGallery'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/GalleryBgUpdateBtn'))

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('null'))

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/WebElements/canvasViewerWrapper'),
	baseDir+'canvasBgClrFromGallery-pre.png')
WebUI.delay(6)
WebUI.click(findTestObject('null'))

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/WebElements/canvasViewerWrapper'),
	baseDir+'canvasBgClrFromGallery.png')

WebUI.delay(1)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/changeBackgroundGallery'))

WebUI.delay(3)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/europePhotosCategory'))

WebUI.switchToDefaultContent()

WebUI.delay(3)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/WebElements/ModalDialogs/galleryDialog'),
	baseDir+'galleryDialogBgImg.png')

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/img_channeljpg_filename'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/GalleryBgUpdateBtn'))

WebUI.switchToDefaultContent()

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/WebElements/canvasViewerWrapper'),
	baseDir+'canvasBgFromGallery.png')

WebUI.delay(3)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/addImageFromAM'))

WebUI.switchToDefaultContent()

WebUI.delay(3)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/WebElements/ModalDialogs/assetManager'),
	baseDir+'assetManager.png')


WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/AssetImg'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/AMInsertBtn'))

WebUI.switchToDefaultContent()

WebUI.delay(3)

CustomKeywords.'scripts.ScreenshotHandler.takeAndSave'(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/WebElements/canvasViewerWrapper'),
	baseDir+'canvasImgFromAM.png')

WebUI.delay(2)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/changeBackgroundAM'))

WebUI.delay(3)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/assetManagerTab'))

WebUI.delay(3)

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/AssetBgImg'))

WebUI.click(findTestObject('Aurigma.DesignEditor/Widgets/AssetManager/Configuration/TestCaseObjects/AMInsertBtn'))

//WebUI.closeBrowser(FailureHandling.CONTINUE_ON_FAILURE)

