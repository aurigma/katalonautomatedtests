
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import java.lang.String

import org.openqa.selenium.WebElement

import com.kms.katalon.core.testobject.TestObject

import org.openqa.selenium.Point

import ru.yandex.qatools.ashot.Screenshot


def static "scripts.BarcodeReader.ReadBarcode"(
    	String url	) {
    (new scripts.BarcodeReader()).ReadBarcode(
        	url)
}

def static "scripts.JSExecutor.executeJs"(
    	String javaScript	
     , 	WebElement element	) {
    (new scripts.JSExecutor()).executeJs(
        	javaScript
         , 	element)
}

def static "scripts.ScreenshotHandler.takeAndSave"(
    	TestObject object	
     , 	String outputFile	) {
    (new scripts.ScreenshotHandler()).takeAndSave(
        	object
         , 	outputFile)
}

def static "scripts.ScreenshotHandler.takeAndCompare"(
    	TestObject object	
     , 	String baseImageFile	
     , 	String reportPath	) {
    (new scripts.ScreenshotHandler()).takeAndCompare(
        	object
         , 	baseImageFile
         , 	reportPath)
}

def static "scripts.ScreenshotHandler.takeAndCompare"(
    	TestObject object	
     , 	Point basePoint	
     , 	String baseImageFile	
     , 	String reportPath	) {
    (new scripts.ScreenshotHandler()).takeAndCompare(
        	object
         , 	basePoint
         , 	baseImageFile
         , 	reportPath)
}

def static "scripts.ScreenshotHandler.takePageAndSave"(
    	String outputFile	) {
    (new scripts.ScreenshotHandler()).takePageAndSave(
        	outputFile)
}

def static "scripts.ScreenshotHandler.takeAndSave"(
    	TestObject object	
     , 	Point basePoint	
     , 	String outputFile	) {
    (new scripts.ScreenshotHandler()).takeAndSave(
        	object
         , 	basePoint
         , 	outputFile)
}

def static "scripts.ScreenshotHandler.takeAndSaveFolder"(
    	String repositoryFolder	
     , 	String outputDir	
     , 	TestObject parentObject	) {
    (new scripts.ScreenshotHandler()).takeAndSaveFolder(
        	repositoryFolder
         , 	outputDir
         , 	parentObject)
}

def static "scripts.ScreenshotHandler.compareToBase"(
    	Screenshot screenshot	
     , 	String baseImagePath	
     , 	String reportPath	) {
    (new scripts.ScreenshotHandler()).compareToBase(
        	screenshot
         , 	baseImagePath
         , 	reportPath)
}

def static "scripts.ScreenshotHandler.takeObjectScreenshot"(
    	TestObject object	) {
    (new scripts.ScreenshotHandler()).takeObjectScreenshot(
        	object)
}

def static "scripts.ScreenshotHandler.takeObjectScreenshot"(
    	TestObject object	
     , 	Point basePoint	) {
    (new scripts.ScreenshotHandler()).takeObjectScreenshot(
        	object
         , 	basePoint)
}

def static "scripts.ScreenshotHandler.takePageScreenshot"() {
    (new scripts.ScreenshotHandler()).takePageScreenshot()
}

def static "scripts.ScreenshotHandler.saveScreenshot"(
    	Screenshot screenshot	
     , 	String imagePath	) {
    (new scripts.ScreenshotHandler()).saveScreenshot(
        	screenshot
         , 	imagePath)
}
