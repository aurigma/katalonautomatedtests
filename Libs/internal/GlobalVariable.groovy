package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object TestStandUrl
     
    /**
     * <p></p>
     */
    public static Object CURRENT_TESTCASE_ID
     
    /**
     * <p></p>
     */
    public static Object TIMEMARK
     
    /**
     * <p></p>
     */
    public static Object Alert
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += RunConfiguration.getOverridingParameters()
    
            TestStandUrl = selectedVariables['TestStandUrl']
            CURRENT_TESTCASE_ID = selectedVariables['CURRENT_TESTCASE_ID']
            TIMEMARK = selectedVariables['TIMEMARK']
            Alert = selectedVariables['Alert']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
