## Step 1. Prerequisites

- Download the latest version of Katalon Studio ( for this you have to create account on https://www.katalon.com/). Katalon Studio is IDE for test automation provided by Katalon LLC for free. Here you can find more about their products: https://docs.katalon.com/katalon-studio/docs/index.html#products. 

- Clone KatalonAutomatedTests. This repository contains Katalon project with automated tests.

- Clone TestStandProducts. This repository contains test-stand which is used to run automated tests along with Customer's Canvas per every product configuration. Note! Test-stand is created by integration team. The current version of stand is built using base-editor and compatible only with Customer's Canvas 5. For bug fixes and updates contact integration team. 

- Copy *assets* folder ftom TestStandProducts repository to the folder with designs specified in the configuration of your Customer's Canvass instance.

## Step 2. Test-stand

Test-stand backend is created with Node.js. In order to start the application open powershell by your repository path and type the following command: `npm run start`.

When test-stand has been launched, you have to configure its settings. For this open global.config.json and replace the following parameters with your values:

- *pathFolder* - path to the folder with configs 

- *CustomersCanvasUrl* - link to your Customer's Canvas instance.

## Step 3. Test run

Automated tests are written on Groovy language, which is extending classic Java. There are two ways to run tests: from Katalon Studio and from command prompt (thee second is preferable since it loads less processes).

1. Test run from Katalon Studio. Double-click katalon.exe and choose VisualTestsProject (you will be asked to sign in with your Katalon account). Navigate to Test Suite, choose the collection of tests you want to run or create your own and click Run at the top toolbar. 

2. Test run from command line. Open command line and navigate to the Katalon folder (it must contain katalon.exe application). You can use ready commands from *katalon console commands.txt* file. Don't forget to change path to your repository.
 

## Katalon Analytics

It is possible to integrate Katalon Studio with Katalon Analytics. Here is the topic explaining how to integrate: https://docs.katalon.com/katalon-analytics/docs/integration-with-katalon-studio.html#enable-integration

You can configuer your project to send reports to your own analytics cloud instance or to ask me to add on team.

## Existing baseline screenshots

Since screenshots may have differences depending on monitors settings, it is recommended to recreate tests on the PC/server where tests will be executed in the future. The stable version of Customer's Canvas should be used.