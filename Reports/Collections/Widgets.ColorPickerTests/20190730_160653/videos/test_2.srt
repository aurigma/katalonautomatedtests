1
00:00:00,279 --> 00:00:00,280
1. openBrowser("")

2
00:00:04,513 --> 00:00:04,513
5. setViewPortSize(1930, 1180)

3
00:00:04,593 --> 00:00:04,594
9. navigateToUrl("http://localhost:3000/")

4
00:00:05,962 --> 00:00:05,963
13. driver = getExecutedBrowser().getName()

5
00:00:05,980 --> 00:00:05,981
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:05,992 --> 00:00:05,993
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:06,005 --> 00:00:06,006
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:06,446 --> 00:00:06,447
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:06,745 --> 00:00:06,745
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:07,047 --> 00:00:07,048
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:07,434 --> 00:00:07,435
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:08,771 --> 00:00:08,772
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:09,248 --> 00:00:09,249
49. delay(4)

14
00:00:13,261 --> 00:00:13,262
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:13,277 --> 00:00:13,278
57. parentElem = findWebElement(parent, 30)

16
00:00:13,486 --> 00:00:13,488
61. basePoint = parentElem.getLocation()

17
00:00:13,509 --> 00:00:13,510
65. switchToFrame(parent, 5)

18
00:00:13,542 --> 00:00:13,542
69. delay(4)

19
00:00:17,558 --> 00:00:17,559
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:18,553 --> 00:00:18,554
77. delay(2)

21
00:00:20,572 --> 00:00:20,573
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

22
00:00:20,652 --> 00:00:20,653
85. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

23
00:00:20,714 --> 00:00:20,715
89. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

24
00:00:21,424 --> 00:00:21,425
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

25
00:00:21,494 --> 00:00:21,495
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

26
00:00:22,470 --> 00:00:22,470
101. delay(1)

27
00:00:23,489 --> 00:00:23,490
105. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

28
00:00:23,578 --> 00:00:23,579
109. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

29
00:00:24,893 --> 00:00:24,894
113. delay(2)

30
00:00:26,910 --> 00:00:26,910
117. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

