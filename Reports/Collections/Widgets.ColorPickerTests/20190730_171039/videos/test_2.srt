1
00:00:00,099 --> 00:00:00,100
1. openBrowser("")

2
00:00:03,041 --> 00:00:03,042
5. setViewPortSize(1930, 1180)

3
00:00:03,113 --> 00:00:03,115
9. navigateToUrl("http://localhost:3000/")

4
00:00:04,534 --> 00:00:04,535
13. driver = getExecutedBrowser().getName()

5
00:00:04,553 --> 00:00:04,554
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:04,613 --> 00:00:04,614
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:04,634 --> 00:00:04,636
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:05,113 --> 00:00:05,114
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:05,404 --> 00:00:05,405
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:05,668 --> 00:00:05,668
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:05,928 --> 00:00:05,928
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:06,965 --> 00:00:06,966
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:07,537 --> 00:00:07,539
49. delay(4)

14
00:00:11,570 --> 00:00:11,572
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:11,592 --> 00:00:11,594
57. parentElem = findWebElement(parent, 30)

16
00:00:12,066 --> 00:00:12,067
61. basePoint = parentElem.getLocation()

17
00:00:12,227 --> 00:00:12,228
65. switchToFrame(parent, 5)

18
00:00:12,488 --> 00:00:12,489
69. delay(4)

19
00:00:16,505 --> 00:00:16,506
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:17,052 --> 00:00:17,053
77. delay(2)

21
00:00:19,064 --> 00:00:19,064
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

22
00:00:19,105 --> 00:00:19,105
85. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

23
00:00:19,141 --> 00:00:19,141
89. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

24
00:00:19,590 --> 00:00:19,591
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

25
00:00:19,631 --> 00:00:19,631
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

26
00:00:20,183 --> 00:00:20,183
101. delay(1)

27
00:00:21,192 --> 00:00:21,192
105. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

28
00:00:21,228 --> 00:00:21,228
109. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

29
00:00:21,904 --> 00:00:21,904
113. delay(2)

30
00:00:23,916 --> 00:00:23,916
117. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

