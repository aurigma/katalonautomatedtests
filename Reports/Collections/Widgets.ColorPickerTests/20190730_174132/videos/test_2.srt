1
00:00:00,087 --> 00:00:00,089
1. openBrowser("")

2
00:00:02,921 --> 00:00:02,921
5. setViewPortSize(1930, 1180)

3
00:00:02,986 --> 00:00:02,986
9. navigateToUrl("http://localhost:3000/")

4
00:00:04,098 --> 00:00:04,099
13. driver = getExecutedBrowser().getName()

5
00:00:04,120 --> 00:00:04,122
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:04,136 --> 00:00:04,137
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:04,143 --> 00:00:04,144
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:04,521 --> 00:00:04,522
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:04,797 --> 00:00:04,797
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:05,060 --> 00:00:05,061
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:05,311 --> 00:00:05,312
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:06,149 --> 00:00:06,149
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:06,485 --> 00:00:06,485
49. delay(4)

14
00:00:10,490 --> 00:00:10,491
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:10,498 --> 00:00:10,499
57. parentElem = findWebElement(parent, 30)

16
00:00:10,514 --> 00:00:10,515
61. basePoint = parentElem.getLocation()

17
00:00:10,525 --> 00:00:10,526
65. switchToFrame(parent, 5)

18
00:00:10,547 --> 00:00:10,548
69. delay(4)

19
00:00:14,556 --> 00:00:14,557
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:15,050 --> 00:00:15,050
77. delay(2)

21
00:00:17,060 --> 00:00:17,060
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

22
00:00:17,091 --> 00:00:17,091
85. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

23
00:00:17,125 --> 00:00:17,125
89. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

24
00:00:17,445 --> 00:00:17,445
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

25
00:00:17,496 --> 00:00:17,496
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

26
00:00:18,089 --> 00:00:18,090
101. delay(1)

27
00:00:19,098 --> 00:00:19,099
105. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

28
00:00:19,132 --> 00:00:19,132
109. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

29
00:00:19,622 --> 00:00:19,623
113. delay(2)

30
00:00:21,635 --> 00:00:21,635
117. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

