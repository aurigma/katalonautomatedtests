1
00:00:00,143 --> 00:00:00,143
1. openBrowser("")

2
00:00:02,982 --> 00:00:02,982
5. setViewPortSize(1930, 1180)

3
00:00:03,034 --> 00:00:03,036
9. navigateToUrl("http://localhost:3000/")

4
00:00:04,296 --> 00:00:04,299
13. driver = getExecutedBrowser().getName()

5
00:00:04,306 --> 00:00:04,307
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:04,338 --> 00:00:04,342
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:04,351 --> 00:00:04,352
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:04,822 --> 00:00:04,822
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:05,094 --> 00:00:05,094
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:05,353 --> 00:00:05,353
37. click(findTestObject("ConfigurationsTree/div_Widgetsjson"))

11
00:00:05,604 --> 00:00:05,604
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:06,665 --> 00:00:06,666
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:07,023 --> 00:00:07,024
49. delay(5)

14
00:00:12,041 --> 00:00:12,043
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/Iframe")

15
00:00:12,060 --> 00:00:12,061
57. parentElem = findWebElement(parent, 30)

16
00:00:12,179 --> 00:00:12,181
61. basePoint = parentElem.getLocation()

17
00:00:12,368 --> 00:00:12,368
65. switchToFrame(parent, 5)

18
00:00:12,562 --> 00:00:12,563
69. delay(4)

19
00:00:16,571 --> 00:00:16,572
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/button_More"))

20
00:00:17,086 --> 00:00:17,086
77. delay(2)

21
00:00:19,101 --> 00:00:19,101
81. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextShadowButton"))

22
00:00:19,625 --> 00:00:19,625
85. delay(2)

23
00:00:21,635 --> 00:00:21,636
89. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextShadowDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Widgets/ShadowDialogInitial.png", failed + "/Widgets/ColorPicker/Widgets/ShadowDialogInitial.png")

24
00:00:21,870 --> 00:00:21,870
93. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/OpenShadowPalette"))

25
00:00:22,231 --> 00:00:22,231
97. delay(2)

26
00:00:24,238 --> 00:00:24,238
101. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextShadowPalette"), basePoint, baseDir + "/Widgets/ColorPicker/PickerRGB/ShadowPaletteInitial.png", failed + "/Widgets/ColorPicker/PickerRGB/ShadowPaletteInitial.png")

27
00:00:24,463 --> 00:00:24,463
105. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/ShadowOK"))

28
00:00:24,917 --> 00:00:24,917
109. delay(2)

29
00:00:26,921 --> 00:00:26,922
113. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextShadowDialog"), basePoint, baseDir + "Widgets/ColorPicker/Widgets/BackToShadowDialog.png", failed + "Widgets/ColorPicker/Widgets/BackToShadowDialog.png")

30
00:00:27,141 --> 00:00:27,142
117. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/OpenShadowPalette"))

31
00:00:27,661 --> 00:00:27,662
121. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/ShadowColors_Blue"))

32
00:00:28,144 --> 00:00:28,144
125. delay(2)

33
00:00:30,155 --> 00:00:30,155
129. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextShadowPalette"), basePoint, baseDir + "/Widgets/ColorPicker/Widgets/ShadowRecentAdded.png", failed + "/Widgets/ColorPicker/Widgets/ShadowRecentAdded.png")

34
00:00:30,386 --> 00:00:30,387
133. switchToDefaultContent()

35
00:00:30,424 --> 00:00:30,424
137. click(findTestObject("TestStandPanel/FinishButton"))

36
00:00:30,906 --> 00:00:30,908
141. delay(2)

37
00:00:32,922 --> 00:00:32,923
145. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), basePoint, baseDir + "/Widgets/ColorPicker/Widgets/ShadowApplied.png", failed + "/Widgets/ColorPicker/Widgets/ShadowApplied.png")

