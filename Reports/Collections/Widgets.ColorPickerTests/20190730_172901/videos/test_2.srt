1
00:00:00,203 --> 00:00:00,204
1. openBrowser("")

2
00:00:01,336 --> 00:00:01,337
5. setViewPortSize(1930, 1180)

3
00:00:01,471 --> 00:00:01,472
9. navigateToUrl("http://localhost:3000/")

4
00:00:02,937 --> 00:00:02,937
13. driver = getExecutedBrowser().getName()

5
00:00:02,948 --> 00:00:02,949
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:02,969 --> 00:00:02,970
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:02,977 --> 00:00:02,978
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,088 --> 00:00:03,088
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,239 --> 00:00:03,240
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:03,388 --> 00:00:03,389
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:03,522 --> 00:00:03,523
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:03,592 --> 00:00:03,593
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:03,775 --> 00:00:03,776
49. delay(4)

14
00:00:07,786 --> 00:00:07,787
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:07,810 --> 00:00:07,811
57. parentElem = findWebElement(parent, 30)

16
00:00:07,876 --> 00:00:07,877
61. basePoint = parentElem.getLocation()

17
00:00:07,910 --> 00:00:07,910
65. switchToFrame(parent, 5)

18
00:00:08,186 --> 00:00:08,187
69. delay(4)

19
00:00:12,195 --> 00:00:12,195
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:12,588 --> 00:00:12,590
77. delay(2)

21
00:00:14,615 --> 00:00:14,615
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

22
00:00:14,656 --> 00:00:14,656
85. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

23
00:00:14,686 --> 00:00:14,686
89. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

24
00:00:14,900 --> 00:00:14,901
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

25
00:00:14,964 --> 00:00:14,965
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

26
00:00:15,231 --> 00:00:15,231
101. delay(1)

27
00:00:16,246 --> 00:00:16,246
105. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

28
00:00:16,281 --> 00:00:16,281
109. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

29
00:00:16,567 --> 00:00:16,568
113. delay(2)

30
00:00:18,582 --> 00:00:18,582
117. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

