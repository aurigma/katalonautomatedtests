1
00:00:00,109 --> 00:00:00,109
1. openBrowser("")

2
00:00:02,913 --> 00:00:02,914
5. setViewPortSize(1930, 1180)

3
00:00:02,988 --> 00:00:02,989
9. navigateToUrl("http://localhost:3000/")

4
00:00:04,127 --> 00:00:04,127
13. driver = getExecutedBrowser().getName()

5
00:00:04,135 --> 00:00:04,136
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:04,146 --> 00:00:04,147
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:04,161 --> 00:00:04,162
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:04,589 --> 00:00:04,589
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:04,873 --> 00:00:04,873
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:05,133 --> 00:00:05,134
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:05,385 --> 00:00:05,385
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:06,250 --> 00:00:06,250
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:06,529 --> 00:00:06,529
49. delay(4)

14
00:00:10,543 --> 00:00:10,544
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:10,553 --> 00:00:10,555
57. parentElem = findWebElement(parent, 30)

16
00:00:10,577 --> 00:00:10,577
61. basePoint = parentElem.getLocation()

17
00:00:10,592 --> 00:00:10,593
65. switchToFrame(parent, 5)

18
00:00:10,646 --> 00:00:10,647
69. delay(4)

19
00:00:14,657 --> 00:00:14,657
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:15,140 --> 00:00:15,141
77. delay(2)

21
00:00:17,148 --> 00:00:17,148
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

22
00:00:17,180 --> 00:00:17,181
85. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

23
00:00:17,217 --> 00:00:17,217
89. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

24
00:00:17,653 --> 00:00:17,654
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

25
00:00:17,705 --> 00:00:17,706
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

26
00:00:18,336 --> 00:00:18,336
101. delay(1)

27
00:00:19,349 --> 00:00:19,349
105. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

28
00:00:19,381 --> 00:00:19,381
109. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

29
00:00:19,977 --> 00:00:19,978
113. delay(2)

30
00:00:21,989 --> 00:00:21,989
117. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

