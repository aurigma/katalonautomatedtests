1
00:00:00,136 --> 00:00:00,137
1. openBrowser("")

2
00:00:01,351 --> 00:00:01,351
5. setViewPortSize(1930, 1180)

3
00:00:01,492 --> 00:00:01,493
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,459 --> 00:00:03,460
13. driver = getExecutedBrowser().getName()

5
00:00:03,471 --> 00:00:03,472
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,478 --> 00:00:03,479
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:03,485 --> 00:00:03,486
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,575 --> 00:00:03,576
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,685 --> 00:00:03,686
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:03,801 --> 00:00:03,802
37. click(findTestObject("ConfigurationsTree/div_SectionCollapsejson"))

11
00:00:03,930 --> 00:00:03,930
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:04,004 --> 00:00:04,004
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:04,206 --> 00:00:04,206
49. delay(4)

14
00:00:08,220 --> 00:00:08,221
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:08,234 --> 00:00:08,235
57. parentElem = findWebElement(parent, 30)

16
00:00:08,303 --> 00:00:08,303
61. basePoint = parentElem.getLocation()

17
00:00:08,344 --> 00:00:08,345
65. switchToFrame(parent, 5)

18
00:00:08,433 --> 00:00:08,434
69. delay(4)

19
00:00:12,440 --> 00:00:12,441
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

20
00:00:13,012 --> 00:00:13,013
77. delay(2)

21
00:00:15,026 --> 00:00:15,026
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), 3)

22
00:00:15,051 --> 00:00:15,052
85. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/PickerWithCollapseSections.png", failed + "/Widgets/ColorPicker/SectionCollapse/PickerWithCollapseSections.png")

23
00:00:15,621 --> 00:00:15,621
89. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/GrayscaleSectionCollapse"))

24
00:00:15,789 --> 00:00:15,789
93. delay(2)

25
00:00:17,796 --> 00:00:17,796
97. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/GrayscaleCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/GrayscaleCollapsed.png")

26
00:00:18,372 --> 00:00:18,373
101. delay(2)

27
00:00:20,379 --> 00:00:20,379
105. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RgbSectionCollapse"))

28
00:00:20,575 --> 00:00:20,575
109. delay(2)

29
00:00:22,584 --> 00:00:22,585
113. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/RGBCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/RGBCollapsed.png")

30
00:00:23,147 --> 00:00:23,148
117. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/CmykSectionCollapse"))

31
00:00:23,304 --> 00:00:23,305
121. delay(2)

32
00:00:25,313 --> 00:00:25,314
125. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CMYKCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/CMYKCollapsed.png")

33
00:00:25,879 --> 00:00:25,879
129. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RecentSectionCollapse"))

34
00:00:26,031 --> 00:00:26,031
133. delay(2)

35
00:00:28,041 --> 00:00:28,041
137. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/AllCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/AllCollapsed.png")

36
00:00:28,600 --> 00:00:28,600
141. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

37
00:00:28,775 --> 00:00:28,776
145. delay(1)

38
00:00:29,786 --> 00:00:29,786
149. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

39
00:00:29,964 --> 00:00:29,964
153. delay(2)

40
00:00:31,978 --> 00:00:31,979
157. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved.png")

41
00:00:32,524 --> 00:00:32,525
161. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/button_OK"))

42
00:00:32,722 --> 00:00:32,723
165. delay(1)

43
00:00:33,740 --> 00:00:33,740
169. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

44
00:00:33,955 --> 00:00:33,956
173. delay(2)

45
00:00:35,964 --> 00:00:35,964
177. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved2.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved2.png")

46
00:00:36,532 --> 00:00:36,532
181. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RecentSectionCollapse"))

47
00:00:36,679 --> 00:00:36,679
185. delay(1)

48
00:00:37,689 --> 00:00:37,689
189. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RgbSectionCollapse"))

49
00:00:37,831 --> 00:00:37,831
193. delay(1)

50
00:00:38,840 --> 00:00:38,840
197. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/OIPickAnotherItem"))

51
00:00:39,227 --> 00:00:39,228
201. delay(1)

52
00:00:40,236 --> 00:00:40,236
205. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

53
00:00:40,564 --> 00:00:40,564
209. delay(2)

54
00:00:42,574 --> 00:00:42,574
213. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved3.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved3.png")

55
00:00:43,141 --> 00:00:43,142
217. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/AddText"))

56
00:00:43,732 --> 00:00:43,732
221. delay(3)

57
00:00:46,741 --> 00:00:46,742
225. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

58
00:00:47,195 --> 00:00:47,195
229. delay(2)

59
00:00:49,207 --> 00:00:49,208
233. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved4.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved4.png")

60
00:00:49,889 --> 00:00:49,890
237. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/button_OK"))

61
00:00:50,298 --> 00:00:50,298
241. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/TextArea"), "Test")

62
00:00:50,732 --> 00:00:50,732
245. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

63
00:00:51,090 --> 00:00:51,090
249. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved5.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved5.png")

64
00:00:51,839 --> 00:00:51,840
253. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/UndoButton"))

65
00:00:52,492 --> 00:00:52,492
257. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/OIPickAnotherItem"))

66
00:00:53,093 --> 00:00:53,093
261. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

67
00:00:54,030 --> 00:00:54,031
265. delay(2)

68
00:00:56,039 --> 00:00:56,039
269. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionReverted.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionReverted.png")

