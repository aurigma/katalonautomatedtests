1
00:00:00,116 --> 00:00:00,117
1. openBrowser("")

2
00:00:01,428 --> 00:00:01,429
5. setViewPortSize(1930, 1180)

3
00:00:01,566 --> 00:00:01,567
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,118 --> 00:00:03,118
13. driver = getExecutedBrowser().getName()

5
00:00:03,132 --> 00:00:03,132
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,146 --> 00:00:03,147
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:03,161 --> 00:00:03,162
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,338 --> 00:00:03,338
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,473 --> 00:00:03,474
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:03,619 --> 00:00:03,620
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:03,747 --> 00:00:03,748
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:03,834 --> 00:00:03,836
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:04,067 --> 00:00:04,068
49. delay(4)

14
00:00:08,081 --> 00:00:08,082
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:08,091 --> 00:00:08,092
57. parentElem = findWebElement(parent, 30)

16
00:00:08,722 --> 00:00:08,722
61. basePoint = parentElem.getLocation()

17
00:00:08,806 --> 00:00:08,807
65. switchToFrame(parent, 5)

18
00:00:09,038 --> 00:00:09,039
69. delay(4)

19
00:00:13,046 --> 00:00:13,047
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:13,515 --> 00:00:13,515
77. delay(2)

21
00:00:15,525 --> 00:00:15,526
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

22
00:00:15,562 --> 00:00:15,563
85. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

23
00:00:15,610 --> 00:00:15,611
89. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

24
00:00:16,000 --> 00:00:16,001
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

25
00:00:16,104 --> 00:00:16,106
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

26
00:00:16,601 --> 00:00:16,601
101. delay(1)

27
00:00:17,612 --> 00:00:17,612
105. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

28
00:00:17,649 --> 00:00:17,649
109. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

29
00:00:17,960 --> 00:00:17,960
113. delay(2)

30
00:00:19,968 --> 00:00:19,968
117. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

