1
00:00:00,233 --> 00:00:00,234
1. openBrowser("")

2
00:00:01,544 --> 00:00:01,544
5. setViewPortSize(1930, 1180)

3
00:00:01,660 --> 00:00:01,661
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,096 --> 00:00:03,098
13. driver = getExecutedBrowser().getName()

5
00:00:03,120 --> 00:00:03,122
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,139 --> 00:00:03,141
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:03,159 --> 00:00:03,160
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,447 --> 00:00:03,448
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,574 --> 00:00:03,575
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:03,773 --> 00:00:03,774
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:03,987 --> 00:00:03,988
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:04,114 --> 00:00:04,115
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:04,623 --> 00:00:04,624
49. delay(4)

14
00:00:08,640 --> 00:00:08,641
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:08,654 --> 00:00:08,656
57. parentElem = findWebElement(parent, 30)

16
00:00:11,690 --> 00:00:11,691
61. basePoint = parentElem.getLocation()

17
00:00:11,978 --> 00:00:11,979
65. switchToFrame(parent, 5)

18
00:00:12,083 --> 00:00:12,084
69. delay(4)

19
00:00:16,098 --> 00:00:16,099
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:17,286 --> 00:00:17,286
77. delay(2)

21
00:00:19,301 --> 00:00:19,302
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

22
00:00:19,436 --> 00:00:19,437
85. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

23
00:00:19,515 --> 00:00:19,516
89. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

24
00:00:20,074 --> 00:00:20,075
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

25
00:00:20,177 --> 00:00:20,178
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

26
00:00:20,792 --> 00:00:20,792
101. delay(1)

27
00:00:21,813 --> 00:00:21,814
105. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

28
00:00:21,900 --> 00:00:21,901
109. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

29
00:00:22,893 --> 00:00:22,893
113. delay(2)

30
00:00:24,934 --> 00:00:24,935
117. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

