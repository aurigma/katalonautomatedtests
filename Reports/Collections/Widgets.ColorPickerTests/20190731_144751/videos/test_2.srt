1
00:00:00,097 --> 00:00:00,100
1. openBrowser("")

2
00:00:02,923 --> 00:00:02,923
5. setViewPortSize(1930, 1180)

3
00:00:02,977 --> 00:00:02,978
9. navigateToUrl("http://localhost:3000/")

4
00:00:04,113 --> 00:00:04,114
13. driver = getExecutedBrowser().getName()

5
00:00:04,125 --> 00:00:04,126
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:04,130 --> 00:00:04,131
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:04,154 --> 00:00:04,155
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:04,585 --> 00:00:04,585
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:04,855 --> 00:00:04,855
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:05,111 --> 00:00:05,112
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:05,366 --> 00:00:05,366
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:06,262 --> 00:00:06,263
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:06,972 --> 00:00:06,973
49. delay(4)

14
00:00:11,001 --> 00:00:11,001
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:11,007 --> 00:00:11,007
57. parentElem = findWebElement(parent, 30)

16
00:00:11,173 --> 00:00:11,174
61. basePoint = parentElem.getLocation()

17
00:00:11,192 --> 00:00:11,192
65. switchToFrame(parent, 5)

18
00:00:11,218 --> 00:00:11,218
69. delay(4)

19
00:00:15,222 --> 00:00:15,223
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:15,733 --> 00:00:15,733
77. delay(2)

21
00:00:17,749 --> 00:00:17,749
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

22
00:00:17,786 --> 00:00:17,786
85. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

23
00:00:17,823 --> 00:00:17,823
89. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

24
00:00:18,288 --> 00:00:18,289
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

25
00:00:18,331 --> 00:00:18,331
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

26
00:00:18,979 --> 00:00:18,979
101. delay(1)

27
00:00:19,985 --> 00:00:19,986
105. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

28
00:00:20,013 --> 00:00:20,014
109. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

29
00:00:20,664 --> 00:00:20,665
113. delay(2)

30
00:00:22,674 --> 00:00:22,675
117. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

31
00:00:22,924 --> 00:00:22,924
121. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 5)

32
00:00:22,963 --> 00:00:22,963
125. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 85)

33
00:00:23,589 --> 00:00:23,589
129. delay(2)

34
00:00:25,600 --> 00:00:25,600
133. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/HueChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/HueChanged1.png")

35
00:00:25,839 --> 00:00:25,839
137. delay(1)

36
00:00:26,848 --> 00:00:26,848
141. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 5)

37
00:00:26,875 --> 00:00:26,875
145. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 110)

38
00:00:27,393 --> 00:00:27,393
149. delay(2)

39
00:00:29,400 --> 00:00:29,400
153. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/PickersFirstCheck.png", failed + "/Widgets/ColorPicker/PickerCMYK/PickersFirstCheck.png")

40
00:00:29,649 --> 00:00:29,650
157. switchToDefaultContent()

41
00:00:29,668 --> 00:00:29,669
161. click(findTestObject("TestStandPanel/FinishButton"))

42
00:00:29,916 --> 00:00:29,917
165. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

43
00:00:29,983 --> 00:00:29,985
169. delay(2)

44
00:00:32,000 --> 00:00:32,000
173. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck1.png", failed + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck1.png")

45
00:00:32,352 --> 00:00:32,353
177. click(findTestObject("TestStandPanel/BackToEditorButton"))

46
00:00:32,609 --> 00:00:32,609
181. switchToFrame(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe"), 5)

47
00:00:32,658 --> 00:00:32,658
185. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

48
00:00:32,715 --> 00:00:32,715
189. delay(1)

49
00:00:33,733 --> 00:00:33,733
193. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

50
00:00:33,784 --> 00:00:33,784
197. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5, 2)

51
00:00:34,323 --> 00:00:34,324
201. delay(2)

52
00:00:36,336 --> 00:00:36,337
205. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged2.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged2.png")

53
00:00:36,642 --> 00:00:36,643
209. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 5)

54
00:00:36,686 --> 00:00:36,687
213. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 20)

55
00:00:37,283 --> 00:00:37,283
217. delay(2)

56
00:00:39,294 --> 00:00:39,294
221. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/HueChanged2.png", failed + "/Widgets/ColorPicker/PickerCMYK/HueChanged2.png")

57
00:00:39,715 --> 00:00:39,716
225. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 5)

58
00:00:39,794 --> 00:00:39,794
229. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 20)

59
00:00:40,332 --> 00:00:40,333
233. delay(2)

60
00:00:42,344 --> 00:00:42,345
237. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/PickersSecondCheck.png", failed + "/Widgets/ColorPicker/PickerCMYK/PickersSecondCheck.png")

