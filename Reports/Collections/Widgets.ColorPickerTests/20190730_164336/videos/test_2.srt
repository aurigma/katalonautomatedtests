1
00:00:00,284 --> 00:00:00,285
1. openBrowser("")

2
00:00:04,562 --> 00:00:04,562
5. setViewPortSize(1930, 1180)

3
00:00:04,675 --> 00:00:04,675
9. navigateToUrl("http://localhost:3000/")

4
00:00:05,957 --> 00:00:05,958
13. driver = getExecutedBrowser().getName()

5
00:00:05,980 --> 00:00:05,980
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:05,997 --> 00:00:05,998
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:06,014 --> 00:00:06,015
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:06,668 --> 00:00:06,669
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:06,994 --> 00:00:06,995
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:07,303 --> 00:00:07,304
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:07,606 --> 00:00:07,607
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:08,983 --> 00:00:08,984
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:09,486 --> 00:00:09,487
49. delay(4)

14
00:00:13,502 --> 00:00:13,503
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:13,529 --> 00:00:13,530
57. parentElem = findWebElement(parent, 30)

16
00:00:13,706 --> 00:00:13,706
61. basePoint = parentElem.getLocation()

17
00:00:13,739 --> 00:00:13,740
65. switchToFrame(parent, 5)

18
00:00:13,806 --> 00:00:13,807
69. delay(4)

19
00:00:17,823 --> 00:00:17,825
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:19,194 --> 00:00:19,194
77. delay(2)

21
00:00:21,216 --> 00:00:21,217
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

22
00:00:21,300 --> 00:00:21,300
85. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

23
00:00:21,385 --> 00:00:21,386
89. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

24
00:00:22,175 --> 00:00:22,176
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

25
00:00:22,263 --> 00:00:22,264
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

26
00:00:23,334 --> 00:00:23,335
101. delay(1)

27
00:00:24,375 --> 00:00:24,377
105. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

28
00:00:24,467 --> 00:00:24,467
109. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

29
00:00:25,494 --> 00:00:25,495
113. delay(2)

30
00:00:27,521 --> 00:00:27,524
117. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

