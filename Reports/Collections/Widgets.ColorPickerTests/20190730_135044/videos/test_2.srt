1
00:00:00,223 --> 00:00:00,224
1. openBrowser("")

2
00:00:01,508 --> 00:00:01,508
5. setViewPortSize(1930, 1180)

3
00:00:01,632 --> 00:00:01,632
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,290 --> 00:00:03,291
13. driver = getExecutedBrowser().getName()

5
00:00:03,314 --> 00:00:03,315
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,327 --> 00:00:03,327
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:03,333 --> 00:00:03,334
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,443 --> 00:00:03,444
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,567 --> 00:00:03,568
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:03,746 --> 00:00:03,749
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:03,879 --> 00:00:03,879
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:03,965 --> 00:00:03,967
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:04,162 --> 00:00:04,164
49. delay(4)

14
00:00:08,186 --> 00:00:08,187
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:08,202 --> 00:00:08,203
57. parentElem = findWebElement(parent, 30)

16
00:00:08,702 --> 00:00:08,702
61. basePoint = parentElem.getLocation()

17
00:00:08,779 --> 00:00:08,779
65. switchToFrame(parent, 5)

18
00:00:08,934 --> 00:00:08,935
69. delay(4)

19
00:00:12,949 --> 00:00:12,950
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:13,436 --> 00:00:13,436
77. delay(2)

21
00:00:15,449 --> 00:00:15,452
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

22
00:00:15,503 --> 00:00:15,504
85. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

23
00:00:15,542 --> 00:00:15,542
89. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

24
00:00:15,792 --> 00:00:15,792
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

25
00:00:15,842 --> 00:00:15,843
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

26
00:00:16,356 --> 00:00:16,357
101. delay(1)

27
00:00:17,362 --> 00:00:17,363
105. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

28
00:00:17,408 --> 00:00:17,409
109. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

29
00:00:17,825 --> 00:00:17,828
113. delay(2)

30
00:00:19,847 --> 00:00:19,847
117. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

31
00:00:20,504 --> 00:00:20,505
121. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 5)

32
00:00:20,544 --> 00:00:20,544
125. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 85)

33
00:00:20,846 --> 00:00:20,846
129. delay(3)

34
00:00:24,204 --> 00:00:24,206
133. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/HueChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/HueChanged1.png")

35
00:00:24,831 --> 00:00:24,832
137. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 5)

36
00:00:24,891 --> 00:00:24,892
141. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 110)

37
00:00:25,372 --> 00:00:25,373
145. delay(2)

38
00:00:27,386 --> 00:00:27,387
149. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/PickersFirstCheck.png", failed + "/Widgets/ColorPicker/PickerCMYK/PickersFirstCheck.png")

