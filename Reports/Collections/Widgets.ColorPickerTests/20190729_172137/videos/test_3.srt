1
00:00:00,407 --> 00:00:00,408
1. openBrowser("")

2
00:00:02,525 --> 00:00:02,525
5. setViewPortSize(1930, 1180)

3
00:00:02,647 --> 00:00:02,648
9. navigateToUrl("http://localhost:3000/")

4
00:00:04,236 --> 00:00:04,236
13. driver = getExecutedBrowser().getName()

5
00:00:04,249 --> 00:00:04,250
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:04,265 --> 00:00:04,266
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:04,278 --> 00:00:04,278
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:04,429 --> 00:00:04,430
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:04,602 --> 00:00:04,602
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:04,827 --> 00:00:04,829
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:05,070 --> 00:00:05,071
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:05,193 --> 00:00:05,193
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:05,637 --> 00:00:05,637
49. delay(5)

14
00:00:10,655 --> 00:00:10,656
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:10,808 --> 00:00:10,809
57. parentElem = findWebElement(parent, 30)

16
00:00:11,494 --> 00:00:11,495
61. basePoint = parentElem.getLocation()

17
00:00:11,631 --> 00:00:11,632
65. switchToFrame(parent, 5)

18
00:00:12,212 --> 00:00:12,213
69. delay(4)

19
00:00:16,230 --> 00:00:16,231
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:17,196 --> 00:00:17,197
77. delay(4)

21
00:00:21,207 --> 00:00:21,208
81. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 10)

22
00:00:21,910 --> 00:00:21,911
85. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"))

23
00:00:22,037 --> 00:00:22,037
89. delay(2)

24
00:00:24,053 --> 00:00:24,054
93. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerRGB/SaturationChanged1.png")

25
00:00:25,051 --> 00:00:25,052
97. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 130)

26
00:00:26,016 --> 00:00:26,017
101. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"))

27
00:00:26,361 --> 00:00:26,361
105. delay(2)

28
00:00:28,368 --> 00:00:28,368
109. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/HueChanged1.png", failed + "/Widgets/ColorPicker/PickerRGB/HueChanged1.png")

29
00:00:29,360 --> 00:00:29,361
113. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 90)

30
00:00:30,185 --> 00:00:30,186
117. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"))

31
00:00:30,452 --> 00:00:30,452
121. delay(2)

32
00:00:32,463 --> 00:00:32,465
125. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/PickersFirstCheck.png", failed + "/Widgets/ColorPicker/PickerRGB/PickersFirstCheck.png")

