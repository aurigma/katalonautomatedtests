1
00:00:00,155 --> 00:00:00,155
1. openBrowser("")

2
00:00:01,449 --> 00:00:01,449
5. setViewPortSize(1930, 1180)

3
00:00:01,572 --> 00:00:01,572
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,217 --> 00:00:03,218
13. driver = getExecutedBrowser().getName()

5
00:00:03,268 --> 00:00:03,269
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,298 --> 00:00:03,299
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:03,318 --> 00:00:03,320
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,441 --> 00:00:03,442
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,573 --> 00:00:03,573
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:03,727 --> 00:00:03,729
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:03,891 --> 00:00:03,892
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:03,994 --> 00:00:03,996
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:04,259 --> 00:00:04,260
49. delay(4)

14
00:00:08,273 --> 00:00:08,274
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:08,299 --> 00:00:08,300
57. parentElem = findWebElement(parent, 30)

16
00:00:10,247 --> 00:00:10,248
61. basePoint = parentElem.getLocation()

17
00:00:10,440 --> 00:00:10,442
65. switchToFrame(parent, 5)

18
00:00:10,529 --> 00:00:10,529
69. delay(4)

19
00:00:14,546 --> 00:00:14,547
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:15,536 --> 00:00:15,537
77. delay(2)

21
00:00:17,552 --> 00:00:17,553
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

22
00:00:17,628 --> 00:00:17,629
85. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

23
00:00:17,689 --> 00:00:17,690
89. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

24
00:00:18,123 --> 00:00:18,124
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

25
00:00:18,243 --> 00:00:18,244
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

26
00:00:18,827 --> 00:00:18,828
101. delay(1)

27
00:00:19,856 --> 00:00:19,857
105. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

28
00:00:19,934 --> 00:00:19,935
109. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

29
00:00:20,508 --> 00:00:20,509
113. delay(2)

30
00:00:22,531 --> 00:00:22,533
117. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

