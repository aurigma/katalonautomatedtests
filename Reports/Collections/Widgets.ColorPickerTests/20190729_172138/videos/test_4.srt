1
00:00:00,180 --> 00:00:00,180
1. openBrowser("")

2
00:00:03,929 --> 00:00:03,932
5. setViewPortSize(1930, 1180)

3
00:00:04,057 --> 00:00:04,057
9. navigateToUrl("http://localhost:3000/")

4
00:00:05,281 --> 00:00:05,281
13. driver = getExecutedBrowser().getName()

5
00:00:05,295 --> 00:00:05,296
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:05,310 --> 00:00:05,311
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:05,322 --> 00:00:05,322
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:05,708 --> 00:00:05,709
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:06,015 --> 00:00:06,015
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:06,335 --> 00:00:06,335
37. click(findTestObject("ConfigurationsTree/div_Widgetsjson"))

11
00:00:06,616 --> 00:00:06,617
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:07,566 --> 00:00:07,567
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:07,959 --> 00:00:07,959
49. delay(5)

14
00:00:12,974 --> 00:00:12,975
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/Iframe")

15
00:00:12,989 --> 00:00:12,990
57. parentElem = findWebElement(parent, 30)

16
00:00:13,130 --> 00:00:13,131
61. basePoint = parentElem.getLocation()

17
00:00:13,161 --> 00:00:13,162
65. switchToFrame(parent, 5)

18
00:00:13,219 --> 00:00:13,220
69. delay(4)

19
00:00:17,235 --> 00:00:17,236
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/AddRichText"))

20
00:00:18,411 --> 00:00:18,411
77. delay(2)

21
00:00:20,432 --> 00:00:20,433
81. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETextColorButton"))

22
00:00:21,723 --> 00:00:21,724
85. delay(2)

23
00:00:23,736 --> 00:00:23,737
89. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETopBar"))

24
00:00:23,877 --> 00:00:23,877
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), 5)

25
00:00:23,926 --> 00:00:23,926
97. delay(1)

26
00:00:24,932 --> 00:00:24,933
101. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), basePoint, baseDir + "/Widgets/ColorPicker/Widgets/RTEPickerOpened.png", failed + "/Widgets/ColorPicker/Widgets/RTEPickerOpened.png")

