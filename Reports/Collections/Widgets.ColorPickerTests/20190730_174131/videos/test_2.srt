1
00:00:00,102 --> 00:00:00,103
1. openBrowser("")

2
00:00:01,325 --> 00:00:01,325
5. setViewPortSize(1930, 1180)

3
00:00:01,452 --> 00:00:01,453
9. navigateToUrl("http://localhost:3000/")

4
00:00:02,940 --> 00:00:02,940
13. driver = getExecutedBrowser().getName()

5
00:00:02,949 --> 00:00:02,950
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:02,960 --> 00:00:02,961
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:02,967 --> 00:00:02,967
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,118 --> 00:00:03,119
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,224 --> 00:00:03,224
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:03,374 --> 00:00:03,375
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:03,501 --> 00:00:03,502
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:03,568 --> 00:00:03,568
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:03,802 --> 00:00:03,803
49. delay(4)

14
00:00:07,823 --> 00:00:07,824
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:07,843 --> 00:00:07,844
57. parentElem = findWebElement(parent, 30)

16
00:00:08,169 --> 00:00:08,170
61. basePoint = parentElem.getLocation()

17
00:00:08,255 --> 00:00:08,257
65. switchToFrame(parent, 5)

18
00:00:08,410 --> 00:00:08,410
69. delay(4)

19
00:00:12,418 --> 00:00:12,419
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:12,871 --> 00:00:12,872
77. delay(2)

21
00:00:14,878 --> 00:00:14,878
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

22
00:00:14,919 --> 00:00:14,919
85. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

23
00:00:14,949 --> 00:00:14,950
89. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

24
00:00:15,157 --> 00:00:15,157
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

25
00:00:15,226 --> 00:00:15,227
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

26
00:00:15,476 --> 00:00:15,476
101. delay(1)

27
00:00:16,486 --> 00:00:16,487
105. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

28
00:00:16,520 --> 00:00:16,520
109. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

29
00:00:16,770 --> 00:00:16,771
113. delay(2)

30
00:00:18,783 --> 00:00:18,784
117. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

