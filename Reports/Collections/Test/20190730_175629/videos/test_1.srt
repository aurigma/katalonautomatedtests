1
00:00:00,294 --> 00:00:00,296
1. openBrowser("")

2
00:00:02,619 --> 00:00:02,620
5. setViewPortSize(1930, 1180)

3
00:00:02,861 --> 00:00:02,862
9. navigateToUrl("http://localhost:3000/")

4
00:00:04,202 --> 00:00:04,203
13. driver = getExecutedBrowser().getName()

5
00:00:04,238 --> 00:00:04,240
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:04,256 --> 00:00:04,257
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:04,271 --> 00:00:04,272
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:04,506 --> 00:00:04,508
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:04,657 --> 00:00:04,657
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:04,786 --> 00:00:04,787
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:04,930 --> 00:00:04,931
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:05,009 --> 00:00:05,014
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:05,266 --> 00:00:05,266
49. delay(4)

14
00:00:09,320 --> 00:00:09,321
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:09,336 --> 00:00:09,337
57. parentElem = findWebElement(parent, 30)

16
00:00:09,479 --> 00:00:09,480
61. basePoint = parentElem.getLocation()

17
00:00:09,496 --> 00:00:09,496
65. switchToFrame(parent, 5)

18
00:00:09,926 --> 00:00:09,927
69. delay(4)

19
00:00:13,947 --> 00:00:13,949
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:14,383 --> 00:00:14,384
77. delay(2)

21
00:00:16,400 --> 00:00:16,401
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

22
00:00:16,455 --> 00:00:16,456
85. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

23
00:00:16,499 --> 00:00:16,500
89. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

24
00:00:16,725 --> 00:00:16,727
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

25
00:00:16,849 --> 00:00:16,851
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

26
00:00:17,201 --> 00:00:17,202
101. delay(1)

27
00:00:18,213 --> 00:00:18,213
105. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

28
00:00:18,251 --> 00:00:18,251
109. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

29
00:00:18,562 --> 00:00:18,562
113. delay(2)

30
00:00:20,576 --> 00:00:20,577
117. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

31
00:00:21,284 --> 00:00:21,285
121. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 5)

32
00:00:21,335 --> 00:00:21,336
125. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 85)

33
00:00:21,651 --> 00:00:21,652
129. delay(2)

34
00:00:23,672 --> 00:00:23,672
133. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/HueChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/HueChanged1.png")

