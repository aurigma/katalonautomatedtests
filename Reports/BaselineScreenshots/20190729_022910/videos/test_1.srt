1
00:00:00,349 --> 00:00:00,352
1. openBrowser("")

2
00:00:02,438 --> 00:00:02,438
5. setViewPortSize(1930, 1180)

3
00:00:02,652 --> 00:00:02,654
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,811 --> 00:00:03,812
13. driver = getExecutedBrowser().getName()

5
00:00:03,865 --> 00:00:03,867
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,889 --> 00:00:03,893
21. logInfo(baseDir)

7
00:00:03,915 --> 00:00:03,916
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:04,051 --> 00:00:04,052
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:04,172 --> 00:00:04,172
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:04,311 --> 00:00:04,313
37. click(findTestObject("ConfigurationsTree/div_Palettejson"))

11
00:00:04,455 --> 00:00:04,456
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:04,562 --> 00:00:04,564
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:04,866 --> 00:00:04,867
49. delay(5)

14
00:00:09,906 --> 00:00:09,908
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:09,922 --> 00:00:09,923
57. parentElem = findWebElement(parent, 30)

16
00:00:10,004 --> 00:00:10,005
61. basePoint = parentElem.getLocation()

17
00:00:10,026 --> 00:00:10,028
65. switchToFrame(parent, 5)

18
00:00:10,094 --> 00:00:10,095
69. delay(2)

19
00:00:12,106 --> 00:00:12,107
73. if (driver == "CHROME_DRIVER")

20
00:00:12,115 --> 00:00:12,117
77. delay(1)

21
00:00:13,126 --> 00:00:13,128
81. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton"))

22
00:00:13,482 --> 00:00:13,483
85. delay(2)

23
00:00:15,510 --> 00:00:15,511
89. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), 5)

24
00:00:15,586 --> 00:00:15,587
93. scripts.ScreenshotHandler.takeAndSave(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/TextColorDialog.png")

25
00:00:16,345 --> 00:00:16,346
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Purple"))

26
00:00:16,586 --> 00:00:16,587
101. delay(1)

27
00:00:17,602 --> 00:00:17,602
105. switchToDefaultContent()

28
00:00:17,637 --> 00:00:17,638
109. click(findTestObject("TestStandPanel/FinishButton"))

29
00:00:17,805 --> 00:00:17,806
113. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

30
00:00:18,374 --> 00:00:18,375
117. delay(2)

31
00:00:20,383 --> 00:00:20,384
121. scripts.ScreenshotHandler.takeAndSave(findTestObject("TestStandPanel/PreviewImage"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png")

32
00:00:21,073 --> 00:00:21,074
125. click(findTestObject("TestStandPanel/BackToEditorButton"))

33
00:00:21,191 --> 00:00:21,192
129. switchToFrame(parent, 5)

34
00:00:21,237 --> 00:00:21,238
133. delay(1)

35
00:00:22,257 --> 00:00:22,257
137. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

36
00:00:22,464 --> 00:00:22,465
141. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

37
00:00:22,594 --> 00:00:22,594
145. scripts.ScreenshotHandler.takeAndSave(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png")

38
00:00:23,203 --> 00:00:23,204
149. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Burgundy"))

39
00:00:23,401 --> 00:00:23,401
153. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

40
00:00:23,689 --> 00:00:23,690
157. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RecentColor_Purple"))

41
00:00:23,938 --> 00:00:23,938
161. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RecentColor_Purple"))

42
00:00:24,135 --> 00:00:24,136
165. delay(2)

43
00:00:26,155 --> 00:00:26,156
169. scripts.ScreenshotHandler.takeAndSave(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck2.png")

44
00:00:26,771 --> 00:00:26,773
173. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

45
00:00:26,937 --> 00:00:26,938
177. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

46
00:00:27,064 --> 00:00:27,064
181. delay(2)

47
00:00:29,069 --> 00:00:29,069
185. scripts.ScreenshotHandler.takeAndSave(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck3.png")

48
00:00:29,673 --> 00:00:29,674
189. navigateToUrl("http://localhost:3000/")

49
00:00:29,692 --> 00:00:29,693
193. if (driver == "CHROME_DRIVER")

50
00:00:29,697 --> 00:00:29,697
197. click(findTestObject("ConfigurationsTree/div_base-editor"))

