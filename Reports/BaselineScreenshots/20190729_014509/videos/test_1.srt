1
00:00:00,430 --> 00:00:00,434
1. openBrowser("")

2
00:00:03,115 --> 00:00:03,121
5. setViewPortSize(1930, 1180)

3
00:00:03,276 --> 00:00:03,279
9. navigateToUrl("http://localhost:3000/")

4
00:00:05,951 --> 00:00:05,952
13. driver = getExecutedBrowser().getName()

5
00:00:05,990 --> 00:00:05,992
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:06,007 --> 00:00:06,010
21. logInfo(baseDir)

7
00:00:06,029 --> 00:00:06,030
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:06,181 --> 00:00:06,182
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:06,311 --> 00:00:06,312
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:06,459 --> 00:00:06,461
37. click(findTestObject("ConfigurationsTree/div_Palettejson"))

11
00:00:06,651 --> 00:00:06,652
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:06,758 --> 00:00:06,760
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:06,995 --> 00:00:06,996
49. delay(5)

14
00:00:12,038 --> 00:00:12,039
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:12,097 --> 00:00:12,100
57. parentElem = findWebElement(parent, 30)

16
00:00:12,448 --> 00:00:12,449
61. basePoint = parentElem.getLocation()

17
00:00:12,565 --> 00:00:12,566
65. switchToFrame(parent, 5)

18
00:00:12,662 --> 00:00:12,663
69. delay(2)

19
00:00:14,670 --> 00:00:14,671
73. if (driver == "CHROME_DRIVER")

20
00:00:14,679 --> 00:00:14,679
77. delay(1)

21
00:00:15,686 --> 00:00:15,687
81. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton"))

22
00:00:16,069 --> 00:00:16,071
85. delay(2)

23
00:00:18,089 --> 00:00:18,093
89. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), 5)

24
00:00:18,242 --> 00:00:18,249
93. scripts.ScreenshotHandler.takeAndSave(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/TextColorDialog.png")

25
00:00:19,375 --> 00:00:19,379
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Purple"))

26
00:00:19,930 --> 00:00:19,933
101. delay(1)

27
00:00:20,955 --> 00:00:20,956
105. switchToDefaultContent()

28
00:00:21,000 --> 00:00:21,001
109. click(findTestObject("TestStandPanel/FinishButton"))

29
00:00:21,152 --> 00:00:21,153
113. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

30
00:00:21,727 --> 00:00:21,728
117. delay(2)

31
00:00:23,740 --> 00:00:23,742
121. scripts.ScreenshotHandler.takeAndSave(findTestObject("TestStandPanel/PreviewImage"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png")

32
00:00:24,408 --> 00:00:24,410
125. click(findTestObject("TestStandPanel/BackToEditorButton"))

33
00:00:24,561 --> 00:00:24,565
129. switchToFrame(parent, 5)

34
00:00:24,629 --> 00:00:24,630
133. delay(1)

35
00:00:25,643 --> 00:00:25,644
137. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

36
00:00:25,862 --> 00:00:25,862
141. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

37
00:00:26,002 --> 00:00:26,003
145. scripts.ScreenshotHandler.takeAndSave(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png")

38
00:00:26,617 --> 00:00:26,618
149. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Burgundy"))

39
00:00:26,833 --> 00:00:26,836
153. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

40
00:00:27,215 --> 00:00:27,216
157. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RecentColor_Purple"))

41
00:00:27,608 --> 00:00:27,610
161. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RecentColor_Purple"))

42
00:00:27,911 --> 00:00:27,912
165. delay(2)

43
00:00:29,931 --> 00:00:29,932
169. scripts.ScreenshotHandler.takeAndSave(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck2.png")

44
00:00:30,564 --> 00:00:30,568
173. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

45
00:00:30,745 --> 00:00:30,747
177. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

46
00:00:30,853 --> 00:00:30,854
181. delay(2)

47
00:00:32,865 --> 00:00:32,865
185. scripts.ScreenshotHandler.takeAndSave(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck3.png")

48
00:00:33,468 --> 00:00:33,469
189. navigateToUrl("http://localhost:3000/")

49
00:00:33,493 --> 00:00:33,493
193. if (driver == "CHROME_DRIVER")

50
00:00:33,506 --> 00:00:33,507
197. click(findTestObject("ConfigurationsTree/div_base-editor"))

