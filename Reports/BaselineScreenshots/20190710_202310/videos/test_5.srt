1
00:00:00,126 --> 00:00:00,126
1. openBrowser("")

2
00:00:02,688 --> 00:00:02,688
5. maximizeWindow()

3
00:00:02,963 --> 00:00:02,963
9. navigateToUrl("http://localhost:3000/")

4
00:00:04,348 --> 00:00:04,348
13. driver = getExecutedBrowser().getName()

5
00:00:04,362 --> 00:00:04,367
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:04,372 --> 00:00:04,373
21. click(findTestObject("ConfigurationsTree/div_base-editor"))

7
00:00:04,683 --> 00:00:04,684
25. click(findTestObject("ConfigurationsTree/div_Widgets"))

8
00:00:04,998 --> 00:00:04,998
29. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

9
00:00:05,287 --> 00:00:05,288
33. click(findTestObject("ConfigurationsTree/div_SectionCollapsejson"))

10
00:00:05,539 --> 00:00:05,540
37. click(findTestObject("TestStandPanel/button_RUN"))

11
00:00:06,502 --> 00:00:06,502
41. click(findTestObject("TestStandPanel/EditorTab"))

12
00:00:06,826 --> 00:00:06,826
45. delay(5)

13
00:00:11,835 --> 00:00:11,836
49. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/OpenTextColorPicker"))

14
00:00:12,462 --> 00:00:12,463
53. delay(2)

15
00:00:14,475 --> 00:00:14,476
57. scripts.ScreenshotHandler.takeAndSave(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/SectionCollapse/PickerWithCollapseSections.png")

16
00:00:14,765 --> 00:00:14,766
61. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/GrayscaleColors_LightGray"))

17
00:00:45,376 --> 00:00:45,377
65. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/AddToRecentButton"))

18
00:01:16,015 --> 00:01:16,015
69. delay(2)

19
00:01:18,036 --> 00:01:18,036
73. scripts.ScreenshotHandler.takeAndSave(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/Widgets/RecentChanged1.png")

20
00:01:18,328 --> 00:01:18,329
77. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/GrayscaleSectionCollapse"))

21
00:01:18,894 --> 00:01:18,895
81. delay(2)

22
00:01:20,903 --> 00:01:20,903
85. scripts.ScreenshotHandler.takeAndSave(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/Widgets/GrayscaleCollapsed.png")

23
00:01:21,214 --> 00:01:21,214
89. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RgbColors_DarkBlue"))

24
00:01:51,808 --> 00:01:51,809
93. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/AddToRecentButton"))

25
00:02:22,426 --> 00:02:22,426
97. delay(2)

26
00:02:24,434 --> 00:02:24,435
101. scripts.ScreenshotHandler.takeAndSave(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/Widgets/RecentChanged2.png")

27
00:02:24,710 --> 00:02:24,710
105. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RgbSectionCollapse"))

28
00:02:25,085 --> 00:02:25,086
109. delay(2)

29
00:02:27,095 --> 00:02:27,096
113. scripts.ScreenshotHandler.takeAndSave(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/Widgets/RGBCollapsed.png")

30
00:02:27,386 --> 00:02:27,386
117. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/CmykColors_DustyYellow"))

31
00:02:57,996 --> 00:02:57,996
121. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/AddToRecentButton"))

32
00:03:28,594 --> 00:03:28,594
125. delay(2)

33
00:03:30,603 --> 00:03:30,603
129. scripts.ScreenshotHandler.takeAndSave(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/Widgets/RecentChanged3.png")

34
00:03:30,874 --> 00:03:30,874
133. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/CmykSectionCollapse"))

35
00:03:31,244 --> 00:03:31,245
137. delay(2)

36
00:03:33,253 --> 00:03:33,253
141. scripts.ScreenshotHandler.takeAndSave(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/Widgets/CMYKCollapsed.png")

37
00:03:33,527 --> 00:03:33,528
145. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RecentSectionCollapse"))

38
00:03:33,911 --> 00:03:33,911
149. delay(2)

39
00:03:35,917 --> 00:03:35,917
153. scripts.ScreenshotHandler.takeAndSave(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/Widgets/AllCollapsed.png")

40
00:03:36,193 --> 00:03:36,194
157. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/OpenTextColorPicker"))

41
00:03:36,653 --> 00:03:36,654
161. delay(1)

42
00:03:37,665 --> 00:03:37,665
165. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/OpenTextColorPicker"))

43
00:03:38,137 --> 00:03:38,137
169. delay(2)

44
00:03:40,147 --> 00:03:40,148
173. scripts.ScreenshotHandler.takeAndSave(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/Widgets/CollapsedConditionSaved.png")

45
00:03:40,425 --> 00:03:40,426
177. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/button_OK"))

46
00:03:40,799 --> 00:03:40,800
181. delay(1)

47
00:03:41,810 --> 00:03:41,810
185. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/OpenTextColorPicker"))

48
00:03:42,291 --> 00:03:42,292
189. delay(2)

49
00:03:44,299 --> 00:03:44,299
193. scripts.ScreenshotHandler.takeAndSave(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/Widgets/CollapsedConditionSaved2.png")

50
00:03:44,563 --> 00:03:44,563
197. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RecentSectionCollapse"))

51
00:03:44,922 --> 00:03:44,922
201. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RgbSectionCollapse"))

52
00:03:45,278 --> 00:03:45,279
205. closeBrowser()

