1
00:00:00,371 --> 00:00:00,375
1. openBrowser("")

2
00:00:02,552 --> 00:00:02,553
5. setViewPortSize(1930, 1180)

3
00:00:02,683 --> 00:00:02,684
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,830 --> 00:00:03,831
13. driver = getExecutedBrowser().getName()

5
00:00:03,872 --> 00:00:03,873
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,890 --> 00:00:03,892
21. logInfo(baseDir)

7
00:00:03,916 --> 00:00:03,917
25. logInfo(driver)

8
00:00:03,929 --> 00:00:03,930
29. click(findTestObject("ConfigurationsTree/div_base-editor"))

9
00:00:04,116 --> 00:00:04,116
33. click(findTestObject("ConfigurationsTree/div_Widgets"))

10
00:00:04,235 --> 00:00:04,240
37. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

11
00:00:04,400 --> 00:00:04,401
41. click(findTestObject("ConfigurationsTree/div_Palettejson"))

12
00:00:04,538 --> 00:00:04,539
45. click(findTestObject("TestStandPanel/button_RUN"))

13
00:00:04,664 --> 00:00:04,665
49. click(findTestObject("TestStandPanel/EditorTab"))

14
00:00:04,901 --> 00:00:04,902
53. delay(5)

15
00:00:09,925 --> 00:00:09,925
57. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

16
00:00:09,932 --> 00:00:09,933
61. parentElem = findWebElement(parent, 30)

17
00:00:10,010 --> 00:00:10,011
65. basePoint = parentElem.getLocation()

18
00:00:10,027 --> 00:00:10,028
69. switchToFrame(parent, 5)

19
00:00:10,079 --> 00:00:10,080
73. delay(2)

20
00:00:12,087 --> 00:00:12,088
77. if (driver == "CHROME_DRIVER")

21
00:00:12,100 --> 00:00:12,101
81. delay(1)

22
00:00:13,109 --> 00:00:13,110
85. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton"))

23
00:00:13,431 --> 00:00:13,432
89. delay(2)

24
00:00:15,444 --> 00:00:15,445
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), 5)

25
00:00:15,540 --> 00:00:15,541
97. scripts.ScreenshotHandler.takeAndSave(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/TextColorDialog.png")

26
00:00:16,365 --> 00:00:16,366
101. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Purple"))

27
00:00:16,778 --> 00:00:16,779
105. delay(1)

28
00:00:17,804 --> 00:00:17,804
109. switchToDefaultContent()

29
00:00:17,866 --> 00:00:17,866
113. click(findTestObject("TestStandPanel/FinishButton"))

30
00:00:17,990 --> 00:00:17,990
117. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

31
00:00:18,569 --> 00:00:18,569
121. delay(2)

32
00:00:20,583 --> 00:00:20,584
125. scripts.ScreenshotHandler.takeAndSave(findTestObject("TestStandPanel/PreviewImage"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png")

33
00:00:21,267 --> 00:00:21,270
129. click(findTestObject("TestStandPanel/BackToEditorButton"))

34
00:00:21,369 --> 00:00:21,370
133. switchToFrame(parent, 5)

35
00:00:21,464 --> 00:00:21,465
137. delay(1)

36
00:00:22,473 --> 00:00:22,474
141. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

37
00:00:22,654 --> 00:00:22,655
145. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

38
00:00:22,787 --> 00:00:22,788
149. scripts.ScreenshotHandler.takeAndSave(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png")

39
00:00:23,404 --> 00:00:23,405
153. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Burgundy"))

40
00:00:23,612 --> 00:00:23,612
157. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

41
00:00:23,916 --> 00:00:23,919
161. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RecentColor_Purple"))

42
00:00:24,164 --> 00:00:24,165
165. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RecentColor_Purple"))

43
00:00:24,347 --> 00:00:24,348
169. delay(2)

44
00:00:26,362 --> 00:00:26,362
173. scripts.ScreenshotHandler.takeAndSave(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck2.png")

45
00:00:26,986 --> 00:00:26,986
177. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

46
00:00:27,167 --> 00:00:27,168
181. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

47
00:00:27,288 --> 00:00:27,289
185. delay(2)

48
00:00:29,296 --> 00:00:29,297
189. scripts.ScreenshotHandler.takeAndSave(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck3.png")

49
00:00:29,921 --> 00:00:29,921
193. navigateToUrl("http://localhost:3000/")

50
00:00:29,933 --> 00:00:29,934
197. if (driver == "CHROME_DRIVER")

51
00:00:29,941 --> 00:00:29,942
201. click(findTestObject("ConfigurationsTree/div_base-editor"))

