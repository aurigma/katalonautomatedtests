1
00:00:00,147 --> 00:00:00,148
1. openBrowser("")

2
00:00:03,035 --> 00:00:03,035
5. setViewPortSize(1930, 1180)

3
00:00:03,123 --> 00:00:03,125
9. navigateToUrl("http://localhost:3000/")

4
00:00:04,706 --> 00:00:04,707
13. driver = getExecutedBrowser().getName()

5
00:00:04,724 --> 00:00:04,726
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:04,787 --> 00:00:04,789
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:04,815 --> 00:00:04,822
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:05,520 --> 00:00:05,522
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:05,833 --> 00:00:05,834
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:06,122 --> 00:00:06,123
37. click(findTestObject("ConfigurationsTree/div_SectionCollapsejson"))

11
00:00:06,393 --> 00:00:06,394
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:07,651 --> 00:00:07,652
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:08,159 --> 00:00:08,161
49. delay(5)

14
00:00:13,179 --> 00:00:13,184
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:13,228 --> 00:00:13,231
57. parentElem = findWebElement(parent, 30)

16
00:00:16,864 --> 00:00:16,866
61. basePoint = parentElem.getLocation()

17
00:00:17,255 --> 00:00:17,257
65. switchToFrame(parent, 5)

18
00:00:19,206 --> 00:00:19,206
69. delay(2)

19
00:00:21,221 --> 00:00:21,221
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

20
00:00:22,073 --> 00:00:22,073
77. delay(2)

21
00:00:24,087 --> 00:00:24,088
81. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/PickerWithCollapseSections.png", failed + "/Widgets/ColorPicker/SectionCollapse/PickerWithCollapseSections.png")

22
00:00:24,370 --> 00:00:24,371
85. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/GrayscaleSectionCollapse"))

23
00:00:25,013 --> 00:00:25,014
89. delay(2)

24
00:00:27,021 --> 00:00:27,021
93. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/GrayscaleCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/GrayscaleCollapsed.png")

25
00:00:27,286 --> 00:00:27,287
97. delay(2)

26
00:00:29,296 --> 00:00:29,297
101. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RgbSectionCollapse"))

27
00:00:29,909 --> 00:00:29,911
105. delay(2)

28
00:00:31,927 --> 00:00:31,928
109. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/RGBCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/RGBCollapsed.png")

29
00:00:32,202 --> 00:00:32,203
113. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/CmykSectionCollapse"))

30
00:00:32,679 --> 00:00:32,680
117. delay(2)

31
00:00:34,691 --> 00:00:34,691
121. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CMYKCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/CMYKCollapsed.png")

32
00:00:34,980 --> 00:00:34,981
125. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RecentSectionCollapse"))

33
00:00:35,330 --> 00:00:35,330
129. delay(2)

34
00:00:37,338 --> 00:00:37,339
133. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/AllCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/AllCollapsed.png")

35
00:00:37,614 --> 00:00:37,614
137. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

36
00:00:38,121 --> 00:00:38,124
141. delay(1)

37
00:00:39,143 --> 00:00:39,144
145. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

38
00:00:39,669 --> 00:00:39,671
149. delay(2)

39
00:00:41,690 --> 00:00:41,690
153. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved.png")

40
00:00:41,952 --> 00:00:41,952
157. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/button_OK"))

41
00:00:42,431 --> 00:00:42,432
161. delay(1)

42
00:00:43,449 --> 00:00:43,449
165. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

43
00:00:43,968 --> 00:00:43,968
169. delay(2)

44
00:00:45,996 --> 00:00:45,996
173. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved2.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved2.png")

45
00:00:46,278 --> 00:00:46,278
177. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RecentSectionCollapse"))

46
00:00:46,792 --> 00:00:46,792
181. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RgbSectionCollapse"))

47
00:00:47,157 --> 00:00:47,158
185. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/OIPickAnotherItem"))

48
00:00:47,968 --> 00:00:47,968
189. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

49
00:00:48,540 --> 00:00:48,541
193. delay(2)

50
00:00:50,551 --> 00:00:50,552
197. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved3.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved3.png")

51
00:00:50,858 --> 00:00:50,859
201. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/AddText"))

52
00:00:51,960 --> 00:00:51,961
205. delay(3)

53
00:00:54,989 --> 00:00:54,990
209. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

54
00:00:55,532 --> 00:00:55,532
213. delay(2)

55
00:00:57,540 --> 00:00:57,541
217. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved4.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved4.png")

56
00:00:57,819 --> 00:00:57,819
221. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/button_OK"))

57
00:00:58,332 --> 00:00:58,333
225. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/TextArea"), "Test")

58
00:00:58,704 --> 00:00:58,704
229. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

59
00:00:59,343 --> 00:00:59,346
233. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved5.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved5.png")

60
00:00:59,780 --> 00:00:59,783
237. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/UndoButton"))

61
00:01:00,730 --> 00:01:00,732
241. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/OIPickAnotherItem"))

62
00:01:01,617 --> 00:01:01,619
245. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

63
00:01:02,432 --> 00:01:02,434
249. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionReverted.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionReverted.png")

