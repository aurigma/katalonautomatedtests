1
00:00:00,326 --> 00:00:00,328
1. openBrowser("")

2
00:00:04,951 --> 00:00:04,952
5. setViewPortSize(1930, 1180)

3
00:00:05,011 --> 00:00:05,012
9. navigateToUrl("http://localhost:3000/")

4
00:00:06,741 --> 00:00:06,742
13. driver = getExecutedBrowser().getName()

5
00:00:06,806 --> 00:00:06,810
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:06,848 --> 00:00:06,876
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:06,910 --> 00:00:06,916
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:07,661 --> 00:00:07,661
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:08,065 --> 00:00:08,067
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:08,412 --> 00:00:08,412
37. click(findTestObject("ConfigurationsTree/div_Palettejson"))

11
00:00:08,686 --> 00:00:08,686
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:09,951 --> 00:00:09,953
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:10,446 --> 00:00:10,448
49. delay(5)

14
00:00:15,544 --> 00:00:15,553
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:15,605 --> 00:00:15,610
57. parentElem = findWebElement(parent, 30)

16
00:00:18,883 --> 00:00:18,893
61. basePoint = parentElem.getLocation()

17
00:00:19,326 --> 00:00:19,328
65. switchToFrame(parent, 5)

18
00:00:20,965 --> 00:00:20,966
69. delay(2)

19
00:00:22,988 --> 00:00:22,989
73. if (driver == "CHROME_DRIVER")

20
00:00:22,996 --> 00:00:22,998
77. delay(1)

21
00:00:24,012 --> 00:00:24,013
81. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton"))

22
00:00:24,695 --> 00:00:24,699
85. delay(2)

23
00:00:26,725 --> 00:00:26,725
89. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), 5)

24
00:00:26,800 --> 00:00:26,800
93. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/TextColorDialog.png", failed + "/Widgets/ColorPicker/Palette/TextColorDialog.png")

25
00:00:27,259 --> 00:00:27,260
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Purple"))

26
00:00:28,127 --> 00:00:28,128
101. delay(1)

27
00:00:29,152 --> 00:00:29,152
105. switchToDefaultContent()

28
00:00:29,184 --> 00:00:29,185
109. click(findTestObject("TestStandPanel/FinishButton"))

29
00:00:29,704 --> 00:00:29,707
113. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

30
00:00:30,094 --> 00:00:30,095
117. delay(2)

31
00:00:32,263 --> 00:00:32,263
121. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png", failed + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png")

32
00:00:32,609 --> 00:00:32,610
125. click(findTestObject("TestStandPanel/BackToEditorButton"))

33
00:00:33,061 --> 00:00:33,062
129. switchToFrame(parent, 5)

34
00:00:33,104 --> 00:00:33,105
133. delay(1)

35
00:00:34,112 --> 00:00:34,112
137. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

36
00:00:34,641 --> 00:00:34,643
141. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

37
00:00:34,917 --> 00:00:34,918
145. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png", failed + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png")

