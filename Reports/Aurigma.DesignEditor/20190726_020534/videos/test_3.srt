1
00:00:00,221 --> 00:00:00,222
1. openBrowser("")

2
00:00:03,175 --> 00:00:03,177
5. setViewPortSize(1930, 1180)

3
00:00:03,264 --> 00:00:03,266
9. navigateToUrl("http://localhost:3000/")

4
00:00:04,734 --> 00:00:04,737
13. driver = getExecutedBrowser().getName()

5
00:00:04,771 --> 00:00:04,784
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:04,807 --> 00:00:04,808
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:04,822 --> 00:00:04,822
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:05,548 --> 00:00:05,550
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:05,881 --> 00:00:05,883
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:06,153 --> 00:00:06,154
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:06,411 --> 00:00:06,412
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:06,682 --> 00:00:06,683
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:08,449 --> 00:00:08,450
49. delay(5)

14
00:00:13,469 --> 00:00:13,471
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:13,490 --> 00:00:13,491
57. parentElem = findWebElement(parent, 30)

16
00:00:13,512 --> 00:00:13,513
61. basePoint = parentElem.getLocation()

17
00:00:13,539 --> 00:00:13,540
65. switchToFrame(parent, 5)

18
00:00:13,613 --> 00:00:13,615
69. delay(2)

19
00:00:15,624 --> 00:00:15,624
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:16,397 --> 00:00:16,398
77. delay(1)

21
00:00:17,405 --> 00:00:17,406
81. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 10)

22
00:00:17,969 --> 00:00:17,969
85. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"))

23
00:00:18,147 --> 00:00:18,148
89. delay(2)

24
00:00:20,160 --> 00:00:20,161
93. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerRGB/SaturationChanged1.png")

25
00:00:20,441 --> 00:00:20,442
97. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 130)

26
00:00:21,108 --> 00:00:21,108
101. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"))

27
00:00:21,366 --> 00:00:21,367
105. delay(2)

28
00:00:23,381 --> 00:00:23,382
109. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/HueChanged1.png", failed + "/Widgets/ColorPicker/PickerRGB/HueChanged1.png")

29
00:00:23,696 --> 00:00:23,697
113. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 90)

30
00:00:24,337 --> 00:00:24,338
117. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"))

31
00:00:24,559 --> 00:00:24,559
121. delay(2)

32
00:00:26,571 --> 00:00:26,571
125. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/PickersFirstCheck.png", failed + "/Widgets/ColorPicker/PickerRGB/PickersFirstCheck.png")

33
00:00:26,879 --> 00:00:26,880
129. switchToDefaultContent()

34
00:00:26,903 --> 00:00:26,903
133. click(findTestObject("TestStandPanel/FinishButton"))

35
00:00:27,328 --> 00:00:27,332
137. delay(2)

36
00:00:29,367 --> 00:00:29,368
141. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), baseDir + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck1.png", failed + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck1.png")

37
00:00:29,710 --> 00:00:29,711
145. click(findTestObject("TestStandPanel/BackToEditorButton"))

38
00:00:29,990 --> 00:00:29,990
149. switchToFrame(parent, 5)

39
00:00:30,090 --> 00:00:30,092
153. delay(2)

40
00:00:32,107 --> 00:00:32,108
157. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), -5, -2)

41
00:00:32,656 --> 00:00:32,657
161. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"))

42
00:00:32,873 --> 00:00:32,874
165. delay(2)

43
00:00:34,895 --> 00:00:34,896
169. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/SaturationChanged2.png", failed + "/Widgets/ColorPicker/PickerRGB/SaturationChanged2.png")

44
00:00:35,170 --> 00:00:35,170
173. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, -70)

45
00:00:35,720 --> 00:00:35,720
177. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"))

46
00:00:35,886 --> 00:00:35,888
181. delay(2)

47
00:00:37,896 --> 00:00:37,896
185. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/HueChanged2.png", failed + "/Widgets/ColorPicker/PickerRGB/HueChanged2.png")

48
00:00:38,189 --> 00:00:38,189
189. delay(1)

49
00:00:39,198 --> 00:00:39,199
193. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, -30)

50
00:00:39,749 --> 00:00:39,749
197. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"))

51
00:00:39,996 --> 00:00:39,997
201. delay(2)

52
00:00:42,015 --> 00:00:42,018
205. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/PickersSecondCheck.png", failed + "/Widgets/ColorPicker/PickerRGB/PickersSecondCheck.png")

53
00:00:42,388 --> 00:00:42,388
209. switchToDefaultContent()

54
00:00:42,406 --> 00:00:42,407
213. click(findTestObject("TestStandPanel/FinishButton"))

55
00:00:42,743 --> 00:00:42,744
217. delay(2)

56
00:00:44,772 --> 00:00:44,772
221. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), baseDir + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck2.png", failed + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck2.png")

57
00:00:45,080 --> 00:00:45,080
225. click(findTestObject("TestStandPanel/BackToEditorButton"))

58
00:00:45,333 --> 00:00:45,334
229. switchToFrame(parent, 5)

59
00:00:45,350 --> 00:00:45,351
233. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"))

60
00:00:45,884 --> 00:00:45,886
237. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"))

61
00:00:46,061 --> 00:00:46,061
241. delay(3)

62
00:00:49,069 --> 00:00:49,069
245. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/RecentPaletteAdded.png", failed + "/Widgets/ColorPicker/PickerRGB/RecentPaletteAdded.png")

