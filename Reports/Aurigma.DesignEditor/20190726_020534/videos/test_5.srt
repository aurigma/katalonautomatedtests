1
00:00:00,158 --> 00:00:00,158
1. openBrowser("")

2
00:00:03,616 --> 00:00:03,617
5. setViewPortSize(1930, 1180)

3
00:00:03,679 --> 00:00:03,684
9. navigateToUrl("http://localhost:3000/")

4
00:00:05,098 --> 00:00:05,100
13. driver = getExecutedBrowser().getName()

5
00:00:05,123 --> 00:00:05,125
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:05,146 --> 00:00:05,147
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:05,175 --> 00:00:05,176
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:05,819 --> 00:00:05,820
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:06,123 --> 00:00:06,123
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:06,408 --> 00:00:06,409
37. click(findTestObject("ConfigurationsTree/div_SectionCollapsejson"))

11
00:00:06,673 --> 00:00:06,674
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:07,819 --> 00:00:07,820
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:08,664 --> 00:00:08,668
49. delay(5)

14
00:00:13,698 --> 00:00:13,701
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:13,716 --> 00:00:13,721
57. parentElem = findWebElement(parent, 30)

16
00:00:13,897 --> 00:00:13,899
61. basePoint = parentElem.getLocation()

17
00:00:13,995 --> 00:00:13,996
65. switchToFrame(parent, 5)

18
00:00:14,067 --> 00:00:14,067
69. delay(2)

19
00:00:16,074 --> 00:00:16,075
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

20
00:00:16,874 --> 00:00:16,875
77. delay(2)

21
00:00:18,892 --> 00:00:18,893
81. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/PickerWithCollapseSections.png", failed + "/Widgets/ColorPicker/SectionCollapse/PickerWithCollapseSections.png")

22
00:00:19,186 --> 00:00:19,187
85. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/GrayscaleSectionCollapse"))

23
00:00:19,803 --> 00:00:19,803
89. delay(2)

24
00:00:21,814 --> 00:00:21,815
93. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/GrayscaleCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/GrayscaleCollapsed.png")

25
00:00:22,088 --> 00:00:22,088
97. delay(2)

26
00:00:24,094 --> 00:00:24,094
101. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RgbSectionCollapse"))

27
00:00:24,700 --> 00:00:24,701
105. delay(2)

28
00:00:26,711 --> 00:00:26,712
109. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/RGBCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/RGBCollapsed.png")

29
00:00:27,001 --> 00:00:27,001
113. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/CmykSectionCollapse"))

30
00:00:27,465 --> 00:00:27,466
117. delay(2)

31
00:00:29,480 --> 00:00:29,480
121. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CMYKCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/CMYKCollapsed.png")

32
00:00:29,822 --> 00:00:29,823
125. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RecentSectionCollapse"))

33
00:00:30,433 --> 00:00:30,434
129. delay(2)

34
00:00:32,442 --> 00:00:32,443
133. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/AllCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/AllCollapsed.png")

35
00:00:32,716 --> 00:00:32,716
137. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

36
00:00:33,260 --> 00:00:33,261
141. delay(1)

37
00:00:34,296 --> 00:00:34,298
145. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

38
00:00:34,817 --> 00:00:34,820
149. delay(2)

39
00:00:36,843 --> 00:00:36,843
153. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved.png")

40
00:00:37,100 --> 00:00:37,100
157. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/button_OK"))

41
00:00:37,473 --> 00:00:37,474
161. delay(1)

42
00:00:38,498 --> 00:00:38,498
165. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

43
00:00:39,013 --> 00:00:39,013
169. delay(2)

44
00:00:41,034 --> 00:00:41,034
173. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved2.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved2.png")

45
00:00:41,311 --> 00:00:41,312
177. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RecentSectionCollapse"))

46
00:00:41,805 --> 00:00:41,806
181. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RgbSectionCollapse"))

47
00:00:42,270 --> 00:00:42,271
185. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/OIPickAnotherItem"))

48
00:00:43,027 --> 00:00:43,027
189. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

49
00:00:43,603 --> 00:00:43,604
193. delay(2)

50
00:00:45,616 --> 00:00:45,617
197. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved3.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved3.png")

51
00:00:45,902 --> 00:00:45,903
201. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/AddText"))

52
00:00:47,002 --> 00:00:47,002
205. delay(3)

53
00:00:50,012 --> 00:00:50,012
209. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

54
00:00:50,554 --> 00:00:50,555
213. delay(2)

55
00:00:52,566 --> 00:00:52,566
217. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved4.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved4.png")

56
00:00:52,838 --> 00:00:52,838
221. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/button_OK"))

57
00:00:53,193 --> 00:00:53,195
225. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/TextArea"), "Test")

58
00:00:53,528 --> 00:00:53,528
229. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

59
00:00:54,097 --> 00:00:54,097
233. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved5.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved5.png")

60
00:00:54,654 --> 00:00:54,656
237. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/UndoButton"))

61
00:00:55,557 --> 00:00:55,559
241. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/OIPickAnotherItem"))

62
00:00:56,475 --> 00:00:56,475
245. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

63
00:00:56,931 --> 00:00:56,931
249. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionReverted.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionReverted.png")

