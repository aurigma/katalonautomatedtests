1
00:00:00,311 --> 00:00:00,313
1. openBrowser("")

2
00:00:04,145 --> 00:00:04,145
5. setViewPortSize(1930, 1180)

3
00:00:04,271 --> 00:00:04,274
9. navigateToUrl("http://localhost:3000/")

4
00:00:05,745 --> 00:00:05,749
13. driver = getExecutedBrowser().getName()

5
00:00:05,827 --> 00:00:05,829
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:05,855 --> 00:00:05,865
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:05,874 --> 00:00:05,875
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:06,463 --> 00:00:06,465
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:06,785 --> 00:00:06,786
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:07,065 --> 00:00:07,066
37. click(findTestObject("ConfigurationsTree/div_Palettejson"))

11
00:00:07,349 --> 00:00:07,350
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:07,631 --> 00:00:07,636
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:08,888 --> 00:00:08,894
49. delay(5)

14
00:00:13,993 --> 00:00:13,998
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:14,021 --> 00:00:14,025
57. parentElem = findWebElement(parent, 30)

16
00:00:15,718 --> 00:00:15,721
61. basePoint = parentElem.getLocation()

17
00:00:16,033 --> 00:00:16,037
65. switchToFrame(parent, 5)

18
00:00:16,982 --> 00:00:16,982
69. delay(2)

19
00:00:18,993 --> 00:00:18,995
73. if (driver == "CHROME_DRIVER")

20
00:00:19,006 --> 00:00:19,007
77. delay(1)

21
00:00:20,015 --> 00:00:20,016
81. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton"))

22
00:00:20,574 --> 00:00:20,576
85. delay(2)

23
00:00:22,591 --> 00:00:22,592
89. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), 5)

24
00:00:22,678 --> 00:00:22,679
93. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/TextColorDialog.png", failed + "/Widgets/ColorPicker/Palette/TextColorDialog.png")

25
00:00:23,101 --> 00:00:23,102
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Purple"))

26
00:00:23,878 --> 00:00:23,879
101. delay(1)

27
00:00:24,890 --> 00:00:24,890
105. switchToDefaultContent()

28
00:00:24,928 --> 00:00:24,929
109. click(findTestObject("TestStandPanel/FinishButton"))

29
00:00:25,464 --> 00:00:25,469
113. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

30
00:00:25,597 --> 00:00:25,601
117. delay(2)

31
00:00:27,623 --> 00:00:27,623
121. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png", failed + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png")

32
00:00:27,947 --> 00:00:27,948
125. click(findTestObject("TestStandPanel/BackToEditorButton"))

33
00:00:28,289 --> 00:00:28,290
129. switchToFrame(parent, 5)

34
00:00:28,337 --> 00:00:28,339
133. delay(1)

35
00:00:29,349 --> 00:00:29,350
137. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

36
00:00:29,854 --> 00:00:29,858
141. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

37
00:00:30,118 --> 00:00:30,119
145. delay(2)

38
00:00:32,127 --> 00:00:32,128
149. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png", failed + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png")

