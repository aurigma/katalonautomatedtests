1
00:00:00,322 --> 00:00:00,325
1. openBrowser("")

2
00:00:03,256 --> 00:00:03,257
5. maximizeWindow()

3
00:00:03,316 --> 00:00:03,317
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,361 --> 00:00:03,364
13. driver = getExecutedBrowser().getName()

5
00:00:03,440 --> 00:00:03,442
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,458 --> 00:00:03,464
21. click(findTestObject("ConfigurationsTree/div_base-editor"))

7
00:00:03,519 --> 00:00:03,520
25. click(findTestObject("ConfigurationsTree/div_Widgets"))

8
00:00:03,635 --> 00:00:03,638
29. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

9
00:00:03,661 --> 00:00:03,661
33. click(findTestObject("ConfigurationsTree/div_Palettejson"))

10
00:00:03,703 --> 00:00:03,706
37. click(findTestObject("TestStandPanel/button_RUN"))

11
00:00:03,745 --> 00:00:03,746
41. click(findTestObject("TestStandPanel/EditorTab"))

12
00:00:03,777 --> 00:00:03,779
45. delay(5)

13
00:00:08,873 --> 00:00:08,874
49. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorButton"))

14
00:00:08,900 --> 00:00:08,902
53. delay(2)

15
00:00:10,913 --> 00:00:10,914
57. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), 5)

16
00:00:10,950 --> 00:00:10,950
61. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/Palette/TextColorDialog.png")

