1
00:00:00,201 --> 00:00:00,202
1. openBrowser("")

2
00:00:03,181 --> 00:00:03,181
5. maximizeWindow()

3
00:00:03,480 --> 00:00:03,482
9. navigateToUrl("http://localhost:3000/")

4
00:00:04,681 --> 00:00:04,682
13. driver = getExecutedBrowser().getName()

5
00:00:04,703 --> 00:00:04,703
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:04,729 --> 00:00:04,732
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:04,749 --> 00:00:04,750
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:05,196 --> 00:00:05,196
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:05,466 --> 00:00:05,466
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:05,743 --> 00:00:05,744
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:06,010 --> 00:00:06,010
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:06,913 --> 00:00:06,914
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:07,383 --> 00:00:07,384
49. delay(5)

14
00:00:12,398 --> 00:00:12,399
53. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorButton"))

15
00:00:13,115 --> 00:00:13,116
57. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 10)

16
00:00:13,792 --> 00:00:13,793
61. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"))

17
00:00:14,030 --> 00:00:14,030
65. delay(2)

18
00:00:16,040 --> 00:00:16,040
69. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerRGB/SaturationChanged1.png")

19
00:00:16,349 --> 00:00:16,350
73. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 130)

20
00:00:17,007 --> 00:00:17,008
77. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"))

21
00:00:17,246 --> 00:00:17,246
81. delay(2)

22
00:00:19,258 --> 00:00:19,258
85. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/HueChanged1.png", failed + "/Widgets/ColorPicker/PickerRGB/HueChanged1.png")

23
00:00:19,580 --> 00:00:19,582
89. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 90)

24
00:00:20,402 --> 00:00:20,403
93. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"))

25
00:00:20,600 --> 00:00:20,601
97. delay(2)

26
00:00:22,610 --> 00:00:22,610
101. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/PickersFirstCheck.png", failed + "/Widgets/ColorPicker/PickerRGB/PickersFirstCheck.png")

27
00:00:22,933 --> 00:00:22,933
105. click(findTestObject("TestStandPanel/FinishButton"))

28
00:00:23,180 --> 00:00:23,181
109. delay(2)

29
00:00:25,191 --> 00:00:25,191
113. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), baseDir + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck1.png", failed + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck1.png")

30
00:00:25,448 --> 00:00:25,448
117. click(findTestObject("TestStandPanel/BackToEditorButton"))

31
00:00:25,704 --> 00:00:25,705
121. delay(2)

32
00:00:27,711 --> 00:00:27,711
125. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), -5, -2)

33
00:00:28,267 --> 00:00:28,268
129. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"))

34
00:00:28,491 --> 00:00:28,492
133. delay(2)

35
00:00:30,502 --> 00:00:30,502
137. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/SaturationChanged2.png", failed + "/Widgets/ColorPicker/PickerRGB/SaturationChanged2.png")

36
00:00:30,841 --> 00:00:30,841
141. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, -70)

37
00:00:31,423 --> 00:00:31,424
145. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"))

38
00:00:31,641 --> 00:00:31,642
149. delay(2)

39
00:00:33,647 --> 00:00:33,647
153. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/HueChanged2.png", failed + "/Widgets/ColorPicker/PickerRGB/HueChanged2.png")

40
00:00:33,936 --> 00:00:33,936
157. delay(1)

41
00:00:34,942 --> 00:00:34,943
161. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, -30)

42
00:00:35,509 --> 00:00:35,510
165. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"))

43
00:00:35,737 --> 00:00:35,738
169. delay(2)

44
00:00:37,750 --> 00:00:37,750
173. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/PickersSecondCheck.png", failed + "/Widgets/ColorPicker/PickerRGB/PickersSecondCheck.png")

45
00:00:38,042 --> 00:00:38,043
177. click(findTestObject("TestStandPanel/FinishButton"))

46
00:00:38,360 --> 00:00:38,360
181. delay(2)

47
00:00:40,371 --> 00:00:40,371
185. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), baseDir + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck2.png", failed + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck2.png")

48
00:00:40,666 --> 00:00:40,666
189. click(findTestObject("TestStandPanel/BackToEditorButton"))

49
00:00:40,907 --> 00:00:40,907
193. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"))

50
00:00:41,244 --> 00:00:41,244
197. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"))

51
00:00:41,431 --> 00:00:41,432
201. delay(3)

52
00:00:44,444 --> 00:00:44,445
205. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/RecentPaletteAdded.png", failed + "/Widgets/ColorPicker/PickerRGB/RecentPaletteAdded.png")

