1
00:00:00,155 --> 00:00:00,155
1. openBrowser("")

2
00:00:01,393 --> 00:00:01,393
5. maximizeWindow()

3
00:00:01,549 --> 00:00:01,549
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,050 --> 00:00:03,050
13. driver = getExecutedBrowser().getName()

5
00:00:03,067 --> 00:00:03,068
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,077 --> 00:00:03,078
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:03,084 --> 00:00:03,086
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,239 --> 00:00:03,239
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,380 --> 00:00:03,380
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:03,532 --> 00:00:03,533
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:03,667 --> 00:00:03,667
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:03,759 --> 00:00:03,760
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:03,941 --> 00:00:03,942
49. delay(5)

14
00:00:08,957 --> 00:00:08,958
53. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorButton"))

15
00:00:09,585 --> 00:00:09,586
57. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 10)

16
00:00:10,194 --> 00:00:10,195
61. delay(2)

17
00:00:12,221 --> 00:00:12,222
65. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerRGB/SaturationChanged1.png")

18
00:00:12,902 --> 00:00:12,903
69. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 130)

19
00:00:13,304 --> 00:00:13,305
73. delay(2)

20
00:00:15,315 --> 00:00:15,315
77. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/HueChanged1.png", failed + "/Widgets/ColorPicker/PickerRGB/HueChanged1.png")

21
00:00:15,937 --> 00:00:15,938
81. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 90)

22
00:00:16,307 --> 00:00:16,307
85. delay(2)

23
00:00:18,313 --> 00:00:18,314
89. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/PickersFirstCheck.png", failed + "/Widgets/ColorPicker/PickerRGB/PickersFirstCheck.png")

24
00:00:18,939 --> 00:00:18,939
93. click(findTestObject("TestStandPanel/FinishButton"))

25
00:00:19,094 --> 00:00:19,095
97. delay(2)

26
00:00:21,209 --> 00:00:21,210
101. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), baseDir + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck1.png", failed + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck1.png")

27
00:00:21,831 --> 00:00:21,831
105. click(findTestObject("TestStandPanel/BackToEditorButton"))

28
00:00:21,929 --> 00:00:21,930
109. delay(2)

29
00:00:23,946 --> 00:00:23,946
113. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), -5, -2)

30
00:00:24,274 --> 00:00:24,275
117. delay(2)

31
00:00:26,286 --> 00:00:26,287
121. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/SaturationChanged2.png", failed + "/Widgets/ColorPicker/PickerRGB/SaturationChanged2.png")

32
00:00:26,904 --> 00:00:26,904
125. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, -70)

33
00:00:27,271 --> 00:00:27,271
129. delay(2)

34
00:00:29,282 --> 00:00:29,282
133. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/HueChanged2.png", failed + "/Widgets/ColorPicker/PickerRGB/HueChanged2.png")

35
00:00:29,905 --> 00:00:29,905
137. delay(1)

36
00:00:30,910 --> 00:00:30,910
141. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, -30)

37
00:00:31,299 --> 00:00:31,300
145. delay(2)

38
00:00:33,310 --> 00:00:33,310
149. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/PickersSecondCheck.png", failed + "/Widgets/ColorPicker/PickerRGB/PickersSecondCheck.png")

