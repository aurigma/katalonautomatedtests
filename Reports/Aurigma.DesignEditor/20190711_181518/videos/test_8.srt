1
00:00:00,282 --> 00:00:00,283
1. openBrowser("")

2
00:00:03,357 --> 00:00:03,359
5. maximizeWindow()

3
00:00:03,656 --> 00:00:03,656
9. navigateToUrl("http://localhost:3000/")

4
00:00:04,881 --> 00:00:04,881
13. driver = getExecutedBrowser().getName()

5
00:00:04,893 --> 00:00:04,894
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:04,911 --> 00:00:04,911
21. click(findTestObject("ConfigurationsTree/div_base-editor"))

7
00:00:05,242 --> 00:00:05,242
25. click(findTestObject("ConfigurationsTree/div_Widgets"))

8
00:00:05,530 --> 00:00:05,531
29. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

9
00:00:05,799 --> 00:00:05,799
33. click(findTestObject("ConfigurationsTree/div_Widgetsjson"))

10
00:00:06,073 --> 00:00:06,073
37. click(findTestObject("TestStandPanel/button_RUN"))

11
00:00:07,007 --> 00:00:07,007
41. click(findTestObject("TestStandPanel/EditorTab"))

12
00:00:07,390 --> 00:00:07,391
45. delay(5)

13
00:00:12,398 --> 00:00:12,398
49. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/button_More"))

14
00:00:13,021 --> 00:00:13,022
53. delay(2)

15
00:00:15,034 --> 00:00:15,034
57. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokeButton"))

16
00:00:15,558 --> 00:00:15,559
61. delay(2)

17
00:00:17,571 --> 00:00:17,572
65. StrokeDialogInitial = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokeDialog"))

18
00:00:17,837 --> 00:00:17,837
69. Dialog1IsSimilar = scripts.ScreenshotHandler.compareToBase(StrokeDialogInitial, baseDir + "/Widgets/ColorPicker/Widgets/StrokeDialogInitial.png")

19
00:00:17,854 --> 00:00:17,854
73. assert Dialog1IsSimilar == true

20
00:00:17,860 --> 00:00:17,860
77. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/OpenStrokePalette"))

21
00:00:18,443 --> 00:00:18,443
81. delay(3)

22
00:00:21,453 --> 00:00:21,454
85. StrokePalette1 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokePalette"))

23
00:00:21,755 --> 00:00:21,755
89. Palette1IsSimilar = scripts.ScreenshotHandler.compareToBase(StrokePalette1, baseDir + "/Widgets/ColorPicker/Widgets/StrokePaletteInitial.png")

24
00:00:21,772 --> 00:00:21,772
93. assert Palette1IsSimilar == true

