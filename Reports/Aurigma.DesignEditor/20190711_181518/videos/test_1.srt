1
00:00:00,327 --> 00:00:00,331
1. openBrowser("")

2
00:00:04,269 --> 00:00:04,269
5. maximizeWindow()

3
00:00:04,582 --> 00:00:04,583
9. navigateToUrl("http://localhost:3000/")

4
00:00:05,788 --> 00:00:05,790
13. driver = getExecutedBrowser().getName()

5
00:00:05,820 --> 00:00:05,820
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:05,827 --> 00:00:05,828
21. click(findTestObject("ConfigurationsTree/div_base-editor"))

7
00:00:06,218 --> 00:00:06,219
25. click(findTestObject("ConfigurationsTree/div_Widgets"))

8
00:00:06,548 --> 00:00:06,549
29. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

9
00:00:06,866 --> 00:00:06,867
33. click(findTestObject("ConfigurationsTree/div_Palettejson"))

10
00:00:07,137 --> 00:00:07,138
37. click(findTestObject("TestStandPanel/button_RUN"))

11
00:00:08,137 --> 00:00:08,138
41. click(findTestObject("TestStandPanel/EditorTab"))

12
00:00:08,505 --> 00:00:08,506
45. delay(5)

13
00:00:13,552 --> 00:00:13,553
49. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorButton"))

14
00:00:20,455 --> 00:00:20,455
53. delay(2)

15
00:00:22,462 --> 00:00:22,463
57. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), 5)

16
00:00:27,764 --> 00:00:27,765
61. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/Palette/TextColorDialog.png")

