1
00:00:00,249 --> 00:00:00,251
1. openBrowser("")

2
00:00:02,036 --> 00:00:02,039
5. setViewPortSize(1930, 1180)

3
00:00:02,164 --> 00:00:02,165
9. navigateToUrl("http://localhost:3000/")

4
00:00:04,866 --> 00:00:04,868
13. driver = getExecutedBrowser().getName()

5
00:00:04,890 --> 00:00:04,891
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:04,904 --> 00:00:04,906
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:04,919 --> 00:00:04,920
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:05,068 --> 00:00:05,069
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:05,185 --> 00:00:05,186
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:05,312 --> 00:00:05,313
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:05,445 --> 00:00:05,446
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:05,540 --> 00:00:05,541
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:05,741 --> 00:00:05,742
49. delay(5)

14
00:00:10,750 --> 00:00:10,751
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:10,758 --> 00:00:10,760
57. parentElem = findWebElement(parent, 30)

16
00:00:10,847 --> 00:00:10,849
61. basePoint = parentElem.getLocation()

17
00:00:10,867 --> 00:00:10,869
65. switchToFrame(parent, 5)

18
00:00:10,921 --> 00:00:10,922
69. delay(2)

19
00:00:12,936 --> 00:00:12,937
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:13,300 --> 00:00:13,301
77. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

21
00:00:13,355 --> 00:00:13,356
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

22
00:00:13,421 --> 00:00:13,422
85. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

23
00:00:13,654 --> 00:00:13,655
89. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

24
00:00:13,712 --> 00:00:13,712
93. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

25
00:00:14,077 --> 00:00:14,078
97. delay(1)

26
00:00:15,094 --> 00:00:15,095
101. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

27
00:00:15,145 --> 00:00:15,145
105. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

28
00:00:15,488 --> 00:00:15,489
109. delay(2)

29
00:00:17,499 --> 00:00:17,499
113. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

