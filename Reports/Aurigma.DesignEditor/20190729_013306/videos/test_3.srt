1
00:00:00,202 --> 00:00:00,203
1. openBrowser("")

2
00:00:01,998 --> 00:00:01,998
5. setViewPortSize(1930, 1180)

3
00:00:02,116 --> 00:00:02,117
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,317 --> 00:00:03,318
13. driver = getExecutedBrowser().getName()

5
00:00:03,335 --> 00:00:03,336
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,352 --> 00:00:03,353
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:03,370 --> 00:00:03,372
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,476 --> 00:00:03,476
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,588 --> 00:00:03,589
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:03,710 --> 00:00:03,710
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:03,837 --> 00:00:03,838
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:03,922 --> 00:00:03,922
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:04,113 --> 00:00:04,114
49. delay(5)

14
00:00:09,124 --> 00:00:09,125
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:09,141 --> 00:00:09,142
57. parentElem = findWebElement(parent, 30)

16
00:00:09,221 --> 00:00:09,221
61. basePoint = parentElem.getLocation()

17
00:00:09,234 --> 00:00:09,235
65. switchToFrame(parent, 5)

18
00:00:09,278 --> 00:00:09,279
69. delay(2)

19
00:00:11,293 --> 00:00:11,294
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:11,668 --> 00:00:11,668
77. delay(4)

21
00:00:15,677 --> 00:00:15,678
81. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 10)

22
00:00:15,968 --> 00:00:15,969
85. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"))

23
00:00:16,085 --> 00:00:16,086
89. delay(2)

24
00:00:18,090 --> 00:00:18,090
93. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerRGB/SaturationChanged1.png")

