1
00:00:00,144 --> 00:00:00,144
1. openBrowser("")

2
00:00:01,926 --> 00:00:01,926
5. setViewPortSize(1930, 1180)

3
00:00:02,052 --> 00:00:02,052
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,104 --> 00:00:03,105
13. driver = getExecutedBrowser().getName()

5
00:00:03,113 --> 00:00:03,114
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,120 --> 00:00:03,121
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:03,142 --> 00:00:03,145
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,250 --> 00:00:03,251
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,375 --> 00:00:03,375
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:03,506 --> 00:00:03,506
37. click(findTestObject("ConfigurationsTree/div_Widgetsjson"))

11
00:00:03,654 --> 00:00:03,655
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:03,742 --> 00:00:03,743
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:03,901 --> 00:00:03,902
49. delay(5)

14
00:00:08,914 --> 00:00:08,914
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/Iframe")

15
00:00:08,926 --> 00:00:08,927
57. parentElem = findWebElement(parent, 30)

16
00:00:09,002 --> 00:00:09,003
61. basePoint = parentElem.getLocation()

17
00:00:09,019 --> 00:00:09,019
65. switchToFrame(parent, 5)

18
00:00:09,057 --> 00:00:09,058
69. delay(2)

19
00:00:11,066 --> 00:00:11,066
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/AddRichText"))

20
00:00:11,435 --> 00:00:11,436
77. delay(2)

21
00:00:13,451 --> 00:00:13,453
81. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETextColorButton"))

22
00:00:14,272 --> 00:00:14,273
85. delay(2)

23
00:00:16,305 --> 00:00:16,305
89. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETopBar"))

24
00:00:16,411 --> 00:00:16,411
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), 5)

25
00:00:16,459 --> 00:00:16,460
97. delay(1)

26
00:00:17,468 --> 00:00:17,469
101. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), basePoint, baseDir + "/Widgets/ColorPicker/Widgets/RTEPickerOpened.png", failed + "/Widgets/ColorPicker/Widgets/RTEPickerOpened.png")

