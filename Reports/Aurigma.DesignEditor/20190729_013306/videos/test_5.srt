1
00:00:00,152 --> 00:00:00,152
1. openBrowser("")

2
00:00:01,927 --> 00:00:01,927
5. setViewPortSize(1930, 1180)

3
00:00:02,055 --> 00:00:02,056
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,118 --> 00:00:03,119
13. driver = getExecutedBrowser().getName()

5
00:00:03,133 --> 00:00:03,134
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,143 --> 00:00:03,144
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:03,160 --> 00:00:03,162
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,252 --> 00:00:03,253
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,417 --> 00:00:03,418
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:03,529 --> 00:00:03,530
37. click(findTestObject("ConfigurationsTree/div_SectionCollapsejson"))

11
00:00:03,668 --> 00:00:03,669
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:03,756 --> 00:00:03,757
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:03,926 --> 00:00:03,927
49. delay(5)

14
00:00:08,937 --> 00:00:08,938
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:08,952 --> 00:00:08,954
57. parentElem = findWebElement(parent, 30)

16
00:00:09,035 --> 00:00:09,036
61. basePoint = parentElem.getLocation()

17
00:00:09,050 --> 00:00:09,050
65. switchToFrame(parent, 5)

18
00:00:09,093 --> 00:00:09,094
69. delay(2)

19
00:00:11,099 --> 00:00:11,100
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

20
00:00:11,454 --> 00:00:11,455
77. delay(2)

21
00:00:13,476 --> 00:00:13,476
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), 3)

22
00:00:13,528 --> 00:00:13,528
85. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/PickerWithCollapseSections.png", failed + "/Widgets/ColorPicker/SectionCollapse/PickerWithCollapseSections.png")

