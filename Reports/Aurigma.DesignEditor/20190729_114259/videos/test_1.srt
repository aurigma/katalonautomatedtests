1
00:00:00,634 --> 00:00:00,641
1. openBrowser("")

2
00:00:06,843 --> 00:00:06,843
5. setViewPortSize(1930, 1180)

3
00:00:06,904 --> 00:00:06,905
9. navigateToUrl("http://localhost:3000/")

4
00:00:08,455 --> 00:00:08,457
13. driver = getExecutedBrowser().getName()

5
00:00:08,490 --> 00:00:08,491
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:08,524 --> 00:00:08,526
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:08,542 --> 00:00:08,543
25. logInfo(baseDir)

8
00:00:08,560 --> 00:00:08,561
29. click(findTestObject("ConfigurationsTree/div_base-editor"))

9
00:00:08,950 --> 00:00:08,951
33. click(findTestObject("ConfigurationsTree/div_Widgets"))

10
00:00:09,234 --> 00:00:09,235
37. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

11
00:00:09,560 --> 00:00:09,561
41. click(findTestObject("ConfigurationsTree/div_Palettejson"))

12
00:00:09,838 --> 00:00:09,839
45. click(findTestObject("TestStandPanel/button_RUN"))

13
00:00:10,748 --> 00:00:10,749
49. click(findTestObject("TestStandPanel/EditorTab"))

14
00:00:11,069 --> 00:00:11,070
53. delay(5)

15
00:00:16,095 --> 00:00:16,095
57. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

16
00:00:16,112 --> 00:00:16,115
61. parentElem = findWebElement(parent, 30)

17
00:00:18,176 --> 00:00:18,177
65. basePoint = parentElem.getLocation()

18
00:00:18,196 --> 00:00:18,197
69. switchToFrame(parent, 5)

19
00:00:18,246 --> 00:00:18,246
73. delay(2)

20
00:00:20,257 --> 00:00:20,258
77. if (driver == "CHROME_DRIVER" || driver == "HEADLESS_DRIVER")

21
00:00:20,272 --> 00:00:20,274
81. delay(1)

22
00:00:21,288 --> 00:00:21,290
85. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton"))

23
00:00:23,316 --> 00:00:23,317
89. delay(2)

24
00:00:25,326 --> 00:00:25,327
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), 5)

25
00:00:30,459 --> 00:00:30,461
97. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/TextColorDialog.png", failed + "/Widgets/ColorPicker/Palette/TextColorDialog.png")

