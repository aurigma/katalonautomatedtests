1
00:00:00,243 --> 00:00:00,243
1. openBrowser("")

2
00:00:07,917 --> 00:00:07,918
5. setViewPortSize(1930, 1180)

3
00:00:08,147 --> 00:00:08,148
9. navigateToUrl("http://localhost:3000/")

4
00:00:10,733 --> 00:00:10,735
13. driver = getExecutedBrowser().getName()

5
00:00:10,772 --> 00:00:10,773
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:10,842 --> 00:00:10,847
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:10,867 --> 00:00:10,871
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:11,801 --> 00:00:11,802
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:12,155 --> 00:00:12,156
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:12,484 --> 00:00:12,485
37. click(findTestObject("ConfigurationsTree/div_Widgetsjson"))

11
00:00:12,825 --> 00:00:12,826
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:13,131 --> 00:00:13,132
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:14,534 --> 00:00:14,535
49. delay(5)

14
00:00:19,560 --> 00:00:19,562
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/Iframe")

15
00:00:19,725 --> 00:00:19,726
57. parentElem = findWebElement(parent, 30)

16
00:00:25,292 --> 00:00:25,293
61. basePoint = parentElem.getLocation()

17
00:00:25,840 --> 00:00:25,840
65. switchToFrame(parent, 5)

18
00:00:28,637 --> 00:00:28,638
69. delay(2)

19
00:00:30,661 --> 00:00:30,664
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/AddRichText"))

20
00:00:31,663 --> 00:00:31,664
77. delay(2)

21
00:00:33,697 --> 00:00:33,699
81. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETextColorButton"))

22
00:00:36,450 --> 00:00:36,451
85. delay(2)

23
00:00:38,493 --> 00:00:38,496
89. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETopBar"))

24
00:00:38,705 --> 00:00:38,705
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), 5)

25
00:00:38,821 --> 00:00:38,824
97. delay(1)

26
00:00:39,884 --> 00:00:39,887
101. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), basePoint, baseDir + "/Widgets/ColorPicker/Widgets/RTEPickerOpened.png", failed + "/Widgets/ColorPicker/Widgets/RTEPickerOpened.png")

27
00:00:40,166 --> 00:00:40,166
105. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorOk"))

28
00:00:40,996 --> 00:00:40,997
109. delay(2)

29
00:00:43,033 --> 00:00:43,034
113. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETopBar"), basePoint, baseDir + "/Widgets/ColorPicker/Widgets/RTEPickerClosed.png", failed + "/Widgets/ColorPicker/Widgets/RTEPickerClosed.png")

30
00:00:43,276 --> 00:00:43,278
117. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETextColorButton"))

31
00:00:44,264 --> 00:00:44,265
121. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColors_Orange"), 5)

32
00:00:44,364 --> 00:00:44,365
125. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColors_Orange"))

33
00:00:45,487 --> 00:00:45,487
129. delay(2)

34
00:00:47,508 --> 00:00:47,508
133. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColors_Orange"))

35
00:00:47,696 --> 00:00:47,696
137. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), basePoint, baseDir + "/Widgets/ColorPicker/Widgets/RTEPickerRecent.png", failed + "/Widgets/ColorPicker/Widgets/RTEPickerRecent.png")

