1
00:00:00,238 --> 00:00:00,238
1. openBrowser("")

2
00:00:03,720 --> 00:00:03,720
5. setViewPortSize(1930, 1180)

3
00:00:03,817 --> 00:00:03,822
9. navigateToUrl("http://localhost:3000/")

4
00:00:05,353 --> 00:00:05,354
13. driver = getExecutedBrowser().getName()

5
00:00:05,370 --> 00:00:05,371
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:05,380 --> 00:00:05,381
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:05,391 --> 00:00:05,392
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:06,304 --> 00:00:06,305
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:06,620 --> 00:00:06,621
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:06,907 --> 00:00:06,908
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:07,193 --> 00:00:07,193
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:08,380 --> 00:00:08,381
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:08,741 --> 00:00:08,743
49. delay(5)

14
00:00:13,757 --> 00:00:13,762
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:13,840 --> 00:00:13,845
57. parentElem = findWebElement(parent, 30)

16
00:00:18,910 --> 00:00:18,911
61. basePoint = parentElem.getLocation()

17
00:00:19,372 --> 00:00:19,374
65. switchToFrame(parent, 5)

18
00:00:21,098 --> 00:00:21,101
69. delay(2)

19
00:00:23,118 --> 00:00:23,119
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:23,890 --> 00:00:23,891
77. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

21
00:00:23,992 --> 00:00:23,993
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

22
00:00:24,059 --> 00:00:24,059
85. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

23
00:00:24,511 --> 00:00:24,514
89. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

24
00:00:24,602 --> 00:00:24,605
93. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

25
00:00:25,465 --> 00:00:25,466
97. delay(1)

26
00:00:26,480 --> 00:00:26,481
101. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

27
00:00:26,536 --> 00:00:26,536
105. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

28
00:00:27,292 --> 00:00:27,292
109. delay(2)

29
00:00:29,304 --> 00:00:29,304
113. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

30
00:00:29,604 --> 00:00:29,605
117. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 5)

31
00:00:29,660 --> 00:00:29,660
121. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 85)

32
00:00:30,425 --> 00:00:30,425
125. delay(3)

33
00:00:33,436 --> 00:00:33,438
129. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/HueChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/HueChanged1.png")

34
00:00:33,744 --> 00:00:33,745
133. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 5)

35
00:00:33,798 --> 00:00:33,798
137. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 110)

36
00:00:34,395 --> 00:00:34,396
141. delay(2)

37
00:00:36,408 --> 00:00:36,408
145. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/PickersFirstCheck.png", failed + "/Widgets/ColorPicker/PickerCMYK/PickersFirstCheck.png")

38
00:00:36,788 --> 00:00:36,792
149. switchToDefaultContent()

39
00:00:36,830 --> 00:00:36,830
153. click(findTestObject("TestStandPanel/FinishButton"))

40
00:00:37,324 --> 00:00:37,324
157. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

41
00:00:37,493 --> 00:00:37,493
161. delay(2)

42
00:00:39,506 --> 00:00:39,507
165. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck1.png", failed + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck1.png")

43
00:00:39,842 --> 00:00:39,842
169. click(findTestObject("TestStandPanel/BackToEditorButton"))

44
00:00:40,221 --> 00:00:40,221
173. switchToFrame(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe"), 5)

45
00:00:40,295 --> 00:00:40,296
177. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

46
00:00:40,499 --> 00:00:40,500
181. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

47
00:00:40,753 --> 00:00:40,755
185. delay(2)

48
00:00:42,782 --> 00:00:42,783
189. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5, 2)

49
00:00:43,478 --> 00:00:43,478
193. delay(2)

50
00:00:45,487 --> 00:00:45,488
197. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged2.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged2.png")

51
00:00:45,933 --> 00:00:45,934
201. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 5)

52
00:00:45,997 --> 00:00:45,998
205. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 20)

53
00:00:46,809 --> 00:00:46,812
209. delay(2)

54
00:00:48,838 --> 00:00:48,839
213. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/HueChanged2.png", failed + "/Widgets/ColorPicker/PickerCMYK/HueChanged2.png")

55
00:00:49,193 --> 00:00:49,194
217. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 5)

56
00:00:49,256 --> 00:00:49,257
221. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 20)

57
00:00:50,034 --> 00:00:50,034
225. delay(2)

58
00:00:52,058 --> 00:00:52,058
229. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/PickersSecondCheck.png", failed + "/Widgets/ColorPicker/PickerCMYK/PickersSecondCheck.png")

59
00:00:52,649 --> 00:00:52,650
233. switchToDefaultContent()

60
00:00:52,680 --> 00:00:52,681
237. click(findTestObject("TestStandPanel/FinishButton"))

61
00:00:53,112 --> 00:00:53,114
241. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

62
00:00:53,248 --> 00:00:53,248
245. delay(2)

63
00:00:55,294 --> 00:00:55,295
249. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck2.png", failed + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck2.png")

64
00:00:56,023 --> 00:00:56,025
253. click(findTestObject("TestStandPanel/BackToEditorButton"))

65
00:00:56,544 --> 00:00:56,545
257. switchToFrame(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe"), 5)

66
00:00:56,577 --> 00:00:56,578
261. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

67
00:00:56,635 --> 00:00:56,636
265. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"), 5)

68
00:00:56,729 --> 00:00:56,730
269. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"))

69
00:00:57,550 --> 00:00:57,551
273. delay(2)

70
00:00:59,582 --> 00:00:59,585
277. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"))

71
00:00:59,895 --> 00:00:59,896
281. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/RecentPaletteAdded.png", failed + "/Widgets/ColorPicker/PickerCMYK/RecentPaletteAdded.png")

72
00:01:00,866 --> 00:01:00,867
285. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel"), 5)

73
00:01:01,627 --> 00:01:01,628
289. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel"))

74
00:01:02,673 --> 00:01:02,673
293. delay(2)

75
00:01:04,693 --> 00:01:04,696
297. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/ChannelSelectedLightning.png", failed + "/Widgets/ColorPicker/PickerCMYK/ChannelSelectedLightning.png")

