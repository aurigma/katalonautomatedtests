1
00:00:00,140 --> 00:00:00,141
1. openBrowser("")

2
00:00:04,084 --> 00:00:04,085
5. setViewPortSize(1930, 1180)

3
00:00:04,160 --> 00:00:04,162
9. navigateToUrl("http://localhost:3000/")

4
00:00:05,474 --> 00:00:05,475
13. driver = getExecutedBrowser().getName()

5
00:00:05,484 --> 00:00:05,485
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:05,502 --> 00:00:05,503
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:05,526 --> 00:00:05,527
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:06,312 --> 00:00:06,312
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:06,595 --> 00:00:06,596
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:06,867 --> 00:00:06,868
37. click(findTestObject("ConfigurationsTree/div_SectionCollapsejson"))

11
00:00:07,142 --> 00:00:07,143
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:08,303 --> 00:00:08,303
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:09,117 --> 00:00:09,117
49. delay(5)

14
00:00:14,155 --> 00:00:14,156
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:14,168 --> 00:00:14,169
57. parentElem = findWebElement(parent, 30)

16
00:00:14,225 --> 00:00:14,225
61. basePoint = parentElem.getLocation()

17
00:00:14,281 --> 00:00:14,282
65. switchToFrame(parent, 5)

18
00:00:14,335 --> 00:00:14,336
69. delay(2)

19
00:00:16,343 --> 00:00:16,344
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

20
00:00:16,976 --> 00:00:16,977
77. delay(2)

21
00:00:19,000 --> 00:00:19,000
81. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/PickerWithCollapseSections.png", failed + "/Widgets/ColorPicker/SectionCollapse/PickerWithCollapseSections.png")

22
00:00:19,272 --> 00:00:19,272
85. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/GrayscaleSectionCollapse"))

23
00:00:19,622 --> 00:00:19,623
89. delay(2)

24
00:00:21,635 --> 00:00:21,635
93. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/GrayscaleCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/GrayscaleCollapsed.png")

25
00:00:21,910 --> 00:00:21,910
97. delay(2)

26
00:00:23,918 --> 00:00:23,918
101. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RgbSectionCollapse"))

27
00:00:24,483 --> 00:00:24,483
105. delay(2)

28
00:00:26,493 --> 00:00:26,493
109. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/RGBCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/RGBCollapsed.png")

29
00:00:26,749 --> 00:00:26,750
113. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/CmykSectionCollapse"))

30
00:00:27,355 --> 00:00:27,355
117. delay(2)

31
00:00:29,361 --> 00:00:29,361
121. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CMYKCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/CMYKCollapsed.png")

32
00:00:29,605 --> 00:00:29,605
125. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RecentSectionCollapse"))

33
00:00:30,058 --> 00:00:30,059
129. delay(2)

34
00:00:32,074 --> 00:00:32,074
133. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/AllCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/AllCollapsed.png")

35
00:00:32,317 --> 00:00:32,318
137. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

36
00:00:32,817 --> 00:00:32,817
141. delay(1)

37
00:00:33,832 --> 00:00:33,833
145. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

38
00:00:34,292 --> 00:00:34,292
149. delay(2)

39
00:00:36,302 --> 00:00:36,302
153. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved.png")

40
00:00:36,546 --> 00:00:36,546
157. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/button_OK"))

41
00:00:36,911 --> 00:00:36,914
161. delay(1)

42
00:00:37,933 --> 00:00:37,934
165. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

43
00:00:38,449 --> 00:00:38,450
169. delay(2)

44
00:00:40,460 --> 00:00:40,460
173. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved2.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved2.png")

45
00:00:40,707 --> 00:00:40,707
177. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RecentSectionCollapse"))

46
00:00:41,040 --> 00:00:41,042
181. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RgbSectionCollapse"))

47
00:00:41,486 --> 00:00:41,487
185. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/OIPickAnotherItem"))

48
00:00:42,232 --> 00:00:42,232
189. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

49
00:00:42,739 --> 00:00:42,740
193. delay(2)

50
00:00:44,765 --> 00:00:44,765
197. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved3.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved3.png")

51
00:00:45,009 --> 00:00:45,009
201. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/AddText"))

52
00:00:46,107 --> 00:00:46,108
205. delay(3)

53
00:00:49,125 --> 00:00:49,126
209. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

54
00:00:49,610 --> 00:00:49,611
213. delay(2)

55
00:00:51,625 --> 00:00:51,625
217. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved4.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved4.png")

56
00:00:51,867 --> 00:00:51,867
221. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/button_OK"))

57
00:00:52,303 --> 00:00:52,305
225. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/TextArea"), "Test")

58
00:00:52,601 --> 00:00:52,601
229. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

59
00:00:53,131 --> 00:00:53,131
233. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved5.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved5.png")

60
00:00:53,562 --> 00:00:53,563
237. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/UndoButton"))

61
00:00:54,346 --> 00:00:54,347
241. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/OIPickAnotherItem"))

62
00:00:55,279 --> 00:00:55,280
245. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

63
00:00:55,792 --> 00:00:55,792
249. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionReverted.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionReverted.png")

