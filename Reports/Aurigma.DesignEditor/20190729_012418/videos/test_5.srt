1
00:00:00,266 --> 00:00:00,267
1. openBrowser("")

2
00:00:02,155 --> 00:00:02,155
5. setViewPortSize(1930, 1180)

3
00:00:02,282 --> 00:00:02,282
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,771 --> 00:00:03,772
13. driver = getExecutedBrowser().getName()

5
00:00:03,830 --> 00:00:03,831
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,866 --> 00:00:03,873
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:03,919 --> 00:00:03,924
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:04,144 --> 00:00:04,144
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:04,334 --> 00:00:04,335
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:04,545 --> 00:00:04,545
37. click(findTestObject("ConfigurationsTree/div_SectionCollapsejson"))

11
00:00:04,812 --> 00:00:04,812
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:04,962 --> 00:00:04,963
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:05,823 --> 00:00:05,824
49. delay(5)

14
00:00:10,891 --> 00:00:10,895
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:10,936 --> 00:00:10,939
57. parentElem = findWebElement(parent, 30)

16
00:00:13,274 --> 00:00:13,274
61. basePoint = parentElem.getLocation()

17
00:00:13,559 --> 00:00:13,560
65. switchToFrame(parent, 5)

18
00:00:14,089 --> 00:00:14,090
69. delay(2)

19
00:00:16,113 --> 00:00:16,127
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

20
00:00:21,839 --> 00:00:21,840
77. delay(2)

21
00:00:23,868 --> 00:00:23,873
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), 3)

22
00:00:24,129 --> 00:00:24,132
85. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/PickerWithCollapseSections.png", failed + "/Widgets/ColorPicker/SectionCollapse/PickerWithCollapseSections.png")

