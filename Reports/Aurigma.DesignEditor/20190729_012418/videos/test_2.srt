1
00:00:00,620 --> 00:00:00,622
1. openBrowser("")

2
00:00:02,803 --> 00:00:02,804
5. setViewPortSize(1930, 1180)

3
00:00:02,950 --> 00:00:02,951
9. navigateToUrl("http://localhost:3000/")

4
00:00:04,340 --> 00:00:04,340
13. driver = getExecutedBrowser().getName()

5
00:00:04,366 --> 00:00:04,368
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:04,403 --> 00:00:04,404
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:04,460 --> 00:00:04,462
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:04,714 --> 00:00:04,715
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:04,911 --> 00:00:04,912
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:05,179 --> 00:00:05,180
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:05,411 --> 00:00:05,412
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:05,574 --> 00:00:05,575
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:06,026 --> 00:00:06,027
49. delay(5)

14
00:00:11,052 --> 00:00:11,053
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:11,071 --> 00:00:11,072
57. parentElem = findWebElement(parent, 30)

16
00:00:12,139 --> 00:00:12,139
61. basePoint = parentElem.getLocation()

17
00:00:12,255 --> 00:00:12,256
65. switchToFrame(parent, 5)

18
00:00:12,430 --> 00:00:12,431
69. delay(2)

19
00:00:14,446 --> 00:00:14,448
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:15,320 --> 00:00:15,322
77. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

21
00:00:15,414 --> 00:00:15,415
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

22
00:00:15,483 --> 00:00:15,486
85. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

23
00:00:15,884 --> 00:00:15,885
89. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

24
00:00:15,966 --> 00:00:15,967
93. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

25
00:00:16,675 --> 00:00:16,677
97. delay(1)

26
00:00:17,694 --> 00:00:17,698
101. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

27
00:00:17,758 --> 00:00:17,762
105. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

28
00:00:18,416 --> 00:00:18,417
109. delay(2)

29
00:00:20,440 --> 00:00:20,442
113. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

