1
00:00:00,338 --> 00:00:00,338
1. openBrowser("")

2
00:00:02,150 --> 00:00:02,151
5. setViewPortSize(1930, 1180)

3
00:00:02,275 --> 00:00:02,276
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,652 --> 00:00:03,653
13. driver = getExecutedBrowser().getName()

5
00:00:03,659 --> 00:00:03,660
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,683 --> 00:00:03,685
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:03,700 --> 00:00:03,701
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,818 --> 00:00:03,820
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,975 --> 00:00:03,976
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:04,159 --> 00:00:04,160
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:04,346 --> 00:00:04,347
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:04,467 --> 00:00:04,468
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:04,760 --> 00:00:04,762
49. delay(5)

14
00:00:09,779 --> 00:00:09,780
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:09,798 --> 00:00:09,800
57. parentElem = findWebElement(parent, 30)

16
00:00:10,402 --> 00:00:10,402
61. basePoint = parentElem.getLocation()

17
00:00:10,504 --> 00:00:10,505
65. switchToFrame(parent, 5)

18
00:00:11,559 --> 00:00:11,561
69. delay(2)

19
00:00:13,581 --> 00:00:13,591
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:15,756 --> 00:00:15,757
77. delay(4)

21
00:00:19,782 --> 00:00:19,783
81. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 10)

22
00:00:21,030 --> 00:00:21,031
85. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"))

23
00:00:21,956 --> 00:00:21,959
89. delay(2)

24
00:00:23,993 --> 00:00:23,997
93. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerRGB/SaturationChanged1.png")

