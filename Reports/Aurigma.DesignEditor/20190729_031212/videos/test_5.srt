1
00:00:00,556 --> 00:00:00,557
1. openBrowser("")

2
00:00:07,655 --> 00:00:07,656
5. setViewPortSize(1930, 1180)

3
00:00:07,749 --> 00:00:07,753
9. navigateToUrl("http://localhost:3000/")

4
00:00:10,527 --> 00:00:10,528
13. driver = getExecutedBrowser().getName()

5
00:00:10,564 --> 00:00:10,565
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:10,614 --> 00:00:10,614
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:10,732 --> 00:00:10,860
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:11,885 --> 00:00:11,885
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:12,390 --> 00:00:12,392
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:12,902 --> 00:00:12,902
37. click(findTestObject("ConfigurationsTree/div_SectionCollapsejson"))

11
00:00:13,269 --> 00:00:13,270
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:13,587 --> 00:00:13,588
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:14,598 --> 00:00:14,601
49. delay(5)

14
00:00:19,619 --> 00:00:19,620
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:19,640 --> 00:00:19,647
57. parentElem = findWebElement(parent, 30)

16
00:00:19,891 --> 00:00:19,893
61. basePoint = parentElem.getLocation()

17
00:00:19,928 --> 00:00:19,929
65. switchToFrame(parent, 5)

18
00:00:26,555 --> 00:00:26,557
69. delay(2)

19
00:00:28,612 --> 00:00:28,613
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

20
00:00:31,416 --> 00:00:31,417
77. delay(2)

21
00:00:33,447 --> 00:00:33,448
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), 3)

22
00:00:36,648 --> 00:00:36,648
85. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/PickerWithCollapseSections.png", failed + "/Widgets/ColorPicker/SectionCollapse/PickerWithCollapseSections.png")

