1
00:00:00,524 --> 00:00:00,529
1. openBrowser("")

2
00:00:07,387 --> 00:00:07,388
5. setViewPortSize(1930, 1180)

3
00:00:07,650 --> 00:00:07,651
9. navigateToUrl("http://localhost:3000/")

4
00:00:10,486 --> 00:00:10,488
13. driver = getExecutedBrowser().getName()

5
00:00:10,516 --> 00:00:10,517
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:10,526 --> 00:00:10,527
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:10,536 --> 00:00:10,536
25. logInfo(baseDir)

8
00:00:10,557 --> 00:00:10,559
29. click(findTestObject("ConfigurationsTree/div_base-editor"))

9
00:00:11,590 --> 00:00:11,591
33. click(findTestObject("ConfigurationsTree/div_Widgets"))

10
00:00:11,877 --> 00:00:11,878
37. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

11
00:00:12,144 --> 00:00:12,144
41. click(findTestObject("ConfigurationsTree/div_Palettejson"))

12
00:00:12,398 --> 00:00:12,398
45. click(findTestObject("TestStandPanel/button_RUN"))

13
00:00:13,055 --> 00:00:13,056
49. click(findTestObject("TestStandPanel/EditorTab"))

14
00:00:13,338 --> 00:00:13,339
53. delay(5)

15
00:00:18,361 --> 00:00:18,363
57. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

16
00:00:18,382 --> 00:00:18,384
61. parentElem = findWebElement(parent, 30)

17
00:00:18,399 --> 00:00:18,399
65. basePoint = parentElem.getLocation()

18
00:00:18,414 --> 00:00:18,414
69. switchToFrame(parent, 5)

19
00:00:18,437 --> 00:00:18,438
73. delay(2)

20
00:00:20,443 --> 00:00:20,444
77. if (driver == "CHROME_DRIVER" || driver == "HEADLESS_DRIVER")

21
00:00:20,449 --> 00:00:20,449
81. delay(1)

22
00:00:21,455 --> 00:00:21,456
85. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton"))

23
00:00:21,923 --> 00:00:21,923
89. delay(2)

24
00:00:23,931 --> 00:00:23,933
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), 5)

25
00:00:23,999 --> 00:00:23,999
97. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/TextColorDialog.png", failed + "/Widgets/ColorPicker/Palette/TextColorDialog.png")

26
00:00:24,355 --> 00:00:24,356
101. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Purple"))

27
00:00:24,704 --> 00:00:24,704
105. delay(1)

28
00:00:25,710 --> 00:00:25,711
109. switchToDefaultContent()

29
00:00:25,750 --> 00:00:25,751
113. click(findTestObject("TestStandPanel/FinishButton"))

30
00:00:26,022 --> 00:00:26,023
117. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

31
00:00:26,060 --> 00:00:26,061
121. delay(2)

32
00:00:28,069 --> 00:00:28,069
125. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png", failed + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png")

33
00:00:28,361 --> 00:00:28,362
129. click(findTestObject("TestStandPanel/BackToEditorButton"))

34
00:00:28,618 --> 00:00:28,619
133. switchToFrame(parent, 5)

35
00:00:28,638 --> 00:00:28,640
137. delay(1)

36
00:00:29,645 --> 00:00:29,645
141. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

37
00:00:29,946 --> 00:00:29,946
145. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

38
00:00:30,094 --> 00:00:30,094
149. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png", failed + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png")

39
00:00:30,357 --> 00:00:30,361
153. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Burgundy"))

40
00:00:30,715 --> 00:00:30,716
157. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

41
00:00:31,023 --> 00:00:31,024
161. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RecentColor_Purple"))

42
00:00:31,364 --> 00:00:31,365
165. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RecentColor_Purple"))

43
00:00:31,544 --> 00:00:31,544
169. delay(2)

44
00:00:33,550 --> 00:00:33,550
173. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck2.png", failed + "/Widgets/ColorPicker/Palette/AddToRecentCheck2.png")

45
00:00:33,762 --> 00:00:33,762
177. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

46
00:00:34,072 --> 00:00:34,073
181. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

47
00:00:34,198 --> 00:00:34,198
185. delay(2)

48
00:00:36,204 --> 00:00:36,205
189. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck3.png", failed + "/Widgets/ColorPicker/Palette/AddToRecentCheck3.png")

