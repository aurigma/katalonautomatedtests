1
00:00:00,153 --> 00:00:00,154
1. openBrowser("")

2
00:00:01,620 --> 00:00:01,620
5. maximizeWindow()

3
00:00:01,795 --> 00:00:01,795
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,416 --> 00:00:03,417
13. driver = getExecutedBrowser().getName()

5
00:00:03,427 --> 00:00:03,428
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,442 --> 00:00:03,444
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:03,452 --> 00:00:03,453
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,687 --> 00:00:03,688
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,935 --> 00:00:03,936
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:04,063 --> 00:00:04,064
37. click(findTestObject("ConfigurationsTree/div_Widgetsjson"))

11
00:00:04,181 --> 00:00:04,181
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:04,316 --> 00:00:04,317
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:04,496 --> 00:00:04,496
49. delay(5)

14
00:00:09,505 --> 00:00:09,507
53. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/button_More"))

15
00:00:10,332 --> 00:00:10,333
57. delay(2)

16
00:00:12,354 --> 00:00:12,355
61. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextShadowButton"))

17
00:00:12,802 --> 00:00:12,803
65. delay(2)

18
00:00:14,813 --> 00:00:14,814
69. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextShadowDialog"), baseDir + "/Widgets/ColorPicker/Widgets/ShadowDialogInitial.png", failed + "/Widgets/ColorPicker/Widgets/ShadowDialogInitial.png")

19
00:00:15,444 --> 00:00:15,444
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/OpenShadowPalette"))

20
00:00:15,695 --> 00:00:15,696
77. delay(2)

21
00:00:17,703 --> 00:00:17,704
81. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextShadowPalette"), baseDir + "/Widgets/ColorPicker/PickerRGB/ShadowPaletteInitial.png", failed + "/Widgets/ColorPicker/PickerRGB/ShadowPaletteInitial.png")

22
00:00:18,337 --> 00:00:18,337
85. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/ShadowOK"))

23
00:00:18,636 --> 00:00:18,636
89. delay(2)

24
00:00:20,646 --> 00:00:20,646
93. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextShadowDialog"), baseDir + "Widgets/ColorPicker/Widgets/BackToShadowDialog.png", failed + "Widgets/ColorPicker/Widgets/BackToShadowDialog.png")

25
00:00:21,262 --> 00:00:21,262
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/OpenShadowPalette"))

26
00:00:21,464 --> 00:00:21,464
101. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/ShadowColors_Blue"))

27
00:00:21,742 --> 00:00:21,743
105. delay(2)

28
00:00:23,757 --> 00:00:23,757
109. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextShadowPalette"), baseDir + "/Widgets/ColorPicker/Widgets/ShadowRecentAdded.png", failed + "/Widgets/ColorPicker/Widgets/ShadowRecentAdded.png")

