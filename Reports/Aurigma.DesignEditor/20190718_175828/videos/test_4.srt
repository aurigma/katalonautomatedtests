1
00:00:00,281 --> 00:00:00,281
1. openBrowser("")

2
00:00:01,994 --> 00:00:01,994
5. maximizeWindow()

3
00:00:02,232 --> 00:00:02,232
9. navigateToUrl("http://localhost:3000/")

4
00:00:05,408 --> 00:00:05,409
13. driver = getExecutedBrowser().getName()

5
00:00:05,425 --> 00:00:05,427
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:05,439 --> 00:00:05,440
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:05,447 --> 00:00:05,448
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:05,595 --> 00:00:05,596
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:05,772 --> 00:00:05,772
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:05,980 --> 00:00:05,981
37. click(findTestObject("ConfigurationsTree/div_Widgetsjson"))

11
00:00:06,164 --> 00:00:06,165
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:06,284 --> 00:00:06,285
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:06,624 --> 00:00:06,624
49. delay(5)

14
00:00:11,642 --> 00:00:11,643
53. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/AddRichText"))

15
00:00:13,113 --> 00:00:13,114
57. delay(2)

16
00:00:15,128 --> 00:00:15,129
61. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETextColorButton"))

17
00:00:17,053 --> 00:00:17,054
65. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), 5)

18
00:00:17,438 --> 00:00:17,439
69. delay(3)

19
00:00:20,451 --> 00:00:20,453
73. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), baseDir + "/Widgets/ColorPicker/Widgets/RTEPickerOpened.png", failed + "/Widgets/ColorPicker/Widgets/RTEPickerOpened.png")

20
00:00:21,196 --> 00:00:21,196
77. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorOk"))

21
00:00:21,745 --> 00:00:21,746
81. delay(2)

22
00:00:23,758 --> 00:00:23,759
85. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETopBar"), baseDir + "/Widgets/ColorPicker/Widgets/RTEPickerClosed.png", failed + "/Widgets/ColorPicker/Widgets/RTEPickerClosed.png")

23
00:00:24,333 --> 00:00:24,334
89. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETextColorButton"))

24
00:00:24,731 --> 00:00:24,732
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColors_Orange"), 5)

25
00:00:24,823 --> 00:00:24,825
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColors_Orange"))

26
00:00:25,552 --> 00:00:25,553
101. delay(2)

27
00:00:27,566 --> 00:00:27,567
105. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), baseDir + "/Widgets/ColorPicker/Widgets/RTEPickerRecent.png", failed + "/Widgets/ColorPicker/Widgets/RTEPickerRecent.png")

28
00:00:28,113 --> 00:00:28,114
109. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/CloseRTEPalette"))

29
00:00:59,555 --> 00:00:59,556
113. delay(2)

30
00:01:01,564 --> 00:01:01,565
117. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETopBar"), baseDir + "/Widgets/ColorPicker/Widgets/RTEFinial.png", failed + "/Widgets/ColorPicker/Widgets/RTEFinial.png")

