1
00:00:00,117 --> 00:00:00,118
1. openBrowser("")

2
00:00:03,688 --> 00:00:03,689
5. maximizeWindow()

3
00:00:04,813 --> 00:00:04,814
9. navigateToUrl("http://localhost:3000/")

4
00:00:05,988 --> 00:00:05,988
13. driver = getExecutedBrowser().getName()

5
00:00:06,023 --> 00:00:06,024
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:06,049 --> 00:00:06,050
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:06,058 --> 00:00:06,059
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:06,480 --> 00:00:06,481
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:06,777 --> 00:00:06,779
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:07,062 --> 00:00:07,062
37. click(findTestObject("ConfigurationsTree/div_Widgetsjson"))

11
00:00:07,347 --> 00:00:07,347
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:08,260 --> 00:00:08,260
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:08,605 --> 00:00:08,605
49. delay(5)

14
00:00:13,612 --> 00:00:13,613
53. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/button_More"))

15
00:00:14,199 --> 00:00:14,200
57. delay(2)

16
00:00:16,208 --> 00:00:16,209
61. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokeButton"))

17
00:00:16,677 --> 00:00:16,679
65. delay(2)

18
00:00:18,690 --> 00:00:18,690
69. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokeDialog"), baseDir + "/Widgets/ColorPicker/Widgets/StrokeDialogInitial.png", failed + "/Widgets/ColorPicker/Widgets/StrokeDialogInitial.png")

19
00:00:18,946 --> 00:00:18,946
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/OpenStrokePalette"))

20
00:00:19,482 --> 00:00:19,483
77. delay(3)

21
00:00:22,487 --> 00:00:22,487
81. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokePalette"), baseDir + "/Widgets/ColorPicker/Widgets/StrokePaletteInitial.png", failed + "/Widgets/ColorPicker/Widgets/StrokePaletteInitial.png")

22
00:00:22,753 --> 00:00:22,753
85. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/StrokeOK"))

23
00:00:23,300 --> 00:00:23,301
89. delay(2)

24
00:00:25,307 --> 00:00:25,308
93. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokeDialog"), baseDir + "/Widgets/ColorPicker/Widgets/BackToStrokeDialog.png", failed + "/Widgets/ColorPicker/Widgets/BackToStrokeDialog.png")

25
00:00:25,558 --> 00:00:25,558
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/OpenStrokePalette"))

26
00:00:26,140 --> 00:00:26,140
101. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/StrokeColors_Yellow"))

27
00:00:26,671 --> 00:00:26,672
105. delay(2)

28
00:00:28,676 --> 00:00:28,678
109. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokeDialog"), 5)

29
00:00:33,915 --> 00:00:33,915
113. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokeDialog"), baseDir + "/Widgets/ColorPicker/Widgets/StrokeRecentAdded.png", failed + "/Widgets/ColorPicker/Widgets/StrokeRecentAdded.png")

