1
00:00:00,338 --> 00:00:00,342
1. openBrowser("")

2
00:00:04,799 --> 00:00:04,800
5. setViewPortSize(1930, 1180)

3
00:00:04,903 --> 00:00:04,907
9. navigateToUrl("http://localhost:3000/")

4
00:00:07,170 --> 00:00:07,173
13. driver = getExecutedBrowser().getName()

5
00:00:07,238 --> 00:00:07,244
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:07,264 --> 00:00:07,267
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:07,291 --> 00:00:07,292
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:08,091 --> 00:00:08,092
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:08,522 --> 00:00:08,525
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:08,925 --> 00:00:08,925
37. click(findTestObject("ConfigurationsTree/div_Palettejson"))

11
00:00:09,217 --> 00:00:09,221
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:09,506 --> 00:00:09,508
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:10,885 --> 00:00:10,886
49. delay(5)

14
00:00:15,983 --> 00:00:16,002
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:16,047 --> 00:00:16,048
57. parentElem = findWebElement(parent, 30)

16
00:00:18,927 --> 00:00:18,932
61. basePoint = parentElem.getLocation()

17
00:00:19,111 --> 00:00:19,112
65. switchToFrame(parent, 5)

18
00:00:20,279 --> 00:00:20,279
69. delay(2)

19
00:00:22,296 --> 00:00:22,297
73. if (driver == "CHROME_DRIVER")

20
00:00:22,303 --> 00:00:22,304
77. delay(1)

21
00:00:23,313 --> 00:00:23,314
81. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton"))

22
00:00:23,997 --> 00:00:24,002
85. delay(2)

23
00:00:26,067 --> 00:00:26,068
89. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), 5)

24
00:00:26,142 --> 00:00:26,143
93. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/TextColorDialog.png", failed + "/Widgets/ColorPicker/Palette/TextColorDialog.png")

25
00:00:26,675 --> 00:00:26,676
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Purple"))

26
00:00:27,656 --> 00:00:27,658
101. delay(1)

27
00:00:28,736 --> 00:00:28,737
105. switchToDefaultContent()

28
00:00:28,770 --> 00:00:28,772
109. click(findTestObject("TestStandPanel/FinishButton"))

29
00:00:29,048 --> 00:00:29,053
113. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

30
00:00:29,780 --> 00:00:29,781
117. delay(2)

31
00:00:31,797 --> 00:00:31,798
121. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png", failed + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png")

32
00:00:32,156 --> 00:00:32,156
125. click(findTestObject("TestStandPanel/BackToEditorButton"))

33
00:00:32,455 --> 00:00:32,456
129. switchToFrame(parent, 5)

34
00:00:32,548 --> 00:00:32,549
133. delay(1)

35
00:00:33,568 --> 00:00:33,568
137. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

36
00:00:34,091 --> 00:00:34,094
141. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

37
00:00:34,413 --> 00:00:34,414
145. delay(2)

38
00:00:36,442 --> 00:00:36,445
149. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png", failed + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png")

39
00:00:36,756 --> 00:00:36,756
153. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Burgundy"))

40
00:00:37,302 --> 00:00:37,302
157. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

41
00:00:37,889 --> 00:00:37,889
161. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RecentColor_Purple"))

42
00:00:38,496 --> 00:00:38,497
165. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RecentColor_Purple"))

43
00:00:38,694 --> 00:00:38,694
169. delay(2)

44
00:00:40,714 --> 00:00:40,714
173. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck2.png", failed + "/Widgets/ColorPicker/Palette/AddToRecentCheck2.png")

45
00:00:40,996 --> 00:00:40,996
177. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

46
00:00:41,486 --> 00:00:41,490
181. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

47
00:00:41,692 --> 00:00:41,693
185. delay(2)

48
00:00:43,709 --> 00:00:43,711
189. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck3.png", failed + "/Widgets/ColorPicker/Palette/AddToRecentCheck3.png")

49
00:00:44,132 --> 00:00:44,132
193. navigateToUrl("http://localhost:3000/")

50
00:00:45,471 --> 00:00:45,474
197. if (driver == "CHROME_DRIVER")

51
00:00:45,495 --> 00:00:45,497
201. click(findTestObject("ConfigurationsTree/div_base-editor"))

52
00:00:46,342 --> 00:00:46,343
205. click(findTestObject("ConfigurationsTree/div_Widgets"))

53
00:00:46,645 --> 00:00:46,647
209. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

54
00:00:46,982 --> 00:00:46,983
213. click(findTestObject("ConfigurationsTree/div_Palettejson"))

55
00:00:47,260 --> 00:00:47,261
217. click(findTestObject("TestStandPanel/button_RUN"))

56
00:00:48,497 --> 00:00:48,497
221. click(findTestObject("TestStandPanel/EditorTab"))

57
00:00:49,507 --> 00:00:49,507
225. delay(5)

58
00:00:54,518 --> 00:00:54,521
229. switchToFrame(parent, 5)

59
00:00:54,934 --> 00:00:54,934
233. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton"))

60
00:00:56,016 --> 00:00:56,017
237. delay(2)

61
00:00:58,027 --> 00:00:58,028
241. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), 5)

62
00:01:03,133 --> 00:01:03,134
245. delay(2)

63
00:01:05,141 --> 00:01:05,142
249. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/CPAtNewPage.png", failed + "/Widgets/ColorPicker/Palette/CPAtNewPage.png")

