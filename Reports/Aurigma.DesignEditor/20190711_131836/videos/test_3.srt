1
00:00:00,562 --> 00:00:00,563
1. openBrowser("")

2
00:00:03,614 --> 00:00:03,615
5. maximizeWindow()

3
00:00:03,912 --> 00:00:03,914
9. navigateToUrl("http://localhost:3000/")

4
00:00:05,104 --> 00:00:05,105
13. driver = getExecutedBrowser().getName()

5
00:00:05,116 --> 00:00:05,117
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:05,127 --> 00:00:05,130
21. click(findTestObject("ConfigurationsTree/div_base-editor"))

7
00:00:05,506 --> 00:00:05,510
25. click(findTestObject("ConfigurationsTree/div_Widgets"))

8
00:00:05,831 --> 00:00:05,831
29. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

9
00:00:06,112 --> 00:00:06,113
33. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

10
00:00:06,382 --> 00:00:06,383
37. click(findTestObject("TestStandPanel/button_RUN"))

11
00:00:07,209 --> 00:00:07,210
41. click(findTestObject("TestStandPanel/EditorTab"))

12
00:00:07,524 --> 00:00:07,524
45. delay(5)

13
00:00:12,535 --> 00:00:12,536
49. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorButton"))

14
00:00:13,211 --> 00:00:13,211
53. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 10)

15
00:00:13,823 --> 00:00:13,824
57. delay(2)

16
00:00:15,830 --> 00:00:15,830
61. Saturation = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

17
00:00:16,196 --> 00:00:16,196
65. SaturationIsSimilar = scripts.ScreenshotHandler.compareToBase(Saturation, baseDir + "/Widgets/ColorPicker/PickerRGB/SaturationChanged1.png")

18
00:00:16,227 --> 00:00:16,229
69. assert SaturationIsSimilar == true

19
00:00:16,237 --> 00:00:16,238
73. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 130)

20
00:00:16,955 --> 00:00:16,958
77. delay(2)

21
00:00:18,966 --> 00:00:18,966
81. Hue = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

22
00:00:19,375 --> 00:00:19,375
85. HueIsSimilar = scripts.ScreenshotHandler.compareToBase(Hue, baseDir + "/Widgets/ColorPicker/PickerRGB/HueChanged1.png")

23
00:00:19,408 --> 00:00:19,408
89. assert HueIsSimilar == true

24
00:00:19,419 --> 00:00:19,422
93. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 90)

25
00:00:20,126 --> 00:00:20,127
97. delay(2)

26
00:00:22,135 --> 00:00:22,135
101. Alpha = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

27
00:00:22,438 --> 00:00:22,438
105. AlphaIsSimilar = scripts.ScreenshotHandler.compareToBase(Alpha, baseDir + "/Widgets/ColorPicker/PickerRGB/PickersFirstCheck.png")

28
00:00:22,477 --> 00:00:22,478
109. assert AlphaIsSimilar == true

