1
00:00:00,253 --> 00:00:00,254
1. openBrowser("")

2
00:00:03,095 --> 00:00:03,095
5. maximizeWindow()

3
00:00:03,386 --> 00:00:03,387
9. navigateToUrl("http://localhost:3000/")

4
00:00:04,492 --> 00:00:04,492
13. driver = getExecutedBrowser().getName()

5
00:00:04,504 --> 00:00:04,505
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:04,514 --> 00:00:04,515
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:04,522 --> 00:00:04,523
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:05,002 --> 00:00:05,003
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:05,276 --> 00:00:05,277
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:05,552 --> 00:00:05,553
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:05,809 --> 00:00:05,809
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:06,610 --> 00:00:06,610
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:06,945 --> 00:00:06,946
49. delay(5)

14
00:00:11,951 --> 00:00:11,952
53. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorButton"))

15
00:00:12,747 --> 00:00:12,748
57. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

16
00:00:12,941 --> 00:00:12,941
61. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

17
00:00:13,019 --> 00:00:13,020
65. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

18
00:00:13,501 --> 00:00:13,504
69. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

19
00:00:13,601 --> 00:00:13,604
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

20
00:00:14,208 --> 00:00:14,208
77. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

21
00:00:14,312 --> 00:00:14,312
81. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

22
00:00:14,904 --> 00:00:14,905
85. delay(2)

23
00:00:16,915 --> 00:00:16,915
89. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

24
00:00:17,194 --> 00:00:17,195
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 5)

25
00:00:17,266 --> 00:00:17,267
97. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 85)

26
00:00:17,846 --> 00:00:17,847
101. delay(3)

27
00:00:20,852 --> 00:00:20,852
105. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/HueChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/HueChanged1.png")

28
00:00:21,221 --> 00:00:21,222
109. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 5)

29
00:00:21,295 --> 00:00:21,295
113. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 110)

30
00:00:22,042 --> 00:00:22,042
117. delay(2)

31
00:00:24,056 --> 00:00:24,057
121. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/PickersFirstCheck.png", failed + "/Widgets/ColorPicker/PickerCMYK/PickersFirstCheck.png")

32
00:00:24,340 --> 00:00:24,341
125. click(findTestObject("TestStandPanel/FinishButton"))

33
00:00:24,654 --> 00:00:24,654
129. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

34
00:00:24,722 --> 00:00:24,723
133. delay(2)

35
00:00:26,740 --> 00:00:26,741
137. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), baseDir + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck1.png", failed + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck1.png")

36
00:00:27,033 --> 00:00:27,033
141. click(findTestObject("TestStandPanel/BackToEditorButton"))

37
00:00:27,278 --> 00:00:27,279
145. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

38
00:00:27,424 --> 00:00:27,425
149. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

39
00:00:27,537 --> 00:00:27,538
153. delay(2)

40
00:00:29,543 --> 00:00:29,544
157. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5, 2)

41
00:00:30,081 --> 00:00:30,082
161. delay(2)

42
00:00:32,090 --> 00:00:32,092
165. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged2.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged2.png")

43
00:00:32,424 --> 00:00:32,425
169. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 5)

44
00:00:32,512 --> 00:00:32,512
173. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 20)

45
00:00:33,117 --> 00:00:33,118
177. delay(2)

46
00:00:35,124 --> 00:00:35,126
181. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/HueChanged2.png", failed + "/Widgets/ColorPicker/PickerCMYK/HueChanged2.png")

47
00:00:35,478 --> 00:00:35,479
185. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 5)

48
00:00:35,559 --> 00:00:35,560
189. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 20)

49
00:00:36,121 --> 00:00:36,121
193. delay(2)

50
00:00:38,131 --> 00:00:38,131
197. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/PickersSecondCheck.png", failed + "/Widgets/ColorPicker/PickerCMYK/PickersSecondCheck.png")

51
00:00:38,425 --> 00:00:38,425
201. click(findTestObject("TestStandPanel/FinishButton"))

52
00:00:38,777 --> 00:00:38,778
205. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

53
00:00:38,846 --> 00:00:38,847
209. delay(2)

54
00:00:40,859 --> 00:00:40,860
213. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), baseDir + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck2.png", failed + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck2.png")

55
00:00:41,167 --> 00:00:41,168
217. click(findTestObject("TestStandPanel/BackToEditorButton"))

56
00:00:41,404 --> 00:00:41,404
221. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

57
00:00:41,468 --> 00:00:41,469
225. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"), 5)

58
00:00:41,533 --> 00:00:41,533
229. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"))

59
00:00:41,898 --> 00:00:41,898
233. delay(2)

60
00:00:43,905 --> 00:00:43,905
237. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/RecentPaletteAdded.png", failed + "/Widgets/ColorPicker/PickerCMYK/RecentPaletteAdded.png")

