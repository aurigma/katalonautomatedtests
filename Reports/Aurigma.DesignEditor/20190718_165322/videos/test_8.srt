1
00:00:00,128 --> 00:00:00,129
1. openBrowser("")

2
00:00:01,358 --> 00:00:01,358
5. maximizeWindow()

3
00:00:01,489 --> 00:00:01,489
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,309 --> 00:00:03,310
13. driver = getExecutedBrowser().getName()

5
00:00:03,325 --> 00:00:03,326
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,341 --> 00:00:03,342
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:03,349 --> 00:00:03,350
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,530 --> 00:00:03,531
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,690 --> 00:00:03,691
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:03,828 --> 00:00:03,829
37. click(findTestObject("ConfigurationsTree/div_Widgetsjson"))

11
00:00:03,946 --> 00:00:03,947
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:04,031 --> 00:00:04,032
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:04,247 --> 00:00:04,247
49. delay(5)

14
00:00:09,257 --> 00:00:09,257
53. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/button_More"))

15
00:00:09,689 --> 00:00:09,690
57. delay(2)

16
00:00:11,705 --> 00:00:11,705
61. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokeButton"))

17
00:00:12,095 --> 00:00:12,096
65. delay(2)

18
00:00:14,106 --> 00:00:14,106
69. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokeDialog"), baseDir + "/Widgets/ColorPicker/Widgets/StrokeDialogInitial.png", failed + "/Widgets/ColorPicker/Widgets/StrokeDialogInitial.png")

19
00:00:14,745 --> 00:00:14,745
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/OpenStrokePalette"))

20
00:00:14,985 --> 00:00:14,986
77. delay(3)

21
00:00:17,999 --> 00:00:17,999
81. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokePalette"), baseDir + "/Widgets/ColorPicker/Widgets/StrokePaletteInitial.png", failed + "/Widgets/ColorPicker/Widgets/StrokePaletteInitial.png")

22
00:00:18,616 --> 00:00:18,616
85. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/StrokeOK"))

23
00:00:18,963 --> 00:00:18,964
89. delay(2)

24
00:00:20,974 --> 00:00:20,974
93. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokeDialog"), baseDir + "/Widgets/ColorPicker/Widgets/BackToStrokeDialog.png", failed + "/Widgets/ColorPicker/Widgets/BackToStrokeDialog.png")

25
00:00:21,562 --> 00:00:21,562
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/OpenStrokePalette"))

26
00:00:21,763 --> 00:00:21,764
101. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/StrokeColors_Yellow"))

27
00:00:22,228 --> 00:00:22,229
105. delay(2)

28
00:00:24,246 --> 00:00:24,247
109. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/StrokeDialog"), 5)

29
00:00:24,329 --> 00:00:24,329
113. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/StrokeDialog"), baseDir + "/Widgets/ColorPicker/Widgets/StrokeRecentAdded.png", failed + "/Widgets/ColorPicker/Widgets/StrokeRecentAdded.png")

