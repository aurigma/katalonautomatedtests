1
00:00:00,290 --> 00:00:00,293
1. openBrowser("")

2
00:00:02,747 --> 00:00:02,748
5. maximizeWindow()

3
00:00:02,908 --> 00:00:02,908
9. navigateToUrl("http://localhost:3000/")

4
00:00:04,423 --> 00:00:04,424
13. driver = getExecutedBrowser().getName()

5
00:00:04,465 --> 00:00:04,466
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:04,481 --> 00:00:04,482
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:04,492 --> 00:00:04,497
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:04,768 --> 00:00:04,770
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:04,949 --> 00:00:04,950
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:05,109 --> 00:00:05,110
37. click(findTestObject("ConfigurationsTree/div_Palettejson"))

11
00:00:05,300 --> 00:00:05,301
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:05,380 --> 00:00:05,386
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:05,628 --> 00:00:05,629
49. delay(7)

14
00:00:12,667 --> 00:00:12,668
53. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton"))

15
00:00:12,903 --> 00:00:12,904
57. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton"), baseDir + "/Widgets/ColorPicker/Palette/SelectColorButton.png", failed + "/Widgets/ColorPicker/Palette/SelectColorButton.png")

16
00:00:13,813 --> 00:00:13,814
61. delay(1)

17
00:00:14,822 --> 00:00:14,823
65. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton"))

18
00:00:15,276 --> 00:00:15,278
69. delay(2)

19
00:00:17,292 --> 00:00:17,292
73. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/ColorPickerDialog"), 5)

20
00:00:17,392 --> 00:00:17,394
77. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/ColorPickerDialog"), baseDir + "/Widgets/ColorPicker/Palette/TextColorDialog.png", failed + "/Widgets/ColorPicker/Palette/TextColorDialog.png")

21
00:00:18,048 --> 00:00:18,049
81. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Purple"))

22
00:00:18,518 --> 00:00:18,518
85. delay(1)

23
00:00:19,527 --> 00:00:19,527
89. click(findTestObject("TestStandPanel/FinishButton"))

24
00:00:19,668 --> 00:00:19,670
93. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

25
00:00:20,262 --> 00:00:20,263
97. delay(2)

26
00:00:22,272 --> 00:00:22,274
101. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), baseDir + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png", failed + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png")

27
00:00:22,837 --> 00:00:22,838
105. click(findTestObject("TestStandPanel/BackToEditorButton"))

28
00:00:22,966 --> 00:00:22,966
109. delay(1)

29
00:00:23,975 --> 00:00:23,976
113. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

30
00:00:24,236 --> 00:00:24,237
117. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/ColorPickerDialog"), 5)

31
00:00:24,342 --> 00:00:24,343
121. delay(2)

32
00:00:26,352 --> 00:00:26,352
125. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/ColorPickerDialog"), baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png", failed + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png")

33
00:00:26,996 --> 00:00:26,996
129. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Burgundy"))

34
00:00:27,305 --> 00:00:27,306
133. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

35
00:00:27,803 --> 00:00:27,804
137. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RecentColor_Purple"))

36
00:00:28,198 --> 00:00:28,199
141. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/ColorPickerDialog"), 5)

37
00:00:28,478 --> 00:00:28,479
145. delay(2)

38
00:00:30,488 --> 00:00:30,488
149. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/ColorPickerDialog"), baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck2.png", failed + "/Widgets/ColorPicker/Palette/AddToRecentCheck2.png")

39
00:00:31,142 --> 00:00:31,143
153. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

40
00:00:31,387 --> 00:00:31,388
157. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/ColorPickerDialog"), 5)

41
00:00:31,487 --> 00:00:31,488
161. delay(2)

42
00:00:33,497 --> 00:00:33,498
165. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/ColorPickerDialog"), baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck3.png", failed + "/Widgets/ColorPicker/Palette/AddToRecentCheck3.png")

