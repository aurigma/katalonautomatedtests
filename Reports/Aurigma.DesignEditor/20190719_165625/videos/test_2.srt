1
00:00:00,169 --> 00:00:00,170
1. openBrowser("")

2
00:00:04,028 --> 00:00:04,029
5. maximizeWindow()

3
00:00:04,306 --> 00:00:04,307
9. navigateToUrl("http://localhost:3000/")

4
00:00:05,458 --> 00:00:05,458
13. driver = getExecutedBrowser().getName()

5
00:00:05,462 --> 00:00:05,463
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:05,471 --> 00:00:05,474
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:05,498 --> 00:00:05,499
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:05,931 --> 00:00:05,932
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:06,215 --> 00:00:06,216
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:06,480 --> 00:00:06,481
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:06,740 --> 00:00:06,740
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:07,607 --> 00:00:07,608
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:08,027 --> 00:00:08,028
49. delay(5)

14
00:00:13,042 --> 00:00:13,043
53. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorButton"))

15
00:00:13,580 --> 00:00:13,581
57. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

16
00:00:13,684 --> 00:00:13,684
61. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

17
00:00:13,800 --> 00:00:13,800
65. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

18
00:00:14,223 --> 00:00:14,225
69. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

19
00:00:14,347 --> 00:00:14,347
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

20
00:00:14,989 --> 00:00:14,990
77. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

21
00:00:15,086 --> 00:00:15,087
81. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

22
00:00:15,838 --> 00:00:15,838
85. delay(2)

23
00:00:17,852 --> 00:00:17,853
89. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

24
00:00:18,137 --> 00:00:18,138
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 5)

25
00:00:18,200 --> 00:00:18,201
97. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 85)

26
00:00:18,839 --> 00:00:18,840
101. delay(3)

27
00:00:21,851 --> 00:00:21,851
105. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/HueChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/HueChanged1.png")

28
00:00:22,180 --> 00:00:22,183
109. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 5)

29
00:00:22,263 --> 00:00:22,264
113. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 110)

30
00:00:22,813 --> 00:00:22,814
117. delay(2)

31
00:00:24,825 --> 00:00:24,825
121. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/PickersFirstCheck.png", failed + "/Widgets/ColorPicker/PickerCMYK/PickersFirstCheck.png")

