1
00:00:00,280 --> 00:00:00,284
1. openBrowser("")

2
00:00:03,935 --> 00:00:03,936
5. maximizeWindow()

3
00:00:04,231 --> 00:00:04,231
9. navigateToUrl("http://localhost:3000/")

4
00:00:05,559 --> 00:00:05,562
13. driver = getExecutedBrowser().getName()

5
00:00:05,607 --> 00:00:05,608
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:05,620 --> 00:00:05,622
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:05,627 --> 00:00:05,628
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:06,134 --> 00:00:06,137
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:06,439 --> 00:00:06,440
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:06,721 --> 00:00:06,721
37. click(findTestObject("ConfigurationsTree/div_Palettejson"))

11
00:00:06,985 --> 00:00:06,985
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:07,868 --> 00:00:07,870
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:08,196 --> 00:00:08,197
49. delay(7)

14
00:00:15,230 --> 00:00:15,230
53. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton"))

15
00:00:15,424 --> 00:00:15,425
57. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton"), baseDir + "/Widgets/ColorPicker/Palette/SelectColorButton.png", failed + "/Widgets/ColorPicker/Palette/SelectColorButton.png")

16
00:00:15,895 --> 00:00:15,897
61. delay(1)

17
00:00:16,907 --> 00:00:16,908
65. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton"))

18
00:00:17,541 --> 00:00:17,542
69. delay(2)

19
00:00:19,554 --> 00:00:19,555
73. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/ColorPickerDialog"), 5)

20
00:00:19,654 --> 00:00:19,654
77. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/ColorPickerDialog"), baseDir + "/Widgets/ColorPicker/Palette/TextColorDialog.png", failed + "/Widgets/ColorPicker/Palette/TextColorDialog.png")

21
00:00:19,943 --> 00:00:19,943
81. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Purple"))

22
00:00:20,475 --> 00:00:20,476
85. delay(1)

23
00:00:21,482 --> 00:00:21,483
89. click(findTestObject("TestStandPanel/FinishButton"))

24
00:00:21,738 --> 00:00:21,739
93. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

25
00:00:21,802 --> 00:00:21,803
97. delay(2)

26
00:00:23,814 --> 00:00:23,814
101. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), baseDir + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png", failed + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png")

27
00:00:24,090 --> 00:00:24,090
105. click(findTestObject("TestStandPanel/BackToEditorButton"))

28
00:00:24,339 --> 00:00:24,341
109. delay(1)

29
00:00:25,355 --> 00:00:25,355
113. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

30
00:00:25,733 --> 00:00:25,734
117. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

31
00:00:25,902 --> 00:00:25,903
121. delay(2)

32
00:00:27,911 --> 00:00:27,911
125. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/ColorPickerDialog"), baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png", failed + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png")

