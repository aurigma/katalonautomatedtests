1
00:00:00,149 --> 00:00:00,150
1. openBrowser("")

2
00:00:02,982 --> 00:00:02,982
5. maximizeWindow()

3
00:00:03,278 --> 00:00:03,279
9. navigateToUrl("http://localhost:3000/")

4
00:00:04,536 --> 00:00:04,537
13. driver = getExecutedBrowser().getName()

5
00:00:04,546 --> 00:00:04,547
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:04,556 --> 00:00:04,557
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:04,580 --> 00:00:04,581
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:04,950 --> 00:00:04,951
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:05,237 --> 00:00:05,238
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:05,529 --> 00:00:05,529
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:05,793 --> 00:00:05,794
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:06,703 --> 00:00:06,703
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:07,030 --> 00:00:07,030
49. delay(5)

14
00:00:12,036 --> 00:00:12,037
53. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorButton"))

15
00:00:12,629 --> 00:00:12,629
57. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 10)

16
00:00:13,214 --> 00:00:13,214
61. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"))

17
00:00:13,406 --> 00:00:13,407
65. delay(2)

18
00:00:15,423 --> 00:00:15,424
69. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerRGB/SaturationChanged1.png")

19
00:00:15,727 --> 00:00:15,727
73. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 130)

20
00:00:16,412 --> 00:00:16,412
77. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"))

21
00:00:16,698 --> 00:00:16,699
81. delay(2)

22
00:00:18,713 --> 00:00:18,714
85. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/HueChanged1.png", failed + "/Widgets/ColorPicker/PickerRGB/HueChanged1.png")

23
00:00:18,996 --> 00:00:18,996
89. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 90)

24
00:00:19,605 --> 00:00:19,606
93. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"))

25
00:00:19,810 --> 00:00:19,811
97. delay(2)

26
00:00:21,818 --> 00:00:21,818
101. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/PickersFirstCheck.png", failed + "/Widgets/ColorPicker/PickerRGB/PickersFirstCheck.png")

27
00:00:22,114 --> 00:00:22,115
105. click(findTestObject("TestStandPanel/FinishButton"))

28
00:00:22,375 --> 00:00:22,376
109. delay(2)

29
00:00:24,400 --> 00:00:24,401
113. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), baseDir + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck1.png", failed + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck1.png")

30
00:00:24,654 --> 00:00:24,654
117. click(findTestObject("TestStandPanel/BackToEditorButton"))

31
00:00:24,894 --> 00:00:24,894
121. delay(2)

32
00:00:26,902 --> 00:00:26,902
125. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), -5, -2)

33
00:00:27,435 --> 00:00:27,436
129. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"))

34
00:00:27,655 --> 00:00:27,655
133. delay(2)

35
00:00:29,665 --> 00:00:29,665
137. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/SaturationChanged2.png", failed + "/Widgets/ColorPicker/PickerRGB/SaturationChanged2.png")

36
00:00:29,933 --> 00:00:29,934
141. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, -70)

37
00:00:30,505 --> 00:00:30,505
145. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"))

38
00:00:30,714 --> 00:00:30,715
149. delay(2)

39
00:00:32,724 --> 00:00:32,724
153. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/HueChanged2.png", failed + "/Widgets/ColorPicker/PickerRGB/HueChanged2.png")

40
00:00:32,990 --> 00:00:32,990
157. delay(1)

41
00:00:33,995 --> 00:00:33,996
161. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, -30)

42
00:00:34,566 --> 00:00:34,567
165. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"))

43
00:00:34,917 --> 00:00:34,918
169. delay(2)

44
00:00:36,926 --> 00:00:36,927
173. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/PickersSecondCheck.png", failed + "/Widgets/ColorPicker/PickerRGB/PickersSecondCheck.png")

45
00:00:37,202 --> 00:00:37,202
177. click(findTestObject("TestStandPanel/FinishButton"))

46
00:00:37,434 --> 00:00:37,435
181. delay(2)

47
00:00:39,444 --> 00:00:39,445
185. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), baseDir + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck2.png", failed + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck2.png")

48
00:00:39,708 --> 00:00:39,709
189. click(findTestObject("TestStandPanel/BackToEditorButton"))

49
00:00:39,940 --> 00:00:39,941
193. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"))

50
00:00:40,316 --> 00:00:40,317
197. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"))

51
00:00:40,473 --> 00:00:40,474
201. delay(3)

52
00:00:43,481 --> 00:00:43,481
205. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/RecentPaletteAdded.png", failed + "/Widgets/ColorPicker/PickerRGB/RecentPaletteAdded.png")

