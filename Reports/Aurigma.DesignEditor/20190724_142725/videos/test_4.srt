1
00:00:00,128 --> 00:00:00,128
1. openBrowser("")

2
00:00:03,856 --> 00:00:03,856
5. maximizeWindow()

3
00:00:04,132 --> 00:00:04,132
9. navigateToUrl("http://localhost:3000/")

4
00:00:05,286 --> 00:00:05,288
13. driver = getExecutedBrowser().getName()

5
00:00:05,305 --> 00:00:05,305
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:05,311 --> 00:00:05,311
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:05,321 --> 00:00:05,322
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:05,786 --> 00:00:05,787
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:06,061 --> 00:00:06,061
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:06,345 --> 00:00:06,347
37. click(findTestObject("ConfigurationsTree/div_Widgetsjson"))

11
00:00:06,604 --> 00:00:06,605
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:07,510 --> 00:00:07,510
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:07,877 --> 00:00:07,878
49. delay(5)

14
00:00:12,893 --> 00:00:12,893
53. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/AddRichText"))

15
00:00:13,437 --> 00:00:13,439
57. delay(2)

16
00:00:15,447 --> 00:00:15,447
61. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETextColorButton"))

17
00:00:16,014 --> 00:00:16,014
65. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), 5)

18
00:00:16,114 --> 00:00:16,114
69. delay(3)

19
00:00:19,121 --> 00:00:19,121
73. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), baseDir + "/Widgets/ColorPicker/Widgets/RTEPickerOpened.png", failed + "/Widgets/ColorPicker/Widgets/RTEPickerOpened.png")

20
00:00:19,316 --> 00:00:19,317
77. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorOk"))

21
00:00:19,777 --> 00:00:19,777
81. delay(2)

22
00:00:21,786 --> 00:00:21,786
85. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETopBar"), baseDir + "/Widgets/ColorPicker/Widgets/RTEPickerClosed.png", failed + "/Widgets/ColorPicker/Widgets/RTEPickerClosed.png")

23
00:00:21,970 --> 00:00:21,970
89. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETextColorButton"))

24
00:00:22,520 --> 00:00:22,520
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColors_Orange"), 5)

25
00:00:22,586 --> 00:00:22,586
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColors_Orange"))

26
00:00:23,096 --> 00:00:23,096
101. delay(2)

27
00:00:25,102 --> 00:00:25,103
105. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), baseDir + "/Widgets/ColorPicker/Widgets/RTEPickerRecent.png", failed + "/Widgets/ColorPicker/Widgets/RTEPickerRecent.png")

