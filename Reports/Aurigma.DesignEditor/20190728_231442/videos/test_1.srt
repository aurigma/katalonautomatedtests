1
00:00:00,338 --> 00:00:00,342
1. openBrowser("")

2
00:00:04,905 --> 00:00:04,906
5. setViewPortSize(1930, 1180)

3
00:00:05,003 --> 00:00:05,007
9. navigateToUrl("http://localhost:3000/")

4
00:00:07,118 --> 00:00:07,119
13. driver = getExecutedBrowser().getName()

5
00:00:07,163 --> 00:00:07,165
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:07,211 --> 00:00:07,224
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:07,253 --> 00:00:07,256
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:08,017 --> 00:00:08,021
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:08,315 --> 00:00:08,318
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:08,622 --> 00:00:08,623
37. click(findTestObject("ConfigurationsTree/div_Palettejson"))

11
00:00:08,883 --> 00:00:08,884
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:09,151 --> 00:00:09,153
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:10,793 --> 00:00:10,795
49. delay(5)

14
00:00:15,970 --> 00:00:15,973
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:16,006 --> 00:00:16,008
57. parentElem = findWebElement(parent, 30)

16
00:00:18,617 --> 00:00:18,618
61. basePoint = parentElem.getLocation()

17
00:00:20,516 --> 00:00:20,517
65. switchToFrame(parent, 5)

18
00:00:20,989 --> 00:00:20,992
69. delay(2)

19
00:00:23,009 --> 00:00:23,010
73. if (driver == "CHROME_DRIVER")

20
00:00:23,017 --> 00:00:23,018
77. delay(1)

21
00:00:24,025 --> 00:00:24,026
81. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton"))

22
00:00:24,790 --> 00:00:24,792
85. delay(2)

23
00:00:26,811 --> 00:00:26,813
89. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), 5)

24
00:00:26,882 --> 00:00:26,883
93. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/TextColorDialog.png", failed + "/Widgets/ColorPicker/Palette/TextColorDialog.png")

25
00:00:27,335 --> 00:00:27,336
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Purple"))

26
00:00:28,136 --> 00:00:28,137
101. delay(1)

27
00:00:29,152 --> 00:00:29,153
105. switchToDefaultContent()

28
00:00:29,191 --> 00:00:29,192
109. click(findTestObject("TestStandPanel/FinishButton"))

29
00:00:29,516 --> 00:00:29,520
113. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

30
00:00:30,136 --> 00:00:30,141
117. delay(2)

31
00:00:32,191 --> 00:00:32,191
121. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png", failed + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png")

32
00:00:32,519 --> 00:00:32,519
125. click(findTestObject("TestStandPanel/BackToEditorButton"))

33
00:00:32,809 --> 00:00:32,810
129. switchToFrame(parent, 5)

34
00:00:32,920 --> 00:00:32,921
133. delay(1)

35
00:00:33,940 --> 00:00:33,941
137. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

36
00:00:34,448 --> 00:00:34,450
141. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

37
00:00:34,751 --> 00:00:34,753
145. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png", failed + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png")

