1
00:00:00,142 --> 00:00:00,143
1. openBrowser("")

2
00:00:03,656 --> 00:00:03,656
5. setViewPortSize(1930, 1180)

3
00:00:03,762 --> 00:00:03,765
9. navigateToUrl("http://localhost:3000/")

4
00:00:05,393 --> 00:00:05,394
13. driver = getExecutedBrowser().getName()

5
00:00:05,405 --> 00:00:05,406
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:05,416 --> 00:00:05,416
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:05,432 --> 00:00:05,432
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:06,280 --> 00:00:06,281
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:06,576 --> 00:00:06,577
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:06,885 --> 00:00:06,885
37. click(findTestObject("ConfigurationsTree/div_Widgetsjson"))

11
00:00:07,159 --> 00:00:07,160
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:07,430 --> 00:00:07,432
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:09,150 --> 00:00:09,151
49. delay(5)

14
00:00:14,163 --> 00:00:14,167
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/Iframe")

15
00:00:14,208 --> 00:00:14,208
57. parentElem = findWebElement(parent, 30)

16
00:00:14,640 --> 00:00:14,641
61. basePoint = parentElem.getLocation()

17
00:00:14,835 --> 00:00:14,836
65. switchToFrame(parent, 5)

18
00:00:16,047 --> 00:00:16,048
69. delay(2)

19
00:00:18,075 --> 00:00:18,076
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/AddRichText"))

20
00:00:18,703 --> 00:00:18,707
77. delay(2)

21
00:00:20,736 --> 00:00:20,737
81. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETextColorButton"))

22
00:00:22,026 --> 00:00:22,027
85. delay(2)

23
00:00:24,039 --> 00:00:24,039
89. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETopBar"))

24
00:00:24,185 --> 00:00:24,185
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), 5)

25
00:00:24,281 --> 00:00:24,282
97. delay(1)

26
00:00:25,291 --> 00:00:25,293
101. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), basePoint, baseDir + "/Widgets/ColorPicker/Widgets/RTEPickerOpened.png", failed + "/Widgets/ColorPicker/Widgets/RTEPickerOpened.png")

27
00:00:25,454 --> 00:00:25,454
105. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorOk"))

28
00:00:26,002 --> 00:00:26,003
109. delay(2)

29
00:00:28,017 --> 00:00:28,019
113. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETopBar"), basePoint, baseDir + "/Widgets/ColorPicker/Widgets/RTEPickerClosed.png", failed + "/Widgets/ColorPicker/Widgets/RTEPickerClosed.png")

30
00:00:28,187 --> 00:00:28,188
117. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETextColorButton"))

31
00:00:28,768 --> 00:00:28,768
121. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColors_Orange"), 5)

32
00:00:28,820 --> 00:00:28,821
125. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColors_Orange"))

33
00:00:29,439 --> 00:00:29,439
129. delay(2)

34
00:00:31,445 --> 00:00:31,445
133. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColors_Orange"))

35
00:00:31,607 --> 00:00:31,608
137. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), basePoint, baseDir + "/Widgets/ColorPicker/Widgets/RTEPickerRecent.png", failed + "/Widgets/ColorPicker/Widgets/RTEPickerRecent.png")

