1
00:00:00,151 --> 00:00:00,151
1. openBrowser("")

2
00:00:01,276 --> 00:00:01,276
5. maximizeWindow()

3
00:00:01,409 --> 00:00:01,409
9. navigateToUrl("http://localhost:3000/")

4
00:00:02,796 --> 00:00:02,798
13. driver = getExecutedBrowser().getName()

5
00:00:02,811 --> 00:00:02,812
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:02,822 --> 00:00:02,823
21. click(findTestObject("ConfigurationsTree/div_base-editor"))

7
00:00:02,965 --> 00:00:02,966
25. click(findTestObject("ConfigurationsTree/div_Widgets"))

8
00:00:03,087 --> 00:00:03,089
29. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

9
00:00:03,223 --> 00:00:03,224
33. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

10
00:00:03,347 --> 00:00:03,348
37. click(findTestObject("TestStandPanel/button_RUN"))

11
00:00:03,418 --> 00:00:03,419
41. click(findTestObject("TestStandPanel/EditorTab"))

12
00:00:03,614 --> 00:00:03,615
45. delay(5)

13
00:00:08,634 --> 00:00:08,635
49. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorButton"))

14
00:00:09,404 --> 00:00:09,405
53. delay(2)

15
00:00:11,418 --> 00:00:11,418
57. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

16
00:00:11,688 --> 00:00:11,689
61. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

17
00:00:12,038 --> 00:00:12,039
65. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

18
00:00:12,649 --> 00:00:12,650
69. delay(2)

19
00:00:14,656 --> 00:00:14,657
73. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

