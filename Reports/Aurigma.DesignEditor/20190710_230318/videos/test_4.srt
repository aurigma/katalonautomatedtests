1
00:00:00,229 --> 00:00:00,230
1. openBrowser("")

2
00:00:01,445 --> 00:00:01,445
5. maximizeWindow()

3
00:00:01,607 --> 00:00:01,607
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,006 --> 00:00:03,006
13. driver = getExecutedBrowser().getName()

5
00:00:03,033 --> 00:00:03,034
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,050 --> 00:00:03,051
21. click(findTestObject("ConfigurationsTree/div_base-editor"))

7
00:00:03,202 --> 00:00:03,204
25. click(findTestObject("ConfigurationsTree/div_Widgets"))

8
00:00:03,328 --> 00:00:03,329
29. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

9
00:00:03,475 --> 00:00:03,476
33. click(findTestObject("ConfigurationsTree/div_Widgetsjson"))

10
00:00:03,602 --> 00:00:03,602
37. click(findTestObject("TestStandPanel/button_RUN"))

11
00:00:03,671 --> 00:00:03,673
41. click(findTestObject("TestStandPanel/EditorTab"))

12
00:00:03,862 --> 00:00:03,862
45. delay(5)

13
00:00:08,877 --> 00:00:08,878
49. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/AddRichText"))

14
00:00:09,445 --> 00:00:09,447
53. delay(4)

15
00:00:13,471 --> 00:00:13,472
57. RTEInitial = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTE"))

16
00:00:13,969 --> 00:00:13,969
61. Dialog1IsSimilar = scripts.ScreenshotHandler.compareToBase(RTEInitial, baseDir + "/Widgets/ColorPicker/Widgets/RTE.png")

17
00:00:14,097 --> 00:00:14,098
65. assert Dialog1IsSimilar == true

18
00:00:14,103 --> 00:00:14,104
69. delay(1)

19
00:00:15,113 --> 00:00:15,113
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETextColorButton"))

20
00:00:15,372 --> 00:00:15,373
77. delay(3)

21
00:00:18,379 --> 00:00:18,379
81. RTEPalette1 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTE"))

22
00:00:18,833 --> 00:00:18,834
85. Palette1IsSimilar = scripts.ScreenshotHandler.compareToBase(RTEPalette1, baseDir + "/Widgets/ColorPicker/Widgets/RTEPickerOpened.png")

23
00:00:18,970 --> 00:00:18,971
89. assert Palette1IsSimilar == true

