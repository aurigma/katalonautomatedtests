1
00:00:00,362 --> 00:00:00,368
1. openBrowser("")

2
00:00:04,933 --> 00:00:04,934
5. maximizeWindow()

3
00:00:05,230 --> 00:00:05,231
9. navigateToUrl("http://localhost:3000/")

4
00:00:06,428 --> 00:00:06,429
13. driver = getExecutedBrowser().getName()

5
00:00:06,453 --> 00:00:06,455
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:06,487 --> 00:00:06,489
21. click(findTestObject("ConfigurationsTree/div_base-editor"))

7
00:00:07,070 --> 00:00:07,070
25. click(findTestObject("ConfigurationsTree/div_Widgets"))

8
00:00:07,399 --> 00:00:07,400
29. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

9
00:00:07,700 --> 00:00:07,700
33. click(findTestObject("ConfigurationsTree/div_Palettejson"))

10
00:00:08,015 --> 00:00:08,017
37. click(findTestObject("TestStandPanel/button_RUN"))

11
00:00:09,073 --> 00:00:09,073
41. click(findTestObject("TestStandPanel/EditorTab"))

12
00:00:09,384 --> 00:00:09,386
45. delay(5)

13
00:00:14,429 --> 00:00:14,430
49. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorButton"))

14
00:00:23,126 --> 00:00:23,126
53. delay(2)

15
00:00:25,139 --> 00:00:25,140
57. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), 5)

16
00:00:30,545 --> 00:00:30,546
61. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/Palette/TextColorDialog.png")

