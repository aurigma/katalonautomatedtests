1
00:00:00,134 --> 00:00:00,134
1. openBrowser("")

2
00:00:02,969 --> 00:00:02,970
5. setViewPortSize(1930, 1180)

3
00:00:03,062 --> 00:00:03,063
9. navigateToUrl("http://localhost:3000/")

4
00:00:04,348 --> 00:00:04,350
13. driver = getExecutedBrowser().getName()

5
00:00:04,364 --> 00:00:04,365
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:04,430 --> 00:00:04,432
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:04,441 --> 00:00:04,443
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:05,021 --> 00:00:05,022
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:05,327 --> 00:00:05,328
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:05,607 --> 00:00:05,607
37. click(findTestObject("ConfigurationsTree/div_Widgetsjson"))

11
00:00:05,859 --> 00:00:05,859
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:06,889 --> 00:00:06,891
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:07,283 --> 00:00:07,286
49. delay(5)

14
00:00:12,307 --> 00:00:12,311
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/Iframe")

15
00:00:12,331 --> 00:00:12,332
57. parentElem = findWebElement(parent, 30)

16
00:00:13,095 --> 00:00:13,096
61. basePoint = parentElem.getLocation()

17
00:00:13,216 --> 00:00:13,216
65. switchToFrame(parent, 5)

18
00:00:13,484 --> 00:00:13,485
69. delay(2)

19
00:00:15,496 --> 00:00:15,497
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/AddRichText"))

20
00:00:16,066 --> 00:00:16,068
77. delay(2)

21
00:00:18,102 --> 00:00:18,103
81. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETextColorButton"))

22
00:00:18,512 --> 00:00:18,513
85. delay(2)

23
00:00:20,521 --> 00:00:20,521
89. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETopBar"))

24
00:00:20,669 --> 00:00:20,669
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), 5)

25
00:00:20,724 --> 00:00:20,727
97. delay(1)

26
00:00:21,741 --> 00:00:21,742
101. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), basePoint, baseDir + "/Widgets/ColorPicker/Widgets/RTEPickerOpened.png", failed + "/Widgets/ColorPicker/Widgets/RTEPickerOpened.png")

27
00:00:21,906 --> 00:00:21,906
105. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorOk"))

28
00:00:22,393 --> 00:00:22,394
109. delay(2)

29
00:00:24,410 --> 00:00:24,411
113. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETopBar"), basePoint, baseDir + "/Widgets/ColorPicker/Widgets/RTEPickerClosed.png", failed + "/Widgets/ColorPicker/Widgets/RTEPickerClosed.png")

30
00:00:24,549 --> 00:00:24,549
117. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETextColorButton"))

31
00:00:25,099 --> 00:00:25,100
121. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColors_Orange"), 5)

32
00:00:25,141 --> 00:00:25,141
125. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColors_Orange"))

33
00:00:25,703 --> 00:00:25,706
129. delay(2)

34
00:00:27,727 --> 00:00:27,727
133. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), basePoint, baseDir + "/Widgets/ColorPicker/Widgets/RTEPickerRecent.png", failed + "/Widgets/ColorPicker/Widgets/RTEPickerRecent.png")

