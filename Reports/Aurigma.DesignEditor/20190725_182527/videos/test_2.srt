1
00:00:00,269 --> 00:00:00,270
1. openBrowser("")

2
00:00:01,437 --> 00:00:01,438
5. setViewPortSize(1930, 1180)

3
00:00:01,610 --> 00:00:01,610
9. navigateToUrl("http://localhost:3000/")

4
00:00:02,977 --> 00:00:02,978
13. driver = getExecutedBrowser().getName()

5
00:00:02,995 --> 00:00:02,997
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,005 --> 00:00:03,006
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:03,014 --> 00:00:03,014
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,232 --> 00:00:03,234
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,361 --> 00:00:03,362
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:03,549 --> 00:00:03,550
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:03,671 --> 00:00:03,672
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:03,749 --> 00:00:03,752
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:03,947 --> 00:00:03,949
49. delay(5)

14
00:00:08,961 --> 00:00:08,962
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:08,987 --> 00:00:08,987
57. parentElem = findWebElement(parent, 30)

16
00:00:09,068 --> 00:00:09,069
61. basePoint = parentElem.getLocation()

17
00:00:09,078 --> 00:00:09,078
65. switchToFrame(parent, 5)

18
00:00:09,128 --> 00:00:09,128
69. delay(2)

19
00:00:11,140 --> 00:00:11,141
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:11,552 --> 00:00:11,552
77. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

21
00:00:11,597 --> 00:00:11,598
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

22
00:00:11,641 --> 00:00:11,641
85. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

23
00:00:11,941 --> 00:00:11,941
89. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

24
00:00:12,017 --> 00:00:12,017
93. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

25
00:00:12,275 --> 00:00:12,275
97. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

26
00:00:12,376 --> 00:00:12,376
101. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

27
00:00:12,846 --> 00:00:12,848
105. delay(2)

28
00:00:14,914 --> 00:00:14,914
109. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

