1
00:00:00,270 --> 00:00:00,271
1. openBrowser("")

2
00:00:02,108 --> 00:00:02,108
5. setViewPortSize(1930, 1180)

3
00:00:02,228 --> 00:00:02,229
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,544 --> 00:00:03,545
13. driver = getExecutedBrowser().getName()

5
00:00:03,561 --> 00:00:03,562
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,583 --> 00:00:03,586
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:03,607 --> 00:00:03,610
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,783 --> 00:00:03,784
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,967 --> 00:00:03,969
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:04,130 --> 00:00:04,131
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:04,288 --> 00:00:04,289
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:04,382 --> 00:00:04,382
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:04,582 --> 00:00:04,584
49. delay(5)

14
00:00:09,602 --> 00:00:09,603
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:09,619 --> 00:00:09,620
57. parentElem = findWebElement(parent, 30)

16
00:00:09,711 --> 00:00:09,712
61. basePoint = parentElem.getLocation()

17
00:00:09,728 --> 00:00:09,728
65. switchToFrame(parent, 5)

18
00:00:09,765 --> 00:00:09,766
69. delay(2)

19
00:00:11,779 --> 00:00:11,782
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:12,219 --> 00:00:12,219
77. delay(4)

21
00:00:16,240 --> 00:00:16,240
81. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 10)

22
00:00:16,547 --> 00:00:16,547
85. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"))

23
00:00:16,689 --> 00:00:16,690
89. delay(2)

24
00:00:18,710 --> 00:00:18,710
93. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerRGB/SaturationChanged1.png")

25
00:00:19,371 --> 00:00:19,374
97. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 130)

26
00:00:19,744 --> 00:00:19,744
101. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"))

27
00:00:19,838 --> 00:00:19,838
105. delay(2)

28
00:00:21,845 --> 00:00:21,846
109. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/HueChanged1.png", failed + "/Widgets/ColorPicker/PickerRGB/HueChanged1.png")

29
00:00:22,510 --> 00:00:22,511
113. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 90)

30
00:00:22,845 --> 00:00:22,846
117. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"))

31
00:00:22,942 --> 00:00:22,943
121. delay(2)

32
00:00:24,950 --> 00:00:24,951
125. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/PickersFirstCheck.png", failed + "/Widgets/ColorPicker/PickerRGB/PickersFirstCheck.png")

33
00:00:25,632 --> 00:00:25,633
129. switchToDefaultContent()

34
00:00:25,660 --> 00:00:25,661
133. click(findTestObject("TestStandPanel/FinishButton"))

35
00:00:25,826 --> 00:00:25,826
137. delay(2)

36
00:00:27,833 --> 00:00:27,834
141. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), baseDir + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck1.png", failed + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck1.png")

37
00:00:28,476 --> 00:00:28,478
145. click(findTestObject("TestStandPanel/BackToEditorButton"))

38
00:00:28,622 --> 00:00:28,623
149. switchToFrame(parent, 5)

39
00:00:28,664 --> 00:00:28,665
153. delay(2)

40
00:00:30,681 --> 00:00:30,682
157. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), -5, -2)

41
00:00:31,117 --> 00:00:31,117
161. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"))

42
00:00:31,289 --> 00:00:31,290
165. delay(2)

43
00:00:33,309 --> 00:00:33,309
169. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/SaturationChanged2.png", failed + "/Widgets/ColorPicker/PickerRGB/SaturationChanged2.png")

44
00:00:33,909 --> 00:00:33,909
173. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, -70)

45
00:00:34,141 --> 00:00:34,142
177. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"))

46
00:00:34,235 --> 00:00:34,236
181. delay(2)

47
00:00:36,240 --> 00:00:36,241
185. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/HueChanged2.png", failed + "/Widgets/ColorPicker/PickerRGB/HueChanged2.png")

48
00:00:36,931 --> 00:00:36,932
189. delay(1)

49
00:00:37,942 --> 00:00:37,943
193. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, -30)

50
00:00:38,349 --> 00:00:38,353
197. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"))

51
00:00:38,492 --> 00:00:38,492
201. delay(2)

52
00:00:40,500 --> 00:00:40,501
205. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/PickersSecondCheck.png", failed + "/Widgets/ColorPicker/PickerRGB/PickersSecondCheck.png")

53
00:00:41,161 --> 00:00:41,162
209. switchToDefaultContent()

54
00:00:41,187 --> 00:00:41,188
213. click(findTestObject("TestStandPanel/FinishButton"))

55
00:00:41,317 --> 00:00:41,318
217. delay(2)

56
00:00:43,332 --> 00:00:43,333
221. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), baseDir + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck2.png", failed + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck2.png")

57
00:00:43,956 --> 00:00:43,962
225. click(findTestObject("TestStandPanel/BackToEditorButton"))

58
00:00:44,090 --> 00:00:44,091
229. switchToFrame(parent, 5)

59
00:00:44,191 --> 00:00:44,191
233. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"))

60
00:00:44,649 --> 00:00:44,650
237. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"))

61
00:00:44,763 --> 00:00:44,764
241. delay(3)

62
00:00:47,771 --> 00:00:47,771
245. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/RecentPaletteAdded.png", failed + "/Widgets/ColorPicker/PickerRGB/RecentPaletteAdded.png")

63
00:00:48,434 --> 00:00:48,434
249. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_R_Channel"))

64
00:00:48,648 --> 00:00:48,649
253. delay(2)

65
00:00:50,663 --> 00:00:50,663
257. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/ChannelSelectedLightning.png", failed + "/Widgets/ColorPicker/PickerRGB/ChannelSelectedLightning.png")

66
00:00:51,336 --> 00:00:51,336
261. if (driver == "CHROME_DRIVER" || driver == "HEADLESS_DRIVER")

67
00:00:51,342 --> 00:00:51,342
1. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_R_Channel"), "25")

68
00:00:51,541 --> 00:00:51,542
5. delay(2)

69
00:00:53,557 --> 00:00:53,558
9. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/RChannelValidInput1.png", failed + "/Widgets/ColorPicker/PickerRGB/RChannelValidInput1.png")

