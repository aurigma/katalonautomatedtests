1
00:00:00,189 --> 00:00:00,190
1. openBrowser("")

2
00:00:01,372 --> 00:00:01,372
5. setViewPortSize(1930, 1180)

3
00:00:01,497 --> 00:00:01,497
9. navigateToUrl("http://localhost:3000/")

4
00:00:02,665 --> 00:00:02,665
13. driver = getExecutedBrowser().getName()

5
00:00:02,686 --> 00:00:02,687
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:02,710 --> 00:00:02,711
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:02,725 --> 00:00:02,725
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:02,855 --> 00:00:02,856
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:02,985 --> 00:00:02,985
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:03,132 --> 00:00:03,134
37. click(findTestObject("ConfigurationsTree/div_SectionCollapsejson"))

11
00:00:03,280 --> 00:00:03,281
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:03,379 --> 00:00:03,380
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:03,573 --> 00:00:03,574
49. delay(5)

14
00:00:08,596 --> 00:00:08,598
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:08,661 --> 00:00:08,662
57. parentElem = findWebElement(parent, 30)

16
00:00:09,000 --> 00:00:09,001
61. basePoint = parentElem.getLocation()

17
00:00:09,017 --> 00:00:09,017
65. switchToFrame(parent, 5)

18
00:00:09,437 --> 00:00:09,437
69. delay(2)

19
00:00:11,449 --> 00:00:11,449
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

20
00:00:11,775 --> 00:00:11,775
77. delay(2)

21
00:00:13,782 --> 00:00:13,782
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), 3)

22
00:00:13,811 --> 00:00:13,812
85. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/PickerWithCollapseSections.png", failed + "/Widgets/ColorPicker/SectionCollapse/PickerWithCollapseSections.png")

23
00:00:14,433 --> 00:00:14,433
89. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/GrayscaleSectionCollapse"))

24
00:00:14,596 --> 00:00:14,596
93. delay(2)

25
00:00:16,608 --> 00:00:16,609
97. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/GrayscaleCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/GrayscaleCollapsed.png")

26
00:00:17,279 --> 00:00:17,279
101. delay(2)

27
00:00:19,292 --> 00:00:19,292
105. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RgbSectionCollapse"))

28
00:00:19,643 --> 00:00:19,644
109. delay(2)

29
00:00:21,662 --> 00:00:21,662
113. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/RGBCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/RGBCollapsed.png")

30
00:00:22,316 --> 00:00:22,316
117. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/CmykSectionCollapse"))

31
00:00:22,665 --> 00:00:22,666
121. delay(2)

32
00:00:24,694 --> 00:00:24,695
125. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CMYKCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/CMYKCollapsed.png")

33
00:00:25,372 --> 00:00:25,375
129. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RecentSectionCollapse"))

34
00:00:25,569 --> 00:00:25,570
133. delay(2)

35
00:00:27,584 --> 00:00:27,584
137. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/AllCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/AllCollapsed.png")

36
00:00:28,257 --> 00:00:28,258
141. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

37
00:00:28,632 --> 00:00:28,633
145. delay(1)

38
00:00:29,647 --> 00:00:29,647
149. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

39
00:00:29,994 --> 00:00:29,997
153. delay(2)

40
00:00:32,008 --> 00:00:32,008
157. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved.png")

41
00:00:32,656 --> 00:00:32,656
161. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/button_OK"))

42
00:00:32,909 --> 00:00:32,909
165. delay(1)

43
00:00:33,922 --> 00:00:33,923
169. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

44
00:00:34,275 --> 00:00:34,276
173. delay(2)

45
00:00:36,287 --> 00:00:36,287
177. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved2.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved2.png")

46
00:00:36,943 --> 00:00:36,946
181. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RecentSectionCollapse"))

47
00:00:37,272 --> 00:00:37,273
185. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RgbSectionCollapse"))

48
00:00:37,749 --> 00:00:37,751
189. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/OIPickAnotherItem"))

49
00:00:38,351 --> 00:00:38,352
193. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

50
00:00:38,946 --> 00:00:38,949
197. delay(2)

51
00:00:40,960 --> 00:00:40,961
201. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved3.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved3.png")

