1
00:00:00,331 --> 00:00:00,332
1. openBrowser("")

2
00:00:02,189 --> 00:00:02,190
5. setViewPortSize(1930, 1180)

3
00:00:02,313 --> 00:00:02,315
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,535 --> 00:00:03,536
13. driver = getExecutedBrowser().getName()

5
00:00:03,545 --> 00:00:03,546
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,597 --> 00:00:03,603
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:03,618 --> 00:00:03,626
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,776 --> 00:00:03,777
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,993 --> 00:00:03,994
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:04,147 --> 00:00:04,150
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:04,287 --> 00:00:04,288
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:04,382 --> 00:00:04,383
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:04,635 --> 00:00:04,635
49. delay(5)

14
00:00:09,656 --> 00:00:09,657
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:09,676 --> 00:00:09,679
57. parentElem = findWebElement(parent, 30)

16
00:00:09,806 --> 00:00:09,807
61. basePoint = parentElem.getLocation()

17
00:00:09,830 --> 00:00:09,831
65. switchToFrame(parent, 5)

18
00:00:09,896 --> 00:00:09,897
69. delay(2)

19
00:00:11,907 --> 00:00:11,909
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:12,375 --> 00:00:12,376
77. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

21
00:00:12,474 --> 00:00:12,474
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

22
00:00:12,531 --> 00:00:12,532
85. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

23
00:00:12,833 --> 00:00:12,834
89. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

24
00:00:12,910 --> 00:00:12,912
93. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

25
00:00:13,311 --> 00:00:13,312
97. delay(1)

26
00:00:14,329 --> 00:00:14,331
101. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

27
00:00:14,391 --> 00:00:14,391
105. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

28
00:00:14,775 --> 00:00:14,775
109. delay(2)

29
00:00:16,800 --> 00:00:16,802
113. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

30
00:00:17,724 --> 00:00:17,728
117. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 5)

31
00:00:17,821 --> 00:00:17,823
121. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 85)

32
00:00:18,260 --> 00:00:18,263
125. delay(3)

33
00:00:21,288 --> 00:00:21,289
129. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/HueChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/HueChanged1.png")

34
00:00:22,017 --> 00:00:22,019
133. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 5)

35
00:00:22,085 --> 00:00:22,087
137. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 110)

36
00:00:22,431 --> 00:00:22,432
141. delay(2)

37
00:00:24,450 --> 00:00:24,451
145. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/PickersFirstCheck.png", failed + "/Widgets/ColorPicker/PickerCMYK/PickersFirstCheck.png")

38
00:00:25,135 --> 00:00:25,137
149. switchToDefaultContent()

39
00:00:25,184 --> 00:00:25,185
153. click(findTestObject("TestStandPanel/FinishButton"))

40
00:00:25,333 --> 00:00:25,335
157. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

41
00:00:25,934 --> 00:00:25,935
161. delay(2)

42
00:00:27,944 --> 00:00:27,945
165. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck1.png", failed + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck1.png")

43
00:00:28,766 --> 00:00:28,767
169. click(findTestObject("TestStandPanel/BackToEditorButton"))

44
00:00:28,880 --> 00:00:28,882
173. switchToFrame(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe"), 5)

45
00:00:28,988 --> 00:00:28,988
177. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

46
00:00:29,043 --> 00:00:29,044
181. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

47
00:00:29,098 --> 00:00:29,099
185. delay(2)

48
00:00:31,116 --> 00:00:31,119
189. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5, 2)

49
00:00:31,489 --> 00:00:31,490
193. delay(2)

50
00:00:33,500 --> 00:00:33,501
197. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged2.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged2.png")

51
00:00:34,399 --> 00:00:34,400
201. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 5)

52
00:00:34,455 --> 00:00:34,455
205. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 20)

53
00:00:34,802 --> 00:00:34,802
209. delay(2)

54
00:00:36,811 --> 00:00:36,812
213. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/HueChanged2.png", failed + "/Widgets/ColorPicker/PickerCMYK/HueChanged2.png")

55
00:00:37,518 --> 00:00:37,520
217. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 5)

56
00:00:37,572 --> 00:00:37,572
221. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 20)

57
00:00:37,878 --> 00:00:37,879
225. delay(2)

58
00:00:39,897 --> 00:00:39,898
229. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/PickersSecondCheck.png", failed + "/Widgets/ColorPicker/PickerCMYK/PickersSecondCheck.png")

59
00:00:40,578 --> 00:00:40,578
233. switchToDefaultContent()

60
00:00:40,610 --> 00:00:40,611
237. click(findTestObject("TestStandPanel/FinishButton"))

61
00:00:40,753 --> 00:00:40,754
241. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

62
00:00:40,810 --> 00:00:40,810
245. delay(2)

63
00:00:42,824 --> 00:00:42,826
249. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck2.png", failed + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck2.png")

64
00:00:43,477 --> 00:00:43,477
253. click(findTestObject("TestStandPanel/BackToEditorButton"))

65
00:00:43,614 --> 00:00:43,614
257. switchToFrame(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe"), 5)

66
00:00:43,674 --> 00:00:43,675
261. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

67
00:00:43,720 --> 00:00:43,721
265. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"), 5)

68
00:00:43,771 --> 00:00:43,771
269. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"))

69
00:00:44,004 --> 00:00:44,004
273. delay(2)

70
00:00:46,030 --> 00:00:46,030
277. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"))

71
00:00:46,127 --> 00:00:46,128
281. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/RecentPaletteAdded.png", failed + "/Widgets/ColorPicker/PickerCMYK/RecentPaletteAdded.png")

72
00:00:46,845 --> 00:00:46,846
285. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel"), 5)

73
00:00:46,899 --> 00:00:46,899
289. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel"))

74
00:00:47,153 --> 00:00:47,154
293. delay(2)

75
00:00:49,176 --> 00:00:49,177
297. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/ChannelSelectedLightning.png", failed + "/Widgets/ColorPicker/PickerCMYK/ChannelSelectedLightning.png")

76
00:00:49,862 --> 00:00:49,863
301. if (driver == "CHROME_DRIVER" || driver == "HEADLESS_DRIVER")

77
00:00:49,870 --> 00:00:49,870
1. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel"), "67")

78
00:00:50,089 --> 00:00:50,090
5. delay(2)

79
00:00:52,112 --> 00:00:52,112
9. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/CChannelValidInput1.png", failed + "/Widgets/ColorPicker/PickerCMYK/CChannelValidInput1.png")

