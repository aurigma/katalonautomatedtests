1
00:00:00,660 --> 00:00:00,661
1. openBrowser("")

2
00:00:04,512 --> 00:00:04,512
5. maximizeWindow()

3
00:00:05,839 --> 00:00:05,839
9. navigateToUrl("http://localhost:3000/")

4
00:00:06,974 --> 00:00:06,975
13. driver = getExecutedBrowser().getName()

5
00:00:06,987 --> 00:00:06,988
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:07,002 --> 00:00:07,004
21. click(findTestObject("ConfigurationsTree/div_base-editor"))

7
00:00:07,301 --> 00:00:07,302
25. click(findTestObject("ConfigurationsTree/div_Widgets"))

8
00:00:07,613 --> 00:00:07,613
29. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

9
00:00:07,893 --> 00:00:07,893
33. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

10
00:00:08,151 --> 00:00:08,151
37. click(findTestObject("TestStandPanel/button_RUN"))

11
00:00:09,036 --> 00:00:09,036
41. click(findTestObject("TestStandPanel/EditorTab"))

12
00:00:09,436 --> 00:00:09,437
45. delay(5)

13
00:00:14,451 --> 00:00:14,451
49. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorButton"))

14
00:00:15,001 --> 00:00:15,002
53. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 10)

15
00:00:15,639 --> 00:00:15,640
57. delay(2)

16
00:00:17,645 --> 00:00:17,646
61. Saturation = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

17
00:00:18,008 --> 00:00:18,008
65. SaturationIsSimilar = scripts.ScreenshotHandler.compareToBase(Saturation, baseDir + "/Widgets/ColorPicker/PickerRGB/SaturationChanged1.png")

18
00:00:18,030 --> 00:00:18,031
69. assert SaturationIsSimilar == true

19
00:00:18,038 --> 00:00:18,039
73. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 130)

20
00:00:18,747 --> 00:00:18,747
77. delay(2)

21
00:00:20,757 --> 00:00:20,757
81. Hue = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

22
00:00:21,022 --> 00:00:21,023
85. HueIsSimilar = scripts.ScreenshotHandler.compareToBase(Hue, baseDir + "/Widgets/ColorPicker/PickerRGB/HueChanged1.png")

23
00:00:21,043 --> 00:00:21,044
89. assert HueIsSimilar == true

24
00:00:21,047 --> 00:00:21,048
93. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 90)

25
00:00:21,635 --> 00:00:21,637
97. delay(2)

26
00:00:23,659 --> 00:00:23,660
101. Alpha = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

27
00:00:23,916 --> 00:00:23,916
105. AlphaIsSimilar = scripts.ScreenshotHandler.compareToBase(Alpha, baseDir + "/Widgets/ColorPicker/PickerRGB/PickersFirstCheck.png")

28
00:00:23,940 --> 00:00:23,941
109. assert AlphaIsSimilar == true

29
00:00:23,946 --> 00:00:23,947
113. click(findTestObject("TestStandPanel/FinishButton"))

30
00:00:24,270 --> 00:00:24,270
117. delay(2)

31
00:00:26,284 --> 00:00:26,284
121. AppliedColor1 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("TestStandPanel/PreviewImage"))

32
00:00:26,498 --> 00:00:26,499
125. AppliedColor1IsSimilar = scripts.ScreenshotHandler.compareToBase(AppliedColor1, baseDir + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck1.png")

33
00:00:26,560 --> 00:00:26,562
129. assert AppliedColor1IsSimilar == true

34
00:00:26,569 --> 00:00:26,570
133. click(findTestObject("TestStandPanel/BackToEditorButton"))

35
00:00:26,812 --> 00:00:26,812
137. delay(2)

36
00:00:28,824 --> 00:00:28,824
141. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), -5, -2)

37
00:00:29,363 --> 00:00:29,365
145. delay(2)

38
00:00:31,376 --> 00:00:31,377
149. Saturation2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

39
00:00:31,687 --> 00:00:31,688
153. Saturation2IsSimilar = scripts.ScreenshotHandler.compareToBase(Saturation2, baseDir + "/Widgets/ColorPicker/PickerRGB/SaturationChanged2.png")

40
00:00:31,708 --> 00:00:31,708
157. assert Saturation2IsSimilar == true

41
00:00:31,715 --> 00:00:31,716
161. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, -70)

42
00:00:32,283 --> 00:00:32,283
165. delay(2)

43
00:00:34,289 --> 00:00:34,290
169. Hue2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

44
00:00:34,553 --> 00:00:34,553
173. Hue2IsSimilar = scripts.ScreenshotHandler.compareToBase(Hue2, baseDir + "/Widgets/ColorPicker/PickerRGB/HueChanged2.png")

45
00:00:34,575 --> 00:00:34,575
177. assert Hue2IsSimilar == true

46
00:00:34,579 --> 00:00:34,579
181. delay(1)

47
00:00:35,584 --> 00:00:35,585
185. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, -30)

48
00:00:36,139 --> 00:00:36,140
189. delay(2)

49
00:00:38,151 --> 00:00:38,152
193. Alpha2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

50
00:00:38,405 --> 00:00:38,405
197. Alpha2IsSimilar = scripts.ScreenshotHandler.compareToBase(Alpha2, baseDir + "/Widgets/ColorPicker/PickerRGB/PickersSecondCheck.png")

51
00:00:38,424 --> 00:00:38,424
201. assert Alpha2IsSimilar == true

52
00:00:38,432 --> 00:00:38,433
205. click(findTestObject("TestStandPanel/FinishButton"))

53
00:00:38,738 --> 00:00:38,739
209. delay(2)

54
00:00:40,749 --> 00:00:40,750
213. AppliedColor2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("TestStandPanel/PreviewImage"))

55
00:00:40,995 --> 00:00:40,995
217. AppliedColor2IsSimilar = scripts.ScreenshotHandler.compareToBase(AppliedColor2, baseDir + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck2.png")

56
00:00:41,058 --> 00:00:41,058
221. assert AppliedColor2IsSimilar == true

57
00:00:41,067 --> 00:00:41,067
225. click(findTestObject("TestStandPanel/BackToEditorButton"))

58
00:00:41,315 --> 00:00:41,316
229. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"))

59
00:00:41,793 --> 00:00:41,795
233. delay(1)

60
00:00:42,804 --> 00:00:42,805
237. RecentColor = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

61
00:00:43,071 --> 00:00:43,071
241. RecentColorIsSimilar = scripts.ScreenshotHandler.compareToBase(RecentColor, baseDir + "/Widgets/ColorPicker/PickerRGB/RecentPaletteAdded.png")

62
00:00:43,092 --> 00:00:43,093
245. assert RecentColorIsSimilar == true

63
00:00:43,099 --> 00:00:43,100
249. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_R_Channel"))

64
00:00:43,623 --> 00:00:43,623
253. delay(2)

65
00:00:45,635 --> 00:00:45,636
257. Lightning = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

66
00:00:45,903 --> 00:00:45,904
261. LightningIsSimilar = scripts.ScreenshotHandler.compareToBase(Lightning, baseDir + "/Widgets/ColorPicker/PickerRGB/ChannelSelectedLightning.png")

67
00:00:45,923 --> 00:00:45,923
265. assert LightningIsSimilar == true

68
00:00:45,927 --> 00:00:45,927
269. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_R_Channel"), "25")

69
00:00:46,064 --> 00:00:46,065
273. delay(2)

70
00:00:48,072 --> 00:00:48,073
277. ValidR1 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

71
00:00:48,501 --> 00:00:48,501
281. ValidR1IsSimilar = scripts.ScreenshotHandler.compareToBase(ValidR1, baseDir + "/Widgets/ColorPicker/PickerRGB/RChannelValidInput1.png")

72
00:00:48,539 --> 00:00:48,540
285. assert ValidR1IsSimilar == true

73
00:00:48,548 --> 00:00:48,548
289. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_R_Channel"), "abc")

74
00:00:48,722 --> 00:00:48,723
293. delay(3)

75
00:00:51,731 --> 00:00:51,732
297. InvalidR1 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

76
00:00:52,004 --> 00:00:52,004
301. InvalidR1IsSimilar = scripts.ScreenshotHandler.compareToBase(InvalidR1, baseDir + "/Widgets/ColorPicker/PickerRGB/RChannelInvalidInput1.png")

77
00:00:52,023 --> 00:00:52,024
305. assert InvalidR1IsSimilar == true

78
00:00:52,030 --> 00:00:52,031
309. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_R_Channel"), "76")

79
00:00:52,147 --> 00:00:52,148
313. delay(2)

80
00:00:54,155 --> 00:00:54,156
317. ValidR2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

81
00:00:54,417 --> 00:00:54,418
321. ValidR2IsSimilar = scripts.ScreenshotHandler.compareToBase(ValidR2, baseDir + "/Widgets/ColorPicker/PickerRGB/RChannelValidInput2.png")

82
00:00:54,439 --> 00:00:54,439
325. assert ValidR2IsSimilar == true

83
00:00:54,443 --> 00:00:54,444
329. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_R_Channel"), "!@")

84
00:00:54,549 --> 00:00:54,550
333. delay(2)

85
00:00:56,562 --> 00:00:56,562
337. InvalidR2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

86
00:00:56,854 --> 00:00:56,854
341. InvalidR2IsSimilar = scripts.ScreenshotHandler.compareToBase(InvalidR2, baseDir + "/Widgets/ColorPicker/PickerRGB/RChannelInvalidInput2.png")

87
00:00:56,877 --> 00:00:56,877
345. assert InvalidR2IsSimilar == true

