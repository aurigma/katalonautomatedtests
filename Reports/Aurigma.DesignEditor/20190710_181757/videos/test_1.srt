1
00:00:00,596 --> 00:00:00,600
1. openBrowser("")

2
00:00:04,517 --> 00:00:04,519
5. maximizeWindow()

3
00:00:04,876 --> 00:00:04,876
9. navigateToUrl("http://localhost:3000/")

4
00:00:06,128 --> 00:00:06,129
13. driver = getExecutedBrowser().getName()

5
00:00:06,196 --> 00:00:06,198
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:06,223 --> 00:00:06,231
21. click(findTestObject("Object Repository/ConfigurationsTree/div_base-editor"))

7
00:00:06,591 --> 00:00:06,592
25. click(findTestObject("Object Repository/ConfigurationsTree/div_Widgets"))

8
00:00:06,930 --> 00:00:06,930
29. click(findTestObject("Object Repository/ConfigurationsTree/div_ColorPicker"))

9
00:00:07,218 --> 00:00:07,219
33. click(findTestObject("Object Repository/ConfigurationsTree/div_Palettejson"))

10
00:00:07,491 --> 00:00:07,492
37. click(findTestObject("Object Repository/TestStandPanel/button_RUN"))

11
00:00:08,428 --> 00:00:08,429
41. click(findTestObject("TestStandPanel/EditorTab"))

12
00:00:08,766 --> 00:00:08,768
45. delay(5)

13
00:00:13,802 --> 00:00:13,803
49. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorButton"))

14
00:00:14,412 --> 00:00:14,412
53. delay(2)

15
00:00:16,417 --> 00:00:16,418
57. CPInitial = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"))

16
00:00:16,827 --> 00:00:16,828
61. CPInitialIsSimilar = scripts.ScreenshotHandler.compareToBase(CPInitial, baseDir + "/Widgets/ColorPicker/Palette/TextColorDialog.png")

17
00:00:16,879 --> 00:00:16,880
65. assert CPInitialIsSimilar == true

18
00:00:16,891 --> 00:00:16,893
69. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Purple"))

19
00:00:17,436 --> 00:00:17,437
73. delay(1)

20
00:00:18,447 --> 00:00:18,448
77. click(findTestObject("TestStandPanel/FinishButton"))

21
00:00:18,701 --> 00:00:18,703
81. delay(2)

22
00:00:20,730 --> 00:00:20,730
85. CPChanged = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("TestStandPanel/PreviewImage"))

23
00:00:20,970 --> 00:00:20,970
89. CPChangedIsSimilar = scripts.ScreenshotHandler.compareToBase(CPChanged, baseDir + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png")

24
00:00:21,031 --> 00:00:21,032
93. assert CPChangedIsSimilar == true

25
00:00:21,040 --> 00:00:21,042
97. click(findTestObject("TestStandPanel/BackToEditorButton"))

26
00:00:21,302 --> 00:00:21,302
101. delay(1)

27
00:00:22,308 --> 00:00:22,308
105. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

28
00:00:22,739 --> 00:00:22,740
109. delay(2)

29
00:00:24,774 --> 00:00:24,775
113. RecentColorPurple = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"))

30
00:00:25,085 --> 00:00:25,086
117. PurpleIsSimilar = scripts.ScreenshotHandler.compareToBase(RecentColorPurple, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png")

31
00:00:25,116 --> 00:00:25,117
121. assert PurpleIsSimilar == true

