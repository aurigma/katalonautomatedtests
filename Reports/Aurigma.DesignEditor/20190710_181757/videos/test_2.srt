1
00:00:00,673 --> 00:00:00,673
1. openBrowser("")

2
00:00:04,382 --> 00:00:04,382
5. maximizeWindow()

3
00:00:04,664 --> 00:00:04,665
9. navigateToUrl("http://localhost:3000/")

4
00:00:05,846 --> 00:00:05,847
13. driver = getExecutedBrowser().getName()

5
00:00:05,863 --> 00:00:05,864
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:05,873 --> 00:00:05,875
21. click(findTestObject("ConfigurationsTree/div_base-editor"))

7
00:00:06,181 --> 00:00:06,183
25. click(findTestObject("ConfigurationsTree/div_Widgets"))

8
00:00:06,493 --> 00:00:06,494
29. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

9
00:00:06,769 --> 00:00:06,769
33. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

10
00:00:07,045 --> 00:00:07,046
37. click(findTestObject("TestStandPanel/button_RUN"))

11
00:00:08,013 --> 00:00:08,014
41. click(findTestObject("TestStandPanel/EditorTab"))

12
00:00:08,443 --> 00:00:08,444
45. delay(5)

13
00:00:13,460 --> 00:00:13,460
49. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorButton"))

14
00:00:14,031 --> 00:00:14,033
53. delay(2)

15
00:00:16,043 --> 00:00:16,044
57. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

16
00:00:16,433 --> 00:00:16,434
61. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

17
00:00:17,162 --> 00:00:17,163
65. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

18
00:00:17,953 --> 00:00:17,954
69. delay(2)

19
00:00:19,960 --> 00:00:19,961
73. Saturation = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

20
00:00:20,232 --> 00:00:20,232
77. SaturationIsSimilar = scripts.ScreenshotHandler.compareToBase(Saturation, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

21
00:00:20,254 --> 00:00:20,254
81. assert SaturationIsSimilar == true

22
00:00:20,259 --> 00:00:20,259
85. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 85)

23
00:00:20,864 --> 00:00:20,864
89. delay(3)

24
00:00:23,876 --> 00:00:23,877
93. Hue = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

25
00:00:24,147 --> 00:00:24,147
97. HueIsSimilar = scripts.ScreenshotHandler.compareToBase(Hue, baseDir + "/Widgets/ColorPicker/PickerCMYK/HueChanged1.png")

26
00:00:24,166 --> 00:00:24,166
101. assert HueIsSimilar == true

27
00:00:24,171 --> 00:00:24,171
105. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 110)

28
00:00:24,804 --> 00:00:24,807
109. delay(2)

29
00:00:26,816 --> 00:00:26,817
113. Alpha = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

30
00:00:27,087 --> 00:00:27,088
117. AlphaIsSimilar = scripts.ScreenshotHandler.compareToBase(Alpha, baseDir + "/Widgets/ColorPicker/PickerCMYK/PickersFirstCheck.png")

31
00:00:27,118 --> 00:00:27,119
121. assert AlphaIsSimilar == true

32
00:00:27,123 --> 00:00:27,124
125. click(findTestObject("TestStandPanel/FinishButton"))

33
00:00:27,466 --> 00:00:27,467
129. delay(2)

34
00:00:29,478 --> 00:00:29,478
133. AppliedColor1 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("TestStandPanel/PreviewImage"))

35
00:00:29,698 --> 00:00:29,699
137. AppliedColor1IsSimilar = scripts.ScreenshotHandler.compareToBase(AppliedColor1, baseDir + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck1.png")

36
00:00:29,775 --> 00:00:29,777
141. assert AppliedColor1IsSimilar == true

37
00:00:29,782 --> 00:00:29,782
145. click(findTestObject("TestStandPanel/BackToEditorButton"))

38
00:00:30,029 --> 00:00:30,029
149. delay(2)

39
00:00:32,039 --> 00:00:32,040
153. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5, 2)

40
00:00:32,602 --> 00:00:32,603
157. delay(2)

41
00:00:34,611 --> 00:00:34,612
161. Saturation2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

42
00:00:34,919 --> 00:00:34,919
165. Saturation2IsSimilar = scripts.ScreenshotHandler.compareToBase(Saturation2, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged2.png")

43
00:00:34,938 --> 00:00:34,939
169. assert Saturation2IsSimilar == true

44
00:00:34,952 --> 00:00:34,953
173. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 20)

45
00:00:35,505 --> 00:00:35,506
177. delay(2)

46
00:00:37,514 --> 00:00:37,515
181. Hue2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

47
00:00:37,776 --> 00:00:37,776
185. Hue2IsSimilar = scripts.ScreenshotHandler.compareToBase(Hue2, baseDir + "/Widgets/ColorPicker/PickerCMYK/HueChanged2.png")

48
00:00:37,797 --> 00:00:37,798
189. assert Hue2IsSimilar == true

49
00:00:37,806 --> 00:00:37,806
193. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 20)

50
00:00:38,365 --> 00:00:38,366
197. delay(2)

51
00:00:40,376 --> 00:00:40,376
201. Alpha2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

52
00:00:40,708 --> 00:00:40,709
205. Alpha2IsSimilar = scripts.ScreenshotHandler.compareToBase(Alpha2, baseDir + "/Widgets/ColorPicker/PickerCMYK/PickersSecondCheck.png")

53
00:00:40,730 --> 00:00:40,731
209. assert Alpha2IsSimilar == true

54
00:00:40,739 --> 00:00:40,740
213. click(findTestObject("TestStandPanel/FinishButton"))

55
00:00:40,993 --> 00:00:40,994
217. delay(2)

56
00:00:43,001 --> 00:00:43,002
221. AppliedColor2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("TestStandPanel/PreviewImage"))

57
00:00:43,215 --> 00:00:43,216
225. AppliedColor2IsSimilar = scripts.ScreenshotHandler.compareToBase(AppliedColor2, baseDir + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck2.png")

58
00:00:43,275 --> 00:00:43,276
229. assert AppliedColor2IsSimilar == true

59
00:00:43,280 --> 00:00:43,281
233. click(findTestObject("TestStandPanel/BackToEditorButton"))

60
00:00:43,512 --> 00:00:43,513
237. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"))

61
00:00:43,985 --> 00:00:43,987
241. delay(1)

62
00:00:44,997 --> 00:00:44,997
245. RecentColor = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

63
00:00:45,255 --> 00:00:45,255
249. RecentColorIsSimilar = scripts.ScreenshotHandler.compareToBase(RecentColor, baseDir + "/Widgets/ColorPicker/PickerCMYK/RecentPaletteAdded.png")

64
00:00:45,275 --> 00:00:45,276
253. assert RecentColorIsSimilar == true

65
00:00:45,281 --> 00:00:45,281
257. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel"))

66
00:00:45,689 --> 00:00:45,690
261. delay(2)

67
00:00:47,696 --> 00:00:47,697
265. Lightning = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

68
00:00:47,950 --> 00:00:47,950
269. LightningIsSimilar = scripts.ScreenshotHandler.compareToBase(Lightning, baseDir + "/Widgets/ColorPicker/PickerCMYK/ChannelSelectedLightning.png")

69
00:00:47,972 --> 00:00:47,972
273. assert LightningIsSimilar == true

70
00:00:47,976 --> 00:00:47,977
277. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel"), "67")

71
00:00:48,134 --> 00:00:48,135
281. delay(2)

72
00:00:50,145 --> 00:00:50,145
285. ValidC1 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

73
00:00:50,418 --> 00:00:50,418
289. ValidC1IsSimilar = scripts.ScreenshotHandler.compareToBase(ValidC1, baseDir + "/Widgets/ColorPicker/PickerCMYK/CChannelValidInput1.png")

74
00:00:50,437 --> 00:00:50,437
293. assert ValidC1IsSimilar == true

75
00:00:50,441 --> 00:00:50,441
297. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel"), "abc")

76
00:00:50,635 --> 00:00:50,636
301. delay(2)

77
00:00:52,644 --> 00:00:52,644
305. InvalidC1 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

78
00:00:52,931 --> 00:00:52,932
309. InvalidC1IsSimilar = scripts.ScreenshotHandler.compareToBase(InvalidC1, baseDir + "/Widgets/ColorPicker/PickerCMYK/CChannelInvalidInput1.png")

79
00:00:52,952 --> 00:00:52,952
313. assert InvalidC1IsSimilar == true

