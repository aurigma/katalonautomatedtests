1
00:00:00,352 --> 00:00:00,356
1. openBrowser("")

2
00:00:02,540 --> 00:00:02,541
5. setViewPortSize(1930, 1180)

3
00:00:02,715 --> 00:00:02,715
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,841 --> 00:00:03,842
13. driver = getExecutedBrowser().getName()

5
00:00:03,884 --> 00:00:03,885
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,900 --> 00:00:03,905
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:03,911 --> 00:00:03,912
25. logInfo(baseDir)

8
00:00:03,934 --> 00:00:03,943
29. click(findTestObject("ConfigurationsTree/div_base-editor"))

9
00:00:04,093 --> 00:00:04,094
33. click(findTestObject("ConfigurationsTree/div_Widgets"))

10
00:00:04,217 --> 00:00:04,218
37. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

11
00:00:04,377 --> 00:00:04,378
41. click(findTestObject("ConfigurationsTree/div_Palettejson"))

12
00:00:04,516 --> 00:00:04,517
45. click(findTestObject("TestStandPanel/button_RUN"))

13
00:00:04,624 --> 00:00:04,625
49. click(findTestObject("TestStandPanel/EditorTab"))

14
00:00:04,785 --> 00:00:04,786
53. delay(5)

15
00:00:09,830 --> 00:00:09,832
57. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

16
00:00:09,857 --> 00:00:09,859
61. parentElem = findWebElement(parent, 30)

17
00:00:09,946 --> 00:00:09,947
65. basePoint = parentElem.getLocation()

18
00:00:10,042 --> 00:00:10,043
69. switchToFrame(parent, 5)

19
00:00:10,098 --> 00:00:10,099
73. delay(2)

20
00:00:12,112 --> 00:00:12,114
77. if (driver == "CHROME_DRIVER")

21
00:00:12,124 --> 00:00:12,126
81. delay(1)

22
00:00:13,141 --> 00:00:13,142
85. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton"))

23
00:00:13,528 --> 00:00:13,530
89. delay(2)

24
00:00:15,545 --> 00:00:15,547
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), 5)

25
00:00:15,638 --> 00:00:15,638
97. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/TextColorDialog.png", failed + "/Widgets/ColorPicker/Palette/TextColorDialog.png")

26
00:00:16,487 --> 00:00:16,490
101. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Purple"))

27
00:00:16,755 --> 00:00:16,756
105. delay(1)

28
00:00:17,770 --> 00:00:17,771
109. switchToDefaultContent()

29
00:00:17,803 --> 00:00:17,806
113. click(findTestObject("TestStandPanel/FinishButton"))

30
00:00:17,933 --> 00:00:17,934
117. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

31
00:00:18,504 --> 00:00:18,505
121. delay(2)

32
00:00:20,516 --> 00:00:20,516
125. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png", failed + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png")

33
00:00:21,131 --> 00:00:21,133
129. click(findTestObject("TestStandPanel/BackToEditorButton"))

34
00:00:21,256 --> 00:00:21,257
133. switchToFrame(parent, 5)

35
00:00:21,315 --> 00:00:21,316
137. delay(1)

36
00:00:22,329 --> 00:00:22,330
141. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

37
00:00:22,553 --> 00:00:22,553
145. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

38
00:00:22,695 --> 00:00:22,696
149. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png", failed + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png")

39
00:00:23,309 --> 00:00:23,311
153. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Burgundy"))

40
00:00:23,530 --> 00:00:23,530
157. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

41
00:00:23,851 --> 00:00:23,851
161. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RecentColor_Purple"))

42
00:00:24,087 --> 00:00:24,088
165. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RecentColor_Purple"))

43
00:00:24,312 --> 00:00:24,314
169. delay(2)

44
00:00:26,327 --> 00:00:26,328
173. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck2.png", failed + "/Widgets/ColorPicker/Palette/AddToRecentCheck2.png")

45
00:00:26,930 --> 00:00:26,931
177. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

46
00:00:27,104 --> 00:00:27,104
181. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

47
00:00:27,213 --> 00:00:27,213
185. delay(2)

48
00:00:29,223 --> 00:00:29,224
189. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck3.png", failed + "/Widgets/ColorPicker/Palette/AddToRecentCheck3.png")

49
00:00:29,850 --> 00:00:29,850
193. navigateToUrl("http://localhost:3000/")

50
00:00:29,870 --> 00:00:29,871
197. if (driver == "CHROME_DRIVER")

51
00:00:29,876 --> 00:00:29,877
201. click(findTestObject("ConfigurationsTree/div_base-editor"))

