1
00:00:00,156 --> 00:00:00,157
1. openBrowser("")

2
00:00:03,000 --> 00:00:03,001
5. maximizeWindow()

3
00:00:03,270 --> 00:00:03,270
9. navigateToUrl("http://localhost:3000/")

4
00:00:04,396 --> 00:00:04,396
13. driver = getExecutedBrowser().getName()

5
00:00:04,405 --> 00:00:04,406
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:04,417 --> 00:00:04,418
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:04,431 --> 00:00:04,431
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:04,906 --> 00:00:04,907
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:05,182 --> 00:00:05,183
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:05,453 --> 00:00:05,453
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:05,718 --> 00:00:05,719
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:06,591 --> 00:00:06,592
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:06,912 --> 00:00:06,913
49. delay(5)

14
00:00:11,924 --> 00:00:11,924
53. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorButton"))

15
00:00:12,522 --> 00:00:12,522
57. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

16
00:00:12,623 --> 00:00:12,624
61. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

17
00:00:12,689 --> 00:00:12,690
65. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

18
00:00:13,254 --> 00:00:13,255
69. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

19
00:00:13,354 --> 00:00:13,354
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

20
00:00:13,933 --> 00:00:13,934
77. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

21
00:00:14,022 --> 00:00:14,022
81. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

22
00:00:14,620 --> 00:00:14,621
85. delay(2)

23
00:00:16,628 --> 00:00:16,628
89. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

24
00:00:16,918 --> 00:00:16,919
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 5)

25
00:00:16,988 --> 00:00:16,990
97. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 85)

26
00:00:17,593 --> 00:00:17,594
101. delay(3)

27
00:00:20,602 --> 00:00:20,602
105. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/HueChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/HueChanged1.png")

28
00:00:20,965 --> 00:00:20,965
109. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 5)

29
00:00:21,031 --> 00:00:21,032
113. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 110)

30
00:00:21,631 --> 00:00:21,634
117. delay(2)

31
00:00:23,643 --> 00:00:23,643
121. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/PickersFirstCheck.png", failed + "/Widgets/ColorPicker/PickerCMYK/PickersFirstCheck.png")

