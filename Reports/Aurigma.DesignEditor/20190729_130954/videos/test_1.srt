1
00:00:00,325 --> 00:00:00,329
1. openBrowser("")

2
00:00:03,948 --> 00:00:03,949
5. setViewPortSize(1930, 1180)

3
00:00:04,031 --> 00:00:04,032
9. navigateToUrl("http://localhost:3000/")

4
00:00:05,239 --> 00:00:05,240
13. driver = getExecutedBrowser().getName()

5
00:00:05,261 --> 00:00:05,262
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:05,290 --> 00:00:05,293
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:05,304 --> 00:00:05,306
25. logInfo(baseDir)

8
00:00:05,328 --> 00:00:05,329
29. click(findTestObject("ConfigurationsTree/div_base-editor"))

9
00:00:05,756 --> 00:00:05,757
33. click(findTestObject("ConfigurationsTree/div_Widgets"))

10
00:00:06,056 --> 00:00:06,057
37. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

11
00:00:06,335 --> 00:00:06,335
41. click(findTestObject("ConfigurationsTree/div_Palettejson"))

12
00:00:06,600 --> 00:00:06,600
45. click(findTestObject("TestStandPanel/button_RUN"))

13
00:00:07,469 --> 00:00:07,470
49. click(findTestObject("TestStandPanel/EditorTab"))

14
00:00:07,816 --> 00:00:07,816
53. delay(5)

15
00:00:12,840 --> 00:00:12,840
57. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

16
00:00:12,847 --> 00:00:12,847
61. parentElem = findWebElement(parent, 30)

17
00:00:12,916 --> 00:00:12,917
65. basePoint = parentElem.getLocation()

18
00:00:13,023 --> 00:00:13,024
69. switchToFrame(parent, 5)

19
00:00:13,056 --> 00:00:13,057
73. delay(2)

20
00:00:15,071 --> 00:00:15,073
77. if (driver == "CHROME_DRIVER" || driver == "HEADLESS_DRIVER")

21
00:00:15,081 --> 00:00:15,082
81. delay(1)

22
00:00:16,096 --> 00:00:16,098
85. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton"))

23
00:00:16,631 --> 00:00:16,631
89. delay(2)

24
00:00:18,641 --> 00:00:18,642
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), 5)

25
00:00:18,708 --> 00:00:18,709
97. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/TextColorDialog.png", failed + "/Widgets/ColorPicker/Palette/TextColorDialog.png")

26
00:00:19,148 --> 00:00:19,150
101. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Purple"))

27
00:00:19,740 --> 00:00:19,741
105. delay(1)

28
00:00:20,749 --> 00:00:20,750
109. switchToDefaultContent()

29
00:00:20,786 --> 00:00:20,787
113. click(findTestObject("TestStandPanel/FinishButton"))

30
00:00:21,131 --> 00:00:21,132
117. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

31
00:00:21,187 --> 00:00:21,187
121. delay(2)

32
00:00:23,196 --> 00:00:23,199
125. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png", failed + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png")

33
00:00:23,717 --> 00:00:23,718
129. click(findTestObject("TestStandPanel/BackToEditorButton"))

34
00:00:23,973 --> 00:00:23,973
133. switchToFrame(parent, 5)

35
00:00:24,015 --> 00:00:24,016
137. delay(1)

36
00:00:25,021 --> 00:00:25,021
141. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

37
00:00:25,338 --> 00:00:25,339
145. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

38
00:00:25,540 --> 00:00:25,540
149. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png", failed + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png")

