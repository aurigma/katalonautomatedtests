1
00:00:00,166 --> 00:00:00,166
1. openBrowser("")

2
00:00:04,089 --> 00:00:04,089
5. setViewPortSize(1930, 1180)

3
00:00:04,194 --> 00:00:04,196
9. navigateToUrl("http://localhost:3000/")

4
00:00:05,994 --> 00:00:05,995
13. driver = getExecutedBrowser().getName()

5
00:00:06,010 --> 00:00:06,012
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:06,032 --> 00:00:06,033
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:06,052 --> 00:00:06,053
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:06,735 --> 00:00:06,735
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:07,080 --> 00:00:07,081
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:07,352 --> 00:00:07,353
37. click(findTestObject("ConfigurationsTree/div_SectionCollapsejson"))

11
00:00:07,643 --> 00:00:07,643
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:08,834 --> 00:00:08,834
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:09,235 --> 00:00:09,238
49. delay(5)

14
00:00:14,260 --> 00:00:14,262
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:14,277 --> 00:00:14,278
57. parentElem = findWebElement(parent, 30)

16
00:00:16,927 --> 00:00:16,928
61. basePoint = parentElem.getLocation()

17
00:00:18,040 --> 00:00:18,041
65. switchToFrame(parent, 5)

18
00:00:18,372 --> 00:00:18,373
69. delay(2)

19
00:00:20,389 --> 00:00:20,389
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

20
00:00:20,995 --> 00:00:20,999
77. delay(2)

21
00:00:23,013 --> 00:00:23,013
81. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/PickerWithCollapseSections.png", failed + "/Widgets/ColorPicker/SectionCollapse/PickerWithCollapseSections.png")

22
00:00:23,367 --> 00:00:23,368
85. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/GrayscaleSectionCollapse"))

23
00:00:23,942 --> 00:00:23,943
89. delay(2)

24
00:00:25,954 --> 00:00:25,955
93. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/GrayscaleCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/GrayscaleCollapsed.png")

25
00:00:26,228 --> 00:00:26,229
97. delay(2)

26
00:00:28,236 --> 00:00:28,237
101. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RgbSectionCollapse"))

27
00:00:28,897 --> 00:00:28,897
105. delay(2)

28
00:00:30,907 --> 00:00:30,908
109. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/RGBCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/RGBCollapsed.png")

29
00:00:31,187 --> 00:00:31,188
113. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/CmykSectionCollapse"))

30
00:00:31,662 --> 00:00:31,662
117. delay(2)

31
00:00:33,672 --> 00:00:33,673
121. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CMYKCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/CMYKCollapsed.png")

32
00:00:33,948 --> 00:00:33,948
125. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RecentSectionCollapse"))

33
00:00:34,408 --> 00:00:34,408
129. delay(2)

34
00:00:36,416 --> 00:00:36,417
133. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/AllCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/AllCollapsed.png")

35
00:00:36,705 --> 00:00:36,705
137. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

36
00:00:37,306 --> 00:00:37,306
141. delay(1)

37
00:00:38,327 --> 00:00:38,327
145. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

38
00:00:38,817 --> 00:00:38,820
149. delay(2)

39
00:00:40,841 --> 00:00:40,842
153. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved.png")

40
00:00:41,110 --> 00:00:41,110
157. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/button_OK"))

41
00:00:41,618 --> 00:00:41,619
161. delay(1)

42
00:00:42,627 --> 00:00:42,627
165. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

43
00:00:43,138 --> 00:00:43,139
169. delay(2)

44
00:00:45,152 --> 00:00:45,153
173. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved2.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved2.png")

45
00:00:45,436 --> 00:00:45,436
177. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RecentSectionCollapse"))

46
00:00:45,885 --> 00:00:45,885
181. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RgbSectionCollapse"))

47
00:00:46,325 --> 00:00:46,326
185. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/OIPickAnotherItem"))

48
00:00:47,092 --> 00:00:47,093
189. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

49
00:00:47,683 --> 00:00:47,683
193. delay(2)

50
00:00:49,694 --> 00:00:49,694
197. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved3.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved3.png")

51
00:00:49,955 --> 00:00:49,955
201. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/AddText"))

52
00:00:50,979 --> 00:00:50,979
205. delay(3)

53
00:00:54,016 --> 00:00:54,016
209. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

54
00:00:54,533 --> 00:00:54,533
213. delay(2)

55
00:00:56,547 --> 00:00:56,547
217. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved4.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved4.png")

56
00:00:56,839 --> 00:00:56,840
221. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/button_OK"))

57
00:00:57,331 --> 00:00:57,335
225. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/TextArea"), "Test")

58
00:00:57,702 --> 00:00:57,702
229. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

59
00:00:58,326 --> 00:00:58,327
233. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved5.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved5.png")

60
00:00:58,970 --> 00:00:58,972
237. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/UndoButton"))

61
00:00:59,795 --> 00:00:59,796
241. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/OIPickAnotherItem"))

62
00:01:00,628 --> 00:01:00,628
245. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

63
00:01:01,321 --> 00:01:01,322
249. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionReverted.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionReverted.png")

