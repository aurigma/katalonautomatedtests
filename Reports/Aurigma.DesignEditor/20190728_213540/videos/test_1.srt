1
00:00:00,349 --> 00:00:00,358
1. openBrowser("")

2
00:00:05,089 --> 00:00:05,090
5. setViewPortSize(1930, 1180)

3
00:00:05,177 --> 00:00:05,178
9. navigateToUrl("http://localhost:3000/")

4
00:00:07,618 --> 00:00:07,626
13. driver = getExecutedBrowser().getName()

5
00:00:07,742 --> 00:00:07,744
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:07,784 --> 00:00:07,785
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:07,827 --> 00:00:07,886
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:08,770 --> 00:00:08,772
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:09,098 --> 00:00:09,100
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:09,414 --> 00:00:09,415
37. click(findTestObject("ConfigurationsTree/div_Palettejson"))

11
00:00:09,689 --> 00:00:09,692
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:09,985 --> 00:00:09,987
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:11,320 --> 00:00:11,322
49. delay(5)

14
00:00:16,403 --> 00:00:16,415
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:16,454 --> 00:00:16,459
57. parentElem = findWebElement(parent, 30)

16
00:00:19,697 --> 00:00:19,704
61. basePoint = parentElem.getLocation()

17
00:00:22,098 --> 00:00:22,100
65. switchToFrame(parent, 5)

18
00:00:22,423 --> 00:00:22,424
69. delay(2)

19
00:00:24,443 --> 00:00:24,446
73. if (driver == "CHROME_DRIVER")

20
00:00:24,455 --> 00:00:24,456
77. delay(1)

21
00:00:25,467 --> 00:00:25,467
81. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton"))

22
00:00:26,183 --> 00:00:26,186
85. delay(2)

23
00:00:28,212 --> 00:00:28,213
89. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), 5)

24
00:00:28,297 --> 00:00:28,298
93. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/TextColorDialog.png", failed + "/Widgets/ColorPicker/Palette/TextColorDialog.png")

25
00:00:28,802 --> 00:00:28,804
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Purple"))

26
00:00:29,800 --> 00:00:29,801
101. delay(1)

27
00:00:30,814 --> 00:00:30,815
105. switchToDefaultContent()

28
00:00:30,873 --> 00:00:30,875
109. click(findTestObject("TestStandPanel/FinishButton"))

29
00:00:31,324 --> 00:00:31,326
113. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

30
00:00:31,468 --> 00:00:31,469
117. delay(2)

31
00:00:33,480 --> 00:00:33,481
121. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png", failed + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png")

32
00:00:33,830 --> 00:00:33,834
125. click(findTestObject("TestStandPanel/BackToEditorButton"))

33
00:00:34,098 --> 00:00:34,100
129. switchToFrame(parent, 5)

34
00:00:34,189 --> 00:00:34,190
133. delay(1)

35
00:00:35,227 --> 00:00:35,229
137. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

36
00:00:35,776 --> 00:00:35,777
141. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

37
00:00:36,053 --> 00:00:36,054
145. delay(2)

38
00:00:38,061 --> 00:00:38,061
149. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png", failed + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png")

