1
00:00:00,157 --> 00:00:00,157
1. openBrowser("")

2
00:00:03,100 --> 00:00:03,101
5. setViewPortSize(1930, 1180)

3
00:00:03,275 --> 00:00:03,275
9. navigateToUrl("http://localhost:3000/")

4
00:00:04,910 --> 00:00:04,913
13. driver = getExecutedBrowser().getName()

5
00:00:04,969 --> 00:00:04,969
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:05,008 --> 00:00:05,010
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:05,022 --> 00:00:05,028
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:05,667 --> 00:00:05,670
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:05,978 --> 00:00:05,978
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:06,258 --> 00:00:06,260
37. click(findTestObject("ConfigurationsTree/div_Widgetsjson"))

11
00:00:06,518 --> 00:00:06,519
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:06,862 --> 00:00:06,864
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:08,706 --> 00:00:08,707
49. delay(5)

14
00:00:13,750 --> 00:00:13,754
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/Iframe")

15
00:00:13,765 --> 00:00:13,766
57. parentElem = findWebElement(parent, 30)

16
00:00:15,736 --> 00:00:15,740
61. basePoint = parentElem.getLocation()

17
00:00:16,410 --> 00:00:16,412
65. switchToFrame(parent, 5)

18
00:00:17,861 --> 00:00:17,863
69. delay(2)

19
00:00:19,873 --> 00:00:19,874
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/AddRichText"))

20
00:00:20,588 --> 00:00:20,592
77. delay(2)

21
00:00:22,616 --> 00:00:22,617
81. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETextColorButton"))

22
00:00:23,458 --> 00:00:23,458
85. delay(2)

23
00:00:25,465 --> 00:00:25,466
89. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETopBar"))

24
00:00:25,629 --> 00:00:25,629
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), 5)

25
00:00:25,731 --> 00:00:25,731
97. delay(1)

26
00:00:26,754 --> 00:00:26,754
101. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), basePoint, baseDir + "/Widgets/ColorPicker/Widgets/RTEPickerOpened.png", failed + "/Widgets/ColorPicker/Widgets/RTEPickerOpened.png")

