1
00:00:00,170 --> 00:00:00,170
1. openBrowser("")

2
00:00:01,943 --> 00:00:01,943
5. setViewPortSize(1930, 1180)

3
00:00:02,079 --> 00:00:02,080
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,190 --> 00:00:03,190
13. driver = getExecutedBrowser().getName()

5
00:00:03,209 --> 00:00:03,210
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,226 --> 00:00:03,228
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:03,236 --> 00:00:03,236
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,343 --> 00:00:03,344
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,464 --> 00:00:03,466
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:03,593 --> 00:00:03,593
37. click(findTestObject("ConfigurationsTree/div_Widgetsjson"))

11
00:00:03,714 --> 00:00:03,715
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:03,804 --> 00:00:03,805
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:03,968 --> 00:00:03,969
49. delay(5)

14
00:00:08,981 --> 00:00:08,983
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/Iframe")

15
00:00:08,992 --> 00:00:08,992
57. parentElem = findWebElement(parent, 30)

16
00:00:09,069 --> 00:00:09,070
61. basePoint = parentElem.getLocation()

17
00:00:09,082 --> 00:00:09,083
65. switchToFrame(parent, 5)

18
00:00:09,120 --> 00:00:09,121
69. delay(2)

19
00:00:11,133 --> 00:00:11,134
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/AddRichText"))

20
00:00:11,530 --> 00:00:11,531
77. delay(2)

21
00:00:13,537 --> 00:00:13,537
81. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETextColorButton"))

22
00:00:14,237 --> 00:00:14,238
85. delay(2)

23
00:00:16,252 --> 00:00:16,253
89. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETopBar"))

24
00:00:16,345 --> 00:00:16,347
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), 5)

25
00:00:16,398 --> 00:00:16,398
97. delay(1)

26
00:00:17,404 --> 00:00:17,404
101. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), basePoint, baseDir + "/Widgets/ColorPicker/Widgets/RTEPickerOpened.png", failed + "/Widgets/ColorPicker/Widgets/RTEPickerOpened.png")

