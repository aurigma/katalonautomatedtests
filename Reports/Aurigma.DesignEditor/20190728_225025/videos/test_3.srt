1
00:00:00,198 --> 00:00:00,200
1. openBrowser("")

2
00:00:02,017 --> 00:00:02,018
5. setViewPortSize(1930, 1180)

3
00:00:02,139 --> 00:00:02,140
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,218 --> 00:00:03,218
13. driver = getExecutedBrowser().getName()

5
00:00:03,232 --> 00:00:03,234
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,252 --> 00:00:03,253
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:03,274 --> 00:00:03,276
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,370 --> 00:00:03,371
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,499 --> 00:00:03,500
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:03,651 --> 00:00:03,652
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:03,789 --> 00:00:03,789
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:03,889 --> 00:00:03,890
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:04,090 --> 00:00:04,091
49. delay(5)

14
00:00:09,104 --> 00:00:09,104
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:09,113 --> 00:00:09,114
57. parentElem = findWebElement(parent, 30)

16
00:00:09,191 --> 00:00:09,192
61. basePoint = parentElem.getLocation()

17
00:00:09,208 --> 00:00:09,209
65. switchToFrame(parent, 5)

18
00:00:09,255 --> 00:00:09,256
69. delay(2)

19
00:00:11,265 --> 00:00:11,265
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:11,641 --> 00:00:11,642
77. delay(4)

21
00:00:15,659 --> 00:00:15,660
81. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 10)

22
00:00:15,947 --> 00:00:15,948
85. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"))

23
00:00:16,062 --> 00:00:16,063
89. delay(2)

24
00:00:18,074 --> 00:00:18,075
93. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerRGB/SaturationChanged1.png")

