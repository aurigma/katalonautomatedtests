1
00:00:00,235 --> 00:00:00,236
1. openBrowser("")

2
00:00:02,016 --> 00:00:02,017
5. setViewPortSize(1930, 1180)

3
00:00:02,151 --> 00:00:02,152
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,250 --> 00:00:03,252
13. driver = getExecutedBrowser().getName()

5
00:00:03,269 --> 00:00:03,271
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,278 --> 00:00:03,279
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:03,293 --> 00:00:03,295
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,401 --> 00:00:03,402
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,538 --> 00:00:03,540
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:03,659 --> 00:00:03,659
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:03,809 --> 00:00:03,810
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:03,902 --> 00:00:03,904
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:04,098 --> 00:00:04,099
49. delay(5)

14
00:00:09,109 --> 00:00:09,110
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:09,129 --> 00:00:09,130
57. parentElem = findWebElement(parent, 30)

16
00:00:09,223 --> 00:00:09,227
61. basePoint = parentElem.getLocation()

17
00:00:09,249 --> 00:00:09,252
65. switchToFrame(parent, 5)

18
00:00:09,639 --> 00:00:09,640
69. delay(2)

19
00:00:11,652 --> 00:00:11,652
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:12,132 --> 00:00:12,132
77. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

21
00:00:12,202 --> 00:00:12,202
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

22
00:00:12,244 --> 00:00:12,244
85. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

23
00:00:12,481 --> 00:00:12,481
89. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

24
00:00:12,577 --> 00:00:12,577
93. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

25
00:00:12,796 --> 00:00:12,797
97. delay(1)

26
00:00:13,807 --> 00:00:13,808
101. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

27
00:00:13,867 --> 00:00:13,868
105. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

28
00:00:14,193 --> 00:00:14,194
109. delay(2)

29
00:00:16,212 --> 00:00:16,213
113. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

