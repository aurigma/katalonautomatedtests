1
00:00:00,164 --> 00:00:00,165
1. openBrowser("")

2
00:00:04,133 --> 00:00:04,134
5. maximizeWindow()

3
00:00:04,417 --> 00:00:04,418
9. navigateToUrl("http://localhost:3000/")

4
00:00:05,606 --> 00:00:05,606
13. driver = getExecutedBrowser().getName()

5
00:00:05,612 --> 00:00:05,615
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:05,629 --> 00:00:05,630
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:05,668 --> 00:00:05,670
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:06,198 --> 00:00:06,199
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:06,498 --> 00:00:06,500
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:06,781 --> 00:00:06,781
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:07,051 --> 00:00:07,052
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:07,984 --> 00:00:07,985
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:08,323 --> 00:00:08,324
49. delay(5)

14
00:00:13,330 --> 00:00:13,331
53. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorButton"))

15
00:00:14,019 --> 00:00:14,019
57. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

16
00:00:14,176 --> 00:00:14,176
61. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

17
00:00:14,306 --> 00:00:14,306
65. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

18
00:00:14,694 --> 00:00:14,695
69. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

19
00:00:14,790 --> 00:00:14,791
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

20
00:00:15,510 --> 00:00:15,511
77. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

21
00:00:15,635 --> 00:00:15,636
81. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

22
00:00:16,267 --> 00:00:16,267
85. delay(2)

23
00:00:18,274 --> 00:00:18,275
89. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

24
00:00:18,637 --> 00:00:18,637
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 5)

25
00:00:18,723 --> 00:00:18,724
97. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 85)

26
00:00:19,376 --> 00:00:19,376
101. delay(3)

27
00:00:22,385 --> 00:00:22,387
105. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/HueChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/HueChanged1.png")

28
00:00:22,731 --> 00:00:22,732
109. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 5)

29
00:00:22,812 --> 00:00:22,813
113. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 110)

30
00:00:23,490 --> 00:00:23,491
117. delay(2)

31
00:00:25,501 --> 00:00:25,502
121. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/PickersFirstCheck.png", failed + "/Widgets/ColorPicker/PickerCMYK/PickersFirstCheck.png")

32
00:00:25,816 --> 00:00:25,817
125. click(findTestObject("TestStandPanel/FinishButton"))

33
00:00:26,179 --> 00:00:26,179
129. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

34
00:00:26,263 --> 00:00:26,264
133. delay(2)

35
00:00:28,273 --> 00:00:28,274
137. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), baseDir + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck1.png", failed + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck1.png")

36
00:00:28,555 --> 00:00:28,556
141. click(findTestObject("TestStandPanel/BackToEditorButton"))

37
00:00:28,823 --> 00:00:28,824
145. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

38
00:00:29,010 --> 00:00:29,011
149. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

39
00:00:29,113 --> 00:00:29,114
153. delay(2)

40
00:00:31,120 --> 00:00:31,120
157. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5, 2)

41
00:00:31,744 --> 00:00:31,744
161. delay(2)

42
00:00:33,751 --> 00:00:33,751
165. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged2.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged2.png")

43
00:00:34,071 --> 00:00:34,071
169. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 5)

44
00:00:34,146 --> 00:00:34,147
173. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 20)

45
00:00:34,762 --> 00:00:34,766
177. delay(2)

46
00:00:36,777 --> 00:00:36,777
181. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/HueChanged2.png", failed + "/Widgets/ColorPicker/PickerCMYK/HueChanged2.png")

47
00:00:37,085 --> 00:00:37,085
185. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 5)

48
00:00:37,154 --> 00:00:37,155
189. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 20)

49
00:00:37,761 --> 00:00:37,761
193. delay(2)

50
00:00:39,770 --> 00:00:39,771
197. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/PickersSecondCheck.png", failed + "/Widgets/ColorPicker/PickerCMYK/PickersSecondCheck.png")

51
00:00:40,062 --> 00:00:40,062
201. click(findTestObject("TestStandPanel/FinishButton"))

52
00:00:40,363 --> 00:00:40,364
205. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

53
00:00:40,429 --> 00:00:40,430
209. delay(2)

54
00:00:42,444 --> 00:00:42,444
213. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), baseDir + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck2.png", failed + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck2.png")

55
00:00:42,767 --> 00:00:42,767
217. click(findTestObject("TestStandPanel/BackToEditorButton"))

56
00:00:43,178 --> 00:00:43,179
221. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

57
00:00:43,271 --> 00:00:43,272
225. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"), 5)

58
00:00:43,365 --> 00:00:43,365
229. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"))

59
00:00:43,966 --> 00:00:43,967
233. delay(1)

60
00:00:44,975 --> 00:00:44,976
237. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/RecentPaletteAdded.png", failed + "/Widgets/ColorPicker/PickerCMYK/RecentPaletteAdded.png")

