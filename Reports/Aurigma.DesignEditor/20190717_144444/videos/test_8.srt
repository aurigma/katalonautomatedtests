1
00:00:00,119 --> 00:00:00,119
1. openBrowser("")

2
00:00:02,954 --> 00:00:02,955
5. maximizeWindow()

3
00:00:03,235 --> 00:00:03,236
9. navigateToUrl("http://localhost:3000/")

4
00:00:04,379 --> 00:00:04,381
13. driver = getExecutedBrowser().getName()

5
00:00:04,410 --> 00:00:04,411
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:04,429 --> 00:00:04,429
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:04,438 --> 00:00:04,439
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:04,975 --> 00:00:04,975
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:05,259 --> 00:00:05,259
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:05,531 --> 00:00:05,531
37. click(findTestObject("ConfigurationsTree/div_Widgetsjson"))

11
00:00:05,787 --> 00:00:05,787
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:06,681 --> 00:00:06,681
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:07,061 --> 00:00:07,062
49. delay(5)

14
00:00:12,068 --> 00:00:12,069
53. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/button_More"))

15
00:00:12,810 --> 00:00:12,811
57. delay(2)

16
00:00:14,817 --> 00:00:14,818
61. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokeButton"))

17
00:00:15,342 --> 00:00:15,343
65. delay(2)

18
00:00:17,354 --> 00:00:17,354
69. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokeDialog"), baseDir + "/Widgets/ColorPicker/Widgets/StrokeDialogInitial.png", failed + "/Widgets/ColorPicker/Widgets/StrokeDialogInitial.png")

19
00:00:17,645 --> 00:00:17,646
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/OpenStrokePalette"))

20
00:00:18,175 --> 00:00:18,176
77. delay(3)

21
00:00:21,185 --> 00:00:21,185
81. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokePalette"), baseDir + "/Widgets/ColorPicker/Widgets/StrokePaletteInitial.png", failed + "/Widgets/ColorPicker/Widgets/StrokePaletteInitial.png")

22
00:00:21,458 --> 00:00:21,459
85. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/StrokeOK"))

23
00:00:22,094 --> 00:00:22,095
89. delay(2)

24
00:00:24,102 --> 00:00:24,102
93. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokeDialog"), baseDir + "/Widgets/ColorPicker/Widgets/BackToStrokeDialog.png", failed + "/Widgets/ColorPicker/Widgets/BackToStrokeDialog.png")

25
00:00:24,503 --> 00:00:24,503
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/OpenStrokePalette"))

26
00:00:25,283 --> 00:00:25,285
101. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/StrokeColors_Yellow"))

27
00:00:25,863 --> 00:00:25,863
105. delay(2)

28
00:00:27,881 --> 00:00:27,881
109. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokeDialog"), baseDir + "/Widgets/ColorPicker/Widgets/StrokeRecentAdded.png", failed + "/Widgets/ColorPicker/Widgets/StrokeRecentAdded.png")

