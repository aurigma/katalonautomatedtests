1
00:00:00,137 --> 00:00:00,138
1. openBrowser("")

2
00:00:02,955 --> 00:00:02,955
5. setViewPortSize(1930, 1180)

3
00:00:03,051 --> 00:00:03,052
9. navigateToUrl("http://localhost:3000/")

4
00:00:04,189 --> 00:00:04,190
13. driver = getExecutedBrowser().getName()

5
00:00:04,198 --> 00:00:04,199
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:04,220 --> 00:00:04,220
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:04,236 --> 00:00:04,237
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:04,563 --> 00:00:04,564
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:04,841 --> 00:00:04,842
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:05,126 --> 00:00:05,126
37. click(findTestObject("ConfigurationsTree/div_Widgetsjson"))

11
00:00:05,386 --> 00:00:05,387
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:06,155 --> 00:00:06,156
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:06,444 --> 00:00:06,444
49. delay(5)

14
00:00:11,451 --> 00:00:11,451
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/Iframe")

15
00:00:11,467 --> 00:00:11,470
57. parentElem = findWebElement(parent, 30)

16
00:00:11,488 --> 00:00:11,489
61. basePoint = parentElem.getLocation()

17
00:00:11,504 --> 00:00:11,505
65. switchToFrame(parent, 5)

18
00:00:11,533 --> 00:00:11,534
69. delay(4)

19
00:00:15,542 --> 00:00:15,543
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/AddRichText"))

20
00:00:16,062 --> 00:00:16,062
77. delay(2)

21
00:00:18,072 --> 00:00:18,073
81. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETextColorButton"))

22
00:00:18,603 --> 00:00:18,603
85. delay(2)

23
00:00:20,611 --> 00:00:20,612
89. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETopBar"))

24
00:00:20,773 --> 00:00:20,774
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), 5)

25
00:00:20,817 --> 00:00:20,818
97. delay(1)

26
00:00:21,825 --> 00:00:21,826
101. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), basePoint, baseDir + "/Widgets/ColorPicker/Widgets/RTEPickerOpened.png", failed + "/Widgets/ColorPicker/Widgets/RTEPickerOpened.png")

