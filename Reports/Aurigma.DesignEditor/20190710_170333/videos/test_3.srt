1
00:00:00,623 --> 00:00:00,635
1. openBrowser("")

2
00:00:04,504 --> 00:00:04,504
5. maximizeWindow()

3
00:00:04,881 --> 00:00:04,881
9. navigateToUrl("http://localhost:3000/")

4
00:00:06,347 --> 00:00:06,348
13. driver = getExecutedBrowser().getName()

5
00:00:06,360 --> 00:00:06,361
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:06,376 --> 00:00:06,377
21. click(findTestObject("ConfigurationsTree/div_base-editor"))

7
00:00:06,642 --> 00:00:06,642
25. click(findTestObject("ConfigurationsTree/div_Widgets"))

8
00:00:06,933 --> 00:00:06,934
29. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

9
00:00:07,265 --> 00:00:07,267
33. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

10
00:00:07,520 --> 00:00:07,521
37. click(findTestObject("TestStandPanel/button_RUN"))

11
00:00:08,486 --> 00:00:08,486
41. click(findTestObject("TestStandPanel/EditorTab"))

12
00:00:08,912 --> 00:00:08,912
45. delay(5)

13
00:00:13,929 --> 00:00:13,929
49. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorButton"))

14
00:00:14,646 --> 00:00:14,647
53. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 10)

15
00:00:15,269 --> 00:00:15,269
57. delay(2)

16
00:00:17,275 --> 00:00:17,275
61. Saturation = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

17
00:00:17,599 --> 00:00:17,600
65. SaturationIsSimilar = scripts.ScreenshotHandler.compareToBase(Saturation, baseDir + "/Widgets/ColorPicker/PickerRGB/SaturationChanged1.png")

18
00:00:17,624 --> 00:00:17,624
69. assert SaturationIsSimilar == true

19
00:00:17,631 --> 00:00:17,631
73. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 130)

20
00:00:18,256 --> 00:00:18,257
77. delay(2)

21
00:00:20,266 --> 00:00:20,266
81. Hue = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

22
00:00:20,578 --> 00:00:20,579
85. HueIsSimilar = scripts.ScreenshotHandler.compareToBase(Hue, baseDir + "/Widgets/ColorPicker/PickerRGB/HueChanged1.png")

23
00:00:20,617 --> 00:00:20,618
89. assert HueIsSimilar == true

24
00:00:20,625 --> 00:00:20,626
93. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 90)

25
00:00:21,285 --> 00:00:21,288
97. delay(2)

26
00:00:23,302 --> 00:00:23,303
101. Alpha = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

27
00:00:23,577 --> 00:00:23,577
105. AlphaIsSimilar = scripts.ScreenshotHandler.compareToBase(Alpha, baseDir + "/Widgets/ColorPicker/PickerRGB/PickersFirstCheck.png")

28
00:00:23,600 --> 00:00:23,600
109. assert AlphaIsSimilar == true

29
00:00:23,618 --> 00:00:23,619
113. click(findTestObject("TestStandPanel/FinishButton"))

30
00:00:23,879 --> 00:00:23,880
117. delay(2)

31
00:00:25,906 --> 00:00:25,907
121. AppliedColor1 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("TestStandPanel/PreviewImage"))

32
00:00:26,128 --> 00:00:26,128
125. AppliedColor1IsSimilar = scripts.ScreenshotHandler.compareToBase(AppliedColor1, baseDir + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck1.png")

33
00:00:26,188 --> 00:00:26,188
129. assert AppliedColor1IsSimilar == true

34
00:00:26,193 --> 00:00:26,193
133. click(findTestObject("TestStandPanel/BackToEditorButton"))

35
00:00:26,456 --> 00:00:26,456
137. delay(2)

36
00:00:28,463 --> 00:00:28,464
141. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), -5, -2)

37
00:00:29,014 --> 00:00:29,015
145. delay(2)

38
00:00:31,027 --> 00:00:31,027
149. Saturation2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

39
00:00:31,296 --> 00:00:31,296
153. Saturation2IsSimilar = scripts.ScreenshotHandler.compareToBase(Saturation2, baseDir + "/Widgets/ColorPicker/PickerRGB/SaturationChanged2.png")

40
00:00:31,317 --> 00:00:31,318
157. assert Saturation2IsSimilar == true

41
00:00:31,326 --> 00:00:31,326
161. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, -70)

42
00:00:31,928 --> 00:00:31,929
165. delay(2)

43
00:00:33,945 --> 00:00:33,945
169. Hue2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

44
00:00:34,248 --> 00:00:34,248
173. Hue2IsSimilar = scripts.ScreenshotHandler.compareToBase(Hue2, baseDir + "/Widgets/ColorPicker/PickerRGB/HueChanged2.png")

45
00:00:34,265 --> 00:00:34,266
177. assert Hue2IsSimilar == true

46
00:00:34,269 --> 00:00:34,270
181. delay(1)

47
00:00:35,275 --> 00:00:35,276
185. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, -30)

48
00:00:35,832 --> 00:00:35,832
189. delay(2)

49
00:00:37,839 --> 00:00:37,839
193. Alpha2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

50
00:00:38,332 --> 00:00:38,332
197. Alpha2IsSimilar = scripts.ScreenshotHandler.compareToBase(Alpha2, baseDir + "/Widgets/ColorPicker/PickerRGB/PickersSecondCheck.png")

51
00:00:38,353 --> 00:00:38,353
201. assert Alpha2IsSimilar == true

52
00:00:38,357 --> 00:00:38,357
205. click(findTestObject("TestStandPanel/FinishButton"))

53
00:00:38,651 --> 00:00:38,651
209. delay(2)

54
00:00:40,657 --> 00:00:40,657
213. AppliedColor2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("TestStandPanel/PreviewImage"))

55
00:00:40,878 --> 00:00:40,878
217. AppliedColor2IsSimilar = scripts.ScreenshotHandler.compareToBase(AppliedColor2, baseDir + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck2.png")

56
00:00:40,939 --> 00:00:40,939
221. assert AppliedColor2IsSimilar == true

57
00:00:40,945 --> 00:00:40,946
225. click(findTestObject("TestStandPanel/BackToEditorButton"))

58
00:00:41,184 --> 00:00:41,185
229. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"))

59
00:00:41,555 --> 00:00:41,555
233. delay(1)

60
00:00:42,556 --> 00:00:42,556
237. RecentColor = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

61
00:00:42,843 --> 00:00:42,843
241. RecentColorIsSimilar = scripts.ScreenshotHandler.compareToBase(RecentColor, baseDir + "/Widgets/ColorPicker/PickerRGB/RecentPaletteAdded.png")

62
00:00:42,863 --> 00:00:42,864
245. assert RecentColorIsSimilar == true

63
00:00:42,872 --> 00:00:42,872
249. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_R_Channel"))

64
00:00:43,256 --> 00:00:43,256
253. delay(2)

65
00:00:45,266 --> 00:00:45,266
257. Lightning = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

66
00:00:45,556 --> 00:00:45,556
261. LightningIsSimilar = scripts.ScreenshotHandler.compareToBase(Lightning, baseDir + "/Widgets/ColorPicker/PickerRGB/ChannelSelectedLightning.png")

67
00:00:45,577 --> 00:00:45,577
265. assert LightningIsSimilar == true

68
00:00:45,586 --> 00:00:45,586
269. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_R_Channel"), "25")

69
00:00:45,760 --> 00:00:45,761
273. delay(2)

70
00:00:47,762 --> 00:00:47,762
277. ValidR1 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

71
00:00:48,067 --> 00:00:48,067
281. ValidR1IsSimilar = scripts.ScreenshotHandler.compareToBase(ValidR1, baseDir + "/Widgets/ColorPicker/PickerRGB/RChannelValidInput1.png")

72
00:00:48,090 --> 00:00:48,090
285. assert ValidR1IsSimilar == true

73
00:00:48,096 --> 00:00:48,097
289. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_R_Channel"), "abc")

74
00:00:48,229 --> 00:00:48,230
293. delay(2)

75
00:00:50,241 --> 00:00:50,241
297. InvalidR1 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

76
00:00:50,536 --> 00:00:50,537
301. InvalidR1IsSimilar = scripts.ScreenshotHandler.compareToBase(InvalidR1, baseDir + "/Widgets/ColorPicker/PickerRGB/RChannelInvalidInput1.png")

77
00:00:50,559 --> 00:00:50,560
305. assert InvalidR1IsSimilar == true

