1
00:00:00,617 --> 00:00:00,619
1. openBrowser("")

2
00:00:03,530 --> 00:00:03,532
5. maximizeWindow()

3
00:00:03,817 --> 00:00:03,817
9. navigateToUrl("http://localhost:3000/")

4
00:00:05,073 --> 00:00:05,073
13. driver = getExecutedBrowser().getName()

5
00:00:05,078 --> 00:00:05,079
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:05,086 --> 00:00:05,087
21. click(findTestObject("ConfigurationsTree/div_base-editor"))

7
00:00:05,374 --> 00:00:05,375
25. click(findTestObject("ConfigurationsTree/div_Widgets"))

8
00:00:05,650 --> 00:00:05,650
29. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

9
00:00:06,022 --> 00:00:06,023
33. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

10
00:00:06,284 --> 00:00:06,284
37. click(findTestObject("TestStandPanel/button_RUN"))

11
00:00:07,161 --> 00:00:07,161
41. click(findTestObject("TestStandPanel/EditorTab"))

12
00:00:07,541 --> 00:00:07,542
45. delay(5)

13
00:00:12,553 --> 00:00:12,553
49. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorButton"))

14
00:00:13,084 --> 00:00:13,084
53. delay(2)

15
00:00:15,090 --> 00:00:15,091
57. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

16
00:00:15,461 --> 00:00:15,462
61. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

17
00:00:16,128 --> 00:00:16,128
65. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

18
00:00:16,784 --> 00:00:16,785
69. delay(2)

19
00:00:18,798 --> 00:00:18,798
73. Saturation = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

20
00:00:19,098 --> 00:00:19,099
77. SaturationIsSimilar = scripts.ScreenshotHandler.compareToBase(Saturation, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

21
00:00:19,125 --> 00:00:19,126
81. assert SaturationIsSimilar == true

22
00:00:19,132 --> 00:00:19,133
85. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 85)

23
00:00:19,758 --> 00:00:19,759
89. delay(2)

24
00:00:21,765 --> 00:00:21,767
93. Hue = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

25
00:00:22,041 --> 00:00:22,041
97. HueIsSimilar = scripts.ScreenshotHandler.compareToBase(Hue, baseDir + "/Widgets/ColorPicker/PickerCMYK/HueChanged1.png")

26
00:00:22,080 --> 00:00:22,081
101. assert HueIsSimilar == true

