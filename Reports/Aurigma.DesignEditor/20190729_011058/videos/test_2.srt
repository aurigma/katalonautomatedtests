1
00:00:00,204 --> 00:00:00,205
1. openBrowser("")

2
00:00:03,289 --> 00:00:03,289
5. setViewPortSize(1930, 1180)

3
00:00:03,418 --> 00:00:03,422
9. navigateToUrl("http://localhost:3000/")

4
00:00:04,792 --> 00:00:04,793
13. driver = getExecutedBrowser().getName()

5
00:00:04,802 --> 00:00:04,803
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:04,836 --> 00:00:04,842
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:04,851 --> 00:00:04,852
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:05,715 --> 00:00:05,716
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:06,017 --> 00:00:06,017
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:06,339 --> 00:00:06,340
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:06,622 --> 00:00:06,623
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:07,760 --> 00:00:07,761
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:08,140 --> 00:00:08,141
49. delay(5)

14
00:00:13,169 --> 00:00:13,170
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:13,182 --> 00:00:13,184
57. parentElem = findWebElement(parent, 30)

16
00:00:13,700 --> 00:00:13,701
61. basePoint = parentElem.getLocation()

17
00:00:14,445 --> 00:00:14,446
65. switchToFrame(parent, 5)

18
00:00:14,946 --> 00:00:14,948
69. delay(2)

19
00:00:16,977 --> 00:00:16,979
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:17,660 --> 00:00:17,661
77. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

21
00:00:17,734 --> 00:00:17,734
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

22
00:00:17,803 --> 00:00:17,804
85. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

23
00:00:18,283 --> 00:00:18,283
89. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

24
00:00:18,378 --> 00:00:18,381
93. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

25
00:00:19,201 --> 00:00:19,202
97. delay(1)

26
00:00:20,226 --> 00:00:20,227
101. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

27
00:00:20,264 --> 00:00:20,265
105. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

28
00:00:20,802 --> 00:00:20,803
109. delay(2)

29
00:00:22,812 --> 00:00:22,813
113. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

30
00:00:23,101 --> 00:00:23,102
117. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 5)

31
00:00:23,156 --> 00:00:23,157
121. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 85)

32
00:00:23,838 --> 00:00:23,838
125. delay(3)

33
00:00:26,851 --> 00:00:26,852
129. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/HueChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/HueChanged1.png")

34
00:00:27,259 --> 00:00:27,260
133. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 5)

35
00:00:27,309 --> 00:00:27,311
137. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 110)

36
00:00:28,015 --> 00:00:28,015
141. delay(2)

37
00:00:30,032 --> 00:00:30,033
145. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/PickersFirstCheck.png", failed + "/Widgets/ColorPicker/PickerCMYK/PickersFirstCheck.png")

38
00:00:30,352 --> 00:00:30,353
149. switchToDefaultContent()

39
00:00:30,369 --> 00:00:30,370
153. click(findTestObject("TestStandPanel/FinishButton"))

40
00:00:30,659 --> 00:00:30,660
157. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

41
00:00:31,251 --> 00:00:31,252
161. delay(2)

42
00:00:33,284 --> 00:00:33,285
165. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck1.png", failed + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck1.png")

43
00:00:33,631 --> 00:00:33,631
169. click(findTestObject("TestStandPanel/BackToEditorButton"))

44
00:00:33,910 --> 00:00:33,911
173. switchToFrame(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe"), 5)

45
00:00:33,960 --> 00:00:33,961
177. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

46
00:00:34,074 --> 00:00:34,074
181. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

47
00:00:34,124 --> 00:00:34,125
185. delay(2)

48
00:00:36,136 --> 00:00:36,137
189. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5, 2)

49
00:00:36,756 --> 00:00:36,757
193. delay(2)

50
00:00:38,765 --> 00:00:38,765
197. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged2.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged2.png")

51
00:00:39,066 --> 00:00:39,066
201. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 5)

52
00:00:39,105 --> 00:00:39,105
205. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 20)

53
00:00:39,758 --> 00:00:39,759
209. delay(2)

54
00:00:41,770 --> 00:00:41,771
213. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/HueChanged2.png", failed + "/Widgets/ColorPicker/PickerCMYK/HueChanged2.png")

55
00:00:42,051 --> 00:00:42,051
217. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 5)

56
00:00:42,089 --> 00:00:42,090
221. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 20)

57
00:00:42,642 --> 00:00:42,643
225. delay(2)

58
00:00:44,653 --> 00:00:44,654
229. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/PickersSecondCheck.png", failed + "/Widgets/ColorPicker/PickerCMYK/PickersSecondCheck.png")

59
00:00:44,958 --> 00:00:44,959
233. switchToDefaultContent()

60
00:00:44,974 --> 00:00:44,975
237. click(findTestObject("TestStandPanel/FinishButton"))

61
00:00:45,246 --> 00:00:45,249
241. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

62
00:00:45,373 --> 00:00:45,374
245. delay(2)

63
00:00:47,390 --> 00:00:47,390
249. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck2.png", failed + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck2.png")

64
00:00:47,729 --> 00:00:47,730
253. click(findTestObject("TestStandPanel/BackToEditorButton"))

65
00:00:47,984 --> 00:00:47,985
257. switchToFrame(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe"), 5)

66
00:00:48,013 --> 00:00:48,013
261. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

67
00:00:48,048 --> 00:00:48,049
265. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"), 5)

68
00:00:48,116 --> 00:00:48,117
269. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"))

69
00:00:48,666 --> 00:00:48,666
273. delay(2)

70
00:00:50,686 --> 00:00:50,687
277. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"))

71
00:00:50,839 --> 00:00:50,840
281. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/RecentPaletteAdded.png", failed + "/Widgets/ColorPicker/PickerCMYK/RecentPaletteAdded.png")

