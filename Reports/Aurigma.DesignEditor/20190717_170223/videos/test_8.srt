1
00:00:00,117 --> 00:00:00,118
1. openBrowser("")

2
00:00:01,351 --> 00:00:01,352
5. maximizeWindow()

3
00:00:01,491 --> 00:00:01,492
9. navigateToUrl("http://localhost:3000/")

4
00:00:02,923 --> 00:00:02,924
13. driver = getExecutedBrowser().getName()

5
00:00:02,928 --> 00:00:02,929
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:02,932 --> 00:00:02,933
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:02,941 --> 00:00:02,941
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,101 --> 00:00:03,101
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,211 --> 00:00:03,212
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:03,356 --> 00:00:03,357
37. click(findTestObject("ConfigurationsTree/div_Widgetsjson"))

11
00:00:03,469 --> 00:00:03,469
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:03,536 --> 00:00:03,536
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:03,717 --> 00:00:03,718
49. delay(5)

14
00:00:08,730 --> 00:00:08,731
53. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/button_More"))

15
00:00:09,243 --> 00:00:09,244
57. delay(2)

16
00:00:11,259 --> 00:00:11,259
61. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokeButton"))

17
00:00:11,599 --> 00:00:11,600
65. delay(2)

18
00:00:13,609 --> 00:00:13,609
69. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokeDialog"), baseDir + "/Widgets/ColorPicker/Widgets/StrokeDialogInitial.png", failed + "/Widgets/ColorPicker/Widgets/StrokeDialogInitial.png")

19
00:00:14,229 --> 00:00:14,229
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/OpenStrokePalette"))

20
00:00:14,438 --> 00:00:14,439
77. delay(3)

21
00:00:17,450 --> 00:00:17,451
81. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokePalette"), baseDir + "/Widgets/ColorPicker/Widgets/StrokePaletteInitial.png", failed + "/Widgets/ColorPicker/Widgets/StrokePaletteInitial.png")

22
00:00:18,050 --> 00:00:18,050
85. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/StrokeOK"))

23
00:00:18,266 --> 00:00:18,266
89. delay(2)

24
00:00:20,283 --> 00:00:20,283
93. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokeDialog"), baseDir + "/Widgets/ColorPicker/Widgets/BackToStrokeDialog.png", failed + "/Widgets/ColorPicker/Widgets/BackToStrokeDialog.png")

25
00:00:20,879 --> 00:00:20,879
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/OpenStrokePalette"))

26
00:00:21,091 --> 00:00:21,092
101. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/StrokeColors_Yellow"))

27
00:00:21,410 --> 00:00:21,411
105. delay(2)

28
00:00:23,427 --> 00:00:23,427
109. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokeDialog"), 5)

29
00:00:28,571 --> 00:00:28,572
113. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/TextStrokeDialog"), baseDir + "/Widgets/ColorPicker/Widgets/StrokeRecentAdded.png", failed + "/Widgets/ColorPicker/Widgets/StrokeRecentAdded.png")

