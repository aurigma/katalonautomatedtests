1
00:00:00,368 --> 00:00:00,371
1. openBrowser("")

2
00:00:05,514 --> 00:00:05,518
5. setViewPortSize(1930, 1180)

3
00:00:06,075 --> 00:00:06,077
9. navigateToUrl("http://localhost:3000/")

4
00:00:09,486 --> 00:00:09,489
13. driver = getExecutedBrowser().getName()

5
00:00:09,577 --> 00:00:09,578
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:09,606 --> 00:00:09,612
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:09,680 --> 00:00:09,705
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:11,049 --> 00:00:11,050
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:11,458 --> 00:00:11,459
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:11,799 --> 00:00:11,799
37. click(findTestObject("ConfigurationsTree/div_Palettejson"))

11
00:00:12,131 --> 00:00:12,133
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:12,483 --> 00:00:12,485
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:14,446 --> 00:00:14,450
49. delay(5)

14
00:00:19,595 --> 00:00:19,604
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:19,630 --> 00:00:19,631
57. parentElem = findWebElement(parent, 30)

16
00:00:23,728 --> 00:00:23,730
61. basePoint = parentElem.getLocation()

17
00:00:26,898 --> 00:00:26,901
65. switchToFrame(parent, 5)

18
00:00:27,157 --> 00:00:27,160
69. delay(2)

19
00:00:29,174 --> 00:00:29,175
73. if (driver == "CHROME_DRIVER")

20
00:00:29,187 --> 00:00:29,188
77. delay(1)

21
00:00:30,206 --> 00:00:30,207
81. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton"))

22
00:00:30,876 --> 00:00:30,877
85. delay(2)

23
00:00:32,895 --> 00:00:32,896
89. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), 5)

24
00:00:32,981 --> 00:00:32,981
93. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/TextColorDialog.png", failed + "/Widgets/ColorPicker/Palette/TextColorDialog.png")

25
00:00:33,438 --> 00:00:33,439
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Purple"))

26
00:00:34,199 --> 00:00:34,200
101. delay(1)

27
00:00:35,208 --> 00:00:35,209
105. switchToDefaultContent()

28
00:00:35,240 --> 00:00:35,241
109. click(findTestObject("TestStandPanel/FinishButton"))

29
00:00:35,665 --> 00:00:35,671
113. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

30
00:00:35,786 --> 00:00:35,788
117. delay(2)

31
00:00:37,806 --> 00:00:37,807
121. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png", failed + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png")

32
00:00:38,147 --> 00:00:38,148
125. click(findTestObject("TestStandPanel/BackToEditorButton"))

33
00:00:38,604 --> 00:00:38,604
129. switchToFrame(parent, 5)

34
00:00:38,677 --> 00:00:38,681
133. delay(1)

35
00:00:39,694 --> 00:00:39,695
137. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

36
00:00:40,200 --> 00:00:40,201
141. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

37
00:00:40,411 --> 00:00:40,412
145. delay(2)

38
00:00:42,430 --> 00:00:42,430
149. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png", failed + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png")

39
00:00:42,734 --> 00:00:42,735
153. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Burgundy"))

40
00:00:43,364 --> 00:00:43,366
157. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

41
00:00:43,856 --> 00:00:43,858
161. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RecentColor_Purple"))

42
00:00:44,582 --> 00:00:44,584
165. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RecentColor_Purple"))

43
00:00:44,759 --> 00:00:44,759
169. delay(2)

44
00:00:46,768 --> 00:00:46,769
173. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck2.png", failed + "/Widgets/ColorPicker/Palette/AddToRecentCheck2.png")

45
00:00:47,068 --> 00:00:47,068
177. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

46
00:00:47,606 --> 00:00:47,609
181. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

47
00:00:47,796 --> 00:00:47,796
185. delay(2)

48
00:00:49,802 --> 00:00:49,803
189. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck3.png", failed + "/Widgets/ColorPicker/Palette/AddToRecentCheck3.png")

49
00:00:50,169 --> 00:00:50,169
193. navigateToUrl("http://localhost:3000/")

50
00:00:51,242 --> 00:00:51,243
197. if (driver == "CHROME_DRIVER")

51
00:00:51,266 --> 00:00:51,269
201. click(findTestObject("ConfigurationsTree/div_base-editor"))

52
00:00:52,148 --> 00:00:52,148
205. click(findTestObject("ConfigurationsTree/div_Widgets"))

53
00:00:52,435 --> 00:00:52,435
209. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

54
00:00:52,769 --> 00:00:52,770
213. click(findTestObject("ConfigurationsTree/div_Palettejson"))

55
00:00:53,055 --> 00:00:53,056
217. click(findTestObject("TestStandPanel/button_RUN"))

56
00:00:54,173 --> 00:00:54,174
221. click(findTestObject("TestStandPanel/EditorTab"))

57
00:00:55,383 --> 00:00:55,383
225. delay(5)

58
00:01:00,402 --> 00:01:00,403
229. switchToFrame(parent, 5)

59
00:01:01,729 --> 00:01:01,730
233. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton"))

60
00:01:02,703 --> 00:01:02,703
237. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), 5)

61
00:01:07,881 --> 00:01:07,882
241. delay(2)

62
00:01:09,889 --> 00:01:09,890
245. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/CPAtNewPage.png", failed + "/Widgets/ColorPicker/Palette/CPAtNewPage.png")

