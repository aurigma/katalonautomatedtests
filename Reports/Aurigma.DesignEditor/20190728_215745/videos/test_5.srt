1
00:00:00,194 --> 00:00:00,195
1. openBrowser("")

2
00:00:04,296 --> 00:00:04,296
5. setViewPortSize(1930, 1180)

3
00:00:04,395 --> 00:00:04,397
9. navigateToUrl("http://localhost:3000/")

4
00:00:06,381 --> 00:00:06,382
13. driver = getExecutedBrowser().getName()

5
00:00:06,404 --> 00:00:06,405
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:06,419 --> 00:00:06,419
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:06,430 --> 00:00:06,431
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:07,337 --> 00:00:07,337
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:07,679 --> 00:00:07,680
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:08,033 --> 00:00:08,034
37. click(findTestObject("ConfigurationsTree/div_SectionCollapsejson"))

11
00:00:08,320 --> 00:00:08,324
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:08,607 --> 00:00:08,608
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:09,969 --> 00:00:09,971
49. delay(5)

14
00:00:15,023 --> 00:00:15,025
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:15,042 --> 00:00:15,069
57. parentElem = findWebElement(parent, 30)

16
00:00:19,073 --> 00:00:19,079
61. basePoint = parentElem.getLocation()

17
00:00:19,622 --> 00:00:19,624
65. switchToFrame(parent, 5)

18
00:00:21,692 --> 00:00:21,695
69. delay(2)

19
00:00:23,710 --> 00:00:23,712
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

20
00:00:24,465 --> 00:00:24,465
77. delay(2)

21
00:00:26,494 --> 00:00:26,495
81. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/PickerWithCollapseSections.png", failed + "/Widgets/ColorPicker/SectionCollapse/PickerWithCollapseSections.png")

22
00:00:26,945 --> 00:00:26,946
85. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/GrayscaleSectionCollapse"))

23
00:00:27,697 --> 00:00:27,698
89. delay(2)

24
00:00:29,705 --> 00:00:29,706
93. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/GrayscaleCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/GrayscaleCollapsed.png")

25
00:00:30,074 --> 00:00:30,075
97. delay(2)

26
00:00:32,083 --> 00:00:32,084
101. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RgbSectionCollapse"))

27
00:00:32,835 --> 00:00:32,836
105. delay(2)

28
00:00:34,849 --> 00:00:34,849
109. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/RGBCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/RGBCollapsed.png")

29
00:00:35,199 --> 00:00:35,200
113. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/CmykSectionCollapse"))

30
00:00:35,751 --> 00:00:35,752
117. delay(2)

31
00:00:37,762 --> 00:00:37,762
121. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CMYKCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/CMYKCollapsed.png")

32
00:00:38,110 --> 00:00:38,110
125. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RecentSectionCollapse"))

33
00:00:38,634 --> 00:00:38,635
129. delay(2)

34
00:00:40,650 --> 00:00:40,651
133. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/AllCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/AllCollapsed.png")

35
00:00:41,002 --> 00:00:41,003
137. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

36
00:00:41,648 --> 00:00:41,649
141. delay(1)

37
00:00:42,679 --> 00:00:42,680
145. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

38
00:00:43,339 --> 00:00:43,341
149. delay(2)

39
00:00:45,376 --> 00:00:45,376
153. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved.png")

40
00:00:45,714 --> 00:00:45,714
157. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/button_OK"))

41
00:00:46,369 --> 00:00:46,369
161. delay(1)

42
00:00:47,385 --> 00:00:47,387
165. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

43
00:00:48,048 --> 00:00:48,050
169. delay(2)

44
00:00:50,074 --> 00:00:50,074
173. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved2.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved2.png")

45
00:00:50,418 --> 00:00:50,420
177. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RecentSectionCollapse"))

46
00:00:50,959 --> 00:00:50,960
181. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RgbSectionCollapse"))

47
00:00:51,543 --> 00:00:51,543
185. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/OIPickAnotherItem"))

48
00:00:52,397 --> 00:00:52,398
189. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

49
00:00:53,215 --> 00:00:53,219
193. delay(2)

50
00:00:55,230 --> 00:00:55,231
197. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved3.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved3.png")

51
00:00:55,614 --> 00:00:55,615
201. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/AddText"))

52
00:00:56,847 --> 00:00:56,848
205. delay(3)

53
00:00:59,874 --> 00:00:59,874
209. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

54
00:01:00,580 --> 00:01:00,580
213. delay(2)

55
00:01:02,595 --> 00:01:02,596
217. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved4.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved4.png")

56
00:01:02,967 --> 00:01:02,968
221. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/button_OK"))

57
00:01:03,557 --> 00:01:03,558
225. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/TextArea"), "Test")

58
00:01:04,002 --> 00:01:04,003
229. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

59
00:01:04,984 --> 00:01:04,985
233. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved5.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved5.png")

60
00:01:05,487 --> 00:01:05,488
237. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/UndoButton"))

61
00:01:06,645 --> 00:01:06,646
241. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/OIPickAnotherItem"))

62
00:01:07,465 --> 00:01:07,466
245. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

63
00:01:08,246 --> 00:01:08,246
249. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionReverted.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionReverted.png")

