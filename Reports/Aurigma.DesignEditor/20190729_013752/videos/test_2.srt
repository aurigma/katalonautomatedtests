1
00:00:00,284 --> 00:00:00,286
1. openBrowser("")

2
00:00:02,054 --> 00:00:02,055
5. setViewPortSize(1930, 1180)

3
00:00:02,170 --> 00:00:02,170
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,256 --> 00:00:03,257
13. driver = getExecutedBrowser().getName()

5
00:00:03,261 --> 00:00:03,261
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,265 --> 00:00:03,267
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:03,287 --> 00:00:03,292
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,417 --> 00:00:03,418
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,533 --> 00:00:03,534
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:03,689 --> 00:00:03,689
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:03,840 --> 00:00:03,841
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:03,939 --> 00:00:03,940
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:04,133 --> 00:00:04,134
49. delay(5)

14
00:00:09,146 --> 00:00:09,146
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:09,152 --> 00:00:09,154
57. parentElem = findWebElement(parent, 30)

16
00:00:09,231 --> 00:00:09,232
61. basePoint = parentElem.getLocation()

17
00:00:09,249 --> 00:00:09,251
65. switchToFrame(parent, 5)

18
00:00:09,319 --> 00:00:09,319
69. delay(2)

19
00:00:11,329 --> 00:00:11,330
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:11,694 --> 00:00:11,695
77. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

21
00:00:11,748 --> 00:00:11,750
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

22
00:00:11,798 --> 00:00:11,798
85. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

23
00:00:12,038 --> 00:00:12,039
89. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

24
00:00:12,090 --> 00:00:12,091
93. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

25
00:00:12,386 --> 00:00:12,387
97. delay(1)

26
00:00:13,398 --> 00:00:13,399
101. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

27
00:00:13,445 --> 00:00:13,445
105. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

28
00:00:13,775 --> 00:00:13,775
109. delay(2)

29
00:00:15,786 --> 00:00:15,786
113. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

