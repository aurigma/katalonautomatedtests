1
00:00:00,153 --> 00:00:00,153
1. openBrowser("")

2
00:00:01,969 --> 00:00:01,969
5. setViewPortSize(1930, 1180)

3
00:00:02,098 --> 00:00:02,099
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,150 --> 00:00:03,151
13. driver = getExecutedBrowser().getName()

5
00:00:03,168 --> 00:00:03,169
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,185 --> 00:00:03,185
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:03,201 --> 00:00:03,202
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,298 --> 00:00:03,299
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,418 --> 00:00:03,419
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:03,553 --> 00:00:03,554
37. click(findTestObject("ConfigurationsTree/div_Widgetsjson"))

11
00:00:03,669 --> 00:00:03,669
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:03,746 --> 00:00:03,747
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:03,942 --> 00:00:03,943
49. delay(5)

14
00:00:08,953 --> 00:00:08,955
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/Iframe")

15
00:00:08,970 --> 00:00:08,971
57. parentElem = findWebElement(parent, 30)

16
00:00:09,047 --> 00:00:09,047
61. basePoint = parentElem.getLocation()

17
00:00:09,060 --> 00:00:09,060
65. switchToFrame(parent, 5)

18
00:00:09,096 --> 00:00:09,096
69. delay(2)

19
00:00:11,106 --> 00:00:11,106
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/AddRichText"))

20
00:00:11,442 --> 00:00:11,443
77. delay(2)

21
00:00:13,452 --> 00:00:13,453
81. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETextColorButton"))

22
00:00:14,161 --> 00:00:14,162
85. delay(2)

23
00:00:16,171 --> 00:00:16,171
89. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETopBar"))

24
00:00:16,258 --> 00:00:16,259
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), 5)

25
00:00:16,311 --> 00:00:16,311
97. delay(1)

26
00:00:17,316 --> 00:00:17,318
101. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), basePoint, baseDir + "/Widgets/ColorPicker/Widgets/RTEPickerOpened.png", failed + "/Widgets/ColorPicker/Widgets/RTEPickerOpened.png")

