1
00:00:00,217 --> 00:00:00,220
1. openBrowser("")

2
00:00:02,016 --> 00:00:02,016
5. setViewPortSize(1930, 1180)

3
00:00:02,135 --> 00:00:02,136
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,191 --> 00:00:03,192
13. driver = getExecutedBrowser().getName()

5
00:00:03,208 --> 00:00:03,209
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,223 --> 00:00:03,225
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:03,239 --> 00:00:03,242
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,340 --> 00:00:03,341
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,448 --> 00:00:03,449
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:03,571 --> 00:00:03,571
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:03,710 --> 00:00:03,711
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:03,817 --> 00:00:03,818
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:03,982 --> 00:00:03,984
49. delay(5)

14
00:00:09,003 --> 00:00:09,003
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:09,008 --> 00:00:09,009
57. parentElem = findWebElement(parent, 30)

16
00:00:09,079 --> 00:00:09,079
61. basePoint = parentElem.getLocation()

17
00:00:09,098 --> 00:00:09,100
65. switchToFrame(parent, 5)

18
00:00:09,149 --> 00:00:09,149
69. delay(2)

19
00:00:11,157 --> 00:00:11,157
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:11,540 --> 00:00:11,540
77. delay(4)

21
00:00:15,559 --> 00:00:15,561
81. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 10)

22
00:00:15,859 --> 00:00:15,859
85. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"))

23
00:00:15,983 --> 00:00:15,983
89. delay(2)

24
00:00:17,991 --> 00:00:17,992
93. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerRGB/SaturationChanged1.png")

