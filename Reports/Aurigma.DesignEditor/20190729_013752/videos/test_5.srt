1
00:00:00,164 --> 00:00:00,165
1. openBrowser("")

2
00:00:01,886 --> 00:00:01,888
5. setViewPortSize(1930, 1180)

3
00:00:02,007 --> 00:00:02,008
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,116 --> 00:00:03,117
13. driver = getExecutedBrowser().getName()

5
00:00:03,126 --> 00:00:03,128
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,142 --> 00:00:03,143
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:03,169 --> 00:00:03,170
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,274 --> 00:00:03,275
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,383 --> 00:00:03,384
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:03,501 --> 00:00:03,502
37. click(findTestObject("ConfigurationsTree/div_SectionCollapsejson"))

11
00:00:03,641 --> 00:00:03,641
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:03,719 --> 00:00:03,720
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:03,902 --> 00:00:03,903
49. delay(5)

14
00:00:08,916 --> 00:00:08,917
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:08,930 --> 00:00:08,931
57. parentElem = findWebElement(parent, 30)

16
00:00:09,014 --> 00:00:09,014
61. basePoint = parentElem.getLocation()

17
00:00:09,026 --> 00:00:09,027
65. switchToFrame(parent, 5)

18
00:00:09,089 --> 00:00:09,091
69. delay(2)

19
00:00:11,106 --> 00:00:11,107
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

20
00:00:11,749 --> 00:00:11,750
77. delay(2)

21
00:00:13,766 --> 00:00:13,766
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), 3)

22
00:00:13,805 --> 00:00:13,807
85. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/PickerWithCollapseSections.png", failed + "/Widgets/ColorPicker/SectionCollapse/PickerWithCollapseSections.png")

