1
00:00:00,152 --> 00:00:00,152
1. openBrowser("")

2
00:00:01,371 --> 00:00:01,372
5. maximizeWindow()

3
00:00:01,513 --> 00:00:01,513
9. navigateToUrl("http://localhost:3000/")

4
00:00:02,906 --> 00:00:02,908
13. driver = getExecutedBrowser().getName()

5
00:00:02,918 --> 00:00:02,918
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:02,929 --> 00:00:02,931
21. click(findTestObject("ConfigurationsTree/div_base-editor"))

7
00:00:03,156 --> 00:00:03,156
25. click(findTestObject("ConfigurationsTree/div_Widgets"))

8
00:00:03,285 --> 00:00:03,287
29. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

9
00:00:03,422 --> 00:00:03,423
33. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

10
00:00:03,583 --> 00:00:03,584
37. click(findTestObject("TestStandPanel/button_RUN"))

11
00:00:03,682 --> 00:00:03,685
41. click(findTestObject("TestStandPanel/EditorTab"))

12
00:00:03,893 --> 00:00:03,894
45. delay(5)

13
00:00:08,907 --> 00:00:08,908
49. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorButton"))

14
00:00:09,411 --> 00:00:09,411
53. delay(2)

15
00:00:11,421 --> 00:00:11,421
57. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

16
00:00:11,714 --> 00:00:11,714
61. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

17
00:00:12,281 --> 00:00:12,282
65. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

18
00:00:12,927 --> 00:00:12,927
69. delay(2)

19
00:00:14,939 --> 00:00:14,940
73. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

