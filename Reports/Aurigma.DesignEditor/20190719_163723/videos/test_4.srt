1
00:00:00,130 --> 00:00:00,131
1. openBrowser("")

2
00:00:02,956 --> 00:00:02,956
5. maximizeWindow()

3
00:00:03,259 --> 00:00:03,260
9. navigateToUrl("http://localhost:3000/")

4
00:00:04,723 --> 00:00:04,723
13. driver = getExecutedBrowser().getName()

5
00:00:04,738 --> 00:00:04,739
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:04,761 --> 00:00:04,763
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:04,773 --> 00:00:04,773
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:05,048 --> 00:00:05,049
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:05,318 --> 00:00:05,318
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:05,574 --> 00:00:05,574
37. click(findTestObject("ConfigurationsTree/div_Widgetsjson"))

11
00:00:05,824 --> 00:00:05,824
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:06,628 --> 00:00:06,629
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:06,926 --> 00:00:06,927
49. delay(5)

14
00:00:11,939 --> 00:00:11,939
53. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/AddRichText"))

15
00:00:12,531 --> 00:00:12,531
57. delay(2)

16
00:00:14,541 --> 00:00:14,541
61. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETextColorButton"))

17
00:00:15,127 --> 00:00:15,127
65. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), 5)

18
00:00:15,197 --> 00:00:15,198
69. delay(3)

19
00:00:18,205 --> 00:00:18,205
73. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), baseDir + "/Widgets/ColorPicker/Widgets/RTEPickerOpened.png", failed + "/Widgets/ColorPicker/Widgets/RTEPickerOpened.png")

20
00:00:18,369 --> 00:00:18,369
77. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorOk"))

21
00:00:18,838 --> 00:00:18,838
81. delay(2)

22
00:00:20,844 --> 00:00:20,844
85. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETopBar"), baseDir + "/Widgets/ColorPicker/Widgets/RTEPickerClosed.png", failed + "/Widgets/ColorPicker/Widgets/RTEPickerClosed.png")

23
00:00:20,990 --> 00:00:20,991
89. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETextColorButton"))

24
00:00:21,531 --> 00:00:21,531
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColors_Orange"), 5)

25
00:00:21,591 --> 00:00:21,591
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColors_Orange"))

26
00:00:22,099 --> 00:00:22,099
101. delay(2)

27
00:00:24,105 --> 00:00:24,106
105. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTEColorPicker"), baseDir + "/Widgets/ColorPicker/Widgets/RTEPickerRecent.png", failed + "/Widgets/ColorPicker/Widgets/RTEPickerRecent.png")

