1
00:00:00,178 --> 00:00:00,179
1. openBrowser("")

2
00:00:01,411 --> 00:00:01,411
5. maximizeWindow()

3
00:00:01,574 --> 00:00:01,575
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,066 --> 00:00:03,066
13. driver = getExecutedBrowser().getName()

5
00:00:03,074 --> 00:00:03,075
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,079 --> 00:00:03,079
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:03,085 --> 00:00:03,086
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,310 --> 00:00:03,311
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,450 --> 00:00:03,450
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:03,596 --> 00:00:03,597
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:03,718 --> 00:00:03,718
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:03,827 --> 00:00:03,827
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:04,045 --> 00:00:04,045
49. delay(5)

14
00:00:09,058 --> 00:00:09,058
53. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorButton"))

15
00:00:09,577 --> 00:00:09,577
57. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 10)

16
00:00:10,268 --> 00:00:10,268
61. delay(2)

17
00:00:12,284 --> 00:00:12,284
65. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerRGB/SaturationChanged1.png")

18
00:00:12,933 --> 00:00:12,933
69. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 130)

19
00:00:13,287 --> 00:00:13,288
73. delay(2)

20
00:00:15,314 --> 00:00:15,315
77. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/HueChanged1.png", failed + "/Widgets/ColorPicker/PickerRGB/HueChanged1.png")

21
00:00:16,021 --> 00:00:16,022
81. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 90)

22
00:00:16,434 --> 00:00:16,435
85. delay(2)

23
00:00:18,444 --> 00:00:18,445
89. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/PickersFirstCheck.png", failed + "/Widgets/ColorPicker/PickerRGB/PickersFirstCheck.png")

24
00:00:19,100 --> 00:00:19,101
93. click(findTestObject("TestStandPanel/FinishButton"))

25
00:00:19,223 --> 00:00:19,224
97. delay(2)

26
00:00:21,235 --> 00:00:21,236
101. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), baseDir + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck1.png", failed + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck1.png")

27
00:00:21,820 --> 00:00:21,821
105. click(findTestObject("TestStandPanel/BackToEditorButton"))

28
00:00:21,899 --> 00:00:21,900
109. delay(2)

29
00:00:23,914 --> 00:00:23,915
113. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), -5, -2)

30
00:00:24,253 --> 00:00:24,253
117. delay(2)

31
00:00:26,265 --> 00:00:26,265
121. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/SaturationChanged2.png", failed + "/Widgets/ColorPicker/PickerRGB/SaturationChanged2.png")

32
00:00:26,905 --> 00:00:26,905
125. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, -70)

33
00:00:27,261 --> 00:00:27,263
129. delay(2)

34
00:00:29,272 --> 00:00:29,272
133. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/HueChanged2.png", failed + "/Widgets/ColorPicker/PickerRGB/HueChanged2.png")

35
00:00:29,893 --> 00:00:29,893
137. delay(1)

36
00:00:30,903 --> 00:00:30,904
141. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, -30)

37
00:00:31,306 --> 00:00:31,307
145. delay(2)

38
00:00:33,314 --> 00:00:33,314
149. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/PickersSecondCheck.png", failed + "/Widgets/ColorPicker/PickerRGB/PickersSecondCheck.png")

39
00:00:33,970 --> 00:00:33,970
153. click(findTestObject("TestStandPanel/FinishButton"))

40
00:00:34,059 --> 00:00:34,060
157. delay(2)

41
00:00:36,068 --> 00:00:36,069
161. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), baseDir + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck2.png", failed + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck2.png")

42
00:00:36,651 --> 00:00:36,652
165. click(findTestObject("TestStandPanel/BackToEditorButton"))

43
00:00:36,785 --> 00:00:36,786
169. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"))

44
00:00:37,067 --> 00:00:37,067
173. delay(1)

45
00:00:38,079 --> 00:00:38,079
177. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerRGB/RecentPaletteAdded.png", failed + "/Widgets/ColorPicker/PickerRGB/RecentPaletteAdded.png")

