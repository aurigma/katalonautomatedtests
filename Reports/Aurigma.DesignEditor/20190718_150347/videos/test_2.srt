1
00:00:00,165 --> 00:00:00,165
1. openBrowser("")

2
00:00:02,040 --> 00:00:02,042
5. maximizeWindow()

3
00:00:02,187 --> 00:00:02,189
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,652 --> 00:00:03,653
13. driver = getExecutedBrowser().getName()

5
00:00:03,662 --> 00:00:03,663
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,671 --> 00:00:03,672
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:03,680 --> 00:00:03,681
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,819 --> 00:00:03,820
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,984 --> 00:00:03,985
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:04,124 --> 00:00:04,126
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:04,291 --> 00:00:04,291
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:04,408 --> 00:00:04,409
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:04,600 --> 00:00:04,601
49. delay(5)

14
00:00:09,620 --> 00:00:09,621
53. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorButton"))

15
00:00:10,207 --> 00:00:10,208
57. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

16
00:00:10,346 --> 00:00:10,347
61. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

17
00:00:10,500 --> 00:00:10,500
65. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

18
00:00:10,876 --> 00:00:10,876
69. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

19
00:00:11,016 --> 00:00:11,017
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

20
00:00:11,484 --> 00:00:11,484
77. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

21
00:00:11,721 --> 00:00:11,722
81. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

22
00:00:12,238 --> 00:00:12,240
85. delay(2)

23
00:00:14,252 --> 00:00:14,253
89. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

24
00:00:15,028 --> 00:00:15,028
93. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 5)

25
00:00:15,105 --> 00:00:15,105
97. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 85)

26
00:00:15,602 --> 00:00:15,604
101. delay(3)

27
00:00:18,615 --> 00:00:18,615
105. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/HueChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/HueChanged1.png")

28
00:00:19,262 --> 00:00:19,262
109. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 5)

29
00:00:19,336 --> 00:00:19,337
113. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 110)

30
00:00:19,739 --> 00:00:19,740
117. delay(2)

31
00:00:21,745 --> 00:00:21,745
121. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/PickersFirstCheck.png", failed + "/Widgets/ColorPicker/PickerCMYK/PickersFirstCheck.png")

32
00:00:22,391 --> 00:00:22,391
125. click(findTestObject("TestStandPanel/FinishButton"))

33
00:00:22,532 --> 00:00:22,532
129. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

34
00:00:23,091 --> 00:00:23,091
133. delay(2)

35
00:00:25,099 --> 00:00:25,100
137. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), baseDir + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck1.png", failed + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck1.png")

36
00:00:25,697 --> 00:00:25,697
141. click(findTestObject("TestStandPanel/BackToEditorButton"))

37
00:00:25,782 --> 00:00:25,786
145. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

38
00:00:25,919 --> 00:00:25,920
149. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

39
00:00:26,040 --> 00:00:26,041
153. delay(2)

40
00:00:28,048 --> 00:00:28,048
157. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5, 2)

41
00:00:28,369 --> 00:00:28,370
161. delay(2)

42
00:00:30,379 --> 00:00:30,379
165. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged2.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged2.png")

43
00:00:31,051 --> 00:00:31,053
169. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 5)

44
00:00:31,131 --> 00:00:31,132
173. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 20)

45
00:00:31,500 --> 00:00:31,501
177. delay(2)

46
00:00:33,513 --> 00:00:33,514
181. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/HueChanged2.png", failed + "/Widgets/ColorPicker/PickerCMYK/HueChanged2.png")

47
00:00:34,186 --> 00:00:34,187
185. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 5)

48
00:00:34,256 --> 00:00:34,256
189. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 20)

49
00:00:34,628 --> 00:00:34,628
193. delay(2)

50
00:00:36,638 --> 00:00:36,638
197. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/PickersSecondCheck.png", failed + "/Widgets/ColorPicker/PickerCMYK/PickersSecondCheck.png")

51
00:00:37,276 --> 00:00:37,276
201. click(findTestObject("TestStandPanel/FinishButton"))

52
00:00:37,384 --> 00:00:37,385
205. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

53
00:00:37,507 --> 00:00:37,508
209. delay(2)

54
00:00:39,518 --> 00:00:39,518
213. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), baseDir + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck2.png", failed + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck2.png")

55
00:00:40,079 --> 00:00:40,080
217. click(findTestObject("TestStandPanel/BackToEditorButton"))

56
00:00:40,150 --> 00:00:40,151
221. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

57
00:00:40,261 --> 00:00:40,262
225. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"), 5)

58
00:00:40,355 --> 00:00:40,355
229. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"))

59
00:00:40,636 --> 00:00:40,637
233. delay(2)

60
00:00:42,647 --> 00:00:42,647
237. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/RecentPaletteAdded.png", failed + "/Widgets/ColorPicker/PickerCMYK/RecentPaletteAdded.png")

