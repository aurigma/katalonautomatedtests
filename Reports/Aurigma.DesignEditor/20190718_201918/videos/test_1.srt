1
00:00:00,329 --> 00:00:00,331
1. openBrowser("")

2
00:00:04,799 --> 00:00:04,800
5. maximizeWindow()

3
00:00:05,132 --> 00:00:05,134
9. navigateToUrl("http://localhost:3000/")

4
00:00:06,420 --> 00:00:06,421
13. driver = getExecutedBrowser().getName()

5
00:00:06,454 --> 00:00:06,456
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:06,494 --> 00:00:06,497
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:06,518 --> 00:00:06,520
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:07,116 --> 00:00:07,119
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:07,422 --> 00:00:07,425
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:07,717 --> 00:00:07,718
37. click(findTestObject("ConfigurationsTree/div_Palettejson"))

11
00:00:07,997 --> 00:00:07,999
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:09,065 --> 00:00:09,066
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:09,424 --> 00:00:09,426
49. delay(7)

14
00:00:16,471 --> 00:00:16,477
53. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton"))

15
00:00:17,025 --> 00:00:17,027
57. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton"), baseDir + "/Widgets/ColorPicker/Palette/SelectColorButton.png", failed + "/Widgets/ColorPicker/Palette/SelectColorButton.png")

16
00:00:18,121 --> 00:00:18,122
61. delay(1)

17
00:00:19,132 --> 00:00:19,133
65. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton"))

18
00:00:19,761 --> 00:00:19,765
69. delay(2)

19
00:00:21,773 --> 00:00:21,775
73. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/ColorPickerDialog"), 5)

20
00:00:21,911 --> 00:00:21,911
77. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/ColorPickerDialog"), baseDir + "/Widgets/ColorPicker/Palette/TextColorDialog.png", failed + "/Widgets/ColorPicker/Palette/TextColorDialog.png")

21
00:00:22,256 --> 00:00:22,256
81. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Purple"))

22
00:00:23,045 --> 00:00:23,046
85. delay(1)

23
00:00:24,072 --> 00:00:24,074
89. click(findTestObject("TestStandPanel/FinishButton"))

24
00:00:24,336 --> 00:00:24,341
93. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

25
00:00:24,677 --> 00:00:24,679
97. delay(2)

26
00:00:26,694 --> 00:00:26,694
101. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), baseDir + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png", failed + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png")

27
00:00:26,974 --> 00:00:26,974
105. click(findTestObject("TestStandPanel/BackToEditorButton"))

28
00:00:27,238 --> 00:00:27,241
109. delay(1)

29
00:00:28,252 --> 00:00:28,252
113. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

30
00:00:28,762 --> 00:00:28,764
117. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

31
00:00:28,963 --> 00:00:28,964
121. delay(2)

32
00:00:30,983 --> 00:00:30,983
125. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/ColorPickerDialog"), baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png", failed + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png")

33
00:00:31,284 --> 00:00:31,285
129. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Burgundy"))

34
00:00:31,822 --> 00:00:31,824
133. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

35
00:00:32,264 --> 00:00:32,268
137. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RecentColor_Purple"))

36
00:00:32,820 --> 00:00:32,821
141. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RecentColor_Purple"))

37
00:00:33,095 --> 00:00:33,095
145. delay(2)

38
00:00:35,105 --> 00:00:35,105
149. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/ColorPickerDialog"), baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck2.png", failed + "/Widgets/ColorPicker/Palette/AddToRecentCheck2.png")

39
00:00:35,401 --> 00:00:35,401
153. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

40
00:00:35,885 --> 00:00:35,886
157. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

41
00:00:36,120 --> 00:00:36,121
161. delay(2)

42
00:00:38,133 --> 00:00:38,133
165. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/ColorPickerDialog"), baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck3.png", failed + "/Widgets/ColorPicker/Palette/AddToRecentCheck3.png")

43
00:00:38,461 --> 00:00:38,461
169. navigateToUrl("http://localhost:3000/")

44
00:00:39,110 --> 00:00:39,111
173. if (driver == "CHROME_DRIVER")

45
00:00:39,127 --> 00:00:39,129
177. click(findTestObject("ConfigurationsTree/div_base-editor"))

46
00:00:40,014 --> 00:00:40,014
181. click(findTestObject("ConfigurationsTree/div_Widgets"))

47
00:00:40,289 --> 00:00:40,290
185. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

48
00:00:40,552 --> 00:00:40,553
189. click(findTestObject("ConfigurationsTree/div_Palettejson"))

49
00:00:40,818 --> 00:00:40,819
193. click(findTestObject("TestStandPanel/button_RUN"))

50
00:00:41,897 --> 00:00:41,897
197. click(findTestObject("TestStandPanel/EditorTab"))

51
00:00:42,519 --> 00:00:42,520
201. delay(5)

52
00:00:47,543 --> 00:00:47,544
205. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton"))

53
00:00:48,523 --> 00:00:48,524
209. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/ColorPickerDialog"), 5)

54
00:00:54,366 --> 00:00:54,367
213. delay(2)

55
00:00:56,375 --> 00:00:56,376
217. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/ColorPickerDialog"), baseDir + "/Widgets/ColorPicker/Palette/CPAtNewPage.png", failed + "/Widgets/ColorPicker/Palette/CPAtNewPage.png")

