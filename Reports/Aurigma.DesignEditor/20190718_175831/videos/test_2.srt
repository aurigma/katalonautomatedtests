1
00:00:00,304 --> 00:00:00,305
1. openBrowser("")

2
00:00:05,369 --> 00:00:05,370
5. maximizeWindow()

3
00:00:05,648 --> 00:00:05,649
9. navigateToUrl("http://localhost:3000/")

4
00:00:07,075 --> 00:00:07,078
13. driver = getExecutedBrowser().getName()

5
00:00:07,092 --> 00:00:07,093
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:07,113 --> 00:00:07,114
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:07,147 --> 00:00:07,149
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:07,948 --> 00:00:07,949
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:08,347 --> 00:00:08,348
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:08,776 --> 00:00:08,777
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:09,110 --> 00:00:09,111
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:09,394 --> 00:00:09,395
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:11,153 --> 00:00:11,157
49. delay(5)

14
00:00:16,175 --> 00:00:16,176
53. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorButton"))

15
00:00:17,061 --> 00:00:17,061
57. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

16
00:00:22,372 --> 00:00:22,373
61. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

17
00:00:27,745 --> 00:00:27,747
65. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

18
00:00:28,175 --> 00:00:28,176
69. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

19
00:00:33,533 --> 00:00:33,533
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

20
00:00:33,878 --> 00:00:33,878
77. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

21
00:00:39,265 --> 00:00:39,265
81. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

22
00:00:39,660 --> 00:00:39,661
85. delay(2)

23
00:00:41,673 --> 00:00:41,674
89. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

