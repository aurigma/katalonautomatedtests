1
00:00:00,590 --> 00:00:00,590
1. openBrowser("")

2
00:00:04,644 --> 00:00:04,645
5. maximizeWindow()

3
00:00:04,939 --> 00:00:04,940
9. navigateToUrl("http://localhost:3000/")

4
00:00:06,249 --> 00:00:06,249
13. driver = getExecutedBrowser().getName()

5
00:00:06,259 --> 00:00:06,260
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:06,273 --> 00:00:06,274
21. click(findTestObject("ConfigurationsTree/div_base-editor"))

7
00:00:06,563 --> 00:00:06,564
25. click(findTestObject("ConfigurationsTree/div_Widgets"))

8
00:00:06,887 --> 00:00:06,887
29. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

9
00:00:07,188 --> 00:00:07,189
33. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

10
00:00:07,459 --> 00:00:07,459
37. click(findTestObject("TestStandPanel/button_RUN"))

11
00:00:08,441 --> 00:00:08,442
41. click(findTestObject("TestStandPanel/EditorTab"))

12
00:00:08,747 --> 00:00:08,749
45. delay(5)

13
00:00:13,760 --> 00:00:13,761
49. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorButton"))

14
00:00:14,420 --> 00:00:14,420
53. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 10)

15
00:00:15,119 --> 00:00:15,120
57. delay(2)

16
00:00:17,124 --> 00:00:17,124
61. Saturation = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

17
00:00:17,465 --> 00:00:17,465
65. SaturationIsSimilar = scripts.ScreenshotHandler.compareToBase(Saturation, baseDir + "/Widgets/ColorPicker/PickerRGB/SaturationChanged1.png")

18
00:00:17,488 --> 00:00:17,489
69. assert SaturationIsSimilar == true

19
00:00:17,498 --> 00:00:17,499
73. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 130)

20
00:00:18,092 --> 00:00:18,093
77. delay(2)

21
00:00:20,105 --> 00:00:20,105
81. Hue = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

22
00:00:20,427 --> 00:00:20,427
85. HueIsSimilar = scripts.ScreenshotHandler.compareToBase(Hue, baseDir + "/Widgets/ColorPicker/PickerRGB/HueChanged1.png")

23
00:00:20,451 --> 00:00:20,452
89. assert HueIsSimilar == true

24
00:00:20,458 --> 00:00:20,458
93. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 90)

25
00:00:21,042 --> 00:00:21,043
97. delay(2)

26
00:00:23,053 --> 00:00:23,054
101. Alpha = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

27
00:00:23,352 --> 00:00:23,352
105. AlphaIsSimilar = scripts.ScreenshotHandler.compareToBase(Alpha, baseDir + "/Widgets/ColorPicker/PickerRGB/PickersFirstCheck.png")

28
00:00:23,374 --> 00:00:23,374
109. assert AlphaIsSimilar == true

29
00:00:23,379 --> 00:00:23,379
113. click(findTestObject("TestStandPanel/FinishButton"))

30
00:00:23,639 --> 00:00:23,643
117. delay(2)

31
00:00:25,655 --> 00:00:25,655
121. AppliedColor1 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("TestStandPanel/PreviewImage"))

32
00:00:25,880 --> 00:00:25,880
125. AppliedColor1IsSimilar = scripts.ScreenshotHandler.compareToBase(AppliedColor1, baseDir + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck1.png")

33
00:00:25,941 --> 00:00:25,941
129. assert AppliedColor1IsSimilar == true

34
00:00:25,947 --> 00:00:25,948
133. click(findTestObject("TestStandPanel/BackToEditorButton"))

35
00:00:26,205 --> 00:00:26,206
137. delay(2)

36
00:00:28,222 --> 00:00:28,222
141. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), -5, -2)

37
00:00:28,744 --> 00:00:28,745
145. delay(2)

38
00:00:30,758 --> 00:00:30,758
149. Saturation2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

39
00:00:31,060 --> 00:00:31,061
153. Saturation2IsSimilar = scripts.ScreenshotHandler.compareToBase(Saturation2, baseDir + "/Widgets/ColorPicker/PickerRGB/SaturationChanged2.png")

40
00:00:31,082 --> 00:00:31,083
157. assert Saturation2IsSimilar == true

41
00:00:31,090 --> 00:00:31,091
161. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, -70)

42
00:00:31,679 --> 00:00:31,680
165. delay(2)

43
00:00:33,695 --> 00:00:33,696
169. Hue2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

44
00:00:33,969 --> 00:00:33,969
173. Hue2IsSimilar = scripts.ScreenshotHandler.compareToBase(Hue2, baseDir + "/Widgets/ColorPicker/PickerRGB/HueChanged2.png")

45
00:00:33,989 --> 00:00:33,990
177. assert Hue2IsSimilar == true

46
00:00:33,995 --> 00:00:33,995
181. delay(1)

47
00:00:35,000 --> 00:00:35,001
185. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, -30)

48
00:00:35,583 --> 00:00:35,583
189. delay(2)

49
00:00:37,588 --> 00:00:37,589
193. Alpha2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

50
00:00:37,883 --> 00:00:37,884
197. Alpha2IsSimilar = scripts.ScreenshotHandler.compareToBase(Alpha2, baseDir + "/Widgets/ColorPicker/PickerRGB/PickersSecondCheck.png")

51
00:00:37,905 --> 00:00:37,906
201. assert Alpha2IsSimilar == true

52
00:00:37,910 --> 00:00:37,911
205. click(findTestObject("TestStandPanel/FinishButton"))

53
00:00:38,153 --> 00:00:38,153
209. delay(2)

54
00:00:40,163 --> 00:00:40,164
213. AppliedColor2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("TestStandPanel/PreviewImage"))

55
00:00:40,395 --> 00:00:40,395
217. AppliedColor2IsSimilar = scripts.ScreenshotHandler.compareToBase(AppliedColor2, baseDir + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck2.png")

56
00:00:40,455 --> 00:00:40,455
221. assert AppliedColor2IsSimilar == true

57
00:00:40,459 --> 00:00:40,459
225. click(findTestObject("TestStandPanel/BackToEditorButton"))

58
00:00:40,696 --> 00:00:40,696
229. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"))

59
00:00:41,092 --> 00:00:41,092
233. delay(1)

60
00:00:42,103 --> 00:00:42,103
237. RecentColor = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

61
00:00:42,370 --> 00:00:42,370
241. RecentColorIsSimilar = scripts.ScreenshotHandler.compareToBase(RecentColor, baseDir + "/Widgets/ColorPicker/PickerRGB/RecentPaletteAdded.png")

62
00:00:42,392 --> 00:00:42,392
245. assert RecentColorIsSimilar == true

63
00:00:42,397 --> 00:00:42,397
249. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_R_Channel"))

64
00:00:42,806 --> 00:00:42,806
253. delay(2)

65
00:00:44,811 --> 00:00:44,811
257. Lightning = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

66
00:00:45,071 --> 00:00:45,071
261. LightningIsSimilar = scripts.ScreenshotHandler.compareToBase(Lightning, baseDir + "/Widgets/ColorPicker/PickerRGB/ChannelSelectedLightning.png")

67
00:00:45,089 --> 00:00:45,089
265. assert LightningIsSimilar == true

68
00:00:45,093 --> 00:00:45,094
269. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_R_Channel"), "25")

69
00:00:45,248 --> 00:00:45,248
273. delay(2)

70
00:00:47,258 --> 00:00:47,258
277. ValidR1 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

71
00:00:47,588 --> 00:00:47,589
281. ValidR1IsSimilar = scripts.ScreenshotHandler.compareToBase(ValidR1, baseDir + "/Widgets/ColorPicker/PickerRGB/RChannelValidInput1.png")

72
00:00:47,609 --> 00:00:47,610
285. assert ValidR1IsSimilar == true

73
00:00:47,613 --> 00:00:47,614
289. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_R_Channel"), "abc")

74
00:00:47,753 --> 00:00:47,753
293. delay(3)

75
00:00:50,762 --> 00:00:50,762
297. InvalidR1 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

76
00:00:51,037 --> 00:00:51,037
301. InvalidR1IsSimilar = scripts.ScreenshotHandler.compareToBase(InvalidR1, baseDir + "/Widgets/ColorPicker/PickerRGB/RChannelInvalidInput1.png")

77
00:00:51,060 --> 00:00:51,060
305. assert InvalidR1IsSimilar == true

78
00:00:51,067 --> 00:00:51,067
309. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_R_Channel"), "76")

79
00:00:51,181 --> 00:00:51,182
313. delay(2)

80
00:00:53,193 --> 00:00:53,193
317. ValidR2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

81
00:00:53,479 --> 00:00:53,480
321. ValidR2IsSimilar = scripts.ScreenshotHandler.compareToBase(ValidR2, baseDir + "/Widgets/ColorPicker/PickerRGB/RChannelValidInput2.png")

82
00:00:53,503 --> 00:00:53,503
325. assert ValidR2IsSimilar == true

83
00:00:53,510 --> 00:00:53,511
329. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_R_Channel"), "!@")

84
00:00:53,639 --> 00:00:53,640
333. delay(2)

85
00:00:55,651 --> 00:00:55,651
337. InvalidR2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

86
00:00:55,990 --> 00:00:55,990
341. InvalidR2IsSimilar = scripts.ScreenshotHandler.compareToBase(InvalidR2, baseDir + "/Widgets/ColorPicker/PickerRGB/RChannelInvalidInput2.png")

87
00:00:56,012 --> 00:00:56,012
345. assert InvalidR2IsSimilar == true

