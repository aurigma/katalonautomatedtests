1
00:00:00,710 --> 00:00:00,712
1. openBrowser("")

2
00:00:03,592 --> 00:00:03,593
5. maximizeWindow()

3
00:00:03,889 --> 00:00:03,889
9. navigateToUrl("http://localhost:3000/")

4
00:00:05,036 --> 00:00:05,037
13. driver = getExecutedBrowser().getName()

5
00:00:05,052 --> 00:00:05,053
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:05,075 --> 00:00:05,076
21. click(findTestObject("ConfigurationsTree/div_base-editor"))

7
00:00:05,398 --> 00:00:05,399
25. click(findTestObject("ConfigurationsTree/div_Widgets"))

8
00:00:05,726 --> 00:00:05,727
29. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

9
00:00:06,032 --> 00:00:06,033
33. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

10
00:00:06,298 --> 00:00:06,298
37. click(findTestObject("TestStandPanel/button_RUN"))

11
00:00:07,276 --> 00:00:07,277
41. click(findTestObject("TestStandPanel/EditorTab"))

12
00:00:07,663 --> 00:00:07,663
45. delay(5)

13
00:00:12,675 --> 00:00:12,675
49. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorButton"))

14
00:00:13,303 --> 00:00:13,304
53. delay(2)

15
00:00:15,316 --> 00:00:15,316
57. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

16
00:00:15,721 --> 00:00:15,721
61. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

17
00:00:16,416 --> 00:00:16,417
65. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

18
00:00:17,134 --> 00:00:17,136
69. delay(2)

19
00:00:19,146 --> 00:00:19,148
73. Saturation = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

20
00:00:19,482 --> 00:00:19,482
77. SaturationIsSimilar = scripts.ScreenshotHandler.compareToBase(Saturation, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

21
00:00:19,532 --> 00:00:19,532
81. assert SaturationIsSimilar == true

22
00:00:19,539 --> 00:00:19,540
85. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 85)

23
00:00:20,209 --> 00:00:20,209
89. delay(3)

24
00:00:23,214 --> 00:00:23,215
93. Hue = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

25
00:00:23,505 --> 00:00:23,506
97. HueIsSimilar = scripts.ScreenshotHandler.compareToBase(Hue, baseDir + "/Widgets/ColorPicker/PickerCMYK/HueChanged1.png")

26
00:00:23,531 --> 00:00:23,531
101. assert HueIsSimilar == true

27
00:00:23,540 --> 00:00:23,540
105. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 110)

28
00:00:24,222 --> 00:00:24,223
109. delay(2)

29
00:00:26,237 --> 00:00:26,238
113. Alpha = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

30
00:00:26,533 --> 00:00:26,534
117. AlphaIsSimilar = scripts.ScreenshotHandler.compareToBase(Alpha, baseDir + "/Widgets/ColorPicker/PickerCMYK/PickersFirstCheck.png")

31
00:00:26,560 --> 00:00:26,561
121. assert AlphaIsSimilar == true

32
00:00:26,566 --> 00:00:26,567
125. click(findTestObject("TestStandPanel/FinishButton"))

33
00:00:26,951 --> 00:00:26,951
129. delay(2)

34
00:00:28,964 --> 00:00:28,965
133. AppliedColor1 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("TestStandPanel/PreviewImage"))

35
00:00:29,254 --> 00:00:29,254
137. AppliedColor1IsSimilar = scripts.ScreenshotHandler.compareToBase(AppliedColor1, baseDir + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck1.png")

36
00:00:29,315 --> 00:00:29,315
141. assert AppliedColor1IsSimilar == true

37
00:00:29,319 --> 00:00:29,319
145. click(findTestObject("TestStandPanel/BackToEditorButton"))

38
00:00:29,592 --> 00:00:29,593
149. delay(2)

39
00:00:31,603 --> 00:00:31,603
153. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5, 2)

40
00:00:32,151 --> 00:00:32,151
157. delay(2)

41
00:00:34,157 --> 00:00:34,157
161. Saturation2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

42
00:00:34,419 --> 00:00:34,419
165. Saturation2IsSimilar = scripts.ScreenshotHandler.compareToBase(Saturation2, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged2.png")

43
00:00:34,442 --> 00:00:34,442
169. assert Saturation2IsSimilar == true

44
00:00:34,446 --> 00:00:34,446
173. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 20)

45
00:00:35,040 --> 00:00:35,041
177. delay(2)

46
00:00:37,063 --> 00:00:37,063
181. Hue2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

47
00:00:37,324 --> 00:00:37,324
185. Hue2IsSimilar = scripts.ScreenshotHandler.compareToBase(Hue2, baseDir + "/Widgets/ColorPicker/PickerCMYK/HueChanged2.png")

48
00:00:37,346 --> 00:00:37,347
189. assert Hue2IsSimilar == true

49
00:00:37,350 --> 00:00:37,351
193. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 20)

50
00:00:37,920 --> 00:00:37,922
197. delay(2)

51
00:00:39,930 --> 00:00:39,930
201. Alpha2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

52
00:00:40,232 --> 00:00:40,232
205. Alpha2IsSimilar = scripts.ScreenshotHandler.compareToBase(Alpha2, baseDir + "/Widgets/ColorPicker/PickerCMYK/PickersSecondCheck.png")

53
00:00:40,256 --> 00:00:40,256
209. assert Alpha2IsSimilar == true

54
00:00:40,262 --> 00:00:40,263
213. click(findTestObject("TestStandPanel/FinishButton"))

55
00:00:40,619 --> 00:00:40,619
217. delay(2)

56
00:00:42,644 --> 00:00:42,644
221. AppliedColor2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("TestStandPanel/PreviewImage"))

57
00:00:42,858 --> 00:00:42,858
225. AppliedColor2IsSimilar = scripts.ScreenshotHandler.compareToBase(AppliedColor2, baseDir + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck2.png")

58
00:00:42,922 --> 00:00:42,922
229. assert AppliedColor2IsSimilar == true

59
00:00:42,930 --> 00:00:42,931
233. click(findTestObject("TestStandPanel/BackToEditorButton"))

60
00:00:43,240 --> 00:00:43,240
237. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"))

61
00:00:43,720 --> 00:00:43,721
241. delay(1)

62
00:00:44,735 --> 00:00:44,735
245. RecentColor = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

63
00:00:45,001 --> 00:00:45,001
249. RecentColorIsSimilar = scripts.ScreenshotHandler.compareToBase(RecentColor, baseDir + "/Widgets/ColorPicker/PickerCMYK/RecentPaletteAdded.png")

64
00:00:45,019 --> 00:00:45,020
253. assert RecentColorIsSimilar == true

65
00:00:45,023 --> 00:00:45,024
257. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel"))

66
00:00:45,510 --> 00:00:45,511
261. delay(2)

67
00:00:47,519 --> 00:00:47,520
265. Lightning = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

68
00:00:47,802 --> 00:00:47,803
269. LightningIsSimilar = scripts.ScreenshotHandler.compareToBase(Lightning, baseDir + "/Widgets/ColorPicker/PickerCMYK/ChannelSelectedLightning.png")

69
00:00:47,917 --> 00:00:47,917
273. assert LightningIsSimilar == true

70
00:00:47,923 --> 00:00:47,924
277. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel"), "67")

71
00:00:48,092 --> 00:00:48,093
281. delay(2)

72
00:00:50,104 --> 00:00:50,104
285. ValidC1 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

73
00:00:50,404 --> 00:00:50,404
289. ValidC1IsSimilar = scripts.ScreenshotHandler.compareToBase(ValidC1, baseDir + "/Widgets/ColorPicker/PickerCMYK/CChannelValidInput1.png")

74
00:00:50,429 --> 00:00:50,429
293. assert ValidC1IsSimilar == true

75
00:00:50,435 --> 00:00:50,435
297. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel"), "abc")

76
00:00:50,600 --> 00:00:50,601
301. delay(2)

77
00:00:52,609 --> 00:00:52,609
305. InvalidC1 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

78
00:00:52,908 --> 00:00:52,908
309. InvalidC1IsSimilar = scripts.ScreenshotHandler.compareToBase(InvalidC1, baseDir + "/Widgets/ColorPicker/PickerCMYK/CChannelInvalidInput1.png")

79
00:00:52,931 --> 00:00:52,931
313. assert InvalidC1IsSimilar == true

80
00:00:52,937 --> 00:00:52,937
317. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel"), "76")

81
00:00:53,061 --> 00:00:53,062
321. delay(2)

82
00:00:55,072 --> 00:00:55,072
325. ValidC2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

83
00:00:55,347 --> 00:00:55,347
329. ValidC2IsSimilar = scripts.ScreenshotHandler.compareToBase(ValidC2, baseDir + "/Widgets/ColorPicker/PickerCMYK/CChannelValidInput2.png")

84
00:00:55,370 --> 00:00:55,370
333. assert ValidC2IsSimilar == true

85
00:00:55,374 --> 00:00:55,375
337. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel"), "!@")

86
00:00:55,510 --> 00:00:55,511
341. delay(2)

87
00:00:57,517 --> 00:00:57,518
345. InvalidС2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

88
00:00:57,813 --> 00:00:57,813
349. InvalidC2IsSimilar = scripts.ScreenshotHandler.compareToBase(InvalidС2, baseDir + "/Widgets/ColorPicker/PickerCMYK/CChannelInvalidInput2.png")

89
00:00:57,836 --> 00:00:57,836
353. assert InvalidC2IsSimilar == true

90
00:00:57,841 --> 00:00:57,841
357. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel"), "150")

91
00:00:58,001 --> 00:00:58,002
361. delay(2)

92
00:01:00,015 --> 00:01:00,015
365. InvalidC3 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

93
00:01:00,320 --> 00:01:00,320
369. InvalidC3IsSimilar = scripts.ScreenshotHandler.compareToBase(InvalidC3, baseDir + "/Widgets/ColorPicker/PickerCMYK/CChannelInvalidInput3.png")

94
00:01:00,347 --> 00:01:00,349
373. assert InvalidC3IsSimilar == true

