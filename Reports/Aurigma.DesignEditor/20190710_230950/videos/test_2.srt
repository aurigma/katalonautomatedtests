1
00:00:00,156 --> 00:00:00,157
1. openBrowser("")

2
00:00:02,988 --> 00:00:02,988
5. maximizeWindow()

3
00:00:03,263 --> 00:00:03,264
9. navigateToUrl("http://localhost:3000/")

4
00:00:04,346 --> 00:00:04,347
13. driver = getExecutedBrowser().getName()

5
00:00:04,352 --> 00:00:04,353
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:04,357 --> 00:00:04,358
21. click(findTestObject("ConfigurationsTree/div_base-editor"))

7
00:00:04,664 --> 00:00:04,664
25. click(findTestObject("ConfigurationsTree/div_Widgets"))

8
00:00:04,946 --> 00:00:04,946
29. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

9
00:00:05,224 --> 00:00:05,225
33. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

10
00:00:05,491 --> 00:00:05,492
37. click(findTestObject("TestStandPanel/button_RUN"))

11
00:00:06,392 --> 00:00:06,393
41. click(findTestObject("TestStandPanel/EditorTab"))

12
00:00:06,782 --> 00:00:06,784
45. delay(5)

13
00:00:11,794 --> 00:00:11,794
49. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorButton"))

14
00:00:12,391 --> 00:00:12,392
53. delay(2)

15
00:00:14,400 --> 00:00:14,400
57. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

16
00:00:14,824 --> 00:00:14,824
61. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

17
00:00:15,600 --> 00:00:15,600
65. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

18
00:00:16,297 --> 00:00:16,298
69. delay(2)

19
00:00:18,303 --> 00:00:18,304
73. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

20
00:00:18,708 --> 00:00:18,709
77. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 85)

21
00:00:19,357 --> 00:00:19,358
81. delay(3)

22
00:00:22,366 --> 00:00:22,367
85. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/HueChanged1.png")

23
00:00:22,708 --> 00:00:22,709
89. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 110)

24
00:00:23,422 --> 00:00:23,422
93. delay(2)

25
00:00:25,428 --> 00:00:25,429
97. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/PickersFirstCheck.png")

26
00:00:25,729 --> 00:00:25,729
101. click(findTestObject("TestStandPanel/FinishButton"))

27
00:00:26,075 --> 00:00:26,076
105. delay(2)

28
00:00:28,090 --> 00:00:28,091
109. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), baseDir + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck1.png")

29
00:00:28,423 --> 00:00:28,424
113. click(findTestObject("TestStandPanel/BackToEditorButton"))

30
00:00:28,759 --> 00:00:28,759
117. delay(2)

31
00:00:30,772 --> 00:00:30,774
121. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5, 2)

32
00:00:31,330 --> 00:00:31,331
125. delay(2)

33
00:00:33,345 --> 00:00:33,345
129. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged2.png")

34
00:00:33,648 --> 00:00:33,649
133. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 20)

35
00:00:34,284 --> 00:00:34,285
137. delay(2)

36
00:00:36,294 --> 00:00:36,294
141. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/HueChanged2.png")

37
00:00:36,597 --> 00:00:36,597
145. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 20)

38
00:00:37,156 --> 00:00:37,157
149. delay(2)

39
00:00:39,166 --> 00:00:39,167
153. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/PickersSecondCheck.png")

40
00:00:39,463 --> 00:00:39,463
157. click(findTestObject("TestStandPanel/FinishButton"))

41
00:00:39,793 --> 00:00:39,793
161. delay(2)

42
00:00:41,804 --> 00:00:41,805
165. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), baseDir + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck2.png")

43
00:00:42,097 --> 00:00:42,097
169. click(findTestObject("TestStandPanel/BackToEditorButton"))

44
00:00:42,330 --> 00:00:42,332
173. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"))

45
00:00:42,719 --> 00:00:42,719
177. delay(1)

46
00:00:43,732 --> 00:00:43,733
181. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/RecentPaletteAdded.png")

47
00:00:44,026 --> 00:00:44,027
185. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel"))

48
00:00:44,428 --> 00:00:44,429
189. delay(2)

49
00:00:46,435 --> 00:00:46,437
193. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/ChannelSelectedLightning.png")

50
00:00:46,726 --> 00:00:46,726
197. if (driver == "CHROME_DRIVER")

51
00:00:46,733 --> 00:00:46,733
201. sendKeys(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel"), Keys.chord(TAB))

52
00:00:46,897 --> 00:00:46,898
205. delay(2)

53
00:00:48,913 --> 00:00:48,913
209. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), baseDir + "/Widgets/ColorPicker/PickerCMYK/TabChangedCursorToM.png")

