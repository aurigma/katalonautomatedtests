1
00:00:00,235 --> 00:00:00,236
1. openBrowser("")

2
00:00:04,016 --> 00:00:04,016
5. maximizeWindow()

3
00:00:04,291 --> 00:00:04,291
9. navigateToUrl("http://localhost:3000/")

4
00:00:05,416 --> 00:00:05,417
13. driver = getExecutedBrowser().getName()

5
00:00:05,426 --> 00:00:05,427
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:05,433 --> 00:00:05,434
21. click(findTestObject("ConfigurationsTree/div_base-editor"))

7
00:00:05,763 --> 00:00:05,763
25. click(findTestObject("ConfigurationsTree/div_Widgets"))

8
00:00:06,086 --> 00:00:06,087
29. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

9
00:00:06,364 --> 00:00:06,364
33. click(findTestObject("ConfigurationsTree/div_Widgetsjson"))

10
00:00:06,625 --> 00:00:06,625
37. click(findTestObject("TestStandPanel/button_RUN"))

11
00:00:07,629 --> 00:00:07,630
41. click(findTestObject("TestStandPanel/EditorTab"))

12
00:00:08,130 --> 00:00:08,130
45. delay(5)

13
00:00:13,150 --> 00:00:13,150
49. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/AddRichText"))

14
00:00:13,747 --> 00:00:13,749
53. delay(4)

15
00:00:17,764 --> 00:00:17,765
57. RTEInitial = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTE"))

16
00:00:17,928 --> 00:00:17,928
61. Dialog1IsSimilar = scripts.ScreenshotHandler.compareToBase(RTEInitial, baseDir + "/Widgets/ColorPicker/Widgets/RTE.png")

17
00:00:18,062 --> 00:00:18,062
65. assert Dialog1IsSimilar == true

18
00:00:18,067 --> 00:00:18,068
69. delay(1)

19
00:00:19,074 --> 00:00:19,074
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTETextColorButton"))

20
00:00:19,620 --> 00:00:19,621
77. delay(3)

21
00:00:22,627 --> 00:00:22,627
81. RTEPalette1 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Widgets/RTE"))

22
00:00:22,785 --> 00:00:22,785
85. Palette1IsSimilar = scripts.ScreenshotHandler.compareToBase(RTEPalette1, baseDir + "/Widgets/ColorPicker/Widgets/RTEPickerOpened.png")

23
00:00:22,958 --> 00:00:22,959
89. assert Palette1IsSimilar == true

