1
00:00:00,650 --> 00:00:00,651
1. openBrowser("")

2
00:00:04,425 --> 00:00:04,425
5. maximizeWindow()

3
00:00:04,703 --> 00:00:04,704
9. navigateToUrl("http://localhost:3000/")

4
00:00:05,899 --> 00:00:05,900
13. driver = getExecutedBrowser().getName()

5
00:00:05,911 --> 00:00:05,911
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:05,934 --> 00:00:05,935
21. click(findTestObject("ConfigurationsTree/div_base-editor"))

7
00:00:06,226 --> 00:00:06,227
25. click(findTestObject("ConfigurationsTree/div_Widgets"))

8
00:00:06,560 --> 00:00:06,560
29. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

9
00:00:06,829 --> 00:00:06,829
33. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

10
00:00:07,099 --> 00:00:07,100
37. click(findTestObject("TestStandPanel/button_RUN"))

11
00:00:08,068 --> 00:00:08,068
41. click(findTestObject("TestStandPanel/EditorTab"))

12
00:00:08,423 --> 00:00:08,424
45. delay(5)

13
00:00:13,434 --> 00:00:13,435
49. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorButton"))

14
00:00:14,087 --> 00:00:14,088
53. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 10)

15
00:00:14,806 --> 00:00:14,807
57. delay(2)

16
00:00:16,817 --> 00:00:16,818
61. Saturation = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

17
00:00:17,154 --> 00:00:17,154
65. SaturationIsSimilar = scripts.ScreenshotHandler.compareToBase(Saturation, baseDir + "/Widgets/ColorPicker/PickerRGB/SaturationChanged1.png")

18
00:00:17,188 --> 00:00:17,189
69. assert SaturationIsSimilar == true

19
00:00:17,206 --> 00:00:17,207
73. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 130)

20
00:00:17,885 --> 00:00:17,886
77. delay(2)

21
00:00:19,893 --> 00:00:19,893
81. Hue = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

22
00:00:20,200 --> 00:00:20,201
85. HueIsSimilar = scripts.ScreenshotHandler.compareToBase(Hue, baseDir + "/Widgets/ColorPicker/PickerRGB/HueChanged1.png")

23
00:00:20,221 --> 00:00:20,222
89. assert HueIsSimilar == true

24
00:00:20,228 --> 00:00:20,229
93. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 90)

25
00:00:20,822 --> 00:00:20,822
97. delay(2)

26
00:00:22,836 --> 00:00:22,837
101. Alpha = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

27
00:00:23,125 --> 00:00:23,125
105. AlphaIsSimilar = scripts.ScreenshotHandler.compareToBase(Alpha, baseDir + "/Widgets/ColorPicker/PickerRGB/PickersFirstCheck.png")

28
00:00:23,146 --> 00:00:23,146
109. assert AlphaIsSimilar == true

29
00:00:23,150 --> 00:00:23,151
113. click(findTestObject("TestStandPanel/FinishButton"))

30
00:00:23,408 --> 00:00:23,409
117. delay(2)

31
00:00:25,417 --> 00:00:25,417
121. AppliedColor1 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("TestStandPanel/PreviewImage"))

32
00:00:25,634 --> 00:00:25,634
125. AppliedColor1IsSimilar = scripts.ScreenshotHandler.compareToBase(AppliedColor1, baseDir + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck1.png")

33
00:00:25,695 --> 00:00:25,695
129. assert AppliedColor1IsSimilar == true

34
00:00:25,700 --> 00:00:25,701
133. click(findTestObject("TestStandPanel/BackToEditorButton"))

35
00:00:25,951 --> 00:00:25,952
137. delay(2)

36
00:00:27,960 --> 00:00:27,960
141. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), -5, -2)

37
00:00:28,537 --> 00:00:28,538
145. delay(2)

38
00:00:30,553 --> 00:00:30,553
149. Saturation2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

39
00:00:30,840 --> 00:00:30,841
153. Saturation2IsSimilar = scripts.ScreenshotHandler.compareToBase(Saturation2, baseDir + "/Widgets/ColorPicker/PickerRGB/SaturationChanged2.png")

40
00:00:30,867 --> 00:00:30,867
157. assert Saturation2IsSimilar == true

41
00:00:30,872 --> 00:00:30,873
161. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, -70)

42
00:00:31,427 --> 00:00:31,428
165. delay(2)

43
00:00:33,438 --> 00:00:33,438
169. Hue2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

44
00:00:33,740 --> 00:00:33,740
173. Hue2IsSimilar = scripts.ScreenshotHandler.compareToBase(Hue2, baseDir + "/Widgets/ColorPicker/PickerRGB/HueChanged2.png")

45
00:00:33,759 --> 00:00:33,760
177. assert Hue2IsSimilar == true

46
00:00:33,768 --> 00:00:33,768
181. delay(1)

47
00:00:34,774 --> 00:00:34,774
185. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, -30)

48
00:00:35,341 --> 00:00:35,342
189. delay(2)

49
00:00:37,362 --> 00:00:37,362
193. Alpha2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

50
00:00:37,693 --> 00:00:37,693
197. Alpha2IsSimilar = scripts.ScreenshotHandler.compareToBase(Alpha2, baseDir + "/Widgets/ColorPicker/PickerRGB/PickersSecondCheck.png")

51
00:00:37,715 --> 00:00:37,716
201. assert Alpha2IsSimilar == true

52
00:00:37,723 --> 00:00:37,724
205. click(findTestObject("TestStandPanel/FinishButton"))

53
00:00:37,966 --> 00:00:37,966
209. delay(2)

54
00:00:39,975 --> 00:00:39,975
213. AppliedColor2 = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("TestStandPanel/PreviewImage"))

55
00:00:40,189 --> 00:00:40,190
217. AppliedColor2IsSimilar = scripts.ScreenshotHandler.compareToBase(AppliedColor2, baseDir + "/Widgets/ColorPicker/PickerRGB/ColorAppliedCheck2.png")

56
00:00:40,252 --> 00:00:40,252
221. assert AppliedColor2IsSimilar == true

57
00:00:40,257 --> 00:00:40,257
225. click(findTestObject("TestStandPanel/BackToEditorButton"))

58
00:00:40,493 --> 00:00:40,495
229. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"))

59
00:00:40,865 --> 00:00:40,866
233. delay(1)

60
00:00:41,876 --> 00:00:41,877
237. RecentColor = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

61
00:00:42,132 --> 00:00:42,133
241. RecentColorIsSimilar = scripts.ScreenshotHandler.compareToBase(RecentColor, baseDir + "/Widgets/ColorPicker/PickerRGB/RecentPaletteAdded.png")

62
00:00:42,154 --> 00:00:42,154
245. assert RecentColorIsSimilar == true

63
00:00:42,161 --> 00:00:42,161
249. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_R_Channel"))

64
00:00:42,543 --> 00:00:42,544
253. delay(2)

65
00:00:44,553 --> 00:00:44,553
257. Lightning = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

66
00:00:44,855 --> 00:00:44,855
261. LightningIsSimilar = scripts.ScreenshotHandler.compareToBase(Lightning, baseDir + "/Widgets/ColorPicker/PickerRGB/ChannelSelectedLightning.png")

67
00:00:44,878 --> 00:00:44,879
265. assert LightningIsSimilar == true

68
00:00:44,884 --> 00:00:44,885
269. if (driver == "CHROME_DRIVER")

69
00:00:44,892 --> 00:00:44,892
273. sendKeys(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_R_Channel"), Keys.chord(TAB))

70
00:00:45,002 --> 00:00:45,003
277. delay(2)

71
00:00:47,017 --> 00:00:47,017
281. TabToG = scripts.ScreenshotHandler.takeObjectScreenshot(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"))

72
00:00:47,349 --> 00:00:47,349
285. TabToGIsSimilar = scripts.ScreenshotHandler.compareToBase(TabToG, baseDir + "/Widgets/ColorPicker/PickerRGB/TabChangedCursorToG.png")

73
00:00:47,371 --> 00:00:47,371
289. assert TabToGIsSimilar == true

