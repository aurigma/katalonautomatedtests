1
00:00:00,163 --> 00:00:00,164
1. openBrowser("")

2
00:00:01,338 --> 00:00:01,338
5. setViewPortSize(1930, 1180)

3
00:00:01,462 --> 00:00:01,462
9. navigateToUrl("http://localhost:3000/")

4
00:00:03,015 --> 00:00:03,016
13. driver = getExecutedBrowser().getName()

5
00:00:03,023 --> 00:00:03,024
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,030 --> 00:00:03,031
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:03,036 --> 00:00:03,036
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,340 --> 00:00:03,341
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,532 --> 00:00:03,533
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:03,670 --> 00:00:03,675
37. click(findTestObject("ConfigurationsTree/div_Pickerjson"))

11
00:00:03,812 --> 00:00:03,813
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:03,885 --> 00:00:03,885
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:04,109 --> 00:00:04,111
49. delay(4)

14
00:00:08,123 --> 00:00:08,124
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:08,134 --> 00:00:08,135
57. parentElem = findWebElement(parent, 30)

16
00:00:08,233 --> 00:00:08,234
61. basePoint = parentElem.getLocation()

17
00:00:08,345 --> 00:00:08,345
65. switchToFrame(parent, 5)

18
00:00:08,462 --> 00:00:08,463
69. delay(4)

19
00:00:12,471 --> 00:00:12,473
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

20
00:00:12,947 --> 00:00:12,947
77. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

21
00:00:12,999 --> 00:00:13,000
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

22
00:00:13,057 --> 00:00:13,058
85. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

23
00:00:13,382 --> 00:00:13,383
89. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

24
00:00:13,480 --> 00:00:13,480
93. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

25
00:00:13,964 --> 00:00:13,965
97. delay(1)

26
00:00:14,980 --> 00:00:14,980
101. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

27
00:00:15,024 --> 00:00:15,025
105. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

28
00:00:15,352 --> 00:00:15,353
109. delay(2)

29
00:00:17,365 --> 00:00:17,366
113. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png")

30
00:00:17,988 --> 00:00:17,989
117. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 5)

31
00:00:18,026 --> 00:00:18,027
121. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 85)

32
00:00:18,330 --> 00:00:18,331
125. delay(3)

33
00:00:21,345 --> 00:00:21,346
129. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/HueChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/HueChanged1.png")

34
00:00:21,934 --> 00:00:21,935
133. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 5)

35
00:00:21,977 --> 00:00:21,977
137. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 110)

36
00:00:22,276 --> 00:00:22,276
141. delay(2)

37
00:00:24,286 --> 00:00:24,287
145. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/PickersFirstCheck.png", failed + "/Widgets/ColorPicker/PickerCMYK/PickersFirstCheck.png")

38
00:00:24,876 --> 00:00:24,876
149. switchToDefaultContent()

39
00:00:24,889 --> 00:00:24,890
153. click(findTestObject("TestStandPanel/FinishButton"))

40
00:00:25,018 --> 00:00:25,019
157. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

41
00:00:25,603 --> 00:00:25,603
161. delay(2)

42
00:00:27,612 --> 00:00:27,613
165. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck1.png", failed + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck1.png")

43
00:00:28,169 --> 00:00:28,169
169. click(findTestObject("TestStandPanel/BackToEditorButton"))

44
00:00:28,241 --> 00:00:28,242
173. switchToFrame(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe"), 5)

45
00:00:28,329 --> 00:00:28,330
177. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

46
00:00:28,372 --> 00:00:28,372
181. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

47
00:00:28,413 --> 00:00:28,413
185. delay(2)

48
00:00:30,426 --> 00:00:30,426
189. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5, 2)

49
00:00:30,707 --> 00:00:30,708
193. delay(2)

50
00:00:32,718 --> 00:00:32,718
197. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged2.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged2.png")

51
00:00:33,301 --> 00:00:33,302
201. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 5)

52
00:00:33,342 --> 00:00:33,343
205. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/HuePicker"), 0, 20)

53
00:00:33,620 --> 00:00:33,622
209. delay(2)

54
00:00:35,631 --> 00:00:35,631
213. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/HueChanged2.png", failed + "/Widgets/ColorPicker/PickerCMYK/HueChanged2.png")

55
00:00:36,223 --> 00:00:36,223
217. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 5)

56
00:00:36,256 --> 00:00:36,257
221. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AlphaChannelPicker"), 0, 20)

57
00:00:36,554 --> 00:00:36,554
225. delay(2)

58
00:00:38,570 --> 00:00:38,571
229. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/PickersSecondCheck.png", failed + "/Widgets/ColorPicker/PickerCMYK/PickersSecondCheck.png")

59
00:00:39,147 --> 00:00:39,147
233. switchToDefaultContent()

60
00:00:39,159 --> 00:00:39,159
237. click(findTestObject("TestStandPanel/FinishButton"))

61
00:00:39,253 --> 00:00:39,254
241. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

62
00:00:39,296 --> 00:00:39,297
245. delay(2)

63
00:00:41,306 --> 00:00:41,306
249. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck2.png", failed + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck2.png")

64
00:00:41,871 --> 00:00:41,872
253. click(findTestObject("TestStandPanel/BackToEditorButton"))

65
00:00:41,934 --> 00:00:41,935
257. switchToFrame(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe"), 5)

66
00:00:42,030 --> 00:00:42,031
261. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

67
00:00:42,069 --> 00:00:42,070
265. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"), 5)

68
00:00:42,143 --> 00:00:42,144
269. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"))

69
00:00:42,379 --> 00:00:42,379
273. delay(2)

70
00:00:44,393 --> 00:00:44,394
277. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/AddToRecentSection"))

71
00:00:44,461 --> 00:00:44,462
281. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/RecentPaletteAdded.png", failed + "/Widgets/ColorPicker/PickerCMYK/RecentPaletteAdded.png")

72
00:00:45,063 --> 00:00:45,063
285. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel"), 5)

73
00:00:45,096 --> 00:00:45,096
289. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel"))

74
00:00:45,292 --> 00:00:45,292
293. delay(2)

75
00:00:47,298 --> 00:00:47,299
297. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/ChannelSelectedLightning.png", failed + "/Widgets/ColorPicker/PickerCMYK/ChannelSelectedLightning.png")

76
00:00:47,866 --> 00:00:47,867
301. if (driver == "CHROME_DRIVER" || driver == "HEADLESS_DRIVER")

77
00:00:47,872 --> 00:00:47,873
1. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel"), "67")

78
00:00:48,189 --> 00:00:48,191
5. delay(2)

79
00:00:50,209 --> 00:00:50,209
9. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/CChannelValidInput1.png", failed + "/Widgets/ColorPicker/PickerCMYK/CChannelValidInput1.png")

80
00:00:50,760 --> 00:00:50,760
13. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel"), "abc")

81
00:00:50,993 --> 00:00:50,994
17. delay(2)

82
00:00:52,999 --> 00:00:52,999
21. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/CChannelInvalidInput1.png", failed + "/Widgets/ColorPicker/PickerCMYK/CChannelInvalidInput1.png")

83
00:00:53,583 --> 00:00:53,583
25. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel"), "76")

84
00:00:53,749 --> 00:00:53,749
29. delay(2)

85
00:00:55,755 --> 00:00:55,756
33. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/CChannelValidInput2.png", failed + "/Widgets/ColorPicker/PickerCMYK/CChannelValidInput2.png")

86
00:00:56,365 --> 00:00:56,365
37. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel"), "!@")

87
00:00:56,601 --> 00:00:56,601
41. delay(2)

88
00:00:58,608 --> 00:00:58,608
45. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/CChannelInvalidInput2.png", failed + "/Widgets/ColorPicker/PickerCMYK/CChannelInvalidInput2.png")

89
00:00:59,178 --> 00:00:59,179
49. setText(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel"), "150")

90
00:00:59,374 --> 00:00:59,374
53. delay(2)

91
00:01:01,379 --> 00:01:01,379
57. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/CChannelInvalidInput3.png", failed + "/Widgets/ColorPicker/PickerCMYK/CChannelInvalidInput3.png")

92
00:01:01,955 --> 00:01:01,955
61. sendKeys(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_C_Channel"), Keys.chord(TAB))

93
00:01:02,030 --> 00:01:02,033
65. delay(2)

94
00:01:04,040 --> 00:01:04,041
69. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/TabChangedCursorToM.png", failed + "/Widgets/ColorPicker/PickerCMYK/TabChangedCursorToM.png")

95
00:01:04,622 --> 00:01:04,622
73. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_M_Channel"), 5)

96
00:01:04,666 --> 00:01:04,667
77. sendKeys(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_M_Channel"), Keys.chord(TAB))

97
00:01:04,755 --> 00:01:04,756
81. delay(2)

98
00:01:06,769 --> 00:01:06,770
85. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/TabChangedCursorToY.png", failed + "/Widgets/ColorPicker/PickerCMYK/TabChangedCursorToY.png")

99
00:01:07,327 --> 00:01:07,327
89. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_Y_Channel"), 5)

100
00:01:07,368 --> 00:01:07,369
93. sendKeys(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_Y_Channel"), Keys.chord(TAB))

101
00:01:07,444 --> 00:01:07,445
97. delay(2)

102
00:01:09,462 --> 00:01:09,463
101. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/TabChangedCursorToK.png", failed + "/Widgets/ColorPicker/PickerCMYK/TabChangedCursorToK.png")

103
00:01:10,027 --> 00:01:10,028
105. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_K_Channel"), 5)

104
00:01:10,066 --> 00:01:10,066
109. sendKeys(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/input_K_Channel"), Keys.chord(TAB))

105
00:01:10,150 --> 00:01:10,151
113. delay(2)

106
00:01:12,168 --> 00:01:12,169
117. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/TabChangedCursorToA.png", failed + "/Widgets/ColorPicker/PickerCMYK/TabChangedCursorToA.png")

