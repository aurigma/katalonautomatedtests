1
00:00:00,149 --> 00:00:00,150
1. openBrowser("")

2
00:00:01,308 --> 00:00:01,309
5. setViewPortSize(1930, 1180)

3
00:00:01,496 --> 00:00:01,497
9. navigateToUrl("http://localhost:3000/")

4
00:00:02,990 --> 00:00:02,990
13. driver = getExecutedBrowser().getName()

5
00:00:03,004 --> 00:00:03,006
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:03,039 --> 00:00:03,040
21. failed = System.getProperty("user.dir") + "/Screenshots/FAILED/" + driver + "/"

7
00:00:03,049 --> 00:00:03,050
25. click(findTestObject("ConfigurationsTree/div_base-editor"))

8
00:00:03,393 --> 00:00:03,393
29. click(findTestObject("ConfigurationsTree/div_Widgets"))

9
00:00:03,550 --> 00:00:03,550
33. click(findTestObject("ConfigurationsTree/div_ColorPicker"))

10
00:00:03,682 --> 00:00:03,682
37. click(findTestObject("ConfigurationsTree/div_SectionCollapsejson"))

11
00:00:03,825 --> 00:00:03,826
41. click(findTestObject("TestStandPanel/button_RUN"))

12
00:00:03,913 --> 00:00:03,914
45. click(findTestObject("TestStandPanel/EditorTab"))

13
00:00:04,165 --> 00:00:04,165
49. delay(4)

14
00:00:08,173 --> 00:00:08,173
53. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

15
00:00:08,201 --> 00:00:08,202
57. parentElem = findWebElement(parent, 30)

16
00:00:08,325 --> 00:00:08,325
61. basePoint = parentElem.getLocation()

17
00:00:08,391 --> 00:00:08,393
65. switchToFrame(parent, 5)

18
00:00:08,525 --> 00:00:08,526
69. delay(4)

19
00:00:12,534 --> 00:00:12,534
73. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

20
00:00:13,149 --> 00:00:13,150
77. delay(2)

21
00:00:15,161 --> 00:00:15,161
81. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), 3)

22
00:00:15,208 --> 00:00:15,208
85. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/PickerWithCollapseSections.png", failed + "/Widgets/ColorPicker/SectionCollapse/PickerWithCollapseSections.png")

23
00:00:15,841 --> 00:00:15,842
89. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/GrayscaleSectionCollapse"))

24
00:00:16,136 --> 00:00:16,137
93. delay(2)

25
00:00:18,149 --> 00:00:18,149
97. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/GrayscaleCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/GrayscaleCollapsed.png")

26
00:00:18,790 --> 00:00:18,790
101. delay(2)

27
00:00:20,797 --> 00:00:20,798
105. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RgbSectionCollapse"))

28
00:00:21,003 --> 00:00:21,004
109. delay(2)

29
00:00:23,016 --> 00:00:23,016
113. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/RGBCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/RGBCollapsed.png")

30
00:00:23,681 --> 00:00:23,682
117. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/CmykSectionCollapse"))

31
00:00:23,954 --> 00:00:23,955
121. delay(2)

32
00:00:25,972 --> 00:00:25,972
125. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CMYKCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/CMYKCollapsed.png")

33
00:00:26,634 --> 00:00:26,635
129. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RecentSectionCollapse"))

34
00:00:26,837 --> 00:00:26,837
133. delay(2)

35
00:00:28,844 --> 00:00:28,845
137. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/AllCollapsed.png", failed + "/Widgets/ColorPicker/SectionCollapse/AllCollapsed.png")

36
00:00:29,427 --> 00:00:29,428
141. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

37
00:00:29,723 --> 00:00:29,723
145. delay(1)

38
00:00:30,737 --> 00:00:30,738
149. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

39
00:00:31,073 --> 00:00:31,073
153. delay(2)

40
00:00:33,089 --> 00:00:33,089
157. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved.png")

41
00:00:33,683 --> 00:00:33,684
161. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/button_OK"))

42
00:00:33,938 --> 00:00:33,939
165. delay(1)

43
00:00:34,950 --> 00:00:34,951
169. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

44
00:00:35,292 --> 00:00:35,293
173. delay(2)

45
00:00:37,304 --> 00:00:37,304
177. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved2.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved2.png")

46
00:00:37,903 --> 00:00:37,903
181. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RecentSectionCollapse"))

47
00:00:38,087 --> 00:00:38,088
185. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RgbSectionCollapse"))

48
00:00:38,456 --> 00:00:38,457
189. click(findTestObject("Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/OIPickAnotherItem"))

49
00:00:39,118 --> 00:00:39,118
193. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

50
00:00:39,631 --> 00:00:39,632
197. delay(2)

51
00:00:41,644 --> 00:00:41,644
201. assert scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved3.png", failed + "/Widgets/ColorPicker/SectionCollapse/CollapsedConditionSaved3.png")

