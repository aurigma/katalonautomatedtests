1
00:00:00,113 --> 00:00:00,113
1. openBrowser("")

2
00:00:04,615 --> 00:00:04,615
5. setViewPortSize(1930, 1180)

3
00:00:04,680 --> 00:00:04,680
9. navigateToUrl(TestStandUrl)

4
00:00:05,907 --> 00:00:05,907
13. driver = getExecutedBrowser().getName()

5
00:00:05,919 --> 00:00:05,920
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:05,931 --> 00:00:05,932
21. reportsDir = System.getProperty("user.dir") + "/Screenshots/Reports/"

7
00:00:05,963 --> 00:00:05,964
25. softAssertion = new org.testng.asserts.SoftAssert()

8
00:00:05,972 --> 00:00:05,973
29. click(findTestObject("ConfigurationsTree/base-editor"))

9
00:00:06,482 --> 00:00:06,483
33. click(findTestObject("ConfigurationsTree/Widgets"))

10
00:00:06,923 --> 00:00:06,923
37. click(findTestObject("ConfigurationsTree/Widgets.ColorPicker/ColorPicker"))

11
00:00:07,279 --> 00:00:07,280
41. click(findTestObject("ConfigurationsTree/Widgets.ColorPicker/OneSection"))

12
00:00:07,592 --> 00:00:07,592
45. click(findTestObject("TestStandPanel/button_RUN"))

13
00:00:09,585 --> 00:00:09,585
49. click(findTestObject("TestStandPanel/EditorTab"))

14
00:00:10,314 --> 00:00:10,314
53. delay(4)

15
00:00:14,342 --> 00:00:14,343
57. parent = findTestObject("Aurigma.DesignEditor/Iframe")

16
00:00:14,481 --> 00:00:14,482
61. parentElem = findWebElement(parent, 30)

17
00:00:14,577 --> 00:00:14,577
65. basePoint = parentElem.getLocation()

18
00:00:15,465 --> 00:00:15,466
69. switchToFrame(parent, 5)

19
00:00:15,676 --> 00:00:15,677
73. delay(4)

20
00:00:19,694 --> 00:00:19,695
77. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

21
00:00:20,621 --> 00:00:20,621
81. delay(2)

22
00:00:22,642 --> 00:00:22,643
85. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), 3)

23
00:00:22,894 --> 00:00:22,894
89. softAssertion.assertTrue(scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/OneSectionPalette.png", reportsDir), Alert + "OneSectionPalette.png")

24
00:00:23,392 --> 00:00:23,393
93. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/OneSection_BlueColor"))

25
00:00:24,755 --> 00:00:24,756
97. delay(2)

26
00:00:26,774 --> 00:00:26,774
101. softAssertion.assertTrue(scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/TextColorButtonChanged.png", reportsDir), Alert + "TextColorButtonChanged.png")

27
00:00:27,806 --> 00:00:27,807
105. switchToDefaultContent()

28
00:00:27,852 --> 00:00:27,853
109. click(findTestObject("TestStandPanel/FinishButton"))

29
00:00:28,770 --> 00:00:28,771
113. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

30
00:00:28,939 --> 00:00:28,939
117. delay(2)

31
00:00:30,963 --> 00:00:30,963
121. softAssertion.assertTrue(scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/ColorAppliedCheck.png", reportsDir), Alert + "ColorAppliedCheck.png")

32
00:00:31,771 --> 00:00:31,771
125. click(findTestObject("TestStandPanel/BackToEditorButton"))

33
00:00:32,607 --> 00:00:32,607
129. switchToFrame(parent, 5)

34
00:00:32,671 --> 00:00:32,671
133. delay(1)

35
00:00:33,686 --> 00:00:33,687
137. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

36
00:00:34,902 --> 00:00:34,902
141. delay(2)

37
00:00:36,917 --> 00:00:36,919
145. softAssertion.assertTrue(scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/TextColorButtonSaved.png", reportsDir), Alert + "TextColorButtonSaved.png")

38
00:00:37,423 --> 00:00:37,423
149. delay(1)

39
00:00:38,458 --> 00:00:38,469
153. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

40
00:00:39,679 --> 00:00:39,680
157. delay(2)

41
00:00:41,690 --> 00:00:41,691
161. softAssertion.assertTrue(scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/PaletteNoChanges1.png", reportsDir), Alert + "PaletteNoChanges1.png")

42
00:00:42,481 --> 00:00:42,481
165. delay(1)

43
00:00:43,495 --> 00:00:43,498
169. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/RevertButton"))

44
00:00:43,967 --> 00:00:43,967
173. if (driver == "CHROME_DRIVER" || driver == "HEADLESS_DRIVER")

45
00:00:43,981 --> 00:00:43,982
177. delay(2)

46
00:00:45,998 --> 00:00:45,998
181. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/OIPickAnotherItem"))

47
00:00:46,603 --> 00:00:46,604
185. delay(1)

48
00:00:47,616 --> 00:00:47,617
189. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/SelectTextColorButton"))

49
00:00:48,345 --> 00:00:48,345
193. delay(2)

50
00:00:50,354 --> 00:00:50,354
197. softAssertion.assertTrue(scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/ColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/SectionCollapse/PaletteNoChanges2.png", reportsDir), Alert + "PaletteNoChanges2.png")

51
00:00:50,839 --> 00:00:50,839
201. closeBrowser()

52
00:00:54,125 --> 00:00:54,125
1. CURRENT_TESTCASE_ID = testCaseContext.getTestCaseId()

53
00:00:54,136 --> 00:00:54,136
5. date = new java.util.Date()

54
00:00:54,144 --> 00:00:54,145
9. sdf = new java.text.SimpleDateFormat(MMddyyyyHHmmss)

55
00:00:54,151 --> 00:00:54,151
13. TIMEMARK = sdf.format(date)

