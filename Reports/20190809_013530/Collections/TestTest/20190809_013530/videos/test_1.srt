1
00:00:00,294 --> 00:00:00,296
1. openBrowser("")

2
00:00:04,887 --> 00:00:04,887
5. setViewPortSize(1930, 1180)

3
00:00:04,970 --> 00:00:04,971
9. navigateToUrl(TestStandUrl)

4
00:00:06,586 --> 00:00:06,588
13. driver = getExecutedBrowser().getName()

5
00:00:06,661 --> 00:00:06,663
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:06,735 --> 00:00:06,746
21. reportsDir = System.getProperty("user.dir") + "/Screenshots/Reports/"

7
00:00:06,781 --> 00:00:06,785
25. softAssertion = new org.testng.asserts.SoftAssert()

8
00:00:06,833 --> 00:00:06,837
29. logInfo(baseDir)

9
00:00:06,872 --> 00:00:06,873
33. click(findTestObject("ConfigurationsTree/base-editor"))

10
00:00:07,383 --> 00:00:07,384
37. click(findTestObject("ConfigurationsTree/Widgets"))

11
00:00:07,776 --> 00:00:07,777
41. click(findTestObject("ConfigurationsTree/Widgets.ColorPicker/ColorPicker"))

12
00:00:08,149 --> 00:00:08,151
45. click(findTestObject("ConfigurationsTree/Widgets.ColorPicker/Palette"))

13
00:00:08,476 --> 00:00:08,478
49. click(findTestObject("TestStandPanel/button_RUN"))

14
00:00:09,644 --> 00:00:09,645
53. click(findTestObject("TestStandPanel/EditorTab"))

15
00:00:10,151 --> 00:00:10,151
57. delay(5)

16
00:00:15,214 --> 00:00:15,216
61. executionPath = getLogFolderPath()

17
00:00:15,247 --> 00:00:15,248
65. logInfo(executionPath)

18
00:00:15,276 --> 00:00:15,278
69. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

19
00:00:15,459 --> 00:00:15,462
73. parentElem = findWebElement(parent, 30)

20
00:00:15,752 --> 00:00:15,756
77. basePoint = parentElem.getLocation()

21
00:00:15,804 --> 00:00:15,807
81. switchToFrame(parent, 5)

22
00:00:16,094 --> 00:00:16,095
85. delay(2)

23
00:00:18,106 --> 00:00:18,107
89. if (driver == "CHROME_DRIVER" || driver == "HEADLESS_DRIVER")

24
00:00:18,110 --> 00:00:18,111
93. delay(1)

25
00:00:19,116 --> 00:00:19,117
97. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton"))

26
00:00:19,803 --> 00:00:19,804
101. delay(2)

27
00:00:21,823 --> 00:00:21,824
105. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), 5)

28
00:00:21,965 --> 00:00:21,965
109. softAssertion.assertTrue(scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/TextColorDialog.png", reportsDir), "Assert false: TextColorDialog.png")

29
00:00:22,505 --> 00:00:22,505
113. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Purple"))

30
00:00:23,284 --> 00:00:23,285
117. delay(1)

31
00:00:24,290 --> 00:00:24,291
121. switchToDefaultContent()

32
00:00:24,327 --> 00:00:24,328
125. click(findTestObject("TestStandPanel/FinishButton"))

33
00:00:24,631 --> 00:00:24,632
129. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

34
00:00:24,857 --> 00:00:24,859
133. delay(2)

35
00:00:26,876 --> 00:00:26,876
137. softAssertion.assertTrue(scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/ColorAppliedCheck1.png", reportsDir), "Assert false: ColorAppliedCheck1.png")

36
00:00:28,019 --> 00:00:28,020
141. click(findTestObject("TestStandPanel/BackToEditorButton"))

37
00:00:28,426 --> 00:00:28,427
145. switchToFrame(parent, 5)

38
00:00:28,449 --> 00:00:28,450
149. delay(1)

39
00:00:29,458 --> 00:00:29,459
153. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

40
00:00:29,945 --> 00:00:29,946
157. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

41
00:00:30,190 --> 00:00:30,191
161. softAssertion.assertTrue(scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck1.png", reportsDir), "Assert false: AddToRecentCheck1.png")

42
00:00:30,603 --> 00:00:30,604
165. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Burgundy"))

43
00:00:31,179 --> 00:00:31,180
169. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

44
00:00:31,782 --> 00:00:31,782
173. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RecentColor_Purple"))

45
00:00:32,378 --> 00:00:32,379
177. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RecentColor_Purple"))

46
00:00:32,595 --> 00:00:32,596
181. delay(2)

47
00:00:34,607 --> 00:00:34,608
185. softAssertion.assertTrue(scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck2.png", reportsDir), "Assert false: AddToRecentCheck2.png")

48
00:00:34,929 --> 00:00:34,929
189. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

49
00:00:35,374 --> 00:00:35,379
193. focus(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/AddToRecentSection"))

50
00:00:35,612 --> 00:00:35,612
197. delay(2)

51
00:00:37,620 --> 00:00:37,621
201. softAssertion.assertTrue(scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/AddToRecentCheck3.png", reportsDir), "Assert false: AddToRecentCheck3.png")

52
00:00:37,906 --> 00:00:37,907
205. navigateToUrl("http://localhost:3000/")

53
00:00:38,560 --> 00:00:38,562
209. if (driver == "CHROME_DRIVER" || driver == "HEADLESS_DRIVER")

54
00:00:38,595 --> 00:00:38,600
213. click(findTestObject("ConfigurationsTree/base-editor"))

55
00:00:39,180 --> 00:00:39,181
217. click(findTestObject("ConfigurationsTree/Widgets"))

56
00:00:39,540 --> 00:00:39,540
221. click(findTestObject("ConfigurationsTree/Widgets.ColorPicker/ColorPicker"))

57
00:00:39,843 --> 00:00:39,843
225. click(findTestObject("ConfigurationsTree/Widgets.ColorPicker/Palette"))

58
00:00:40,150 --> 00:00:40,151
229. click(findTestObject("TestStandPanel/button_RUN"))

59
00:00:41,185 --> 00:00:41,185
233. click(findTestObject("TestStandPanel/EditorTab"))

60
00:00:42,192 --> 00:00:42,193
237. delay(5)

61
00:00:47,212 --> 00:00:47,213
241. switchToFrame(parent, 5)

62
00:00:47,358 --> 00:00:47,358
245. delay(2)

63
00:00:49,370 --> 00:00:49,371
249. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/SelectTextColorButton"))

64
00:00:49,982 --> 00:00:49,983
253. delay(2)

65
00:00:51,999 --> 00:00:52,000
257. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), 5)

66
00:00:52,080 --> 00:00:52,081
261. delay(2)

67
00:00:54,086 --> 00:00:54,086
265. softAssertion.assertTrue(scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/CPAtNewPage.png", reportsDir), "Assert false: CPAtNewPage.png")

68
00:00:54,379 --> 00:00:54,380
269. delay(1)

69
00:00:55,388 --> 00:00:55,389
273. softAssertion.assertTrue(scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/FillColorButton"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/FillButtonNoColor.png", reportsDir), "Assert false: FillButtonNoColor.png")

70
00:00:55,662 --> 00:00:55,662
277. delay(1)

71
00:00:56,668 --> 00:00:56,669
281. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/FillColorButton"))

72
00:00:57,345 --> 00:00:57,345
285. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/FillColorPickerDialog"), 5)

73
00:00:57,461 --> 00:00:57,462
289. delay(2)

74
00:00:59,493 --> 00:00:59,493
293. softAssertion.assertTrue(scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/FillColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/FillColorDialog.png", reportsDir), "Assert false: FillColorDialog.png")

75
00:00:59,817 --> 00:00:59,818
297. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/RgbColors_Yellow"))

76
00:01:00,538 --> 00:01:00,539
301. delay(2)

77
00:01:02,545 --> 00:01:02,546
305. softAssertion.assertTrue(scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/FillColorButton"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/FillButtonColored.png", reportsDir))

78
00:01:02,863 --> 00:01:02,863
309. switchToDefaultContent()

79
00:01:02,882 --> 00:01:02,883
313. click(findTestObject("TestStandPanel/FinishButton"))

80
00:01:03,286 --> 00:01:03,288
317. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

81
00:01:03,450 --> 00:01:03,450
321. delay(2)

82
00:01:05,461 --> 00:01:05,462
325. softAssertion.assertTrue(scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/ColorAppliedCheck2.png", reportsDir))

83
00:01:05,799 --> 00:01:05,800
329. click(findTestObject("TestStandPanel/BackToEditorButton"))

84
00:01:06,082 --> 00:01:06,083
333. switchToFrame(parent, 5)

85
00:01:06,174 --> 00:01:06,174
337. delay(2)

86
00:01:08,189 --> 00:01:08,189
341. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/NoColorSection"))

87
00:01:08,866 --> 00:01:08,867
345. delay(2)

88
00:01:10,882 --> 00:01:10,883
349. softAssertion.assertTrue(scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Palette/FillColorButton"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/FillButtonColorRemoved.png", reportsDir))

89
00:01:11,206 --> 00:01:11,207
353. switchToDefaultContent()

90
00:01:11,227 --> 00:01:11,227
357. click(findTestObject("TestStandPanel/FinishButton"))

91
00:01:11,594 --> 00:01:11,595
361. delay(2)

92
00:01:13,620 --> 00:01:13,621
365. waitForElementVisible(findTestObject("TestStandPanel/PreviewImage"), 5)

93
00:01:13,696 --> 00:01:13,697
369. delay(2)

94
00:01:15,705 --> 00:01:15,706
373. softAssertion.assertTrue(scripts.ScreenshotHandler.takeAndCompare(findTestObject("TestStandPanel/PreviewImage"), basePoint, baseDir + "/Widgets/ColorPicker/Palette/ColorCanceledCheck.png", reportsDir))

95
00:01:16,039 --> 00:01:16,040
377. softAssertion.assertAll()

96
00:01:18,172 --> 00:01:18,172
1. CURRENT_TESTCASE_ID = testCaseContext.getTestCaseId()

97
00:01:18,178 --> 00:01:18,178
5. date = new java.util.Date()

98
00:01:18,183 --> 00:01:18,183
9. sdf = new java.text.SimpleDateFormat(MMddyyyyHHmmss)

99
00:01:18,187 --> 00:01:18,188
13. TIMEMARK = sdf.format(date)

