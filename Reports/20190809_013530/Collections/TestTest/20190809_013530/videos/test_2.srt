1
00:00:00,136 --> 00:00:00,140
1. openBrowser("")

2
00:00:04,142 --> 00:00:04,142
5. setViewPortSize(1930, 1180)

3
00:00:04,233 --> 00:00:04,238
9. navigateToUrl(TestStandUrl)

4
00:00:05,584 --> 00:00:05,585
13. driver = getExecutedBrowser().getName()

5
00:00:05,594 --> 00:00:05,595
17. baseDir = System.getProperty("user.dir") + "/Screenshots/Baseline/" + driver + "/"

6
00:00:05,603 --> 00:00:05,604
21. reportsDir = System.getProperty("user.dir") + "/Screenshots/Reports/"

7
00:00:05,621 --> 00:00:05,621
25. softAssertion = new org.testng.asserts.SoftAssert()

8
00:00:05,638 --> 00:00:05,639
29. click(findTestObject("ConfigurationsTree/base-editor"))

9
00:00:06,225 --> 00:00:06,226
33. click(findTestObject("ConfigurationsTree/Widgets"))

10
00:00:06,538 --> 00:00:06,539
37. click(findTestObject("ConfigurationsTree/Widgets.ColorPicker/ColorPicker"))

11
00:00:06,839 --> 00:00:06,839
41. click(findTestObject("ConfigurationsTree/Widgets.ColorPicker/Picker"))

12
00:00:07,130 --> 00:00:07,130
45. click(findTestObject("TestStandPanel/button_RUN"))

13
00:00:08,187 --> 00:00:08,187
49. click(findTestObject("TestStandPanel/EditorTab"))

14
00:00:08,604 --> 00:00:08,608
53. delay(4)

15
00:00:12,630 --> 00:00:12,631
57. parent = findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/Iframe")

16
00:00:12,805 --> 00:00:12,810
61. parentElem = findWebElement(parent, 30)

17
00:00:14,606 --> 00:00:14,608
65. basePoint = parentElem.getLocation()

18
00:00:15,558 --> 00:00:15,560
69. switchToFrame(parent, 5)

19
00:00:15,750 --> 00:00:15,751
73. delay(4)

20
00:00:19,770 --> 00:00:19,771
77. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SelectTextColorButton"))

21
00:00:20,324 --> 00:00:20,328
81. delay(2)

22
00:00:22,365 --> 00:00:22,367
85. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), 5)

23
00:00:22,465 --> 00:00:22,466
89. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"), 5)

24
00:00:22,543 --> 00:00:22,543
93. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/ColorSpaceSwitch"))

25
00:00:23,013 --> 00:00:23,018
97. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"), 5)

26
00:00:23,128 --> 00:00:23,128
101. click(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/a_CMYK"))

27
00:00:23,893 --> 00:00:23,893
105. delay(1)

28
00:00:24,903 --> 00:00:24,903
109. waitForElementVisible(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 5)

29
00:00:24,984 --> 00:00:24,984
113. dragAndDropByOffset(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/SaturationPicker"), 0, 15)

30
00:00:25,704 --> 00:00:25,704
117. delay(2)

31
00:00:27,713 --> 00:00:27,714
121. softAssertion.assertTrue(scripts.ScreenshotHandler.takeAndCompare(findTestObject("Aurigma.DesignEditor/Widgets/ColorPicker/Picker/TextColorPickerDialog"), basePoint, baseDir + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png", failed + "/Widgets/ColorPicker/PickerCMYK/SaturationChanged1.png"))

