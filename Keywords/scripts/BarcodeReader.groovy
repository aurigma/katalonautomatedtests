package scripts

import java.awt.image.BufferedImage

import javax.imageio.ImageIO

import com.google.zxing.BinaryBitmap
import com.google.zxing.LuminanceSource
import com.google.zxing.MultiFormatReader
import com.google.zxing.Result
import com.google.zxing.client.j2se.BufferedImageLuminanceSource
import com.google.zxing.common.HybridBinarizer
import com.kms.katalon.core.annotation.Keyword

public class BarcodeReader {

	@Keyword
	public static String ReadBarcode(String url) {
		URL testUrl = new URL(url);
		BufferedImage barCodeBufferedImage = ImageIO.read(testUrl.openStream());

		LuminanceSource source = new BufferedImageLuminanceSource(barCodeBufferedImage);
		BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
		MultiFormatReader reader = new MultiFormatReader();
		Result result = reader.decode(bitmap);

		return result.getText();
	}
}
