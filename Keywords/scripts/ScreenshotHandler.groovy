package scripts

import java.awt.image.BufferedImage

import javax.imageio.ImageIO

import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable
import ru.yandex.qatools.ashot.AShot
import ru.yandex.qatools.ashot.Screenshot
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;
import ru.yandex.qatools.ashot.coordinates.WebDriverCoordsProvider
import ru.yandex.qatools.ashot.shooting.ShootingStrategies

public class ScreenshotHandler {

	/**
	 * Take object screenshot and save it to the specified output path.
	 * NOTE: If object has a parent iframe, you should not switch frame to it before hand.
	 * This method handles switching itself.
	 * @param object TestObject to be screenshoted.
	 * @param outputFile Output save path.
	 */
	@Keyword
	static void takeAndSave(TestObject object, String outputFile) {
		Screenshot screenshot = takeObjectScreenshot(object);
		saveScreenshot(screenshot, outputFile);
	}

	@Keyword
	static boolean takeAndCompare(TestObject object, String baseImageFile, String reportPath){
		Screenshot screenshot = takeObjectScreenshot(object);
		return compareToBase(screenshot, baseImageFile, reportPath);
	}

	@Keyword
	static boolean takeAndCompare(TestObject object, Point basePoint, String baseImageFile, String reportPath){
		Screenshot screenshot = takeObjectScreenshot(object, basePoint);
		return compareToBase(screenshot, baseImageFile, reportPath);
	}

	/**
	 * Take page screenshot and save it to the specified output path.	 *
	 * @param outputFile Output save path.
	 */
	@Keyword
	static void takePageAndSave(String outputFile) {
		Screenshot screenshot = takePageScreenshot();
		saveScreenshot(screenshot, outputFile);
	}

	/**
	 * Take object screenshot and save it to the specified output path.
	 * NOTE: If object has a parent iframe, you should pass it's location as 'base' parameter and
	 * switch to this frame.
	 * @param object TestObject to be screenshoted.
	 * @param base Point from which object's location will be calculated.
	 * @param outputFile Output save path.
	 */
	@Keyword
	static void takeAndSave(TestObject object, Point basePoint, String outputFile) {
		Screenshot screenshot = takeObjectScreenshot(object, basePoint);
		saveScreenshot(screenshot, outputFile);
	}

	/**
	 * Take each element from object repository folder and save their screenshots to specified directory.
	 * NOTE: All elements in the folder must have same parent object(if there is none, then all elements should not have any).
	 * You should pass this parent object as a parameter (or null, if there's none).
	 * @param repositoryFolder Target folder from repository.
	 * @param outputDir Directory where screenshots will be saved.
	 * @param parentObject Common (for all objects in target folder) parent.
	 */
	@Keyword
	static void takeAndSaveFolder(String repositoryFolder, String outputDir, TestObject parentObject) {
		KeywordUtil.logInfo("Searching objects in "+repositoryFolder);
		List<String> objectNames = getChildrenTestObject(repositoryFolder);
		Point base;
		if(parentObject != null) {
			WebElement parent = WebUiCommonHelper.findWebElement(parentObject, 30);
			base = parent.getLocation();
			WebUI.switchToFrame(parentObject, 30);
		}

		for(String obj in objectNames) {
			String path = outputDir +obj+ ".png";
			KeywordUtil.logInfo("Taking screenshot at:"+path)
			takeAndSave(ObjectRepository.findTestObject(obj), base, path);
		}

		if(parentObject != null){
			WebUI.switchToDefaultContent();
		}
	}

	/**
	 * Compares screenshot to base image loaded from the disk.
	 * @param screenshot Screenshot to be compared.
	 * @param baseImagePath Path to base image.
	 */
	@Keyword
	static boolean compareToBase(Screenshot screenshot, String baseImagePath, String reportPath) {
		BufferedImage baseImage = ImageIO.read(new File(baseImagePath));
		ImageDiff result = new ImageDiffer().makeDiff(baseImage, screenshot.getImage());
		if(result.hasDiff()){
			int differenceTrigger = 80;
			result = result.withDiffSizeTrigger(differenceTrigger);
			String name = org.apache.commons.io.FilenameUtils.getBaseName(baseImagePath);
			if(!result.hasDiff()){
				makeWarnReport(result.getMarkedImage(), screenshot.getImage(), baseImage, name, reportPath);
				KeywordUtil.markWarning("Warning: images have difference. Size: "+ result.getDiffSize());
			} else	{
				makeErrReport(result.getMarkedImage(), screenshot.getImage(), baseImage, name, reportPath);
			}
		}
		return !result.hasDiff();
	}

	/*
	 @Keyword
	 static boolean compareToBase(Screenshot screenshot, String baseImagePath, String failedImagePath) {
	 BufferedImage baseImage = ImageIO.read(new File(baseImagePath));
	 ImageDiff result = new ImageDiffer().makeDiff(baseImage, screenshot.getImage());
	 if(result.hasDiff()){
	 int differenceTrigger = 80;
	 result = result.withDiffSizeTrigger(differenceTrigger);
	 String failedPath = failedImagePath;
	 KeywordUtil.markWarning("Warning: images have difference. Size: "+ result.getDiffSize());
	 if(!result.hasDiff()){
	 failedPath = failedPath + ".warn.png";
	 }
	 saveScreenshot(screenshot, failedPath);
	 }
	 return !result.hasDiff();
	 }
	 */


	/**
	 * Takes and returns object's screenshot
	 * @param screenshot Screenshot to be compared.
	 * NOTE: If object has a parent iframe, you should not switch frame to it before hand.
	 * This method handles switching itself.
	 * @param object TestObject to be screenshoted.
	 * @param outputFile Output save path.
	 */
	/*	@Keyword
	 static boolean compareAndSaveFailed(TestObject object, String baseImagePath, String failedImagePath){
	 boolean result = takeAndCompare(object, baseImagePath);
	 return result;
	 }*/

	@Keyword
	static Screenshot takeObjectScreenshot(TestObject object) {
		TestObject parentObject = object.getParentObject();
		Point base = new Point(0,0);
		if(parentObject != null) {
			WebElement parent = WebUiCommonHelper.findWebElement(parentObject, 30);
			base = parent.getLocation();
			WebUI.switchToFrame(parentObject, 30);
		}
		return takeObjectScreenshot(object, base);
	}

	@Keyword
	static Screenshot takeObjectScreenshot(TestObject object, Point basePoint) {

		Screenshot result = takeScreenshotManual(object, basePoint);

		/*if(parentObject != null){
		 WebUI.switchToDefaultContent();
		 }*/

		return result;
	}

	/**
	 * Takes and whole page screenshot using AShot.
	 */
	@Keyword
	static Screenshot takePageScreenshot() {
		WebDriver driver = DriverFactory.getWebDriver();
		Screenshot screenshot = new AShot().coordsProvider(new WebDriverCoordsProvider()).shootingStrategy(ShootingStrategies.viewportPasting(100)).takeScreenshot(driver);
		return screenshot;
	}

	/**
	 * Saves passed screenshot to the specified output path.
	 * @param screenshot Screenshot to be saved.
	 * @param outputFile Output save path.
	 */
	@Keyword
	static void saveScreenshot(Screenshot screenshot, String imagePath) {
		saveImage(screenshot.getImage(), imagePath);
	}

	/*******************************************************
	 **************** PRIVATE METHODS GO HERE **************
	 *******************************************************/

	private static void saveImage(BufferedImage image, String imagePath){
		File outputFile = new File(imagePath);
		outputFile.getParentFile().mkdirs();
		ImageIO.write(image, "PNG", outputFile);
	}

	private static void makeWarnReport(BufferedImage markedImage, BufferedImage screenshot, BufferedImage baseImage, String name, String reportPath){
		makeReport(markedImage,screenshot, baseImage, name, reportPath, 'warning');
	}

	private static void makeErrReport(BufferedImage markedImage, BufferedImage screenshot, BufferedImage baseImage, String name, String reportPath){
		makeReport(markedImage,screenshot, baseImage, name, reportPath, 'error');
	}

	private static void makeReport(BufferedImage markedImage, BufferedImage screenshot, BufferedImage baseImage, String name, String reportPath, String type){
		String driver = DriverFactory.getExecutedBrowser().getName();
		String folder = reportPath + '/' +  GlobalVariable.CURRENT_TESTCASE_ID + '/'+ driver + '/'+ GlobalVariable.TIMEMARK + '/' + type + '/' + name + '/';
		saveImage(baseImage, folder + 'base.png');
		saveImage(screenshot, folder + 'screenshot.png');
		saveImage(markedImage, folder + 'diff.png');
	}


	/**
	 * Internal method for taking screenshot.
	 * 'Manual' because without Ashot.
	 * @param object TestObject to be screenshoted.
	 * @param base base Point from which object's location will be calculated.
	 */
	private static Screenshot takeScreenshotManual(TestObject object, Point base) {
		WebElement element = WebUiCommonHelper.findWebElement(object, 30);
		WebDriver driver = DriverFactory.getWebDriver();
		File screen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		Point p = element.getLocation();
		if(DriverFactory.getExecutedBrowser().getName() == "CHROME_DRIVER" || DriverFactory.getExecutedBrowser().getName() == "HEADLESS_DRIVER") {
			p = p.moveBy(base.getX(), base.getY());
		}

		int width = element.getSize().getWidth();
		int height = element.getSize().getHeight();

		BufferedImage img = ImageIO.read(screen);

		BufferedImage dest = img.getSubimage(p.getX(), p.getY(), width,
				height);
		return new Screenshot(dest);
	}

	/**
	 * Internal method for gathering names of the test objects from object repository folder.
	 * @param folder Target folder from repository.
	 */
	private static List<String> getChildrenTestObject(String folder) {
		String pathToObjRep = RunConfiguration.getProjectDir() + "\\Object Repository\\" + folder

		File yourObjects = new File(pathToObjRep)

		// get all files from the directory
		String[] fileNames = yourObjects.list()
		List<String> testObjects = new ArrayList<>()

		// get only test objects, not other subdirectories if there are any (with .rs extension)
		for(int i = 0; i < fileNames.length; i++) {
			if(fileNames[i].contains(".rs")) {
				fileNames[i] = fileNames[i].replace(".rs", "")
				// add to a list a relative path to TestObject within Object Repository
				testObjects.add(folder +"/"+ fileNames[i])
			}
		}
		return testObjects;
	}

}