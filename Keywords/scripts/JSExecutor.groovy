package scripts

import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.webui.driver.DriverFactory

public class JSExecutor {

	@Keyword
	public void executeJs(String javaScript, WebElement element){
		WebDriver driver = DriverFactory.getWebDriver()
		JavascriptExecutor executor = ((JavascriptExecutor)driver)
		executor.executeScript(javaScript, element)
	}
}
