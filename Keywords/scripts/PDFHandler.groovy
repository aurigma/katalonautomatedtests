package scripts

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.text.PDFTextStripper;

public class PDFHandler {

	public String ReadPDF(String url) {
		URL testUrl = new URL(url);
		String result = "";
		PDDocument document = null;
		try {
			document = PDDocument.load(testUrl.openStream());

			AccessPermission ap = document.getCurrentAccessPermission();
			if (!ap.canExtractContent()) {
				throw new IOException("You do not have permission to extract text");
			}

			PDFTextStripper stripper = new PDFTextStripper();

			//stripper.setSortByPosition(true);

			result = stripper.getText(document).trim();
		}
		finally {
			if(document != null)
				document.close();
		}
		return result;
	}
}