<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CmykRgbCmyk</name>
   <tag></tag>
   <elementGuidId>72dd4dd1-4e20-4795-8d2c-2ca38d01ba14</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//tree-viewport//span[contains(text(), 'CmykRgbCmyk.json')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//tree-viewport//span[contains(text(), 'CmykRgbCmyk.json')]</value>
   </webElementProperties>
</WebElementEntity>
