<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CmykRgb</name>
   <tag></tag>
   <elementGuidId>9ad77eab-6bef-4278-a297-8f4be6014d87</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//tree-viewport//span[contains(text(), 'CmykRgb.json')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//tree-viewport//span[contains(text(), 'CmykRgb.json')]</value>
   </webElementProperties>
</WebElementEntity>
