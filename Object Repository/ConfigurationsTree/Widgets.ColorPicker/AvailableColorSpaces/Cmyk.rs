<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Cmyk</name>
   <tag></tag>
   <elementGuidId>cb19c7d7-d42f-4779-bda1-dfa600024b41</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//tree-viewport//span[contains(text(), 'Cmyk.json')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//tree-viewport//span[contains(text(), 'Cmyk.json')]</value>
   </webElementProperties>
</WebElementEntity>
