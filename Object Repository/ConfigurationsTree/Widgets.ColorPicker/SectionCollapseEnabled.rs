<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>SectionCollapseEnabled</name>
   <tag></tag>
   <elementGuidId>63095a33-58d7-4c0c-8e0a-dfe8599c769e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//tree-viewport//span[contains(text(), 'SectionCollapseEnabled.json')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//tree-viewport//span[contains(text(), 'SectionCollapseEnabled.json')]</value>
   </webElementProperties>
</WebElementEntity>
