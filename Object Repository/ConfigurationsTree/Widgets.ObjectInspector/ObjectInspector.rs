<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ObjectInspector</name>
   <tag></tag>
   <elementGuidId>97e9288b-441a-477d-9391-f277b04a6564</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//tree-node[1]//span[contains(text(), 'ObjectInspector')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//tree-node[1]//span[contains(text(), 'ObjectInspector')]</value>
   </webElementProperties>
</WebElementEntity>
