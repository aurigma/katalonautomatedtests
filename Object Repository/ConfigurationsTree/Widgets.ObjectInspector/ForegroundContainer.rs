<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ForegroundContainer</name>
   <tag></tag>
   <elementGuidId>c94321be-8934-498b-93ff-a013032a5e21</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//tree-node[1]//span[contains(text(), 'ForegroundContainer.json')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//tree-node[1]//span[contains(text(), 'ForegroundContainer.json')]</value>
   </webElementProperties>
</WebElementEntity>
