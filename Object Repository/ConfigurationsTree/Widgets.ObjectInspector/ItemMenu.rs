<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ItemMenu</name>
   <tag></tag>
   <elementGuidId>f7ed884f-a37e-4502-b722-89903456ec5e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//tree-node[1]//span[contains(text(), 'ItemMenu.json')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//tree-node[1]//span[contains(text(), 'ItemMenu.json')]</value>
   </webElementProperties>
</WebElementEntity>
