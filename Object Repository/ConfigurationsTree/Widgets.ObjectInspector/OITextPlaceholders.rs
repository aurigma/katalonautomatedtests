<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OITextPlaceholders</name>
   <tag></tag>
   <elementGuidId>ae5e8d13-6a67-4d11-a4d9-9937eb8ec7bc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//tree-node[1]//span[contains(text(), 'OITextPlaceholders.json')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//tree-node[1]//span[contains(text(), 'OITextPlaceholders.json')]</value>
   </webElementProperties>
</WebElementEntity>
