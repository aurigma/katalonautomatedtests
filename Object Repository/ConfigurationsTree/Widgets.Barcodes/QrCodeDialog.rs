<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>QrCodeDialog</name>
   <tag></tag>
   <elementGuidId>185a22a3-6526-4014-b035-7d095cac082a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//tree-root//tree-viewport//span[contains(text(), 'QrCodeDialog')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//tree-root//tree-viewport//span[contains(text(), 'QrCodeDialog')]</value>
   </webElementProperties>
</WebElementEntity>
