<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LinearBarcodeDialog</name>
   <tag></tag>
   <elementGuidId>3abecf19-b07a-4550-aed1-90e0b68e985b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//tree-root//tree-viewport//span[contains(text(), 'LinearBarcodeDialog')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//tree-root//tree-viewport//span[contains(text(), 'LinearBarcodeDialog')]</value>
   </webElementProperties>
</WebElementEntity>
