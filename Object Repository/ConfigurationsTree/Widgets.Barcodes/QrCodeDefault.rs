<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>QrCodeDefault</name>
   <tag></tag>
   <elementGuidId>18db1982-54ec-4b9c-81a7-91227a00e06e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/tree-root//tree-viewport//span[contains(text(), 'QrcodeDefault.json')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/tree-root//tree-viewport//span[contains(text(), 'QrcodeDefault.json')]</value>
   </webElementProperties>
</WebElementEntity>
