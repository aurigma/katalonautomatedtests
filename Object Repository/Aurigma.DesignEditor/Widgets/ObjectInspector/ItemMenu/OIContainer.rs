<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OIContainer</name>
   <tag></tag>
   <elementGuidId>0fc02063-f095-4d91-a51f-2e736cab3297</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//cc-object-inspector//div[@class='object-inspector-items ng-scope']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//cc-object-inspector//div[@class='object-inspector-items ng-scope']</value>
   </webElementProperties>
</WebElementEntity>
