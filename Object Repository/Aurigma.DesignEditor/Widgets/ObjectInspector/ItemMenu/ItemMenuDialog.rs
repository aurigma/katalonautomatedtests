<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ItemMenuDialog</name>
   <tag></tag>
   <elementGuidId>017337cd-ce65-46cb-a112-0718f67c981a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;mainContainer&quot;]//div[@class='mainEditMenuRoot']/ul[@class='dropdown-menu icon-menu textAlignLeft itemMenu mainEditMenu']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;mainContainer&quot;]//div[@class='mainEditMenuRoot']/ul[@class='dropdown-menu icon-menu textAlignLeft itemMenu mainEditMenu']</value>
   </webElementProperties>
</WebElementEntity>
