<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OpenTextMenu</name>
   <tag></tag>
   <elementGuidId>eb541480-b189-4c36-b38a-a66dd1fbfb78</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//cc-object-inspector//cc-tab-swicher[2]//div[@class='oi-item__edit ng-scope']//button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//cc-object-inspector//cc-tab-swicher[2]//div[@class='oi-item__edit ng-scope']//button</value>
   </webElementProperties>
</WebElementEntity>
