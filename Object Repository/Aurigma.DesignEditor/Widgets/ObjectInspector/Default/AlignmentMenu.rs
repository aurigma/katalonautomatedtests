<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AlignmentMenu</name>
   <tag></tag>
   <elementGuidId>cd27a4c3-3e78-4694-af31-7bb613de2567</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='mainEditMenuRoot']//a[@class='subEditMenuTitle ng-binding']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='mainEditMenuRoot']//a[@class='subEditMenuTitle ng-binding']</value>
   </webElementProperties>
</WebElementEntity>
