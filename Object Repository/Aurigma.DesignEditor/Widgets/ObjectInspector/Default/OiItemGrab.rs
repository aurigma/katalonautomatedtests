<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OiItemGrab</name>
   <tag></tag>
   <elementGuidId>624fa707-9dfb-4d09-826a-f61795e49670</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@id='mainRow']//cc-object-inspector//cc-tab-swicher[1]//div[@class='btn-group oi-item__dnd-handler ng-scope grab']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@id='mainRow']//cc-object-inspector//cc-tab-swicher[1]//div[@class='btn-group oi-item__dnd-handler ng-scope grab']</value>
   </webElementProperties>
</WebElementEntity>
