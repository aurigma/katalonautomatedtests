<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OIContainer</name>
   <tag></tag>
   <elementGuidId>585b2e01-3bdb-47f7-989e-75d2732b32b4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//cc-object-inspector//div[@class='object-inspector-items ng-scope']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//cc-object-inspector//div[@class='object-inspector-items ng-scope']</value>
   </webElementProperties>
</WebElementEntity>
