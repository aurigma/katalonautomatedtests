<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ckEditorFrame</name>
   <tag></tag>
   <elementGuidId>20200646-7b4e-4839-b2bc-2ab2e45870b2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//iframe[@class='cke_wysiwyg_frame cke_reset']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//iframe[@class='cke_wysiwyg_frame cke_reset']</value>
   </webElementProperties>
</WebElementEntity>
