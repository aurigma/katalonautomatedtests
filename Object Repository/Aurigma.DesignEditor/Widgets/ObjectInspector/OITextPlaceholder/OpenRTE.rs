<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OpenRTE</name>
   <tag></tag>
   <elementGuidId>564b0455-42e0-4aae-b9e6-01812d782195</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='mainEditMenuRoot']//ul//a[contains(text(),'Change rich text')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='mainEditMenuRoot']//ul//a[contains(text(),'Change rich text')]</value>
   </webElementProperties>
</WebElementEntity>
