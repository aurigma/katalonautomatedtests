<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>RTETextArea2</name>
   <tag></tag>
   <elementGuidId>ebff931e-54e3-4134-8657-18b6a679f7a5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//body[@class='cke_editable cke_editable_themed cke_contents_ltr']//p//span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//body[@class='cke_editable cke_editable_themed cke_contents_ltr']//p//span</value>
   </webElementProperties>
</WebElementEntity>
