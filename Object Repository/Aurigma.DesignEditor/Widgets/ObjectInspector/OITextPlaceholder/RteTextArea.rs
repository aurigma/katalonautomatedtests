<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>RteTextArea</name>
   <tag></tag>
   <elementGuidId>d396e288-c0cf-4610-89c8-39615fd7c6a9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//body[@class='cke_editable cke_editable_themed cke_contents_ltr']//p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//body[@class='cke_editable cke_editable_themed cke_contents_ltr']//p</value>
   </webElementProperties>
</WebElementEntity>
