<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OpenRectangle1Menu</name>
   <tag></tag>
   <elementGuidId>2916d458-635c-49ac-8c34-5f2fe85b55c6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//cc-object-inspector//cc-tab-swicher[1]//div[@class='oi-item__edit ng-scope']//button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//cc-object-inspector//cc-tab-swicher[1]//div[@class='oi-item__edit ng-scope']//button</value>
   </webElementProperties>
</WebElementEntity>
