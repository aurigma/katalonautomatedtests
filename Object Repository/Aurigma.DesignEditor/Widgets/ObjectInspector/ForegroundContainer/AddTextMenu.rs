<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AddTextMenu</name>
   <tag></tag>
   <elementGuidId>eb8ef7fe-ff03-44a0-8e49-9368d7febd9e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='toolbarContainer']//div[@title='Add a text to the canvas']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='toolbarContainer']//div[@title='Add a text to the canvas']</value>
   </webElementProperties>
</WebElementEntity>
