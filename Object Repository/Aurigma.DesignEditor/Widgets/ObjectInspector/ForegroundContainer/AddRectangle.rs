<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AddRectangle</name>
   <tag></tag>
   <elementGuidId>5ef402ad-71f8-440f-9fc4-08bf47f7109f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='toolbarContainer']//a[@title='Add a rectangle to the canvas']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='toolbarContainer']//a[@title='Add a rectangle to the canvas']</value>
   </webElementProperties>
</WebElementEntity>
