<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ItemMenuDialog</name>
   <tag></tag>
   <elementGuidId>2b2ac7b5-74a2-4efc-8a1b-e334c14df3f2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;mainContainer&quot;]//div[@class='mainEditMenuRoot']/ul[@class='dropdown-menu icon-menu textAlignLeft itemMenu mainEditMenu']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;mainContainer&quot;]//div[@class='mainEditMenuRoot']/ul[@class='dropdown-menu icon-menu textAlignLeft itemMenu mainEditMenu']</value>
   </webElementProperties>
</WebElementEntity>
