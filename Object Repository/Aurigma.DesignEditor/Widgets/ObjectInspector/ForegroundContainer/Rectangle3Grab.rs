<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Rectangle3Grab</name>
   <tag></tag>
   <elementGuidId>92a1e5ff-e177-4d3d-b0b0-704b66ba1300</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//cc-object-inspector//cc-tab-swicher[3]//div[@class='btn-group oi-item__dnd-handler ng-scope grab']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//cc-object-inspector//cc-tab-swicher[3]//div[@class='btn-group oi-item__dnd-handler ng-scope grab']</value>
   </webElementProperties>
</WebElementEntity>
