<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AddPlainText</name>
   <tag></tag>
   <elementGuidId>474ba2d9-9dac-4144-9afa-27a161b62df1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='toolbarContainer']//ul//li//a[@title='Add a text to the canvas']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='toolbarContainer']//ul//li//a[@title='Add a text to the canvas']</value>
   </webElementProperties>
</WebElementEntity>
