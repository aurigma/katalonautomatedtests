<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AddShapeItem</name>
   <tag></tag>
   <elementGuidId>fef485d2-bf12-4646-b851-e247662d182a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='toolbarContainer']//div[@title='Add a shape to the canvas']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='toolbarContainer']//div[@title='Add a shape to the canvas']</value>
   </webElementProperties>
</WebElementEntity>
