<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AddedRectangleGrab</name>
   <tag></tag>
   <elementGuidId>e2d5fac0-af5d-448c-988e-f9ea78cbfe0c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//cc-object-inspector//cc-tab-swicher[2]//div[@class='btn-group oi-item__dnd-handler ng-scope grab']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//cc-object-inspector//cc-tab-swicher[2]//div[@class='btn-group oi-item__dnd-handler ng-scope grab']</value>
   </webElementProperties>
</WebElementEntity>
