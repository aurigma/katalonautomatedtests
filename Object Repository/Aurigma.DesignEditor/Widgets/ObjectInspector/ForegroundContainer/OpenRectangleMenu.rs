<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OpenRectangleMenu</name>
   <tag></tag>
   <elementGuidId>1f5065af-fdac-488d-83a5-fafbd188bf99</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//cc-object-inspector//cc-tab-swicher[2]//div[@class='oi-item__edit ng-scope']//button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//cc-object-inspector//cc-tab-swicher[2]//div[@class='oi-item__edit ng-scope']//button</value>
   </webElementProperties>
</WebElementEntity>
