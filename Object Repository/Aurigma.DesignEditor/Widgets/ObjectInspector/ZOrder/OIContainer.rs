<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OIContainer</name>
   <tag></tag>
   <elementGuidId>ef3b3f9a-9d5c-437b-a659-e02fe3c2b8de</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//cc-object-inspector//div[@class='object-inspector-items ng-scope']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//cc-object-inspector//div[@class='object-inspector-items ng-scope']</value>
   </webElementProperties>
</WebElementEntity>
