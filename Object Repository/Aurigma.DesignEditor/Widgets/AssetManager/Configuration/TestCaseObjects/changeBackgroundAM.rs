<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>changeBackgroundAM</name>
   <tag></tag>
   <elementGuidId>67eb5ed6-4649-46d6-b022-cc91df1e1c25</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#mainRow > div.toolbox.ng-scope > div > div > div > div:nth-child(4)</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#mainRow > div.toolbox.ng-scope > div > div > div > div:nth-child(4)</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;mainRow&quot;]/div[1]/div/div/div/div[4]/div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Background by asset manager</value>
   </webElementProperties>
</WebElementEntity>
