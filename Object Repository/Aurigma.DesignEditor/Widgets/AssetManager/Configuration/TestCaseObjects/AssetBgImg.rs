<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AssetBgImg</name>
   <tag></tag>
   <elementGuidId>86e0e134-d793-4f93-9370-5b5512fe7cee</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#mainContainer > router-outlet-wrapper > cc-asset-manager > div > div > div.manager-body > div.content > div.assets-container > div > div:nth-child(7)</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#mainContainer > router-outlet-wrapper > cc-asset-manager > div > div > div.manager-body > div.content > div.assets-container > div > div:nth-child(7)</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;mainContainer&quot;]/router-outlet-wrapper/cc-asset-manager/div/div/div[2]/div[2]/div[2]/div/div[7]</value>
   </webElementProperties>
</WebElementEntity>
