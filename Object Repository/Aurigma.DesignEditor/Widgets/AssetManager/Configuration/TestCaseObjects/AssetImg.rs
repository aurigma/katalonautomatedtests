<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AssetImg</name>
   <tag></tag>
   <elementGuidId>5c26e6bf-2fb7-493e-ba90-19d63a179ea0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#mainContainer > router-outlet-wrapper > cc-asset-manager > div > div > div.manager-body > div.content > div.assets-container > div > div:nth-child(5) > asset</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#mainContainer > router-outlet-wrapper > cc-asset-manager > div > div > div.manager-body > div.content > div.assets-container > div > div:nth-child(5) > asset</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;mainContainer&quot;]/router-outlet-wrapper/cc-asset-manager/div/div/div[2]/div[2]/div[2]/div/div[5]/asset</value>
   </webElementProperties>
</WebElementEntity>
