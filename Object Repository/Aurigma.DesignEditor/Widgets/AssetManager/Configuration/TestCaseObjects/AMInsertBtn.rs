<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AMInsertBtn</name>
   <tag></tag>
   <elementGuidId>445f04b9-0e79-411d-a76e-61d11fa97cd2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#mainContainer > router-outlet-wrapper > cc-asset-manager > div > div > div.manager-action-toolbar > button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#mainContainer > router-outlet-wrapper > cc-asset-manager > div > div > div.manager-action-toolbar > button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;mainContainer&quot;]/router-outlet-wrapper/cc-asset-manager/div/div/div[3]/button</value>
   </webElementProperties>
</WebElementEntity>
