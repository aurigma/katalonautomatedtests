<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>bgColorFromGallery</name>
   <tag></tag>
   <elementGuidId>c66e347d-9781-4818-84cf-3adbaf671ef9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#mainContainer > div.ng-scope > div > div > div > div.modal-body > div > div.tab-content > div.sp-gallery-previews > div > div.ng-scope > cc-color-picker > div > palette > div:nth-child(6) > div:nth-child(10)</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#mainContainer > div.ng-scope > div > div > div > div.modal-body > div > div.tab-content > div.sp-gallery-previews > div > div.ng-scope > cc-color-picker > div > palette > div:nth-child(6) > div:nth-child(10)</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;mainContainer&quot;]/div[4]/div/div/div/div[2]/div/div[2]/div[2]/div/div[2]/cc-color-picker/div/palette/div[6]/div[10]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>color-container</value>
   </webElementProperties>
</WebElementEntity>
