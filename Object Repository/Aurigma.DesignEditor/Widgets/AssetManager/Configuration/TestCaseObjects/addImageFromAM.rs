<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>addImageFromAM</name>
   <tag></tag>
   <elementGuidId>1f13d22f-2c96-4d00-9820-b7f6e22c1f3d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#mainRow > div.toolbox.ng-scope > div > div > div > div:nth-child(3)</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#mainRow > div.toolbox.ng-scope > div > div > div > div:nth-child(3)</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;mainRow&quot;]/div[1]/div/div/div/div[3]</value>
   </webElementProperties>
</WebElementEntity>
