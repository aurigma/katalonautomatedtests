<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>changeBackgroundGallery</name>
   <tag></tag>
   <elementGuidId>6503a33c-daaf-478d-a34c-32fb117c4458</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;mainRow&quot;]/div[1]/div/div/div/div[2]/div[count(. | //*[@title = 'Change background' and contains(@class, 'btn-default ng-scope')]) = count(//*[@title = 'Change background' and contains(@class, 'btn-default ng-scope')])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#mainRow > div.toolbox.ng-scope > div > div > div > div:nth-child(2) </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;mainRow&quot;]/div[1]/div/div/div/div[2]/div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Change background</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn-default ng-scope</value>
   </webElementProperties>
</WebElementEntity>
