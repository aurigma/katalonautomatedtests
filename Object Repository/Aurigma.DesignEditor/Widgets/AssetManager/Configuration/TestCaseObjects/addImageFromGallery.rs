<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>addImageFromGallery</name>
   <tag></tag>
   <elementGuidId>fd14b226-0399-487c-918a-ecd29ee0e443</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#mainRow > div.toolbox.ng-scope > div > div > div > div:nth-child(1) > div</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#mainRow > div.toolbox.ng-scope > div > div > div > div:nth-child(1) > div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;mainRow&quot;]/div[1]/div/div/div/div[1]/div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default ng-scope</value>
   </webElementProperties>
</WebElementEntity>
