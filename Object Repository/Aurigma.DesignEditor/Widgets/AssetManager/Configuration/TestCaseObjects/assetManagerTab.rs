<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>assetManagerTab</name>
   <tag></tag>
   <elementGuidId>2f943bc9-788b-4d17-b27b-3c5e93160961</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#mainContainer > router-outlet-wrapper > cc-asset-manager > div > div > tab-viewer > div > div > li:nth-child(4)</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#mainContainer > router-outlet-wrapper > cc-asset-manager > div > div > tab-viewer > div > div > li:nth-child(4)</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;mainContainer&quot;]/router-outlet-wrapper/cc-asset-manager/div/div/tab-viewer/div/div/li[4]</value>
   </webElementProperties>
</WebElementEntity>
