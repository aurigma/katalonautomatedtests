<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>europePhotosCategory</name>
   <tag></tag>
   <elementGuidId>5db53d92-3d4b-495d-bcf6-41f19c1fb616</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#Gallery_1_tab > ul > li:nth-child(2) > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#Gallery_1_tab > ul > li:nth-child(2) > a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;Gallery_1_tab&quot;]/ul/li[2]/a
</value>
   </webElementProperties>
</WebElementEntity>
