<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>galleryDialog</name>
   <tag></tag>
   <elementGuidId>506bef53-a58d-44c1-a5fc-beec81691385</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#mainContainer > div.ng-scope > div > div > div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;mainContainer&quot;]/div[4]/div/div/div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#mainContainer > div.ng-scope > div > div > div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-dialog galleryDialog modal-lg</value>
   </webElementProperties>
</WebElementEntity>
