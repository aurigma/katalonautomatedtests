<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>assetManager</name>
   <tag></tag>
   <elementGuidId>c4c7080d-bdbc-443d-9e7d-d7539485c196</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#mainContainer > router-outlet-wrapper > cc-asset-manager > div > div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#mainContainer > router-outlet-wrapper > cc-asset-manager > div > div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;mainContainer&quot;]/router-outlet-wrapper/cc-asset-manager/div/div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>manager</value>
   </webElementProperties>
</WebElementEntity>
