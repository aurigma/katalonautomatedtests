<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ObjectInspector</name>
   <tag></tag>
   <elementGuidId>0c1de8fc-5e26-4540-b5e9-52ee1b89ad97</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#mainRow > cc-object-inspector</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;mainRow&quot;]/cc-object-inspector</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#mainRow > cc-object-inspector</value>
   </webElementProperties>
</WebElementEntity>
