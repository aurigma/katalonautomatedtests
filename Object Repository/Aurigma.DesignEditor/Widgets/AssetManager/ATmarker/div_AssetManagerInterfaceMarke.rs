<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_AssetManagerInterfaceMarke</name>
   <tag></tag>
   <elementGuidId>d0403e79-fe00-4e8f-8cdf-01b3adf10e38</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='AssetManagerInterface.json'])[1]/following::div[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>node-content-wrapper node-file</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>AssetManagerInterfaceMarkerAT.json</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/app-root[1]/div[1]/mat-drawer-container[@class=&quot;example-container mat-drawer-container&quot;]/mat-drawer[@class=&quot;mat-drawer ng-tns-c2-0 ng-trigger ng-trigger-transform mat-drawer-side ng-star-inserted&quot;]/app-file-tree[1]/tree-root[1]/tree-viewport[1]/div[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;angular-tree-component&quot;]/tree-node-collection[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;ng-star-inserted&quot;]/tree-node[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;tree-node-level-1 tree-node ng-star-inserted tree-node-expanded&quot;]/tree-node-children[1]/div[@class=&quot;tree-children ng-star-inserted&quot;]/tree-node-collection[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;ng-star-inserted&quot;]/tree-node[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;tree-node-level-2 tree-node ng-star-inserted tree-node-expanded&quot;]/tree-node-children[1]/div[@class=&quot;tree-children ng-star-inserted&quot;]/tree-node-collection[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;ng-star-inserted&quot;]/tree-node[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;tree-node-level-3 tree-node ng-star-inserted tree-node-expanded&quot;]/tree-node-children[1]/div[@class=&quot;tree-children ng-star-inserted&quot;]/tree-node-collection[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;ng-star-inserted&quot;]/tree-node[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;tree-node-level-4 tree-node tree-node-leaf ng-star-inserted&quot;]/tree-node-wrapper[1]/div[@class=&quot;node-wrapper ng-star-inserted&quot;]/div[@class=&quot;node-content-wrapper node-file&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='AssetManagerInterface.json'])[1]/following::div[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='AssetManagerConfiguration.json'])[1]/following::div[8]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='AssetManagerInterfaceMarkerIASF.json'])[1]/preceding::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tree-node-children/div/tree-node-collection/div/tree-node/div/tree-node-children/div/tree-node-collection/div/tree-node/div/tree-node-children/div/tree-node-collection/div/tree-node[6]/div/tree-node-wrapper/div/div</value>
   </webElementXpaths>
</WebElementEntity>
