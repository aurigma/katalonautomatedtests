<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>asset_     8</name>
   <tag></tag>
   <elementGuidId>f42cb701-88ed-4af3-bf37-2a435be3ba42</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='mainContainer']/router-outlet-wrapper/cc-asset-manager/div/div/div[2]/div[2]/div[2]/div/div[4]/asset</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>asset</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Название подлиннее, чтобы влезло много 8</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        
    
    

    
        
    



    
        
            Название подлиннее, чтобы влезло много 8
        
        
    
    

</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mainContainer&quot;)/router-outlet-wrapper[1]/cc-asset-manager[1]/div[@class=&quot;manager-wrapper&quot;]/div[@class=&quot;manager&quot;]/div[@class=&quot;manager-body&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;assets-container&quot;]/div[@class=&quot;assets&quot;]/div[@class=&quot;asset-wrapper&quot;]/asset[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Aurigma.DesignEditor/Widgets/AssetManager/ATmarker/iframe_Please wait ..._editorF</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='mainContainer']/router-outlet-wrapper/cc-asset-manager/div/div/div[2]/div[2]/div[2]/div/div[4]/asset</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Название подлиннее, чтобы влезло много 6'])[1]/following::asset[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Название подлиннее, чтобы влезло много 4'])[1]/following::asset[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Название подлиннее, чтобы влезло много 13'])[1]/preceding::asset[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/asset</value>
   </webElementXpaths>
</WebElementEntity>
