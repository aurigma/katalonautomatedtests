<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_AssetManager</name>
   <tag></tag>
   <elementGuidId>bfc56224-a5b5-458e-8ca0-83eb4def8062</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Widgets'])[1]/following::div[6]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>node-content-wrapper node-folder</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>AssetManager</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/app-root[1]/div[1]/mat-drawer-container[@class=&quot;example-container mat-drawer-container&quot;]/mat-drawer[@class=&quot;mat-drawer ng-tns-c2-0 ng-trigger ng-trigger-transform mat-drawer-side ng-star-inserted&quot;]/app-file-tree[1]/tree-root[1]/tree-viewport[1]/div[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;angular-tree-component&quot;]/tree-node-collection[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;ng-star-inserted&quot;]/tree-node[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;tree-node-level-1 tree-node ng-star-inserted tree-node-expanded&quot;]/tree-node-children[1]/div[@class=&quot;tree-children ng-star-inserted&quot;]/tree-node-collection[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;ng-star-inserted&quot;]/tree-node[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;tree-node-level-2 tree-node ng-star-inserted tree-node-expanded&quot;]/tree-node-children[1]/div[@class=&quot;tree-children ng-star-inserted&quot;]/tree-node-collection[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;ng-star-inserted&quot;]/tree-node[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;tree-node-level-3 tree-node tree-node-collapsed ng-star-inserted&quot;]/tree-node-wrapper[1]/div[@class=&quot;node-wrapper ng-star-inserted&quot;]/div[@class=&quot;node-content-wrapper node-folder&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Widgets'])[1]/following::div[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='base-editor'])[1]/following::div[12]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='BottomToolbar'])[1]/preceding::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tree-node-children/div/tree-node-collection/div/tree-node/div/tree-node-children/div/tree-node-collection/div/tree-node/div/tree-node-wrapper/div/div</value>
   </webElementXpaths>
</WebElementEntity>
