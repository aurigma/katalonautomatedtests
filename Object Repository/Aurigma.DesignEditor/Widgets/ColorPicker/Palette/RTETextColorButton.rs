<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>RTETextColorButton</name>
   <tag></tag>
   <elementGuidId>1de37976-e7b1-4b0d-9375-4a79ae4500f6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;Content_editorRoot&quot;]//div[@title='Text Color']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;Content_editorRoot&quot;]//div[@title='Text Color']</value>
   </webElementProperties>
</WebElementEntity>
