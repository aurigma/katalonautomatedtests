<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>RgbColors_Purple</name>
   <tag></tag>
   <elementGuidId>cbf021f4-0256-4781-b752-f39d6c5536da</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Rgb Colors'])[2]/following::div[37]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//cc-dropdown-content[@class='ng-scope open']/cc-color-picker[1]//section-view[3]//palette//div[@title='rgb(153,0,255)'][count(. | //*[@ref_element = 'Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/Palette/iframe']) = count(//*[@ref_element = 'Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/Palette/iframe'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//cc-dropdown-content[@class='ng-scope open']/cc-color-picker[1]//section-view[3]//palette//div[@title='rgb(153,0,255)']</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rgb Colors'])[2]/following::div[37]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Grayscale Colors'])[2]/following::div[63]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cmyk Colors'])[2]/preceding::div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OK'])[3]/preceding::div[203]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section-view[3]/palette/div/div[9]/div[3]</value>
   </webElementXpaths>
</WebElementEntity>
