<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TextColorPickerDialog</name>
   <tag></tag>
   <elementGuidId>2618ba2d-2081-4f7b-a48c-d775664ee5d8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>cc-color-picker[(on-submit)^=$ctrl.onUserColorChooseWrapper]</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//cc-dropdown-content[@class='ng-scope open']/cc-color-picker[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//cc-dropdown-content[@class='ng-scope open']/cc-color-picker[1]</value>
   </webElementProperties>
</WebElementEntity>
