<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ColorSpacesList</name>
   <tag></tag>
   <elementGuidId>7ed9e88b-fa91-49e7-a081-525e23a83ff2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//cc-dropdown-content[4]//cc-color-picker[1]//picker//*[@class='color-space-container']//ul[@class='dropdown-menu']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//cc-dropdown-content[4]//cc-color-picker[1]//picker//*[@class='color-space-container']//ul[@class='dropdown-menu']</value>
   </webElementProperties>
</WebElementEntity>
