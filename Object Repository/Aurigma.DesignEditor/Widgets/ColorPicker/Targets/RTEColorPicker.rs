<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>RTEColorPicker</name>
   <tag></tag>
   <elementGuidId>c36e7caf-49ad-4ef7-80ae-19c60711c2f6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//cc-dropdown-content//cc-color-picker[@class='rich-text-color-picker colorPicker']/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//cc-dropdown-content//cc-color-picker[@class='rich-text-color-picker colorPicker']/div</value>
   </webElementProperties>
</WebElementEntity>
