<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>SelectBorderColorButton</name>
   <tag></tag>
   <elementGuidId>0646f1dd-9fb5-44ff-b1b1-4e994f7a4bd2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;mainRow&quot;]//cc-top-toolbar//cc-toolbar-pagination//cc-dropdown-color-picker[@title='Select border color']//button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;mainRow&quot;]//cc-top-toolbar//cc-toolbar-pagination//cc-dropdown-color-picker[@title='Select border color']//button</value>
   </webElementProperties>
</WebElementEntity>
