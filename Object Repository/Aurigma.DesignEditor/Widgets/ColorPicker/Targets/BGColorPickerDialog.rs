<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BGColorPickerDialog</name>
   <tag></tag>
   <elementGuidId>fdca5be9-a649-4bc0-ab6b-15c95eb3eb53</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//cc-dropdown-content//div[@class='bg-color-picker ui-draggable']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//cc-dropdown-content//div[@class='bg-color-picker ui-draggable']</value>
   </webElementProperties>
</WebElementEntity>
