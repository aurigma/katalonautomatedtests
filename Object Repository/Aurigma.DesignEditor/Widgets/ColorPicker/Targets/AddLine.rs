<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AddLine</name>
   <tag></tag>
   <elementGuidId>26e25d23-8357-408b-bc4c-d597589bdfad</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;mainRow&quot;]//div[@title='Add a line to the canvas']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;mainRow&quot;]//div[@title='Add a line to the canvas']</value>
   </webElementProperties>
</WebElementEntity>
