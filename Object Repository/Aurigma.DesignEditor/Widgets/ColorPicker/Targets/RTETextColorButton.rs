<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>RTETextColorButton</name>
   <tag></tag>
   <elementGuidId>4241dc63-d273-4abe-918c-957ff42142e3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@id='Content_editorRoot']//div//div[@class='modal-body richTextDialogBody']//div[@class='rich-color-picker']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@id='Content_editorRoot']//div//div[@class='modal-body richTextDialogBody']//div[@class='rich-color-picker']</value>
   </webElementProperties>
</WebElementEntity>
