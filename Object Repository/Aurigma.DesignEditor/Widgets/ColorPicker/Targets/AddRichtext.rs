<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AddRichtext</name>
   <tag></tag>
   <elementGuidId>eb176714-d971-4b80-b03b-45e286d42e96</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;mainRow&quot;]//div[@title='Add a rich text to the canvas']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;mainRow&quot;]//div[@title='Add a rich text to the canvas']</value>
   </webElementProperties>
</WebElementEntity>
