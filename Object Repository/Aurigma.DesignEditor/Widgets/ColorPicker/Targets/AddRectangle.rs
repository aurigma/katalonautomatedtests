<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AddRectangle</name>
   <tag></tag>
   <elementGuidId>52b16366-58b8-496e-82f3-d1b4fb11e0be</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;mainRow&quot;]//div[@class='toolbarContainer']//div[@title='Add a rectangle to the canvas']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;mainRow&quot;]//div[@class='toolbarContainer']//div[@title='Add a rectangle to the canvas']</value>
   </webElementProperties>
</WebElementEntity>
