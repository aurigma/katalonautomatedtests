<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>SelectLineColorButton</name>
   <tag></tag>
   <elementGuidId>ebe2304a-4a1e-4492-ac46-6e14256cbdfb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//cc-toolbar-pagination//cc-dropdown-color-picker[@title='Select line color']/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//cc-toolbar-pagination//cc-dropdown-color-picker[@title='Select line color']/button</value>
   </webElementProperties>
</WebElementEntity>
