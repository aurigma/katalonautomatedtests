<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TextColorPickerDialog</name>
   <tag></tag>
   <elementGuidId>b429d9c1-a2c9-4ca8-b307-fe1e5f485829</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//cc-dropdown-content[@class='ng-scope open']/cc-color-picker[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>cc-color-picker[(on-submit)^=$ctrl.onUserColorChooseWrapper]</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//cc-dropdown-content[@class='ng-scope open']/cc-color-picker[1]</value>
   </webElementProperties>
</WebElementEntity>
