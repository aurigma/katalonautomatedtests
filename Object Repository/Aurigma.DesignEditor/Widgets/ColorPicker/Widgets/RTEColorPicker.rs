<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>RTEColorPicker</name>
   <tag></tag>
   <elementGuidId>8eae3f3f-85bd-4f2d-9dd6-7e74236216fa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//cc-dropdown-content//cc-color-picker[@class='rich-text-color-picker colorPicker']/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//cc-dropdown-content//cc-color-picker[@class='rich-text-color-picker colorPicker']/div</value>
   </webElementProperties>
</WebElementEntity>
