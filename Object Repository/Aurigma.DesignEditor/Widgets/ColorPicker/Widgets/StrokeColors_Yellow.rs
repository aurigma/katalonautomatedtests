<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>StrokeColors_Yellow</name>
   <tag></tag>
   <elementGuidId>42960803-85e3-4d10-9eeb-dbbe18598dc6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Rgb Colors'])[6]/following::div[17]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//cc-dropdown-content[@class='preventCanvasKeyHandle preventCanvasClickHandle moveable-dropdown stroke-settings-content ng-scope open']//cc-color-picker//section-view//palette//div[@title='rgb(255,255,0)']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//cc-dropdown-content[@class='preventCanvasKeyHandle preventCanvasClickHandle moveable-dropdown stroke-settings-content ng-scope open']//cc-color-picker//section-view//palette//div[@title='rgb(255,255,0)']</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rgb Colors'])[6]/following::div[17]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Grayscale Colors'])[6]/following::div[43]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cmyk Colors'])[6]/preceding::div[25]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OK'])[6]/preceding::div[223]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//cc-dropdown-content[12]/div/div[3]/cc-color-picker/div/sections-view/div/section-view[3]/palette/div/div[4]/div[3]</value>
   </webElementXpaths>
</WebElementEntity>
