<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OpenStrokeDialog</name>
   <tag></tag>
   <elementGuidId>504655b0-17f6-455d-9e2b-dfdb7a3ed9e0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#mainRow > cc-top-toolbar > div > div.animate-toolbar.topToolbarGroup.ng-scope > cc-toolbar-pagination > div > div > cc-text-stroke-settings > div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#mainRow > cc-top-toolbar > div > div.animate-toolbar.topToolbarGroup.ng-scope > cc-toolbar-pagination > div > div > cc-text-stroke-settings > div</value>
   </webElementProperties>
</WebElementEntity>
