<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>RTEColors_Orange</name>
   <tag></tag>
   <elementGuidId>1b0e2b6b-837c-4829-b668-164eefb6d66e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>.rich-text-color-picker > div:nth-child(1) > sections-view:nth-child(1) > div:nth-child(1) > section-view:nth-child(3) > palette:nth-child(2) > div:nth-child(1) > div:nth-child(3)</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>.rich-text-color-picker > div:nth-child(1) > sections-view:nth-child(1) > div:nth-child(1) > section-view:nth-child(3) > palette:nth-child(2) > div:nth-child(1) > div:nth-child(3)</value>
   </webElementProperties>
</WebElementEntity>
