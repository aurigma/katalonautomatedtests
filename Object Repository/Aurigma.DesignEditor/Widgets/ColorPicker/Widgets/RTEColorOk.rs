<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>RTEColorOk</name>
   <tag></tag>
   <elementGuidId>34c76f29-dc8b-4df8-9490-f94da5b53564</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//cc-dropdown-content//cc-color-picker[@class='rich-text-color-picker colorPicker']//div[@class='bottom-panel']/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//cc-dropdown-content//cc-color-picker[@class='rich-text-color-picker colorPicker']//div[@class='bottom-panel']/button</value>
   </webElementProperties>
</WebElementEntity>
