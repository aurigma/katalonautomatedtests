<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>RTETextColorButton</name>
   <tag></tag>
   <elementGuidId>1eb05476-7b8b-419d-9a12-9188289981e4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@id='Content_editorRoot']//div//div[@class='modal-body richTextDialogBody']//div[@class='rich-color-picker']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@id='Content_editorRoot']//div//div[@class='modal-body richTextDialogBody']//div[@class='rich-color-picker']</value>
   </webElementProperties>
</WebElementEntity>
