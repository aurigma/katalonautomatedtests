<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CKEditorFrame</name>
   <tag></tag>
   <elementGuidId>99e99549-711b-4643-8f98-25a550671543</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@id='cke_1_contents']//iframe[@class='cke_wysiwyg_frame cke_reset']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@id='cke_1_contents']//iframe[@class='cke_wysiwyg_frame cke_reset']</value>
   </webElementProperties>
</WebElementEntity>
