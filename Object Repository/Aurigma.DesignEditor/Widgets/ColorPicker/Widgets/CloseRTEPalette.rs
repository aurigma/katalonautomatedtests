<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CloseRTEPalette</name>
   <tag></tag>
   <elementGuidId>d85cc6f3-fca7-47cd-80d1-ac53cf316eaf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>cc-dropdown-content.moveable-dropdown:nth-child(19) > div:nth-child(1) > div:nth-child(1) > span:nth-child(2) > span:nth-child(1)</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>cc-dropdown-content.moveable-dropdown:nth-child(19) > div:nth-child(1) > div:nth-child(1) > span:nth-child(2) > span:nth-child(1)</value>
   </webElementProperties>
</WebElementEntity>
