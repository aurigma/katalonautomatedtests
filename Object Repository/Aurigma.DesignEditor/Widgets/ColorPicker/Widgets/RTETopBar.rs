<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>RTETopBar</name>
   <tag></tag>
   <elementGuidId>a40cfdf5-3f29-49dd-a6b7-c851b90b6099</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@id='cke_richTextEditor']//span[@id='cke_1_toolbox']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#cke_1_top</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@id='cke_richTextEditor']//span[@id='cke_1_toolbox']</value>
   </webElementProperties>
</WebElementEntity>
