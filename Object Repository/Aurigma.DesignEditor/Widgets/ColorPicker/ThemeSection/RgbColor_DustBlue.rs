<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>RgbColor_DustBlue</name>
   <tag></tag>
   <elementGuidId>4ffae218-ed9a-4050-ad03-f07749971c1e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//cc-dropdown-content[4]//cc-color-picker//palette//div[@title='rgb(74,134,232)'] </value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//cc-dropdown-content[4]//cc-color-picker//palette//div[@title='rgb(74,134,232)'] </value>
   </webElementProperties>
</WebElementEntity>
