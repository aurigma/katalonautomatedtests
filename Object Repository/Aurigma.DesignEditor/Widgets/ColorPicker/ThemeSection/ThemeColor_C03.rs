<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ThemeColor_C03</name>
   <tag></tag>
   <elementGuidId>987e0613-2aaf-4e4c-84b8-f8f58006f63e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//cc-dropdown-content[4]//cc-color-picker//palette//div[@title='C03']//*[@class='caption']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//cc-dropdown-content[4]//cc-color-picker//palette//div[@title='C03']//*[@class='caption']</value>
   </webElementProperties>
</WebElementEntity>
