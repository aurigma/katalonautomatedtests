<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TextArea</name>
   <tag></tag>
   <elementGuidId>66cb6828-3b83-4777-95af-71f5b6be3795</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@id='mainRow']/cc-object-inspector/div/div/cc-tab-swicher[1]//textarea</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@id='mainRow']/cc-object-inspector/div/div/cc-tab-swicher[1]//textarea</value>
   </webElementProperties>
</WebElementEntity>
