<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AddToRecentButton</name>
   <tag></tag>
   <elementGuidId>2d3b8b66-ca80-4abc-8b2b-8d1c06b9b64f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='d78aecae-a9f4-462a-878f-0cc7489d363f']/palette/div/div/div</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//cc-color-picker[1]//section-view[@_ngcontent-c2=''][1]//div[2]/palette//div[@class='cc-icon-add'][count(. | //*[@ref_element = 'Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/Iframe_new']) = count(//*[@ref_element = 'Object Repository/Aurigma.DesignEditor/Widgets/ColorPicker/SectionCollapse/Iframe_new'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//cc-color-picker[1]//section-view[@_ngcontent-c2=''][1]//div[2]/palette//div[@class='cc-icon-add']</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='d78aecae-a9f4-462a-878f-0cc7489d363f']/palette/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Recent'])[2]/following::div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OK'])[2]/following::div[10]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Grayscale Colors'])[2]/preceding::div[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rgb Colors'])[2]/preceding::div[34]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//cc-dropdown-content[4]/cc-color-picker/div/sections-view/div/section-view/div/div[2]/palette/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
