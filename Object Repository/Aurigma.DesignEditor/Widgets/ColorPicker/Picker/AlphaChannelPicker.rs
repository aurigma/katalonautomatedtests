<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AlphaChannelPicker</name>
   <tag></tag>
   <elementGuidId>d72efecb-9455-4446-827c-9044c9251c7b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//cc-dropdown-content[@class='ng-scope open']/cc-color-picker[1]//picker//cc-no-ui-slider[2]//div[@class='noUi-handle noUi-handle-upper']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='OK'])[3]/following::div[12]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//cc-dropdown-content[@class='ng-scope open']/cc-color-picker[1]//picker//cc-no-ui-slider[2]//div[@class='noUi-handle noUi-handle-upper']</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OK'])[3]/following::div[12]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cmyk Colors'])[2]/following::div[210]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='RGB'])[3]/preceding::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='R'])[2]/preceding::div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//cc-dropdown-content[4]/cc-color-picker/div/picker/div/cc-no-ui-slider[2]/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
