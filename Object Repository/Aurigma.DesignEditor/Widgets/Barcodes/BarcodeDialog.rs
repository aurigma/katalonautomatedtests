<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BarcodeDialog</name>
   <tag></tag>
   <elementGuidId>53d73b0b-4890-4bc8-9108-5bc7924e0645</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class='modal-dialog qrBarDialog']//div[@class='modal-content']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@class='modal-dialog qrBarDialog']//div[@class='modal-content']</value>
   </webElementProperties>
</WebElementEntity>
