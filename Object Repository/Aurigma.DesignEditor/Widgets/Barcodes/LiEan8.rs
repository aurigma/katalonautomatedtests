<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LiEan8</name>
   <tag></tag>
   <elementGuidId>ce31a7e1-9f97-42d0-8d3d-afbd7e2a9bda</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='dropdown-menu open']//span[contains(text(), 'Ean 8')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='dropdown-menu open']//span[contains(text(), 'Ean 8')]</value>
   </webElementProperties>
</WebElementEntity>
