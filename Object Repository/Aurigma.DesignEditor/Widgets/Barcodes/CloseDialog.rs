<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CloseDialog</name>
   <tag></tag>
   <elementGuidId>e54bd9e8-93c8-4258-b723-15ceb7976a60</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class='modal-dialog qrBarDialog']//button[@class='close']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@class='modal-dialog qrBarDialog']//button[@class='close']</value>
   </webElementProperties>
</WebElementEntity>
