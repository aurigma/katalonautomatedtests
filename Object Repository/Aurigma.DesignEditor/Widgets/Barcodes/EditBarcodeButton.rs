<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EditBarcodeButton</name>
   <tag></tag>
   <elementGuidId>2fb77d66-3dc9-4b35-8c5d-dd415b049343</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class='mainEditMenuRoot']//ul//a[contains(text(), 'Edit barcode')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@class='mainEditMenuRoot']//ul//a[contains(text(), 'Edit barcode')]</value>
   </webElementProperties>
</WebElementEntity>
