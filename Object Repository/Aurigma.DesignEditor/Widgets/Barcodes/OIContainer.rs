<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OIContainer</name>
   <tag></tag>
   <elementGuidId>79346a79-252c-42a5-bb28-eba8b461b69a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//cc-object-inspector//div[@class='object-inspector-items ng-scope']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//cc-object-inspector//div[@class='object-inspector-items ng-scope']</value>
   </webElementProperties>
</WebElementEntity>
