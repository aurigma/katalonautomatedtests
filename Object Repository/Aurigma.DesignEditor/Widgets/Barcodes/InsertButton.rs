<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>InsertButton</name>
   <tag></tag>
   <elementGuidId>627f5b27-31dc-4ef5-ae64-0ff1c0b8b898</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class='modal-dialog qrBarDialog']//button[contains(text(), 'Insert')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@class='modal-dialog qrBarDialog']//button[contains(text(), 'Insert')]</value>
   </webElementProperties>
</WebElementEntity>
