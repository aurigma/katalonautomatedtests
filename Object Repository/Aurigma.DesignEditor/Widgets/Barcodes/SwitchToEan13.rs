<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>SwitchToEan13</name>
   <tag></tag>
   <elementGuidId>52cd06f4-af4d-4810-a4a0-d93ee2c81983</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class='modal-dialog qrBarDialog']//button[@title='Ean 8']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@class='modal-dialog qrBarDialog']//button[@title='Ean 8']</value>
   </webElementProperties>
</WebElementEntity>
