<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OpenBarcodeMenu</name>
   <tag></tag>
   <elementGuidId>aee0fa1e-0bc9-4125-a1b9-201e2acbcd21</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class='toolbarContainer']//div[@title='Add a barcode to the canvas']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@class='toolbarContainer']//div[@title='Add a barcode to the canvas']</value>
   </webElementProperties>
</WebElementEntity>
