<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OpenMenuFromOI</name>
   <tag></tag>
   <elementGuidId>371d966a-c57e-4fd6-b746-e8e4156add59</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//cc-object-inspector//cc-tab-swicher[1]//div[@class='oi-item__edit ng-scope']//button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//cc-object-inspector//cc-tab-swicher[1]//div[@class='oi-item__edit ng-scope']//button</value>
   </webElementProperties>
</WebElementEntity>
