<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AddBarcode</name>
   <tag></tag>
   <elementGuidId>b78a1a65-6a4b-42ee-8835-3db1e062838d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class='toolbarContainer']//ul//li//span[contains(text(), 'Barcode')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@class='toolbarContainer']//ul//li//span[contains(text(), 'Barcode')]</value>
   </webElementProperties>
</WebElementEntity>
